package dk.sdu.imada.ticone.table;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.model.CyTableFactory;
import org.cytoscape.model.CyTableManager;

import de.wiwie.wiutils.utils.ArraysExt;
import dk.sdu.imada.ticone.clustering.ClusterObjectMappingNotFoundException;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.similarity.ICompositeSimilarityFunction;
import dk.sdu.imada.ticone.similarity.ICompositeSimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimpleSimilarityFunction;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.statistics.IPValueCalculationResult;
import dk.sdu.imada.ticone.statistics.IPvalue;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

public class TableFactory {

	public final static String pvalueTableNameFormatString = "TiCoNE P-values Table: '%s', Iteration %d";
	public final static String clusteringTableNameFormatString = "TiCoNE Clustering Table: '%s', Iteration %d";
	public final static String prototypesTableNameFormatString = "TiCoNE Prototypes Table: '%s', Iteration %d";

	public static void setupClusterTables(final TiconeCytoscapeClusteringResult clusteringResult) {
//		final int iteration = clusteringResult.getClusterHistory().getIterationNumber();
//		// remove old tables first
//		destroyClusterTables(iteration, clusteringResult);
//		// create a table for the whole clustering
//		clusteringResult.setClusteringTable(iteration, createClusteringTable(clusteringResult));
//		// create a table for the prototypes of the clustering
//		clusteringResult.setPrototypesTable(iteration, createPrototypesTable(clusteringResult));
//		// create a table for the p-value calculations
//		if (clusteringResult.getPvalueCalculationResult() != null)
//			clusteringResult.setPvaluesTable(iteration, createPvaluesTable(clusteringResult));
	}

	private static CyTable createClusteringTable(final TiconeCytoscapeClusteringResult clusteringResult) {
		final CyTableFactory cyTableFactory = ServiceHelper.getService(CyTableFactory.class);
		final CyTableManager cyTableManager = ServiceHelper.getService(CyTableManager.class);

		final String tableName = String.format(clusteringTableNameFormatString, clusteringResult.getName(),
				clusteringResult.getClusterHistory().getIterationNumber());

		final CyTable table = cyTableFactory.createTable(tableName, "object", String.class, true, true);
		table.createColumn("cluster", Integer.class, true);
		table.createColumn("similarity", Double.class, true);

		final ISimilarityFunction similarityFunction = clusteringResult.getSimilarityFunction();
		if (similarityFunction instanceof ICompositeSimilarityFunction) {
			ISimpleSimilarityFunction[] similarityFunctions = ((ICompositeSimilarityFunction) similarityFunction)
					.getSimilarityFunctions();
			for (ISimpleSimilarityFunction sf : similarityFunctions)
				table.createColumn(sf.toString(), Double.class, true);
		}

		final IClusterObjectMapping pom = clusteringResult.getClusterHistory().getClusterObjectMapping();
		for (final ICluster cluster : pom.getClusters()) {
			for (final ITimeSeriesObject o : cluster.getObjects()) {
				CyRow row = table.getRow(o.getName());
				row.set("cluster", cluster.getClusterNumber());
				try {
					final ISimilarityValue sim = cluster.getSimilarity(o);
					row.set("similarity", sim.get());
					if (sim instanceof ICompositeSimilarityValue) {
						int c = 0;
						for (ISimpleSimilarityFunction sf : ((ICompositeSimilarityValue) sim).getChildFunctions()) {
							row.set(sf.toString(), ((ICompositeSimilarityValue) sim).getScaledChildSimilarityValue(c));
						}
					}
				} catch (ClusterObjectMappingNotFoundException | SimilarityCalculationException e) {
				}
			}
		}

		cyTableManager.addTable(table);

		return table;
	}

	private static CyTable createPrototypesTable(final TiconeCytoscapeClusteringResult clusteringResult) {
		final CyTableFactory cyTableFactory = ServiceHelper.getService(CyTableFactory.class);
		final CyTableManager cyTableManager = ServiceHelper.getService(CyTableManager.class);

		final String tableName = String.format(prototypesTableNameFormatString, clusteringResult.getName(),
				clusteringResult.getClusterHistory().getIterationNumber());

		final CyTable table = cyTableFactory.createTable(tableName, "cluster", String.class, true, true);
		table.createListColumn("prototype", Double.class, false);
		table.createColumn("prototype_mean", Double.class, false);
		table.createColumn("prototype_stdvar", Double.class, false);
		table.createColumn("number_objects", Integer.class, false);

		final IClusterObjectMapping pom = clusteringResult.getClusterHistory().getClusterObjectMapping();

		for (final ICluster cluster : pom.getClusters()) {
			try {
				final CyRow row = table.getRow(cluster.getName());
				final double[] timeSeries = PrototypeComponentType.TIME_SERIES.getComponent(cluster.getPrototype())
						.getTimeSeries().asArray();
				row.set("prototype", Arrays.asList(ArrayUtils.toObject(timeSeries)));
				row.set("prototype_mean", ArraysExt.mean(timeSeries));
				row.set("prototype_stdvar", ArraysExt.stdvariance(timeSeries));
				row.set("number_objects", cluster.getObjects().size());
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
				e.printStackTrace();
			}
		}

		cyTableManager.addTable(table);

		return table;
	}

	private static CyTable createPvaluesTable(final TiconeCytoscapeClusteringResult clusteringResult)
			throws NotAnArithmeticFeatureValueException, ToNumberConversionException,
			IncompatibleFeatureValueProviderException {
		final CyTableFactory cyTableFactory = ServiceHelper.getService(CyTableFactory.class);
		final CyTableManager cyTableManager = ServiceHelper.getService(CyTableManager.class);

		final String tableName = String.format(pvalueTableNameFormatString, clusteringResult.getName(),
				clusteringResult.getClusterHistory().getIterationNumber());
		final CyTable table = cyTableFactory.createTable(tableName, "cluster", String.class, true, true);
		table.createColumn("p", Double.class, false);

		final IPValueCalculationResult pvalueCalculationResult = clusteringResult.getPvalueCalculationResult();

		final IFeatureStore featureVals = clusteringResult.getFeatureStore();

		for (final IArithmeticFeature<? extends Comparable<?>> f : pvalueCalculationResult
				.getFeatures(ObjectType.CLUSTER)) {
			table.createColumn(f.getName(), Double.class, false);
			for (final ICluster c : pvalueCalculationResult.getFeatureStore().keySet(ObjectType.CLUSTER)) {
				final CyRow row = table.getRow(c.getName());
				if (featureVals.contains(c) && featureVals.getFeaturesFor(c).contains(f))
					row.set(f.getName(), featureVals.getFeatureValue(c, f).toNumber().doubleValue());
			}
		}

		for (final ICluster c : pvalueCalculationResult.getObjects(ObjectType.CLUSTER)) {
			final CyRow row = table.getRow(c.getName());
			row.set("p", pvalueCalculationResult.getPValue(c).getDouble());
		}

		final List<IFitnessScore> fitnessScores = pvalueCalculationResult.getFitnessScores(ObjectType.CLUSTER);

		for (final IFitnessScore fs : fitnessScores) {
			final String colName = "p (" + fs.getName() + ")";
			table.createColumn(colName, Double.class, false);

			for (final ICluster c : pvalueCalculationResult.getObjects(ObjectType.CLUSTER)) {
				final CyRow row = table.getRow(c.getName());
				final IPvalue pvals = pvalueCalculationResult.getPValue(fs, c);
				row.set(colName, pvals);
			}
		}

		cyTableManager.addTable(table);

		return table;
	}

	public static <T> CyTable addColumnToPrototypesTable(final CyTable table, final String columnName,
			final Class<T> type, final Map<ICluster, T> values) {
		for (final ICluster cluster : values.keySet()) {
			final CyRow row = table.getRow(cluster.getName());
			row.set(columnName, values.get(cluster));
		}

		return table;
	}

	private static CyTable createClusterTable(final TiconeCytoscapeClusteringResult clusteringResult,
			final ICluster cluster) {
		return null;
	}

	public static void destroyClusterTables(final int iteration,
			final TiconeCytoscapeClusteringResult clusteringResult) {
		final CyTableManager cyTableManager = ServiceHelper.getService(CyTableManager.class);

		CyTable table = clusteringResult.getClusteringTable(iteration);
		if (table != null) {
			cyTableManager.deleteTable(table.getSUID());
		}

		for (final ICluster c : clusteringResult.getClusterHistory().getClusterObjectMapping().getClusters()) {
			if (clusteringResult.getClusterTable(iteration, c) != null)
				cyTableManager.deleteTable(clusteringResult.getClusterTable(iteration, c).getSUID());
		}

		table = clusteringResult.getPrototypesTable(iteration);
		if (table != null) {
			cyTableManager.deleteTable(table.getSUID());
		}
	}

}
