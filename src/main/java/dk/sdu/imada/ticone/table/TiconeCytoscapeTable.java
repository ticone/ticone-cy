/**
 * 
 */
package dk.sdu.imada.ticone.table;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

import org.cytoscape.model.CyTable;
import org.cytoscape.model.CyTableManager;

import dk.sdu.imada.ticone.util.ServiceHelper;

/**
 * @author Christian Wiwie
 * 
 * @since Dec 4, 2018
 *
 */
public class TiconeCytoscapeTable implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 690971983761243523L;
	protected String name;
	protected transient CyTable cyTable;

	public TiconeCytoscapeTable(final CyTable cyTable) {
		super();
		this.setCyTable(cyTable);
	}
//
//	@Override
//	protected void finalize() throws Throwable {
//		super.finalize();
//		final CyTable cyTable = getCyTable();
//		if (cyTable != null) {
//			final CyTableManager cyTableManager = ServiceHelper.getService(CyTableManager.class);
//			cyTableManager.deleteTable(cyTable.getSUID());
//		}
//
//	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TiconeCytoscapeTable)
			return Objects.equals(name, ((TiconeCytoscapeTable) obj).name);
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	public CyTable getCyTable() {
		if (this.cyTable == null && this.name != null) {
			final CyTableManager cyTableManager = ServiceHelper.getService(CyTableManager.class);
			final Set<CyTable> tables = cyTableManager.getAllTables(true);
			final Iterator<CyTable> tableIterator1 = tables.iterator();
			while (tableIterator1.hasNext()) {
				final CyTable cyTable = tableIterator1.next();
				if (cyTable.getTitle().equals(this.name)) {
					this.cyTable = cyTable;
				}
			}
		}
		return this.cyTable;
	}

	/**
	 * @param cyTable the cyTable to set
	 */
	public void setCyTable(CyTable cyTable) {
		this.cyTable = cyTable;
		if (this.cyTable != null)
			this.name = cyTable.getTitle();
	}

	/**
	 * @return the tableName
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		if (name.equals(this.name))
			return;
		this.name = name;
		this.cyTable = null;
	}

	/**
	 * @return
	 */
	public Long getSUID() {
		return getCyTable().getSUID();
	}
}
