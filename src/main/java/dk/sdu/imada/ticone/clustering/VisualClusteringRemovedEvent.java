/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 26, 2018
 *
 */
public class VisualClusteringRemovedEvent {

	protected final TiconeClusteringResultPanel clusteringResult;

	/**
	 * @param clusteringResult
	 */
	public VisualClusteringRemovedEvent(final TiconeClusteringResultPanel clusteringResult) {
		super();
		this.clusteringResult = clusteringResult;
	}

	/**
	 * @return the clusteringResult
	 */
	public TiconeClusteringResultPanel getClusteringResult() {
		return this.clusteringResult;
	}
}
