package dk.sdu.imada.ticone.clustering;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Paint;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTable;
import org.cytoscape.model.CyTableManager;
import org.cytoscape.view.model.VisualProperty;
import org.cytoscape.view.presentation.RenderingEngineManager;
import org.cytoscape.view.presentation.customgraphics.CyCustomGraphics;
import org.cytoscape.view.presentation.property.ArrowShapeVisualProperty;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.view.presentation.property.NodeShapeVisualProperty;
import org.cytoscape.view.presentation.property.values.Justification;
import org.cytoscape.view.presentation.property.values.NodeShape;
import org.cytoscape.view.presentation.property.values.ObjectPosition;
import org.cytoscape.view.presentation.property.values.Position;
import org.cytoscape.view.vizmap.VisualMappingFunction;
import org.cytoscape.view.vizmap.VisualMappingFunctionFactory;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualPropertyDependency;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.view.vizmap.VisualStyleFactory;
import org.cytoscape.view.vizmap.mappings.BoundaryRangeValues;
import org.cytoscape.view.vizmap.mappings.ContinuousMapping;
import org.cytoscape.view.vizmap.mappings.DiscreteMapping;

import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.connectivity.ConnectivityResult;
import dk.sdu.imada.ticone.connectivity.ConnectivityResultWrapper;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer;
import dk.sdu.imada.ticone.io.ILoadDataMethod;
import dk.sdu.imada.ticone.network.NetworkUtil;
import dk.sdu.imada.ticone.preprocessing.IPreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.statistics.IPvalue;
import dk.sdu.imada.ticone.table.TableFactory;
import dk.sdu.imada.ticone.table.TiconeCytoscapeTable;
import dk.sdu.imada.ticone.util.IClusterHistory;
import dk.sdu.imada.ticone.util.IIdMapMethod;
import dk.sdu.imada.ticone.util.ITimePointWeighting;
import dk.sdu.imada.ticone.util.MyCustomGraphics;
import dk.sdu.imada.ticone.util.MyCustomGraphicsFactory;
import dk.sdu.imada.ticone.util.ServiceHelper;

public class TiconeCytoscapeClusteringResult extends TiconeVisualClusteringResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2125707749467995400L;

	protected transient Map<Integer, VisualStyle> visualStyleUndirectedConnectivity, visualStyleDirectedConnectivity,
			visualStyleClusterSimilarity, visualStyleObjectSimilarity;

	protected Map<Integer, TiconeCytoscapeTable> clusteringTable, prototypesTable, pvaluesTable;
	protected Map<Integer, Map<ICluster, TiconeCytoscapeTable>> clusterTables;

	public static TiconeCytoscapeClusteringResult getInstance(final TiconeVisualClusteringResult clusteringResult) {
		if (clusteringResult instanceof TiconeCytoscapeClusteringResult)
			return (TiconeCytoscapeClusteringResult) clusteringResult;
		return new TiconeCytoscapeClusteringResult(clusteringResult);
	}

	public TiconeCytoscapeClusteringResult(long seed, ILoadDataMethod loadDataMethod,
			IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider, int numberOfTimePoints,
			ITimePointWeighting timePointWeighting, IIdMapMethod idMapMethod,
			IPreprocessingSummary preprocessingSummary, ISimilarityFunction similarityFunction,
			IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethod,
			ITimeSeriesPreprocessor timeSeriesPreprocessor, IPrototypeBuilder prototypeBuilder) {
		super(seed, loadDataMethod, initialClusteringProvider, numberOfTimePoints, timePointWeighting, idMapMethod,
				preprocessingSummary, similarityFunction, clusteringMethod, timeSeriesPreprocessor, prototypeBuilder);

		this.clusteringTable = new HashMap<>();
		this.prototypesTable = new HashMap<>();
		this.pvaluesTable = new HashMap<>();
		this.visualStyleUndirectedConnectivity = new HashMap<>();
		this.visualStyleDirectedConnectivity = new HashMap<>();
		this.visualStyleClusterSimilarity = new HashMap<>();
	}

	protected TiconeCytoscapeClusteringResult(final TiconeVisualClusteringResult clusteringResult) {
		super(clusteringResult);
		if (!(clusteringResult instanceof TiconeCytoscapeClusteringResult)
				&& clusteringResult.getClass().isAssignableFrom(TiconeCytoscapeClusteringResult.class))
			clusteringResult.addNameChangeListener(this);
	}

	public void clearVisualStyles() {
		if (this.visualStyleDirectedConnectivity != null)
			for (int iteration : this.visualStyleDirectedConnectivity.keySet())
				ServiceHelper.getService(VisualMappingManager.class)
						.removeVisualStyle(this.visualStyleDirectedConnectivity.get(iteration));
		if (this.visualStyleUndirectedConnectivity != null)
			for (int iteration : this.visualStyleUndirectedConnectivity.keySet())
				ServiceHelper.getService(VisualMappingManager.class)
						.removeVisualStyle(this.visualStyleUndirectedConnectivity.get(iteration));
		if (this.visualStyleClusterSimilarity != null)
			for (int iteration : this.visualStyleClusterSimilarity.keySet())
				ServiceHelper.getService(VisualMappingManager.class)
						.removeVisualStyle(this.visualStyleClusterSimilarity.get(iteration));
	}

	public VisualStyle getConnectivityVisualStyle(final ConnectivityResultWrapper wrapper, final boolean isDirected) {
		final int iteration = wrapper.getClusteringIteration();

		if (isDirected && this.visualStyleDirectedConnectivity != null
				&& this.visualStyleDirectedConnectivity.containsKey(iteration))
			return this.visualStyleDirectedConnectivity.get(iteration);
		else if (!isDirected && this.visualStyleUndirectedConnectivity != null
				&& this.visualStyleUndirectedConnectivity.containsKey(iteration))
			return this.visualStyleUndirectedConnectivity.get(iteration);

		final VisualMappingFunctionFactory vmfFactoryP = ServiceHelper.getService(VisualMappingFunctionFactory.class,
				"(mapping.type=discrete)");
		final VisualMappingFunctionFactory vmfPassthroughFactoryP = ServiceHelper
				.getService(VisualMappingFunctionFactory.class, "(mapping.type=passthrough)");

		final ConnectivityResult result = isDirected ? wrapper.getDirectedConnectivityResult()
				: wrapper.getUndirectedConnectivityResult();
		final Map<IClusterPair, IPvalue> edgePvalues = wrapper.getConnectivityPValueResult() == null ? null
				: (isDirected ? wrapper.getConnectivityPValueResult().getDirectedEdgeCountPvalues()
						: wrapper.getConnectivityPValueResult().getUndirectedEdgeCountPvalues());

		final VisualStyleFactory visStyleFactory = ServiceHelper.getService(VisualStyleFactory.class);
		final VisualStyle vs = visStyleFactory
				.createVisualStyle(String.format("TiCoNE Connectivity (%s, Iteration %d, %s)", this.getName(),
						wrapper.getClusteringIteration(), isDirected ? "Directed" : "Undirected"));

		vs.setDefaultValue(BasicVisualLexicon.NODE_WIDTH, 250.0);
		vs.setDefaultValue(BasicVisualLexicon.NODE_HEIGHT, 150.0);
		vs.setDefaultValue(BasicVisualLexicon.NODE_BORDER_WIDTH, 10.0);
		vs.setDefaultValue(BasicVisualLexicon.NODE_SHAPE, NodeShapeVisualProperty.RECTANGLE);
		vs.setDefaultValue(BasicVisualLexicon.NODE_FILL_COLOR, Color.WHITE);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_COLOR, Color.BLACK);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_FONT_SIZE, 35);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_WIDTH, 300.0);

		final VisualProperty<String> nodeLabelProperty = BasicVisualLexicon.NODE_LABEL;
		final VisualMappingFunction<String, String> nodeLabelMapping = vmfPassthroughFactoryP
				.createVisualMappingFunction("nodeLabel", String.class, nodeLabelProperty);
		vs.addVisualMappingFunction(nodeLabelMapping);

		for (final VisualPropertyDependency<?> dependency : vs.getAllVisualPropertyDependencies()) {
			if (dependency.getIdString().equals("nodeSizeLocked")) {
				dependency.setDependency(false);
			} else if (dependency.getIdString().equals("arrowColorMatchesEdge")) {
				dependency.setDependency(true);
			}
		}

		final VisualProperty<CyCustomGraphics> property = (VisualProperty<CyCustomGraphics>) ServiceHelper
				.getService(RenderingEngineManager.class).getDefaultVisualLexicon()
				.lookup(CyNode.class, "NODE_CUSTOMGRAPHICS_1");
		final DiscreteMapping<String, CyCustomGraphics> pMapping = (DiscreteMapping<String, CyCustomGraphics>) vmfFactoryP
				.createVisualMappingFunction(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME, String.class, property);

		for (final ICluster p : this.getClusterHistory().getClusterObjectMapping().getClusters()) {
			try {
				final ITimeSeriesObjectList timeSeriesDatas = p.getObjects();
				ClusterChartContainer container = this.getDefaultClusterChartContainer(p);
				if (container == null) {
					container = new ClusterChartContainer(this, p, timeSeriesDatas);
					this.addClusterChartContainer(p, timeSeriesDatas, container);
				}
				final MyCustomGraphicsFactory factory = new MyCustomGraphicsFactory(container);
				final MyCustomGraphics mcg = factory.getInstance((URL) null);
				// mcg.setDisplayName("Cluster " + p.getClusterNumber());
				pMapping.putMapValue(p.getName(), mcg);
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
				e.printStackTrace();
			}
		}

		vs.addVisualMappingFunction(pMapping);

		final VisualProperty<ObjectPosition> labelPositionProp = (VisualProperty<ObjectPosition>) ServiceHelper
				.getService(RenderingEngineManager.class).getDefaultVisualLexicon()
				.lookup(CyNode.class, "NODE_LABEL_POSITION");
		vs.setDefaultValue(labelPositionProp,
				new ObjectPosition(Position.NORTH, Position.SOUTH, Justification.JUSTIFY_CENTER, 0, 0));

		// EDGES
		final VisualProperty<String> edgeLabelProperty = BasicVisualLexicon.EDGE_LABEL;
		final VisualMappingFunction<String, String> edgeLabelMapping = vmfPassthroughFactoryP
				.createVisualMappingFunction("edgeLabel", String.class, edgeLabelProperty);
		vs.addVisualMappingFunction(edgeLabelMapping);
		vs.setDefaultValue(BasicVisualLexicon.EDGE_LABEL_FONT_SIZE, 30);
		vs.setDefaultValue(BasicVisualLexicon.EDGE_LABEL_COLOR, Color.BLACK);
		if (isDirected) {
			vs.setDefaultValue(BasicVisualLexicon.EDGE_TARGET_ARROW_SHAPE, ArrowShapeVisualProperty.DELTA);
		}

		// edge attribute for edge width, edge transparency
		final String edgeWidthAttr = edgePvalues != null ? "p" : "log-odds";
		final String fcAttr = "log-odds";

		// calculate min and max fold change
		// exclude infinite values
		double minFC = Double.MAX_VALUE;
		for (final double d : result.getEdgeCountEnrichment().values())
			if (Double.isFinite(d) && d < minFC)
				minFC = d;
		double maxFC = -Double.MAX_VALUE;
		for (final double d : result.getEdgeCountEnrichment().values())
			if (Double.isFinite(d) && d > maxFC)
				maxFC = d;

		// edge width
		final double minEdgeWidthAttr = edgePvalues != null ? Collections.min(edgePvalues.values()).getDouble() : minFC;
		final double maxEdgeWidthAttr = edgePvalues != null ? Collections.min(edgePvalues.values()).getDouble() : maxFC;

		final VisualMappingFunctionFactory vmfContinuousFactoryP = ServiceHelper
				.getService(VisualMappingFunctionFactory.class, "(mapping.type=continuous)");
		final VisualProperty<Double> edgeWidthProperty = BasicVisualLexicon.EDGE_WIDTH;
		final VisualMappingFunction<Double, Double> edgeWidthMapping = vmfContinuousFactoryP
				.createVisualMappingFunction(edgeWidthAttr, Double.class, edgeWidthProperty);

		final BoundaryRangeValues<Double> minEdgeWidth = new BoundaryRangeValues<>(0.0, 0.0, 0.0);
		final BoundaryRangeValues<Double> maxEdgeWidth = new BoundaryRangeValues<>(20.0, 20.0, 20.0);

		// edge width based on log-odds
		if (edgeWidthAttr.equals(fcAttr)) {
			((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(minEdgeWidthAttr, maxEdgeWidth);
			((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(-0.25, maxEdgeWidth);
			((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(0.0, minEdgeWidth);
			((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(0.25, maxEdgeWidth);
			((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(maxEdgeWidthAttr, maxEdgeWidth);
		}
		// ... based on p-value
		else {
			((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(0.0, maxEdgeWidth);
			((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(0.01, maxEdgeWidth);
			((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(0.05, minEdgeWidth);
			((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(1.0, minEdgeWidth);
		}
		vs.addVisualMappingFunction(edgeWidthMapping);

		// edge color; always use log-odds
		final VisualProperty<Paint> edgeColorProperty = BasicVisualLexicon.EDGE_UNSELECTED_PAINT;
		final VisualMappingFunction<Double, Paint> edgeColorMapping = vmfContinuousFactoryP
				.createVisualMappingFunction(fcAttr, Double.class, edgeColorProperty);

		final BoundaryRangeValues<Paint> minEdgeColor = new BoundaryRangeValues<>(Color.decode("#66CCFF"),
				Color.decode("#66CCFF"), Color.decode("#66CCFF"));
		final BoundaryRangeValues<Paint> zeroEdgeColor = new BoundaryRangeValues<>(Color.BLACK, Color.BLACK,
				Color.BLACK);
		final BoundaryRangeValues<Paint> maxEdgeColor = new BoundaryRangeValues<>(Color.GREEN, Color.GREEN,
				Color.GREEN);

		((ContinuousMapping<Double, Paint>) edgeColorMapping).addPoint(minFC, minEdgeColor);
		((ContinuousMapping<Double, Paint>) edgeColorMapping).addPoint(-0.25, minEdgeColor);
		((ContinuousMapping<Double, Paint>) edgeColorMapping).addPoint(0.0, zeroEdgeColor);
		((ContinuousMapping<Double, Paint>) edgeColorMapping).addPoint(0.25, maxEdgeColor);
		((ContinuousMapping<Double, Paint>) edgeColorMapping).addPoint(maxFC, maxEdgeColor);
		vs.addVisualMappingFunction(edgeColorMapping);

		// edge transparency
		final VisualProperty<Integer> edgeTransparencyProperty = BasicVisualLexicon.EDGE_TRANSPARENCY;
		final VisualMappingFunction<Double, Integer> edgeTransMapping = vmfContinuousFactoryP
				.createVisualMappingFunction(edgeWidthAttr, Double.class, edgeTransparencyProperty);

		final BoundaryRangeValues<Integer> minEdgeTrans = new BoundaryRangeValues<>(0, 0, 0);
		final BoundaryRangeValues<Integer> maxEdgeTrans = new BoundaryRangeValues<>(255, 255, 255);

		// edge transparency based on log-odds
		if (edgeWidthAttr.equals(fcAttr)) {
			((ContinuousMapping<Double, Integer>) edgeTransMapping).addPoint(minEdgeWidthAttr, maxEdgeTrans);
			((ContinuousMapping<Double, Integer>) edgeTransMapping).addPoint(-0.25, maxEdgeTrans);
			((ContinuousMapping<Double, Integer>) edgeTransMapping).addPoint(0.0, minEdgeTrans);
			((ContinuousMapping<Double, Integer>) edgeTransMapping).addPoint(0.25, maxEdgeTrans);
			((ContinuousMapping<Double, Integer>) edgeTransMapping).addPoint(maxEdgeWidthAttr, maxEdgeTrans);
		}
		// ... based on p-value
		else {
			((ContinuousMapping<Double, Integer>) edgeTransMapping).addPoint(0.0, maxEdgeTrans);
			((ContinuousMapping<Double, Integer>) edgeTransMapping).addPoint(0.01, maxEdgeTrans);
			((ContinuousMapping<Double, Integer>) edgeTransMapping).addPoint(0.05, minEdgeTrans);
			((ContinuousMapping<Double, Integer>) edgeTransMapping).addPoint(1.0, minEdgeTrans);
		}

		vs.addVisualMappingFunction(edgeTransMapping);

		ServiceHelper.getService(VisualMappingManager.class).addVisualStyle(vs);
		if (isDirected)
			this.visualStyleDirectedConnectivity.put(iteration, vs);
		else
			this.visualStyleUndirectedConnectivity.put(iteration, vs);
		return vs;
	}

	public void setClusteringTable(final int iteration, final CyTable clusteringTable) {
		if (this.clusteringTable == null)
			this.clusteringTable = new HashMap<>();
		this.clusteringTable.put(iteration, new TiconeCytoscapeTable(clusteringTable));
	}

	public CyTable getClusteringTable(final int iteration) {
		if (this.clusteringTable.containsKey(iteration))
			return this.clusteringTable.get(iteration).getCyTable();
		return null;
	}

	public void setPrototypesTable(final int iteration, final CyTable prototypesTable) {
		if (this.prototypesTable == null)
			this.prototypesTable = new HashMap<>();
		this.prototypesTable.put(iteration, new TiconeCytoscapeTable(prototypesTable));
	}

	/**
	 * @param pvaluesTable the pvaluesTable to set
	 */
	public void setPvaluesTable(final int iteration, final CyTable pvaluesTable) {
		if (this.pvaluesTable == null)
			this.pvaluesTable = new HashMap<>();
		this.pvaluesTable.put(iteration, new TiconeCytoscapeTable(pvaluesTable));
	}

	public CyTable getPrototypesTable(final int iteration) {
		if (this.prototypesTable.containsKey(iteration))
			return this.prototypesTable.get(iteration).getCyTable();
		return null;
	}

	public CyTable getPvaluesTable(final int iteration) {
		if (pvaluesTable.containsKey(iteration))
			return this.pvaluesTable.get(iteration).getCyTable();
		return null;
	}

	public void setClusterTable(final int iteration, final ICluster cluster, final CyTable table) {
		if (this.clusterTables == null)
			this.clusterTables = new HashMap<>();
		if (!this.clusterTables.containsKey(iteration))
			this.clusterTables.put(iteration, new HashMap<>());
		this.clusterTables.get(iteration).put(cluster, new TiconeCytoscapeTable(table));
	}

	public CyTable getClusterTable(final int iteration, final ICluster cluster) {
		return this.clusterTables != null ? this.clusterTables.get(iteration).get(cluster).getCyTable() : null;
	}

	@Override
	protected void fireStateChanged() {
		super.fireStateChanged();
		try {
			TableFactory.setupClusterTables(this);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void fireNameChanged() {
		super.fireNameChanged();
		IClusterHistory hist = this.getClusterHistory();
		do {
			final int iteration = hist.getIterationNumber();

			if (this.getClusteringTable(iteration) != null)
				this.getClusteringTable(iteration)
						.setTitle(String.format(TableFactory.clusteringTableNameFormatString, this.name, iteration));
			if (this.getPvaluesTable(iteration) != null)
				this.getPvaluesTable(iteration).setTitle(String.format(TableFactory.pvalueTableNameFormatString,
						this.name, this.getClusterHistory().getIterationNumber()));
			if (this.getPrototypesTable(iteration) != null)
				this.getPrototypesTable(iteration).setTitle(String.format(TableFactory.prototypesTableNameFormatString,
						this.name, this.getClusterHistory().getIterationNumber()));

			hist = hist.getParent();
		} while (hist.getParent() != null);
	}

	@Override
	public void destroy() {
		super.destroy();
		this.clearVisualStyles();
		if (this.prototypesTable != null)
			for (final TiconeCytoscapeTable prototypeTable : this.prototypesTable.values())
				ServiceHelper.getService(CyTableManager.class).deleteTable(prototypeTable.getSUID());
		if (this.clusteringTable != null)
			for (final TiconeCytoscapeTable prototypeTable : this.clusteringTable.values())
				ServiceHelper.getService(CyTableManager.class).deleteTable(prototypeTable.getSUID());
		if (this.pvaluesTable != null)
			for (final TiconeCytoscapeTable prototypeTable : this.pvaluesTable.values())
				ServiceHelper.getService(CyTableManager.class).deleteTable(prototypeTable.getSUID());
	}

	@Override
	protected void fireClusteringIterationDeleted(int iteration) {
		super.fireClusteringIterationDeleted(iteration);

		// remove the corresponding tables
		if (this.prototypesTable != null && this.prototypesTable.containsKey(iteration))
			ServiceHelper.getService(CyTableManager.class).deleteTable(this.prototypesTable.get(iteration).getSUID());
		if (this.clusteringTable != null && this.clusteringTable.containsKey(iteration))
			ServiceHelper.getService(CyTableManager.class).deleteTable(this.clusteringTable.get(iteration).getSUID());
		if (this.pvaluesTable != null && this.pvaluesTable.containsKey(iteration))
			ServiceHelper.getService(CyTableManager.class).deleteTable(this.pvaluesTable.get(iteration).getSUID());
	}

	public VisualStyle getClusterSimilarityVisualStyle(final int iteration,
			final ISimilarityValuesAllPairs<ICluster, ICluster, IClusterPair> similarities)
			throws SimilarityCalculationException, SimilarityValuesException, NoComparableSimilarityValuesException,
			InterruptedException, IncompatiblePrototypeComponentException, MissingPrototypeException {
		if (this.visualStyleClusterSimilarity != null && this.visualStyleClusterSimilarity.containsKey(iteration))
			return this.visualStyleClusterSimilarity.get(iteration);

		final VisualMappingFunctionFactory vmfDiscreteFactory = ServiceHelper
				.getService(VisualMappingFunctionFactory.class, "(mapping.type=discrete)");
		final VisualMappingFunctionFactory vmfPassthroughFactory = ServiceHelper
				.getService(VisualMappingFunctionFactory.class, "(mapping.type=passthrough)");

		final VisualStyleFactory visStyleFactory = ServiceHelper.getService(VisualStyleFactory.class);
		final VisualStyle vs = visStyleFactory.createVisualStyle(
				String.format("TiCoNE Cluster Similarity (%s, Iteration %d)", this.getName(), iteration));

		// get number of time points
		int numberTimepoints = PrototypeComponentType.TIME_SERIES
				.getComponent(similarities.getObject1(0).getPrototype()).getTimeSeries().getNumberTimePoints();

		vs.setDefaultValue(BasicVisualLexicon.NODE_WIDTH, numberTimepoints * 25.0);
		vs.setDefaultValue(BasicVisualLexicon.NODE_HEIGHT, 75.0);
		vs.setDefaultValue(BasicVisualLexicon.NODE_BORDER_WIDTH, 10.0);
		vs.setDefaultValue(BasicVisualLexicon.NODE_SHAPE, NodeShapeVisualProperty.RECTANGLE);
		vs.setDefaultValue(BasicVisualLexicon.NODE_FILL_COLOR, Color.WHITE);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_COLOR, Color.BLACK);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_FONT_SIZE, 18);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_WIDTH, 150.0);

		final VisualProperty<Paint> borderPaintProperty = BasicVisualLexicon.NODE_BORDER_PAINT;
		final DiscreteMapping<Boolean, Paint> borderPaintMapping = (DiscreteMapping<Boolean, Paint>) vmfDiscreteFactory
				.createVisualMappingFunction("selected", Boolean.class, borderPaintProperty);
		borderPaintMapping.putMapValue(true, Color.ORANGE);
		borderPaintMapping.putMapValue(false, Color.BLACK);
		vs.addVisualMappingFunction(borderPaintMapping);

		final VisualProperty<String> nodeLabelProperty = BasicVisualLexicon.NODE_LABEL;
		final VisualMappingFunction<String, String> nodeLabelMapping = vmfPassthroughFactory
				.createVisualMappingFunction("nodeLabel", String.class, nodeLabelProperty);
		vs.addVisualMappingFunction(nodeLabelMapping);

		for (final VisualPropertyDependency<?> dependency : vs.getAllVisualPropertyDependencies()) {
			if (dependency.getIdString().equals("nodeSizeLocked")) {
				dependency.setDependency(false);
			} else if (dependency.getIdString().equals("arrowColorMatchesEdge")) {
				dependency.setDependency(true);
			}
		}

		final VisualProperty<CyCustomGraphics> property = (VisualProperty<CyCustomGraphics>) ServiceHelper
				.getService(RenderingEngineManager.class).getDefaultVisualLexicon()
				.lookup(CyNode.class, "NODE_CUSTOMGRAPHICS_1");
		final DiscreteMapping<String, CyCustomGraphics> pMapping = (DiscreteMapping<String, CyCustomGraphics>) vmfDiscreteFactory
				.createVisualMappingFunction(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME, String.class, property);

		for (final ICluster p : this.getClusterHistory().getClusterObjectMapping().getClusters()) {
			try {
				final ITimeSeriesObjectList timeSeriesDatas = p.getObjects();
				ClusterChartContainer container = this.getDefaultClusterChartContainer(p);
				if (container == null) {
					container = new ClusterChartContainer(this, p, timeSeriesDatas);
					this.addClusterChartContainer(p, timeSeriesDatas, container);
				}
				final MyCustomGraphicsFactory factory = new MyCustomGraphicsFactory(container);
				final MyCustomGraphics mcg = factory.getInstance((URL) null);
				// mcg.setDisplayName("Cluster " + p.getClusterNumber());
				pMapping.putMapValue(p.getName(), mcg);
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
				e.printStackTrace();
			}
		}

		vs.addVisualMappingFunction(pMapping);

		final VisualProperty<ObjectPosition> labelPositionProp = (VisualProperty<ObjectPosition>) ServiceHelper
				.getService(RenderingEngineManager.class).getDefaultVisualLexicon()
				.lookup(CyNode.class, "NODE_LABEL_POSITION");
		vs.setDefaultValue(labelPositionProp,
				new ObjectPosition(Position.NORTH, Position.SOUTH, Justification.JUSTIFY_CENTER, 0, 0));

		// EDGES
		final VisualProperty<String> edgeLabelProperty = BasicVisualLexicon.EDGE_LABEL;
		final VisualMappingFunction<String, String> edgeLabelMapping = vmfPassthroughFactory
				.createVisualMappingFunction("edgeLabel", String.class, edgeLabelProperty);
		vs.addVisualMappingFunction(edgeLabelMapping);
		vs.setDefaultValue(BasicVisualLexicon.EDGE_LABEL_FONT_SIZE, 16);
		vs.setDefaultValue(BasicVisualLexicon.EDGE_LABEL_COLOR, Color.BLACK);

		// edge attribute for edge width, edge transparency
		final String similarityAttr = "similarity";

		// calculate min and max similarities
		// exclude infinite values
		final double minSim = similarities.min().get();
		final double maxSim = similarities.max().get();

		final VisualMappingFunctionFactory vmfContinuousFactoryP = ServiceHelper
				.getService(VisualMappingFunctionFactory.class, "(mapping.type=continuous)");
		final VisualProperty<Double> edgeWidthProperty = BasicVisualLexicon.EDGE_WIDTH;
		final VisualMappingFunction<Double, Double> edgeWidthMapping = vmfContinuousFactoryP
				.createVisualMappingFunction(similarityAttr, Double.class, edgeWidthProperty);

		final BoundaryRangeValues<Double> minEdgeWidth = new BoundaryRangeValues<>(5.0, 5.0, 5.0);
		final BoundaryRangeValues<Double> maxEdgeWidth = new BoundaryRangeValues<>(20.0, 20.0, 20.0);

		// edge width based on similarity
		((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(minSim, minEdgeWidth);
		((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(maxSim, maxEdgeWidth);
		vs.addVisualMappingFunction(edgeWidthMapping);

		// if we have more than 1,000 edges we hide them by default to improve
		// performance
		if (similarities.size() > 5000)
			vs.setDefaultValue(BasicVisualLexicon.EDGE_VISIBLE, false);

		// edge color based on similarity
		final VisualProperty<Paint> edgeColorProperty = BasicVisualLexicon.EDGE_UNSELECTED_PAINT;
		final VisualMappingFunction<Double, Paint> edgeColorMapping = vmfContinuousFactoryP
				.createVisualMappingFunction(similarityAttr, Double.class, edgeColorProperty);

		final BoundaryRangeValues<Paint> minEdgeColor = new BoundaryRangeValues<>(Color.decode("#66CCFF"),
				Color.decode("#66CCFF"), Color.decode("#66CCFF"));
		final BoundaryRangeValues<Paint> maxEdgeColor = new BoundaryRangeValues<>(Color.GREEN, Color.GREEN,
				Color.GREEN);

		((ContinuousMapping<Double, Paint>) edgeColorMapping).addPoint(minSim, minEdgeColor);
		((ContinuousMapping<Double, Paint>) edgeColorMapping).addPoint(maxSim, maxEdgeColor);
		vs.addVisualMappingFunction(edgeColorMapping);

		// edge transparency
		final VisualProperty<Integer> edgeTransparencyProperty = BasicVisualLexicon.EDGE_TRANSPARENCY;
		final VisualMappingFunction<Double, Integer> edgeTransMapping = vmfContinuousFactoryP
				.createVisualMappingFunction(similarityAttr, Double.class, edgeTransparencyProperty);

		final BoundaryRangeValues<Integer> minEdgeTrans = new BoundaryRangeValues<>(0, 0, 0);
		final BoundaryRangeValues<Integer> maxEdgeTrans = new BoundaryRangeValues<>(255, 255, 255);

		((ContinuousMapping<Double, Integer>) edgeTransMapping).addPoint(minSim, maxEdgeTrans);
		((ContinuousMapping<Double, Integer>) edgeTransMapping).addPoint(maxSim, minEdgeTrans);

		vs.addVisualMappingFunction(edgeTransMapping);

		ServiceHelper.getService(VisualMappingManager.class).addVisualStyle(vs);
		this.visualStyleClusterSimilarity.put(iteration, vs);
		return vs;
	}

	public VisualStyle getObjectSimilarityVisualStyle(final int iteration,
			final ISimilarityValuesAllPairs<ITimeSeriesObject, ?, ?> similarities)
			throws SimilarityCalculationException, SimilarityValuesException, NoComparableSimilarityValuesException,
			InterruptedException, IncompatiblePrototypeComponentException, MissingPrototypeException {
		if (this.visualStyleObjectSimilarity != null && this.visualStyleObjectSimilarity.containsKey(iteration))
			return this.visualStyleObjectSimilarity.get(iteration);

		final VisualMappingFunctionFactory vmfDiscreteFactory = ServiceHelper
				.getService(VisualMappingFunctionFactory.class, "(mapping.type=discrete)");
		final VisualMappingFunctionFactory vmfPassthroughFactory = ServiceHelper
				.getService(VisualMappingFunctionFactory.class, "(mapping.type=passthrough)");

		final VisualStyleFactory visStyleFactory = ServiceHelper.getService(VisualStyleFactory.class);
		final VisualStyle vs = visStyleFactory.createVisualStyle(
				String.format("TiCoNE Object Similarity (%s, Iteration %d)", this.getName(), iteration));

		final double nodeBorderWidth = 10.0;

		vs.setDefaultValue(BasicVisualLexicon.NODE_BORDER_WIDTH, nodeBorderWidth);
		vs.setDefaultValue(BasicVisualLexicon.NODE_FILL_COLOR, Color.WHITE);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_COLOR, Color.BLACK);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_FONT_SIZE, 18);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_WIDTH, 150.0);

		// get number of time points
		int numberTimepoints = similarities.getObject1(0).getPreprocessedTimeSeriesList()[0].getNumberTimePoints();
		// find length of longest node label
		int longestLabel = 0;

		Font font = vs.getDefaultValue(BasicVisualLexicon.NODE_LABEL_FONT_FACE);
		if (font == null)
			font = BasicVisualLexicon.NODE_LABEL_FONT_FACE.getDefault();

		Integer fontSize = vs.getDefaultValue(BasicVisualLexicon.NODE_LABEL_FONT_SIZE);
		if (fontSize == null)
			fontSize = BasicVisualLexicon.NODE_LABEL_FONT_SIZE.getDefault();

		font = font.deriveFont(fontSize.floatValue());
		Canvas c = new Canvas();
		FontMetrics fm = c.getFontMetrics(font);

		for (ITimeSeriesObject o : similarities.getObjects1()) {
			final String label;
			if (o.hasAlternativeName())
				label = o.getAlternativeName();
			else
				label = o.getName();

			longestLabel = Math.max(fm.stringWidth(label), longestLabel);
		}

		final VisualProperty<Double> nodeWidthProperty = BasicVisualLexicon.NODE_WIDTH;
		final DiscreteMapping<Boolean, Double> nodeWidthMapping = (DiscreteMapping<Boolean, Double>) vmfDiscreteFactory
				.createVisualMappingFunction(NetworkUtil.NODE_ATTRIBUTE_IS_CLUSTER, Boolean.class, nodeWidthProperty);
		nodeWidthMapping.putMapValue(true, Math.min(Math.max(100, numberTimepoints * 25.0), 400));
		nodeWidthMapping.putMapValue(false, Math.min(Math.max(75.0, longestLabel + nodeBorderWidth * 2 + 20), 300));
		vs.addVisualMappingFunction(nodeWidthMapping);

		final VisualProperty<Double> nodeHeightProperty = BasicVisualLexicon.NODE_HEIGHT;
		final DiscreteMapping<Boolean, Double> nodeHeightMapping = (DiscreteMapping<Boolean, Double>) vmfDiscreteFactory
				.createVisualMappingFunction(NetworkUtil.NODE_ATTRIBUTE_IS_CLUSTER, Boolean.class, nodeHeightProperty);
		nodeHeightMapping.putMapValue(true, 150.0);
		nodeHeightMapping.putMapValue(false, 75.0);
		vs.addVisualMappingFunction(nodeHeightMapping);

		final VisualProperty<NodeShape> nodeShapeProperty = BasicVisualLexicon.NODE_SHAPE;
		final DiscreteMapping<Boolean, NodeShape> nodeShapeMapping = (DiscreteMapping<Boolean, NodeShape>) vmfDiscreteFactory
				.createVisualMappingFunction(NetworkUtil.NODE_ATTRIBUTE_IS_CLUSTER, Boolean.class, nodeShapeProperty);
		nodeShapeMapping.putMapValue(true, NodeShapeVisualProperty.RECTANGLE);
		nodeShapeMapping.putMapValue(false, NodeShapeVisualProperty.ELLIPSE);
		vs.addVisualMappingFunction(nodeShapeMapping);

		// TODO: commented out for now, because it leads to performance problems on
		// large networks as of Cytoscape 3.7.1
//		final VisualProperty<Paint> borderPaintProperty = BasicVisualLexicon.NODE_BORDER_PAINT;
//		final DiscreteMapping<Boolean, Paint> borderPaintMapping = (DiscreteMapping<Boolean, Paint>) vmfDiscreteFactory
//				.createVisualMappingFunction("selected", Boolean.class, borderPaintProperty);
//		borderPaintMapping.putMapValue(true, Color.ORANGE);
//		borderPaintMapping.putMapValue(false, Color.BLACK);
//		vs.addVisualMappingFunction(borderPaintMapping);

		vs.setDefaultValue(BasicVisualLexicon.NODE_BORDER_PAINT, Color.BLACK);

		final VisualProperty<String> nodeLabelProperty = BasicVisualLexicon.NODE_LABEL;
		final VisualMappingFunction<String, String> nodeLabelMapping = vmfPassthroughFactory
				.createVisualMappingFunction("nodeLabel", String.class, nodeLabelProperty);
		vs.addVisualMappingFunction(nodeLabelMapping);

		for (final VisualPropertyDependency<?> dependency : vs.getAllVisualPropertyDependencies()) {
			if (dependency.getIdString().equals("nodeSizeLocked")) {
				dependency.setDependency(false);
			} else if (dependency.getIdString().equals("arrowColorMatchesEdge")) {
				dependency.setDependency(true);
			}
		}

		final VisualProperty<CyCustomGraphics> nodeCustomGraphicsProperty = (VisualProperty<CyCustomGraphics>) ServiceHelper
				.getService(RenderingEngineManager.class).getDefaultVisualLexicon()
				.lookup(CyNode.class, "NODE_CUSTOMGRAPHICS_1");
		final DiscreteMapping<String, CyCustomGraphics> nodeCustomGraphicsMapping = (DiscreteMapping<String, CyCustomGraphics>) vmfDiscreteFactory
				.createVisualMappingFunction(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME, String.class,
						nodeCustomGraphicsProperty);

		for (final ICluster p : this.getClusterHistory().getClusterObjectMapping().getClusters()) {
			try {
				final ITimeSeriesObjectList timeSeriesDatas = p.getObjects();
				ClusterChartContainer container = this.getDefaultClusterChartContainer(p);
				if (container == null) {
					container = new ClusterChartContainer(this, p, timeSeriesDatas);
					this.addClusterChartContainer(p, timeSeriesDatas, container);
				}
				final MyCustomGraphicsFactory factory = new MyCustomGraphicsFactory(container);
				final MyCustomGraphics mcg = factory.getInstance((URL) null);
				// mcg.setDisplayName("Cluster " + p.getClusterNumber());
				nodeCustomGraphicsMapping.putMapValue(p.getName(), mcg);
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
				e.printStackTrace();
			}
		}

		vs.addVisualMappingFunction(nodeCustomGraphicsMapping);

		final VisualProperty<Paint> nodeFillColorProperty = BasicVisualLexicon.NODE_FILL_COLOR;
		final DiscreteMapping<Integer, Paint> nodeFillColorMapping = (DiscreteMapping<Integer, Paint>) vmfDiscreteFactory
				.createVisualMappingFunction(NetworkUtil.NODE_ATTRIBUTE_CLUSTER_NUMBER, Integer.class,
						nodeFillColorProperty);

		for (final ICluster p : this.getClusterHistory().getClusterObjectMapping().getClusters()) {
			try {
				final ITimeSeriesObjectList timeSeriesDatas = p.getObjects();
				ClusterChartContainer container = this.getDefaultClusterChartContainer(p);
				if (container == null) {
					container = new ClusterChartContainer(this, p, timeSeriesDatas);
					this.addClusterChartContainer(p, timeSeriesDatas, container);
				}
				nodeFillColorMapping.putMapValue(p.getClusterNumber(), container.getGraphColor());
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
				e.printStackTrace();
			}
		}

		vs.addVisualMappingFunction(nodeFillColorMapping);

		final VisualProperty<ObjectPosition> labelPositionProp = (VisualProperty<ObjectPosition>) ServiceHelper
				.getService(RenderingEngineManager.class).getDefaultVisualLexicon()
				.lookup(CyNode.class, "NODE_LABEL_POSITION");
		final DiscreteMapping<Boolean, ObjectPosition> labelPositionMapping = (DiscreteMapping<Boolean, ObjectPosition>) vmfDiscreteFactory
				.createVisualMappingFunction(NetworkUtil.NODE_ATTRIBUTE_IS_CLUSTER, Boolean.class, labelPositionProp);
		labelPositionMapping.putMapValue(true,
				new ObjectPosition(Position.NORTH, Position.SOUTH, Justification.JUSTIFY_CENTER, 0, 0));
		labelPositionMapping.putMapValue(false,
				new ObjectPosition(Position.CENTER, Position.CENTER, Justification.JUSTIFY_CENTER, 0, 0));

		vs.addVisualMappingFunction(labelPositionMapping);

		// EDGES
		final VisualProperty<String> edgeLabelProperty = BasicVisualLexicon.EDGE_LABEL;
		final VisualMappingFunction<String, String> edgeLabelMapping = vmfPassthroughFactory
				.createVisualMappingFunction("edgeLabel", String.class, edgeLabelProperty);
		vs.addVisualMappingFunction(edgeLabelMapping);
		vs.setDefaultValue(BasicVisualLexicon.EDGE_LABEL_FONT_SIZE, 16);
		vs.setDefaultValue(BasicVisualLexicon.EDGE_LABEL_COLOR, Color.BLACK);
		vs.setDefaultValue(BasicVisualLexicon.EDGE_VISIBLE, false);

		// edge attribute for edge width, edge transparency
		final String similarityAttr = "similarity";

		// calculate min and max similarities
		// exclude infinite values
		final double minSim = similarities.min().get();
		final double maxSim = similarities.max().get();

		final VisualMappingFunctionFactory vmfContinuousFactoryP = ServiceHelper
				.getService(VisualMappingFunctionFactory.class, "(mapping.type=continuous)");
		final VisualProperty<Double> edgeWidthProperty = BasicVisualLexicon.EDGE_WIDTH;
		final VisualMappingFunction<Double, Double> edgeWidthMapping = vmfContinuousFactoryP
				.createVisualMappingFunction(similarityAttr, Double.class, edgeWidthProperty);

		final BoundaryRangeValues<Double> minEdgeWidth = new BoundaryRangeValues<>(5.0, 5.0, 5.0);
		final BoundaryRangeValues<Double> maxEdgeWidth = new BoundaryRangeValues<>(20.0, 20.0, 20.0);

		// edge width based on similarity
		((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(minSim, minEdgeWidth);
		((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(maxSim, maxEdgeWidth);
		vs.addVisualMappingFunction(edgeWidthMapping);

		// if we have more than 1,000 edges we hide them by default to improve
		// performance
		if (similarities.size() > 5000)
			vs.setDefaultValue(BasicVisualLexicon.EDGE_VISIBLE, false);

		// edge color based on similarity
		final VisualProperty<Paint> edgeColorProperty = BasicVisualLexicon.EDGE_UNSELECTED_PAINT;
		final VisualMappingFunction<Double, Paint> edgeColorMapping = vmfContinuousFactoryP
				.createVisualMappingFunction(similarityAttr, Double.class, edgeColorProperty);

		final BoundaryRangeValues<Paint> minEdgeColor = new BoundaryRangeValues<>(Color.decode("#66CCFF"),
				Color.decode("#66CCFF"), Color.decode("#66CCFF"));
		final BoundaryRangeValues<Paint> maxEdgeColor = new BoundaryRangeValues<>(Color.GREEN, Color.GREEN,
				Color.GREEN);

		((ContinuousMapping<Double, Paint>) edgeColorMapping).addPoint(minSim, minEdgeColor);
		((ContinuousMapping<Double, Paint>) edgeColorMapping).addPoint(maxSim, maxEdgeColor);
		vs.addVisualMappingFunction(edgeColorMapping);

		// edge transparency
		final VisualProperty<Integer> edgeTransparencyProperty = BasicVisualLexicon.EDGE_TRANSPARENCY;
		final VisualMappingFunction<Double, Integer> edgeTransMapping = vmfContinuousFactoryP
				.createVisualMappingFunction(similarityAttr, Double.class, edgeTransparencyProperty);

		final BoundaryRangeValues<Integer> minEdgeTrans = new BoundaryRangeValues<>(0, 0, 0);
		final BoundaryRangeValues<Integer> maxEdgeTrans = new BoundaryRangeValues<>(255, 255, 255);

		((ContinuousMapping<Double, Integer>) edgeTransMapping).addPoint(minSim, maxEdgeTrans);
		((ContinuousMapping<Double, Integer>) edgeTransMapping).addPoint(maxSim, minEdgeTrans);

		vs.addVisualMappingFunction(edgeTransMapping);

		ServiceHelper.getService(VisualMappingManager.class).addVisualStyle(vs);
		this.visualStyleClusterSimilarity.put(iteration, vs);
		return vs;
	}
}