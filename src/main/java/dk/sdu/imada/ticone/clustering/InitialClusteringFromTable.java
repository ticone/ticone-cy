package dk.sdu.imada.ticone.clustering;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.cytoscape.model.CyRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectSet;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesObjectSet;
import dk.sdu.imada.ticone.io.ClusteringImportFromTableConfig;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.similarity.AbstractSimilarityValue;

public class InitialClusteringFromTable extends AbstractInitialClusteringProvider {

	/**
	 * 
	 */
	private static final long serialVersionUID = -612257102286541207L;
	protected ClusteringImportFromTableConfig config;

	/**
	 * 
	 */
	public InitialClusteringFromTable() {
	}

	public InitialClusteringFromTable(final ClusteringImportFromTableConfig config) {
		super();
		this.config = config;
	}

	/**
	 * @return the config
	 */
	public ClusteringImportFromTableConfig getConfig() {
		return this.config;
	}

	/**
	 * @param config the config to set
	 */
	public void setConfig(ClusteringImportFromTableConfig config) {
		this.config = config;
	}

	@Override
	public ClusterObjectMapping getInitialClustering(final ITimeSeriesObjects timeSeriesDatas)
			throws ClusterOperationException, InterruptedException {
		try {
			final Map<String, ITimeSeriesObject> nameToObject = new HashMap<>();
			for (final ITimeSeriesObject obj : timeSeriesDatas) {
				nameToObject.put(obj.getName(), obj);
			}

			final Map<String, ITimeSeriesObjectList> clusterObjects = new HashMap<>();

			for (final CyRow row : this.config.getTable().getAllRows()) {
				final String clusterId = row.get(this.config.getClusterIdColumnName(), Object.class) + "";
				final String objectId = row.get(this.config.getObjectIdColumnName(), Object.class) + "";

				if (!nameToObject.containsKey(objectId))
					continue;

				if (!clusterObjects.containsKey(clusterId)) {
					clusterObjects.put(clusterId, new TimeSeriesObjectList());
				}
				clusterObjects.get(clusterId).add(nameToObject.get(objectId));

				nameToObject.remove(objectId);
			}

			final IAggregateCluster<ITimeSeries> aggregate = new AggregateClusterMeanTimeSeries();

			final PrototypeBuilder prototypeFactory = this.config.getPrototypeFactory();
			final ClusterObjectMapping result = new ClusterObjectMapping(timeSeriesDatas.asSet().copy(),
					prototypeFactory);

			for (final String clusterId : clusterObjects.keySet()) {
				ITimeSeries proto;
				proto = aggregate.aggregate(clusterObjects.get(clusterId));
				PrototypeComponentType.TIME_SERIES.getComponentFactory(prototypeFactory).setTimeSeries(proto);
				final ICluster c = result.addCluster(prototypeFactory.build());
				c.setKeep(true);
				c.setName(clusterId);

				for (final ITimeSeriesObject obj : clusterObjects.get(clusterId)) {
					result.addMapping(obj, c, AbstractSimilarityValue.MAX);
				}
			}

			// if we have ticked the option, create another cluster containing all
			// the remaining objects
			if (this.config.isCollectMissingObjectsIntoCluster()) {
				final ITimeSeriesObjectSet objs = new TimeSeriesObjectSet(nameToObject.values());

				ITimeSeries proto;
				proto = aggregate.aggregate(objs);
				PrototypeComponentType.TIME_SERIES.getComponentFactory(prototypeFactory).setTimeSeries(proto);
				final ICluster c = result.addCluster(prototypeFactory.build());
				c.setKeep(true);

				for (final ITimeSeriesObject obj : objs) {
					result.addMapping(obj, c, AbstractSimilarityValue.MAX);
				}
			}

			return result;
		} catch (final InterruptedException e) {
			throw e;
		} catch (final Exception e) {
			throw new ClusterOperationException(e);
		}
	}

	@Override
	public int getInitialNumberOfClusters() {
		final Set<String> clusterIds = new HashSet<>();

		Logger logger = LoggerFactory.getLogger(this.getClass());
		logger.debug("{}", this.config);
		logger.debug("{}", this.config.getTable());

		for (final CyRow row : this.config.getTable().getAllRows()) {
			clusterIds.add(row.get(this.config.getClusterIdColumnName(), Object.class) + "");
		}
		return clusterIds.size();
	}

	@Override
	public String toString() {
		return "Import from table";
	}
}
