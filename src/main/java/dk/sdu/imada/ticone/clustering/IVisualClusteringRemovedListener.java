/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 26, 2018
 *
 */
public interface IVisualClusteringRemovedListener {

	/**
	 * @param e
	 */
	void clusteringRemoved(VisualClusteringRemovedEvent e);

}
