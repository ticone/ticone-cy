/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.io.Serializable;

import dk.sdu.imada.ticone.io.ILoadDataMethod;
import dk.sdu.imada.ticone.preprocessing.IPreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;
import dk.sdu.imada.ticone.util.IIdMapMethod;
import dk.sdu.imada.ticone.util.ITimePointWeighting;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 8, 2019
 *
 */
public class TiconeCytoscapeClusteringResultFactory
		implements ITiconeClusteringResultFactory<ClusterObjectMapping, TiconeCytoscapeClusteringResult>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3888994248334469469L;

	@Override
	public TiconeCytoscapeClusteringResultFactory copy() {
		return new TiconeCytoscapeClusteringResultFactory();
	}

	@Override
	public TiconeCytoscapeClusteringResult createInstance(long seed, ILoadDataMethod loadDataMethod,
			IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider, int numberOfTimePoints,
			ITimePointWeighting timePointWeighting, IIdMapMethod idMapMethod,
			IPreprocessingSummary<ClusterObjectMapping> preprocessingSummary, ISimilarityFunction similarityFunction,
			IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethod,
			ITimeSeriesPreprocessor timeSeriesPreprocessor, IPrototypeBuilder prototypeBuilder)
			throws FactoryException, CreateInstanceFactoryException, InterruptedException {
		return new TiconeCytoscapeClusteringResult(seed, loadDataMethod, initialClusteringProvider, numberOfTimePoints,
				timePointWeighting, idMapMethod, preprocessingSummary, similarityFunction, clusteringMethod,
				timeSeriesPreprocessor, prototypeBuilder);
	}
}
