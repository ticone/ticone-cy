/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.HashSet;
import java.util.Set;

import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 26, 2018
 *
 */
public class VisualClusteringManager {

	protected Set<TiconeClusteringResultPanel> clusteringResults;

	protected Set<IVisualClusteringAddedListener> addedListener;

	protected Set<IVisualClusteringRemovedListener> removedListener;

	/**
	 * 
	 */
	public VisualClusteringManager() {
		super();
		this.clusteringResults = new HashSet<>();
		this.addedListener = new HashSet<>();
		this.removedListener = new HashSet<>();
	}

	public boolean addClusteringResult(final TiconeClusteringResultPanel clusteringResult) {
		final boolean res = this.clusteringResults.add(clusteringResult);
		final VisualClusteringAddedEvent event = new VisualClusteringAddedEvent(clusteringResult);
		if (res)
			this.fireClusteringAdded(event);
		return res;
	}

	public boolean removeClusteringResult(final TiconeClusteringResultPanel clusteringResult) {
		final boolean res = this.clusteringResults.remove(clusteringResult);
		final VisualClusteringRemovedEvent event = new VisualClusteringRemovedEvent(clusteringResult);
		if (res)
			this.fireClusteringRemoved(event);
		return res;
	}

	public boolean addClusteringAddedListener(final IVisualClusteringAddedListener listener) {
		return this.addedListener.add(listener);
	}

	public boolean addClusteringRemovedListener(final IVisualClusteringRemovedListener listener) {
		return this.removedListener.add(listener);
	}

	public boolean removeClusteringAddedListener(final IVisualClusteringAddedListener listener) {
		return this.addedListener.remove(listener);
	}

	public boolean removeClusteringRemovedListener(final IVisualClusteringRemovedListener listener) {
		return this.removedListener.remove(listener);
	}

	public void fireClusteringAdded(final VisualClusteringAddedEvent event) {
		for (final IVisualClusteringAddedListener l : this.addedListener)
			l.clusteringAdded(event);
	}

	public void fireClusteringRemoved(final VisualClusteringRemovedEvent event) {
		for (final IVisualClusteringRemovedListener l : this.removedListener)
			l.clusteringRemoved(event);
	}

	/**
	 * @return the clusteringResults
	 */
	public Set<TiconeClusteringResultPanel> getClusteringResults() {
		return this.clusteringResults;
	}
}
