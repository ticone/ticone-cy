/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 26, 2018
 *
 */
public interface IVisualClusteringAddedListener {
	void clusteringAdded(VisualClusteringAddedEvent event);
}
