package dk.sdu.imada.ticone;

import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.UIDefaults;
import javax.swing.UIManager;

import org.cytoscape.application.swing.CyMenuItem;
import org.cytoscape.application.swing.CyNodeViewContextMenuFactory;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTable;
import org.cytoscape.model.CyTableManager;
import org.cytoscape.service.util.AbstractCyActivator;
import org.cytoscape.session.events.SessionAboutToBeLoadedListener;
import org.cytoscape.session.events.SessionAboutToBeSavedListener;
import org.cytoscape.session.events.SessionLoadedListener;
import org.cytoscape.view.layout.AbstractLayoutAlgorithm;
import org.cytoscape.view.layout.AbstractLayoutTask;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.layout.EdgeWeighter;
import org.cytoscape.view.layout.WeightTypes;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.model.VisualProperty;
import org.cytoscape.view.presentation.customgraphics.CyCustomGraphics2Factory;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.ServiceProperties;
import org.cytoscape.work.Task;
import org.cytoscape.work.TaskFactory;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskManager;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.TunableSetter;
import org.cytoscape.work.undo.UndoSupport;
import org.osgi.framework.BundleContext;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.gui.TiconeClusteringResultsPanel;
import dk.sdu.imada.ticone.gui.TiconeComparisonResultsPanel;
import dk.sdu.imada.ticone.gui.TiconeConnectivityResultsPanel;
import dk.sdu.imada.ticone.gui.TiconePanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.tasks.ClearTiconeTablesTaskFactory;
import dk.sdu.imada.ticone.tasks.ClearTiconeVisualStylesTaskFactory;
import dk.sdu.imada.ticone.tasks.ClusterSimilarityNetworkTaskFactory;
import dk.sdu.imada.ticone.tasks.CreateNewClusterWithObjectsTaskFactory;
import dk.sdu.imada.ticone.tasks.ObjectSimilarityNetworkTaskFactory;
import dk.sdu.imada.ticone.tasks.coloring.LinearGradientFactoryListener;
import dk.sdu.imada.ticone.tasks.delete.DeleteClustersOrObjectsTaskFactory;
import dk.sdu.imada.ticone.tasks.merge.MergeSelectedClustersTaskFactory;
import dk.sdu.imada.ticone.util.CyNetworkUtil;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.MySaveAndLoadSession;
import dk.sdu.imada.ticone.util.SelectedNodesAndEdgesHandler;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * Created by christian on 3/3/15.
 */
public class CyTiconeActivator extends AbstractCyActivator {

	@Override
	public void start(final BundleContext context) throws Exception {
		final UIDefaults defaults = UIManager.getLookAndFeelDefaults();
		if (defaults.get("Table.alternateRowColor") == null)
			defaults.put("Table.alternateRowColor", new Color(240, 240, 240));

		final Properties properties = new Properties();
		this.registerAllServices(context, new SelectedNodesAndEdgesHandler());

		final MySaveAndLoadSession saveSession = new MySaveAndLoadSession();
		this.registerService(context, saveSession, SessionAboutToBeSavedListener.class, properties);
		this.registerService(context, saveSession, SessionAboutToBeLoadedListener.class, properties);
		this.registerService(context, saveSession, SessionLoadedListener.class, properties);

		final Properties myLayoutProps = new Properties();
		myLayoutProps.setProperty(ServiceProperties.PREFERRED_MENU, "My Layouts");
		this.registerService(context, new ClusteringComparisonLayout(null), CyLayoutAlgorithm.class, myLayoutProps);
		this.registerService(context, new ClusterSimilarityNetworkLayout(null), CyLayoutAlgorithm.class, myLayoutProps);

		this.registerServiceListener(context, new LinearGradientFactoryListener(), "addCustomGraphicsFactory",
				"removeCustomGraphicsFactory", CyCustomGraphics2Factory.class);

		final Properties ticoneNodeContextMenuProps = new Properties();
		ticoneNodeContextMenuProps.setProperty(ServiceProperties.TITLE, "TiCoNE");
		ticoneNodeContextMenuProps.setProperty(ServiceProperties.PREFERRED_MENU, "Apps");
		this.registerService(context, new TiconeNodeContextMenuFactory(), CyNodeViewContextMenuFactory.class,
				ticoneNodeContextMenuProps);

		ClearTiconeTablesTaskFactory clearTablesTaskFactory = new ClearTiconeTablesTaskFactory();
		Properties proprs = new Properties();
		proprs.setProperty(ServiceProperties.PREFERRED_MENU, "Apps.TiCoNE");
		proprs.setProperty(ServiceProperties.TITLE, "Clear TiCoNE tables");
		proprs.setProperty(ServiceProperties.TOOLTIP,
				"This command deletes all tables created by TiCoNE, as identified by a table name starting with 'TiCoNE'.");
		registerService(context, clearTablesTaskFactory, TaskFactory.class, proprs);

		ClearTiconeVisualStylesTaskFactory clearStylesTaskFactory = new ClearTiconeVisualStylesTaskFactory();
		proprs = new Properties();
		proprs.setProperty(ServiceProperties.PREFERRED_MENU, "Apps.TiCoNE");
		proprs.setProperty(ServiceProperties.TITLE, "Clear TiCoNE visual styles");
		proprs.setProperty(ServiceProperties.TOOLTIP,
				"This command deletes all visual styles created by TiCoNE, as identified by a name starting with 'TiCoNE'.");
		registerService(context, clearStylesTaskFactory, TaskFactory.class, proprs);

		this.removePreviousTables(context);

		final TiconeClusteringResultsPanel ticoneClusteringResultsPanel = new TiconeClusteringResultsPanel();
		GUIUtility.setTiconeClusteringResultsPanel(ticoneClusteringResultsPanel);

		final TiconeComparisonResultsPanel ticoneComparisonResultsPanel = new TiconeComparisonResultsPanel();
		GUIUtility.setTiconeComparisonResultsPanel(ticoneComparisonResultsPanel);

		final TiconeConnectivityResultsPanel ticoneConnectivityResultsPanel = new TiconeConnectivityResultsPanel();
		GUIUtility.setTiconeConnectivityResultsPanel(ticoneConnectivityResultsPanel);

		final TiconePanel ticonePanel = new TiconePanel();
		ticoneClusteringResultsPanel.addChangeListener(ticonePanel);
		ticoneClusteringResultsPanel.addContainerListener(ticonePanel);

		ticoneComparisonResultsPanel.addChangeListener(ticonePanel);
		ticoneComparisonResultsPanel.addContainerListener(ticonePanel);

		ticoneConnectivityResultsPanel.addChangeListener(ticonePanel);
		ticoneConnectivityResultsPanel.addContainerListener(ticonePanel);

		this.registerAllServices(context, ticonePanel, properties);
		this.registerAllServices(context, ticoneClusteringResultsPanel, properties);
		this.registerAllServices(context, ticoneComparisonResultsPanel, properties);
		this.registerAllServices(context, ticoneConnectivityResultsPanel, properties);
		// listener to network changes
		// TODO: every results panel inference should be listening
		// registerAllServices(context,
		// ticoneResultsPanel.getClusterConnectivityFormPanel(), properties);

	}

	static class TiconeNodeContextMenuFactory implements CyNodeViewContextMenuFactory {
		@Override
		public CyMenuItem createMenuItem(final CyNetworkView netView, final View<CyNode> nodeView) {
			final JMenu ticoneMenu = new JMenu("TiCoNE");
			try {
				final CyNetwork network = netView.getModel();
				boolean isCurrentlyVisibleClustering = CyNetworkUtil.isNetworkForCurrentlyVisibleClustering(network);

				final JMenuItem mergeClusters = new JMenuItem("Merge selected clusters");
				final JMenuItem deleteClusters = new JMenuItem("Delete selected clusters");
				final JMenuItem createClusterFromSelectedObjects = new JMenuItem(
						"Create new cluster from selected objects");
				if (!isCurrentlyVisibleClustering) {
					mergeClusters.setEnabled(false);
					mergeClusters.setToolTipText(
							"The currently visible clustering does not correspond to this network. Either choose the correct clustering in the Clusterings tab, or create a new cluster similarity network for the currently visible clustering.");

					deleteClusters.setEnabled(false);
					deleteClusters.setToolTipText(
							"The currently visible clustering does not correspond to this network. Either choose the correct clustering in the Clusterings tab, or create a new cluster similarity network for the currently visible clustering.");

					createClusterFromSelectedObjects.setEnabled(false);
					createClusterFromSelectedObjects.setToolTipText(
							"The currently visible clustering does not correspond to this network. Either choose the correct clustering in the Clusterings tab, or create a new cluster similarity network for the currently visible clustering.");
				} else {
					final TiconeClusteringResultPanel clusteringResultPanel = GUIUtility
							.getCurrentlySelectedClusteringResultPanel();
					final ClusterObjectMapping visibleClustering = clusteringResultPanel.getResult()
							.getClusteringProcess().getLatestClustering();

					final IClusters selectedClusters = CyNetworkUtil.getClustersForSelectedNodesInNetwork(network,
							visibleClustering);
					if (selectedClusters.size() < 2) {
						mergeClusters.setEnabled(false);
						mergeClusters.setToolTipText("Please select at least two cluster nodes.");
					} else {
						mergeClusters.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								try {
									if (!CyNetworkUtil.isNetworkForCurrentlyVisibleClustering(network))
										return;
									if (selectedClusters.size() < 2) {
										JOptionPane.showMessageDialog(null, "Please select at least two cluster nodes");
										return;
									}

									final MergeSelectedClustersTaskFactory mergePatternsTaskFactory = new MergeSelectedClustersTaskFactory(
											selectedClusters.stream().mapToInt(c -> c.getClusterNumber()).toArray(),
											clusteringResultPanel);
									final TaskIterator taskIterator = mergePatternsTaskFactory.createTaskIterator();

									if (CyNetworkUtil.isClusterSimilarityNetwork(network)) {
										taskIterator
												.append(new ClusterSimilarityNetworkTaskFactory(clusteringResultPanel)
														.createTaskIterator());
									} else if (CyNetworkUtil.isObjectSimilarityNetwork(network)) {
										taskIterator
												.append(new ObjectSimilarityNetworkTaskFactory(clusteringResultPanel)
														.setOnlyObjectClusterEdges(
																!CyNetworkUtil.isTotalObjectSimilarityNetwork(network))
														.createTaskIterator());
									}

									final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
									taskManager.execute(taskIterator);
								} catch (HeadlessException | InterruptedException | TiconeUnloadingException e1) {
								}
							}

						});

					}

					if (selectedClusters.size() < 1) {
						deleteClusters.setEnabled(false);
						deleteClusters.setToolTipText("Please select at least one cluster node.");
					} else {
						deleteClusters.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								try {
									if (!CyNetworkUtil.isNetworkForCurrentlyVisibleClustering(network))
										return;
									final IClusters selectedClusters = CyNetworkUtil
											.getClustersForSelectedNodesInNetwork(network, visibleClustering);

									if (selectedClusters.size() < 1) {
										JOptionPane.showMessageDialog(null, "Please select at least one cluster node");
										return;
									}

									final DeleteClustersOrObjectsTaskFactory deleteTaskFactory = new DeleteClustersOrObjectsTaskFactory(
											IClusterObjectMapping.DELETE_METHOD.BOTH_PROTOTYPE_AND_OBJECTS,
											selectedClusters, clusteringResultPanel, false);
									final TaskIterator taskIterator = deleteTaskFactory.createTaskIterator();

									if (CyNetworkUtil.isClusterSimilarityNetwork(network)) {
										taskIterator
												.append(new ClusterSimilarityNetworkTaskFactory(clusteringResultPanel)
														.createTaskIterator());
									} else if (CyNetworkUtil.isObjectSimilarityNetwork(network)) {
										taskIterator
												.append(new ObjectSimilarityNetworkTaskFactory(clusteringResultPanel)
														.setOnlyObjectClusterEdges(
																!CyNetworkUtil.isTotalObjectSimilarityNetwork(network))
														.createTaskIterator());
									}

									final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
									taskManager.execute(taskIterator);
								} catch (HeadlessException | InterruptedException | TiconeUnloadingException e1) {
								}
							}

						});
					}

					final ITimeSeriesObjects selectedObjects = CyNetworkUtil
							.getObjectsForSelectedNodesInNetwork(network, visibleClustering);

					if (selectedObjects.size() < 1) {
						createClusterFromSelectedObjects.setEnabled(false);
						createClusterFromSelectedObjects.setToolTipText("Please select at least one object node.");
					} else {
						createClusterFromSelectedObjects.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								try {
									if (!CyNetworkUtil.isNetworkForCurrentlyVisibleClustering(network))
										return;
									final ITimeSeriesObjects selectedObjects = CyNetworkUtil
											.getObjectsForSelectedNodesInNetwork(network, visibleClustering);

									if (selectedObjects.size() < 1) {
										JOptionPane.showMessageDialog(null, "Please select at least one object node");
										return;
									}

									final CreateNewClusterWithObjectsTaskFactory taskFactory = new CreateNewClusterWithObjectsTaskFactory(
											clusteringResultPanel, selectedObjects);
									final TaskIterator taskIterator = taskFactory.createTaskIterator();

									if (CyNetworkUtil.isClusterSimilarityNetwork(network)) {
										taskIterator
												.append(new ClusterSimilarityNetworkTaskFactory(clusteringResultPanel)
														.createTaskIterator());
									} else if (CyNetworkUtil.isObjectSimilarityNetwork(network)) {
										taskIterator
												.append(new ObjectSimilarityNetworkTaskFactory(clusteringResultPanel)
														.setOnlyObjectClusterEdges(
																!CyNetworkUtil.isTotalObjectSimilarityNetwork(network))
														.createTaskIterator());
									}

									final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
									taskManager.execute(taskIterator);
								} catch (HeadlessException | InterruptedException | TiconeUnloadingException e1) {
								}
							}

						});
					}
				}
				ticoneMenu.add(mergeClusters);
				ticoneMenu.add(deleteClusters);
				ticoneMenu.add(createClusterFromSelectedObjects);
			} catch (TiconeUnloadingException e) {
			}
			return new CyMenuItem(ticoneMenu, 3.0f);
		}
	}

	@Override
	public void shutDown() {
		try {
			while (GUIUtility.getTiconeClusteringsResultsPanel().getTabCount() > 0) {
				GUIUtility.getTiconeClusteringsResultsPanel().removeTabAt(0);
			}

			GUIUtility.getTiconeClusteringsResultsPanel().removeChangeListener(GUIUtility.getTiconePanel());
			GUIUtility.getTiconeClusteringsResultsPanel().removeContainerListener(GUIUtility.getTiconePanel());
		} catch (final TiconeUnloadingException e) {
		}
		super.shutDown();
	}

	/**
	 * This function is for when testing, and often loading the app to cytoscape
	 *
	 */
	private void removePreviousTables(final BundleContext context) {
		final CyTableManager cyTableManager = getService(context, CyTableManager.class);
		final Iterator<CyTable> tableIterator = cyTableManager.getAllTables(true).iterator();
		while (tableIterator.hasNext()) {
			final CyTable table = tableIterator.next();
			if (table.getTitle().endsWith("TSeries")) {
				cyTableManager.deleteTable(table.getSUID());
			}
		}
	}
}

class ClusteringComparisonLayout extends AbstractLayoutAlgorithm {
	/**
	 * Creates a new MyLayout object.
	 */
	public ClusteringComparisonLayout(final UndoSupport undo) {
		super("clusteringComparisonLayout", "TiCoNE Clustering Comparison Layout", undo);
	}

	@Override
	public TaskIterator createTaskIterator(final CyNetworkView networkView, final Object context,
			final Set<View<CyNode>> nodesToLayOut, final String attrName) {
		// TODO: context needed?
		// final CustomLayoutContext myContext = (CustomLayoutContext) context;
		final Task task = new AbstractLayoutTask(this.toString(), networkView, nodesToLayOut, attrName,
				this.undoSupport) {
			@Override
			protected void doLayout(final TaskMonitor taskMonitor) {
				double currX = 0.0d;
				double currY = 0.0d;

				final VisualProperty<Double> xLoc = BasicVisualLexicon.NODE_X_LOCATION;
				final VisualProperty<Double> yLoc = BasicVisualLexicon.NODE_Y_LOCATION;

				final VisualProperty<Double> nodeHeight = BasicVisualLexicon.NODE_HEIGHT;

				final CyNetwork net = this.networkView.getModel();

				// Set visual property.
				double clustering1Pos = 0, clustering2Pos = 0;

				for (final View<CyNode> nView : this.nodesToLayOut) {

					final String clustering = net.getRow(nView.getModel()).get("clustering", String.class);

					if (clustering.equals("1")) {
						currX = 0;
						currY = clustering1Pos;
						clustering1Pos += nView.getVisualProperty(nodeHeight) + 10;
					} else {
						currX = 1000;
						currY = clustering2Pos;
						clustering2Pos += nView.getVisualProperty(nodeHeight) + 10;
					}

					nView.setVisualProperty(xLoc, currX);
					nView.setVisualProperty(yLoc, currY);
				}
			}
		};
		return new TaskIterator(task);
	}
}

class ClusterSimilarityNetworkLayout extends AbstractLayoutAlgorithm {
	private CyLayoutAlgorithm forceDirectedLayout;

	public ClusterSimilarityNetworkLayout(final UndoSupport undo) {
		super("clusterSimilarityNetworkLayout", "TiCoNE Cluster Similarity Network Layout", undo);

		final CyLayoutAlgorithmManager layoutManager = ServiceHelper.getService(CyLayoutAlgorithmManager.class);
		this.forceDirectedLayout = layoutManager.getLayout("force-directed");
	}

	@Override
	public Object createLayoutContext() {
		Object context = this.forceDirectedLayout.getDefaultLayoutContext();

		final Map<String, Object> settings = new HashMap<>();
		settings.put("singlePartition", true);
		EdgeWeighter edgeWeighter = new EdgeWeighter();
		edgeWeighter.type = WeightTypes.WEIGHT;
		settings.put("edgeWeighter", edgeWeighter);
		settings.put("defaultSpringCoefficient", 0.01);
		settings.put("defaultSpringLength", 300);

		TunableSetter setter = ServiceHelper.getService(TunableSetter.class);
		setter.applyTunables(context, settings);

		return context;
	}

	@Override
	public TaskIterator createTaskIterator(final CyNetworkView networkView, final Object context,
			final Set<View<CyNode>> nodesToLayOut, String attrName) {

//		final Map<String, Object> settings = new HashMap<>();
//		settings.put("defaultSpringLength", ServiceHelper.getService(VisualMappingManager.class)
//				.getVisualStyle(networkView).getDefaultValue(BasicVisualLexicon.NODE_WIDTH) * 2);
//
//		TunableSetter setter = CyTiconeActivator.getTunableSetter();
//		setter.applyTunables(context, settings);

		attrName = "similarity";
		return forceDirectedLayout.createTaskIterator(networkView, context, nodesToLayOut, attrName);
	}
}