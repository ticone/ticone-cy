package dk.sdu.imada.ticone.tasks.coloring;

import java.awt.Color;
import java.awt.Paint;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.VisualLexicon;
import org.cytoscape.view.model.VisualProperty;
import org.cytoscape.view.presentation.RenderingEngine;
import org.cytoscape.view.presentation.RenderingEngineManager;
import org.cytoscape.view.presentation.customgraphics.CyCustomGraphics;
import org.cytoscape.view.presentation.customgraphics.CyCustomGraphics2;
import org.cytoscape.view.presentation.customgraphics.CyCustomGraphics2Factory;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.view.vizmap.VisualMappingFunction;
import org.cytoscape.view.vizmap.VisualMappingFunctionFactory;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.view.vizmap.VisualStyleFactory;
import org.cytoscape.view.vizmap.mappings.DiscreteMapping;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.network.NetworkUtil;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork.TiconeCytoscapeNetworkNode;
import dk.sdu.imada.ticone.util.ColorUtility;
import dk.sdu.imada.ticone.util.ServiceHelper;

/**
 * Created by christian on 3/24/15.
 */
public class ColorClusterPairsTask extends AbstractTask {

	private final CyNetwork cyNetwork;
	private final CyNetworkView cyNetworkView;
	private final IClusterObjectMapping clustering;
	private final Set<Pair<ICluster, ICluster>> clusterPairsToColor;
	// private Set<String> toColor;
	// private Pair<Color, Color> colorPair;
	private final String visualStyleName;

	public ColorClusterPairsTask(final CyNetwork cyNetwork, final CyNetworkView cyNetworkView,
			final IClusterObjectMapping clustering, final Set<Pair<ICluster, ICluster>> clusterPairsToColor,
			final String visualStyleName) {
		super();
		;
		this.cyNetwork = cyNetwork;
		this.cyNetworkView = cyNetworkView;
		// this.toColor = toColor;
		// this.colorPair = colorPair;
		this.clustering = clustering;
		this.clusterPairsToColor = clusterPairsToColor;
		this.visualStyleName = visualStyleName;
	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		tm.setTitle("Coloring nodes with gradients..");
		tm.setProgress(0.1);

		// make sure that we have the node attribute for coloring created
		final TiconeCytoscapeNetwork net = new TiconeCytoscapeNetwork(this.cyNetwork);
		if (!net.hasNodeAttribute("clusterPair")) {
			net.createNodeAttribute("clusterPair", String.class, false);
			for (final TiconeCytoscapeNetworkNode node : net.getNodeSet()) {
				net.setNodeAttribute(node, "clusterPair", String.format("%s, %s",
						net.getValue(node, "cluster 1", String.class), net.getValue(node, "cluster 2", String.class)));
			}
		}

		final VisualStyleFactory visStyleFactory = ServiceHelper.getService(VisualStyleFactory.class);
		final VisualStyle vs = visStyleFactory.createVisualStyle(this.visualStyleName);

		final VisualMappingFunctionFactory vmfDiscreteFactoryP = ServiceHelper
				.getService(VisualMappingFunctionFactory.class, "(mapping.type=discrete)");
		final VisualMappingFunctionFactory visualMappingFunctionPassthroughFactory = ServiceHelper
				.getService(VisualMappingFunctionFactory.class, "(mapping.type=passthrough)");

		final RenderingEngineManager renderingEngineManager = ServiceHelper.getService(RenderingEngineManager.class);
		final VisualLexicon defaultVisualLexicon = renderingEngineManager.getDefaultVisualLexicon();

		// node labels
		final VisualMappingFunction<String, String> createVisualMappingFunction = visualMappingFunctionPassthroughFactory
				.createVisualMappingFunction(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME, String.class,
						BasicVisualLexicon.NODE_LABEL);
		vs.addVisualMappingFunction(createVisualMappingFunction);

		// create node gradients
		final VisualProperty<CyCustomGraphics> property = (VisualProperty<CyCustomGraphics>) defaultVisualLexicon
				.lookup(CyNode.class, "NODE_CUSTOMGRAPHICS_1");

		final DiscreteMapping<String, CyCustomGraphics> clusterPairColorMapping = (DiscreteMapping<String, CyCustomGraphics>) vmfDiscreteFactoryP
				.createVisualMappingFunction("clusterPair", String.class, property);

		// node label color mapping
		final DiscreteMapping<String, Paint> nodeLabelColorMapping = (DiscreteMapping<String, Paint>) vmfDiscreteFactoryP
				.createVisualMappingFunction("clusterPair", String.class, BasicVisualLexicon.NODE_LABEL_COLOR);

		for (final Pair<ICluster, ICluster> clusterPair : this.clusterPairsToColor) {
			final Pair<Color, Color> colorPair = Pair.of(Color.decode(ColorUtility.getColor(clusterPair.getLeft())),
					Color.decode(ColorUtility.getColor(clusterPair.getRight())));

			// node gradient
			final CyCustomGraphics2Factory<?> customGraphicsFactory = ServiceHelper
					.getService(LinearGradientFactoryListener.class).getFactory();
			final Map<String, Object> chartProps = new HashMap<>();
			chartProps.put("cy_gradientFractions", Arrays.asList(0.0f, 0.475f, 0.5f, 0.525f, 1.0f));
			chartProps.put("cy_gradientColors", Arrays.asList(colorPair.getLeft(), colorPair.getLeft(), Color.WHITE,
					colorPair.getRight(), colorPair.getRight()));
			chartProps.put("cy_angle", -90.0);
			final CyCustomGraphics2<?> customGraphics = customGraphicsFactory.getInstance(chartProps);

			final String clusterPairString = String.format("%s, %s", clusterPair.getLeft().getName(),
					clusterPair.getRight().getName());

			clusterPairColorMapping.putMapValue(clusterPairString, customGraphics);

			// node label color
			final Color textColor = ColorUtility.calculateTextColor(colorPair);
			nodeLabelColorMapping.putMapValue(clusterPairString, textColor);
		}

		vs.addVisualMappingFunction(clusterPairColorMapping);
		vs.addVisualMappingFunction(nodeLabelColorMapping);

		// node position to upper half of node
		final Collection<RenderingEngine<?>> engines = renderingEngineManager.getRenderingEngines(this.cyNetworkView);
		final VisualLexicon lex = engines.iterator().next().getVisualLexicon();
		final VisualProperty<Object> prop = (VisualProperty<Object>) lex.lookup(CyNode.class, "NODE_LABEL_POSITION");
		vs.setDefaultValue(prop, prop.parseSerializableString("N,N,c,0.0,0.0"));

		ServiceHelper.getService(VisualMappingManager.class).addVisualStyle(vs);

		vs.apply(this.cyNetworkView);
		this.cyNetworkView.updateView();

		ServiceHelper.getService(VisualMappingManager.class).setCurrentVisualStyle(vs);
		tm.setProgress(1.0);
		tm.setStatusMessage(" ... ok ");

	}
}
