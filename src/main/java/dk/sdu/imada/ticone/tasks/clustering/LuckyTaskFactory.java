package dk.sdu.imada.ticone.tasks.clustering;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;

/**
 * Created by christian on 26-01-16.
 */
public class LuckyTaskFactory extends AbstractTaskFactory {

	private final LuckyTask luckyTask;

	public LuckyTaskFactory(final TiconeClusteringResultPanel resultPanel) {
		this.luckyTask = new LuckyTask(resultPanel);
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.luckyTask);
		return taskIterator;
	}
}
