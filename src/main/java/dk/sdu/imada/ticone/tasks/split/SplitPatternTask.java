package dk.sdu.imada.ticone.tasks.split;

import javax.swing.JFrame;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.splitpattern.ISplitCluster;
import dk.sdu.imada.ticone.clustering.splitpattern.ISplitClusterContainer;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.panels.popup.SplitPatternAcceptPanel;
import dk.sdu.imada.ticone.util.Utility;

/**
 * Created by christian on 8/31/15.
 */
public class SplitPatternTask extends AbstractTask {

	private final ISplitCluster splitPattern;
	private final int clusterNumber;
	protected TiconeClusteringResultPanel resultPanel;

	public SplitPatternTask(final ISplitCluster splitPattern, final int clusterNumber,
			final TiconeClusteringResultPanel resultPanel) {
		super();
		this.splitPattern = splitPattern;
		this.clusterNumber = clusterNumber;
		this.resultPanel = resultPanel;
	}

	@Override
	public void run(final TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setTitle("Splitting cluster");
		taskMonitor.setStatusMessage("Splitting cluster " + this.clusterNumber);

		this.resultPanel.getClusteringResult().getSimilarityFunction().initialize();
		final IClusteringProcess timeSeriesClusteringWithOverrepresentedPatterns = this.resultPanel
				.getClusteringResult().getClusteringProcess();
		final ISplitClusterContainer splitPatternContainer = timeSeriesClusteringWithOverrepresentedPatterns
				.splitCluster(this.splitPattern, this.clusterNumber);
		final JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		if (!Utility.getProgress().getStatus()) {
			return;
		}
		final SplitPatternAcceptPanel splitPatternAcceptPanel = new SplitPatternAcceptPanel(this.splitPattern,
				splitPatternContainer, frame, this.resultPanel);
		frame.add(splitPatternAcceptPanel);
		frame.pack();
		frame.setVisible(true);
	}

	@Override
	public void cancel() {
		Utility.getProgress().stop();
		super.cancel();
	}
}
