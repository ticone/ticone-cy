package dk.sdu.imada.ticone.tasks.clustering;

import java.util.Observable;
import java.util.Observer;

import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.util.IProgress;

/**
 * Created by christian on 6/29/15.
 */
public class ClusterObjectsObserver implements Observer {

	private final IProgress progress;
	private final TaskMonitor taskMonitor;

	public ClusterObjectsObserver(final IProgress progress, final TaskMonitor tm) {
		this.progress = progress;
		this.taskMonitor = tm;
	}

	@Override
	public void update(final Observable observable, final Object o) {
		if (!Double.isNaN(this.progress.getProgress()))
			this.taskMonitor.setProgress(this.progress.getProgress());
		if (this.progress.getProgressMessage() != null)
			this.taskMonitor.setStatusMessage(this.progress.getProgressMessage());
		if (this.progress.getTitle() != null)
			this.taskMonitor.setTitle(this.progress.getTitle());
	}
}
