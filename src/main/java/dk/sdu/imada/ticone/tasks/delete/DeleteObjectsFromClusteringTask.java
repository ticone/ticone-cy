package dk.sdu.imada.ticone.tasks.delete;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.util.GUIUtility;

/**
 * Created by christian on 03-03-16.
 */
public class DeleteObjectsFromClusteringTask extends AbstractTask {

	private final ITimeSeriesObjects objectsToDelete;

	protected TiconeClusteringResultPanel resultPanel;

	public DeleteObjectsFromClusteringTask(final ITimeSeriesObjects objectsToDelete,
			final TiconeClusteringResultPanel resultPanel) {
		super();
		this.objectsToDelete = objectsToDelete;
		this.resultPanel = resultPanel;

	}

	@Override
	public void run(final TaskMonitor taskMonitor) throws Exception {
		try {
			final IClusteringProcess timeSeriesClusteringWithOverrepresentedPatterns = this.resultPanel
					.getClusteringResult().getClusteringProcess();
			taskMonitor.setTitle("Deleting objects");
			taskMonitor.setStatusMessage("Deleting objects from Clusters");
			timeSeriesClusteringWithOverrepresentedPatterns.removeObjectsFromClusters(this.objectsToDelete);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			GUIUtility.updateGraphPanel(this.resultPanel);
		}
	}
}
