/**
 * 
 */
package dk.sdu.imada.ticone.tasks;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 2, 2019
 *
 */
public class ClearTiconeTablesTaskFactory extends AbstractTaskFactory {

	@Override
	public TaskIterator createTaskIterator() {
		return new TaskIterator(new ClearTiconeTablesTask());
	}

}
