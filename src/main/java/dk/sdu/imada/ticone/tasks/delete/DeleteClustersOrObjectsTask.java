package dk.sdu.imada.ticone.tasks.delete;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.util.ActionContainer;
import dk.sdu.imada.ticone.util.ClusterStatusMapping;
import dk.sdu.imada.ticone.util.GUIUtility;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 10, 2019
 *
 */
public class DeleteClustersOrObjectsTask extends AbstractTask {

	private boolean showPreview;

	private final IClusterObjectMapping.DELETE_METHOD deleteMethod;

	private final IClusters clusters;

	protected TiconeClusteringResultPanel resultPanel;

	public DeleteClustersOrObjectsTask(final IClusterObjectMapping.DELETE_METHOD deleteMethod, final IClusters clusters,
			final TiconeClusteringResultPanel resultPanel, final boolean showPreview) {
		super();
		this.deleteMethod = deleteMethod;
		this.clusters = clusters;
		this.resultPanel = resultPanel;
		this.showPreview = showPreview;

	}

	@Override
	public void run(final TaskMonitor taskMonitor) throws Exception {
		for (ICluster cluster : this.clusters) {
			final ActionContainer actionContainer = new ActionContainer(cluster, deleteMethod);
			this.resultPanel.getClusteringProcess().addNewActionsToApplyBeforeNextIteration(actionContainer);
			if (showPreview) {
				GUIUtility.showActionPanel(this.resultPanel);
				final ClusterStatusMapping patternStatusMapping = this.resultPanel.getClusteringResult()
						.getClusterStatusMapping();
				patternStatusMapping.setDeleted(cluster);
			}
			if (!showPreview)
				this.resultPanel.getClusteringProcess().applyActionsBeforeNextIteration();
			GUIUtility.updateGraphPanel(this.resultPanel);
		}
	}
}
