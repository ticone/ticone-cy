package dk.sdu.imada.ticone.tasks.split;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.clustering.splitpattern.ISplitCluster;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;

/**
 * Created by christian on 8/31/15.
 */
public class SplitPatternTaskFactory extends AbstractTaskFactory {

	private final SplitPatternTask splitPatternTask;

	public SplitPatternTaskFactory(final ISplitCluster splitPatternInterface, final int clusterNumber,
			final TiconeClusteringResultPanel resultPanel) {
		this.splitPatternTask = new SplitPatternTask(splitPatternInterface, clusterNumber, resultPanel);
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.splitPatternTask);
		return taskIterator;
	}
}
