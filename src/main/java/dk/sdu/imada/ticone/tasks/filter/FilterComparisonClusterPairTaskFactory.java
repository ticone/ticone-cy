package dk.sdu.imada.ticone.tasks.filter;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.filter.IFilter;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.comparison.CytoscapeClusteringComparisonResult;
import dk.sdu.imada.ticone.feature.ClusterPairFeaturePvalue;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.gui.panels.MyDialogPanel;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Dec 14, 2018
 *
 */
public class FilterComparisonClusterPairTaskFactory extends AbstractTaskFactory {

	protected CytoscapeClusteringComparisonResult utils;
	protected IArithmeticFeature<?> feature;
	protected IFilter.FILTER_OPERATOR operator;
	protected double filterInput;

	public FilterComparisonClusterPairTaskFactory(final CytoscapeClusteringComparisonResult utils,
			final IArithmeticFeature<?> feature, final IFilter.FILTER_OPERATOR operator, final double filterInput) {
		super();
		this.utils = utils;
		this.feature = feature;
		this.operator = operator;
		this.filterInput = filterInput;
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(new FilterComparisonClusterPairTask(utils, feature, operator, filterInput));
		return taskIterator;
	}

	class FilterComparisonClusterPairTask extends AbstractTask {

		protected final CytoscapeClusteringComparisonResult comparisonResult;
		protected final IArithmeticFeature<?> feature;
		protected final IFilter.FILTER_OPERATOR operator;
		protected final double filterInput;

		public FilterComparisonClusterPairTask(final CytoscapeClusteringComparisonResult utils,
				final IArithmeticFeature<?> feature, final IFilter.FILTER_OPERATOR operator, final double filterInput) {
			super();
			this.comparisonResult = utils;
			this.feature = feature;
			this.operator = operator;
			this.filterInput = filterInput;
		}

		@Override
		public void run(final TaskMonitor tm) throws Exception {
			tm.setTitle("Applying Filter");
			tm.setStatusMessage("Evaluating filter condition for each cluster pair ...");

			final IFilter<IClusterPair> filter = this.comparisonResult.getFilter();
			if (this.feature instanceof ClusterPairFeaturePvalue) {
				if (this.comparisonResult.getPvalues() == null) {
					MyDialogPanel.showMessageDialog("Error applying filter",
							"P-values have not yet been calculated. Please do so first and then reapply the filter afterwards");
					return;
				}
			} else if (!this.comparisonResult.getFeatureStore().featureSet().contains(this.feature)) {
				MyDialogPanel.showMessageDialog("Error applying filter",
						"Your filter settings could not be applied. Please try again");
				return;
			}

			filter.setActive(true);
			filter.setFeature(this.feature);
			filter.setFilterOperator(this.operator);
			filter.setFilterInput(this.filterInput);

			if (this.feature instanceof ClusterPairFeaturePvalue) {
				((ClusterPairFeaturePvalue) this.feature).setFeatureValueProvider(this.comparisonResult.getPvalues());
			}

			filter.fireFilterChanged();
		}
	}

}
