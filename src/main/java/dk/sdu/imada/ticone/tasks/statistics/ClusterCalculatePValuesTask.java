package dk.sdu.imada.ticone.tasks.statistics;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.statistics.BasicCalculatePValues;
import dk.sdu.imada.ticone.tasks.clustering.ClusterObjectsObserver;
import dk.sdu.imada.ticone.util.IProgress;
import dk.sdu.imada.ticone.util.Progress;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Apr 26, 2017
 *
 */
public class ClusterCalculatePValuesTask extends AbstractTask {

	private IProgress progress;

	protected TiconeCytoscapeClusteringResult utils;
	protected BasicCalculatePValues calculateClusterPValues;
	private final long seed;

	public ClusterCalculatePValuesTask(final TiconeCytoscapeClusteringResult utils,
			final BasicCalculatePValues calculateClusterPValues, final long seed) {
		super();
		this.utils = utils;
		this.calculateClusterPValues = calculateClusterPValues;
		this.seed = seed;
	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		tm.setTitle("Calculating Cluster P-values");
		this.progress = this.utils.getClusteringProcess().getProgress();
		final ClusterObjectsObserver clusterObjectsObserver = new ClusterObjectsObserver(this.progress, tm);
		((Progress) this.progress).addObserver(clusterObjectsObserver);

		try {
			this.utils.getSimilarityFunction().initialize();
			this.utils.getClusteringProcess().calculatePValues(this.calculateClusterPValues, seed);

		} catch (InterruptedException e) {
			tm.setProgress(1.0);
			((Progress) this.progress).deleteObserver(clusterObjectsObserver);
			tm.setStatusMessage("Done!");
		}
	}

	@Override
	public void cancel() {
		calculateClusterPValues.cancel();
	}
}
