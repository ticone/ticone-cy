package dk.sdu.imada.ticone.tasks.clustering;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.table.TableFactory;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.IProgress;
import dk.sdu.imada.ticone.util.Progress;

/**
 * Created by christian on 5/26/15.
 */
public class ClusterObjectsTask extends AbstractTask {

	private IProgress progress;

	protected IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> process;

	public ClusterObjectsTask(final IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> process) {
		super();
		this.process = process;
	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		try {
			final long time = System.currentTimeMillis();
			boolean firstIteration = this.process.isFirstIteration();
			tm.setTitle("Performing clustering iteration");
			tm.setStatusMessage("Finishing preparations ...");
			this.progress = this.process.getProgress();
			final ClusterObjectsObserver clusterObjectsObserver = new ClusterObjectsObserver(this.progress, tm);
			((Progress) this.progress).addObserver(clusterObjectsObserver);

			this.process.getSimilarityFunction().initialize();
			// Apply all changes before next iteration (that means delete patterns,
			// and add new ones)
			this.process.applyActionsBeforeNextIteration();
			tm.setStatusMessage("Performing clustering iteration ...");
			final IClusterObjectMapping pom = this.process.doIteration();
			if (!this.progress.getStatus()) {
				this.progress.start();
				((Progress) this.progress).deleteObserver(clusterObjectsObserver);
				return;
			}

			tm.setStatusMessage("Setting up cytoscape tables");
			try {
				TableFactory.setupClusterTables(this.process.getClusteringResult());
			} catch (final Exception e) {
				e.printStackTrace();
			}

			// For graph gui
			this.process.getClusteringResult().setupPatternStatusMapping();

			tm.setStatusMessage("Drawing cluster charts");

			TiconeClusteringResultPanel p;
			if (firstIteration) {
				p = new TiconeClusteringResultPanel(this.process);
			} else
				p = GUIUtility.getCurrentlySelectedClusteringResultPanel();
			GUIUtility.updateGraphPanel(p);
			tm.setProgress(1.0);
			final long totalTime = (System.currentTimeMillis() - time) / 1000;
			((Progress) this.progress).deleteObserver(clusterObjectsObserver);

			Logger logger = LoggerFactory.getLogger(this.getClass());
			logger.info("Seconds: " + totalTime);
			logger.info("Minutes: " + totalTime / 60);
			GUIUtility.setGraphTabInFocus();
			tm.setStatusMessage("Done!");

			GUIUtility.getTiconePanel().getDataFormPanel().resetComponents();
		} catch (final InterruptedException e) {
		}
	}

	@Override
	public void cancel() {
		this.progress.stop();
	}
}
