package dk.sdu.imada.ticone.tasks;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.layout.EdgeWeighter;
import org.cytoscape.view.layout.WeightTypes;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskManager;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.TunableSetter;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair.ClusterPairsFactory;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.util.CyNetworkUtil;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeProgressEvent;
import dk.sdu.imada.ticone.util.TiconeTaskProgressListener;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 8, 2019
 *
 */
public class ClusterSimilarityNetworkTask extends AbstractTask implements TiconeTaskProgressListener {

	private final TiconeClusteringResultPanel resultPanel;

	protected TaskMonitor tm;

	protected int iterationNumber;

	protected TiconeCytoscapeNetwork clusterSimilarityNetwork;

	protected IClusters clusters;

	public ClusterSimilarityNetworkTask(final TiconeClusteringResultPanel resultPanel) throws InterruptedException {
		this(resultPanel, null);
	}

	public ClusterSimilarityNetworkTask(final TiconeClusteringResultPanel resultPanel, final IClusters clusters)
			throws InterruptedException {
		super();
		this.resultPanel = resultPanel;
		this.clusters = clusters;
		this.iterationNumber = resultPanel.getClusteringResult().getClusterHistory().getIterationNumber();
	}

	@Override
	public void progressUpdated(final TiconeProgressEvent event) {
		if (event.getTitle() != null)
			this.tm.setTitle(event.getTitle());
		if (event.getStatusMessage() != null)
			this.tm.setStatusMessage(event.getStatusMessage());
		if (event.getPercent() != null)
			this.tm.setProgress(event.getPercent());
	}

	@Override
	public void cancel() {
		super.cancel();
	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		try {
			this.tm = tm;

			tm.setTitle("Create Cluster Similarity Network");
			tm.setStatusMessage("Initializing similarity function ...");

			TiconeCytoscapeClusteringResult result = this.resultPanel.getResult();
			ISimilarityFunction simFunc = result.getSimilarityFunction();
			simFunc.initialize();

			if (this.clusters == null)
				this.clusters = result.getClusteringProcess().getLatestClustering().getClusters();

			if (this.clusters.isEmpty())
				return;

			tm.setStatusMessage("Calculating cluster similarities ...");

			ICluster[] clusterArray = this.clusters.toArray();
			final ISimilarityValuesAllPairs<ICluster, ICluster, IClusterPair> similarities = simFunc
					.calculateSimilarities(clusterArray, clusterArray, new ClusterPairsFactory(),
							ObjectType.CLUSTER_PAIR);

			tm.setStatusMessage("Creating network ...");

			this.clusterSimilarityNetwork = CyNetworkUtil.createClusterSimilarityNetwork(result, similarities,
					this.clusters);

			tm.setStatusMessage("Layouting network ...");

			final CyLayoutAlgorithmManager layoutManager = ServiceHelper.getService(CyLayoutAlgorithmManager.class);
			final CyLayoutAlgorithm layout = layoutManager.getLayout("force-directed");

			Object context = layout.getDefaultLayoutContext();
			Map<String, Object> settings = new HashMap<>();
			settings.put("defaultSpringLength", 200.0);
			settings.put("singlePartition", true);
			EdgeWeighter edgeWeighter = new EdgeWeighter();
			edgeWeighter.type = WeightTypes.WEIGHT;
			settings.put("edgeWeighter", edgeWeighter);
			settings.put("defaultSpringCoefficient", 0.01);

			TunableSetter setter = ServiceHelper.getService(TunableSetter.class);
			setter.applyTunables(context, settings);

			final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);

			final Collection<CyNetworkView> views = ServiceHelper.getService(CyNetworkViewManager.class)
					.getNetworkViews(this.clusterSimilarityNetwork.getCyNetwork());
			for (final CyNetworkView view : views) {
				taskManager.execute(
						layout.createTaskIterator(view, context, CyLayoutAlgorithm.ALL_NODE_VIEWS, "similarity"));
			}
		} catch (InterruptedException e) {
		}
	}

	public static double round(final double value, final int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		if (Double.isNaN(value) || Double.isInfinite(value))
			return value;

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
}
