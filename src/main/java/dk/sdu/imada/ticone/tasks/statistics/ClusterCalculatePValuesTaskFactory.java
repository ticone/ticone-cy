package dk.sdu.imada.ticone.tasks.statistics;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.statistics.BasicCalculatePValues;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Apr 26, 2017
 *
 */
public class ClusterCalculatePValuesTaskFactory extends AbstractTaskFactory {

	ClusterCalculatePValuesTask clusterObjectsTask;

	public ClusterCalculatePValuesTaskFactory(final TiconeCytoscapeClusteringResult utils,
			final BasicCalculatePValues calculateClusterPValues, final long seed) {
		super();

		this.clusterObjectsTask = new ClusterCalculatePValuesTask(utils, calculateClusterPValues, seed);
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.clusterObjectsTask);
		return taskIterator;
	}
}
