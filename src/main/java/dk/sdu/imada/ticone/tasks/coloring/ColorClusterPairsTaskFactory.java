package dk.sdu.imada.ticone.tasks.coloring;

import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;

/**
 * Created by christian on 3/24/15.
 */
public class ColorClusterPairsTaskFactory extends AbstractTaskFactory {

	private final CyNetwork cyNetwork;
	private final CyNetworkView cyNetworkView;
	// private List<Set<String>> objectsToColor;
	// private List<Pair<Color, Color>> colors;
	private final String visualStyleName;
	private final IClusterObjectMapping clustering;
	private final Set<Pair<ICluster, ICluster>> clusterPairsToColor;

	public ColorClusterPairsTaskFactory(final CyNetwork cyNetwork, final CyNetworkView cyNetworkView,
			final IClusterObjectMapping clustering, final Set<Pair<ICluster, ICluster>> clusterPairsToColor,
			final String visualStyleName) {

		this.cyNetwork = cyNetwork;
		this.cyNetworkView = cyNetworkView;
		// this.objectsToColor = objectsToColor;
		// this.colors = colors;
		this.clustering = clustering;
		this.clusterPairsToColor = clusterPairsToColor;
		this.visualStyleName = visualStyleName;
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		// Resetting the coloring of all nodes in the network.
		taskIterator.append(new ResetNetworkTask(this.cyNetwork, this.cyNetworkView));
		// for (int i = 0; i < colors.size(); i++) {
		// taskIterator.append(new ColorClusterPairsTask(cyNetwork,
		// cyNetworkView, objectsToColor.get(i),
		// colors.get(i), visualStyleName));
		// }
		taskIterator.append(new ColorClusterPairsTask(this.cyNetwork, this.cyNetworkView, this.clustering,
				this.clusterPairsToColor, this.visualStyleName));
		return taskIterator;
	}
}
