package dk.sdu.imada.ticone.tasks;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.util.TiconeProgressEvent;
import dk.sdu.imada.ticone.util.TiconeTaskProgressListener;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 25, 2019
 *
 */
public class CreateNewClusterWithObjectsTask extends AbstractTask implements TiconeTaskProgressListener {

	private final TiconeClusteringResultPanel resultPanel;

	protected TaskMonitor tm;

	protected final int iterationNumber;

	protected final ITimeSeriesObjects objects;

	public CreateNewClusterWithObjectsTask(final TiconeClusteringResultPanel resultPanel,
			final ITimeSeriesObjects objects) throws InterruptedException {
		super();
		;
		this.resultPanel = resultPanel;
		this.objects = objects;
		this.iterationNumber = resultPanel.getClusteringResult().getClusterHistory().getIterationNumber();
	}

	@Override
	public void progressUpdated(final TiconeProgressEvent event) {
		if (event.getTitle() != null)
			this.tm.setTitle(event.getTitle());
		if (event.getStatusMessage() != null)
			this.tm.setStatusMessage(event.getStatusMessage());
		if (event.getPercent() != null)
			this.tm.setProgress(event.getPercent());
	}

	@Override
	public void cancel() {
		super.cancel();
	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		try {
			this.tm = tm;

			tm.setTitle("Create new cluster with objects");
			tm.setStatusMessage("Creating cluster ...");

			final TiconeCytoscapeClusteringResult result = this.resultPanel.getResult();
			result.getClusteringProcess().createClusterWithObjects(objects);
		} catch (InterruptedException e) {
		}
	}

	public static double round(final double value, final int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		if (Double.isNaN(value) || Double.isInfinite(value))
			return value;

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
}
