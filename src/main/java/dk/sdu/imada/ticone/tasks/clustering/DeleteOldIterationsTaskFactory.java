package dk.sdu.imada.ticone.tasks.clustering;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;

/**
 * Created by christian on 5/26/15.
 */
public class DeleteOldIterationsTaskFactory extends AbstractTaskFactory {

	protected TiconeCytoscapeClusteringResult utils;

	protected int firstIterationToKeep;

	public DeleteOldIterationsTaskFactory(final TiconeCytoscapeClusteringResult utils, final int firstIterationToKeep) {
		super();
		this.utils = utils;
		this.firstIterationToKeep = firstIterationToKeep;
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(new DeleteOldIterationsTask(this.utils, this.firstIterationToKeep));
		return taskIterator;
	}
}
