package dk.sdu.imada.ticone.tasks;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 25, 2019
 *
 */
public class CreateNewClusterWithObjectsTaskFactory extends AbstractTaskFactory {

	private final CreateNewClusterWithObjectsTask task;

	public CreateNewClusterWithObjectsTaskFactory(final TiconeClusteringResultPanel resultPanel,
			final ITimeSeriesObjects objects) throws InterruptedException {
		this.task = new CreateNewClusterWithObjectsTask(resultPanel, objects);
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.task);
		return taskIterator;
	}
}
