package dk.sdu.imada.ticone.tasks.filter;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.clustering.filter.IFilter;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since May 9, 2017
 *
 */
public class FilterClusterTaskFactory extends AbstractTaskFactory {

	FilterClusterTask task;

	public FilterClusterTaskFactory(final TiconeCytoscapeClusteringResult utils, final IArithmeticFeature<?> feature,
			final IFilter.FILTER_OPERATOR operator, final double filterInput) {
		super();
		this.task = new FilterClusterTask(utils, feature, operator, filterInput);
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.task);
		return taskIterator;
	}
}
