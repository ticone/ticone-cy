package dk.sdu.imada.ticone.tasks.delete;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;

/**
 * Created by christian on 03-03-16.
 */
public class DeleteObjectsFromClusteringTaskFactory extends AbstractTaskFactory {

	private final DeleteObjectsFromClusteringTask deleteObjectsFromClusteringTask;

	public DeleteObjectsFromClusteringTaskFactory(final ITimeSeriesObjects objectsToDelete,
			final TiconeClusteringResultPanel resultPanel) {
		this.deleteObjectsFromClusteringTask = new DeleteObjectsFromClusteringTask(objectsToDelete, resultPanel);

	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.deleteObjectsFromClusteringTask);
		return taskIterator;
	}
}
