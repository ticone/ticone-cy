package dk.sdu.imada.ticone.tasks;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.layout.EdgeWeighter;
import org.cytoscape.view.layout.WeightTypes;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskManager;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.TunableSetter;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterSet;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair.ClusterPairsFactory;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectSet;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.ObjectClusterPair.ObjectClusterPairsFactory;
import dk.sdu.imada.ticone.data.ObjectPair.ObjectPairsFactory;
import dk.sdu.imada.ticone.data.TimeSeriesObjectSet;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.util.CyNetworkUtil;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeProgressEvent;
import dk.sdu.imada.ticone.util.TiconeTaskProgressListener;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 22, 2019
 *
 */
public class ObjectSimilarityNetworkTask extends AbstractTask implements TiconeTaskProgressListener {

	private final TiconeClusteringResultPanel resultPanel;

	protected TaskMonitor tm;

	protected int iterationNumber;

	protected TiconeCytoscapeNetwork objectSimilarityNetwork;

	protected ITimeSeriesObjects objects;

	protected boolean onlyObjectClusterEdges;

	public ObjectSimilarityNetworkTask(final TiconeClusteringResultPanel resultPanel) throws InterruptedException {
		this(resultPanel, null);
	}

	public ObjectSimilarityNetworkTask(final TiconeClusteringResultPanel resultPanel, final ITimeSeriesObjects objects)
			throws InterruptedException {
		super();
		this.resultPanel = resultPanel;
		this.objects = objects;
		this.iterationNumber = resultPanel.getClusteringResult().getClusterHistory().getIterationNumber();
	}

	/**
	 * @param onlyObjectClusterEdges the onlyObjectClusterEdges to set
	 */
	public void setOnlyObjectClusterEdges(boolean onlyObjectClusterEdges) {
		this.onlyObjectClusterEdges = onlyObjectClusterEdges;
	}

	/**
	 * @return the onlyObjectClusterEdges
	 */
	public boolean isOnlyObjectClusterEdges() {
		return this.onlyObjectClusterEdges;
	}

	@Override
	public void progressUpdated(final TiconeProgressEvent event) {
		if (event.getTitle() != null)
			this.tm.setTitle(event.getTitle());
		if (event.getStatusMessage() != null)
			this.tm.setStatusMessage(event.getStatusMessage());
		if (event.getPercent() != null)
			this.tm.setProgress(event.getPercent());
	}

	@Override
	public void cancel() {
		super.cancel();
	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		try {
			this.tm = tm;

			tm.setTitle("Create Object Similarity Network");
			tm.setStatusMessage("Initializing similarity function ...");

			TiconeCytoscapeClusteringResult result = this.resultPanel.getResult();
			ISimilarityFunction simFunc = result.getSimilarityFunction();
			simFunc.initialize();

			final ClusterObjectMapping clustering = result.getClusteringProcess().getLatestClustering();

			if (this.objects == null)
				this.objects = clustering.getAllObjects();

			if (this.objects.isEmpty())
				return;

			ITimeSeriesObject[] allObjectArray = this.objects.toArray();
			if (this.onlyObjectClusterEdges) {
				tm.setStatusMessage("Calculating object similarities ...");

				final Map<ICluster, ITimeSeriesObjectSet> objectPerClusters = new HashMap<>();
				this.objects.stream().forEach(o -> {
					if (clustering.getSimilarities(o).isEmpty())
						return;
					final ICluster cluster = clustering.getSimilarities(o).keySet().iterator().next();
					if (!objectPerClusters.containsKey(cluster))
						objectPerClusters.put(cluster, new TimeSeriesObjectSet(o));
					else
						objectPerClusters.get(cluster).add(o);
				});
				final Collection<ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair>> objectSimilaritiesIterable = new ArrayList<>();

				for (ITimeSeriesObjectSet selectedClusterObjects : objectPerClusters.values()) {
					ITimeSeriesObject[] objectArray = selectedClusterObjects.toArray();
					final ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> objectSimilarities = simFunc
							.calculateSimilarities(objectArray, objectArray, new ObjectPairsFactory(),
									ObjectType.OBJECT_PAIR);
					objectSimilaritiesIterable.add(objectSimilarities);
				}

				tm.setStatusMessage("Calculating object-cluster similarities ...");

				final ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair> similarities = result
						.getSimilarityFunction().calculateSimilarities(allObjectArray,
								result.getClusteringProcess().getLatestClustering().getClusters().toArray(),
								new ObjectClusterPairsFactory(), ObjectType.OBJECT_CLUSTER_PAIR);

				final IClusterSet clusters = result.getClusteringProcess().getLatestClustering().getClusters();

				ICluster[] clusterArray = clusters.toArray();
				final ISimilarityValuesAllPairs<ICluster, ICluster, IClusterPair> clusterSimilarities = (result
						.getSimilarityFunction()).calculateSimilarities(clusterArray, clusterArray,
								new ClusterPairsFactory(), ObjectType.CLUSTER_PAIR);

				tm.setStatusMessage("Creating network ...");

				this.objectSimilarityNetwork = CyNetworkUtil.createObjectClusterSimilarityNetwork(result, similarities,
						clusterSimilarities, objectSimilaritiesIterable, this.objects);

			} else {
				tm.setStatusMessage("Calculating object similarities ...");

				final ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> objectSimilarities = simFunc
						.calculateSimilarities(allObjectArray, allObjectArray, new ObjectPairsFactory(),
								ObjectType.OBJECT_PAIR);

				tm.setStatusMessage("Calculating object-cluster similarities ...");

				final IClusterSet clusters = result.getClusteringProcess().getLatestClustering().getClusters();

				final ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair> objectClusterSimilarities = result
						.getSimilarityFunction().calculateSimilarities(allObjectArray, clusters.toArray(),
								new ObjectClusterPairsFactory(), ObjectType.OBJECT_CLUSTER_PAIR);

				tm.setStatusMessage("Creating network ...");

				this.objectSimilarityNetwork = CyNetworkUtil.createObjectClusterSimilarityNetwork(result,
						objectClusterSimilarities, null, Collections.singletonList(objectSimilarities), this.objects);
			}
			tm.setStatusMessage("Layouting network ...");

			final CyLayoutAlgorithmManager layoutManager = ServiceHelper.getService(CyLayoutAlgorithmManager.class);
			final CyLayoutAlgorithm layout = layoutManager.getLayout("force-directed");

			Object context = layout.getDefaultLayoutContext();
			Map<String, Object> settings = new HashMap<>();
			settings.put("defaultSpringLength", 200.0);
			settings.put("singlePartition", true);
			EdgeWeighter edgeWeighter = new EdgeWeighter();
			edgeWeighter.type = WeightTypes.WEIGHT;
			settings.put("edgeWeighter", edgeWeighter);
			settings.put("defaultSpringCoefficient", 0.01);

			TunableSetter setter = ServiceHelper.getService(TunableSetter.class);
			setter.applyTunables(context, settings);

			final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);

			final Collection<CyNetworkView> views = ServiceHelper.getService(CyNetworkViewManager.class)
					.getNetworkViews(this.objectSimilarityNetwork.getCyNetwork());
			for (final CyNetworkView view : views) {
				taskManager.execute(
						layout.createTaskIterator(view, context, CyLayoutAlgorithm.ALL_NODE_VIEWS, "similarity"));
			}
		} catch (InterruptedException e) {
		}
	}

	public static double round(final double value, final int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		if (Double.isNaN(value) || Double.isInfinite(value))
			return value;

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
}
