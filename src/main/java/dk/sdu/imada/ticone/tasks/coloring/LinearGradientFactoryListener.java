package dk.sdu.imada.ticone.tasks.coloring;

import java.util.Map;

import org.cytoscape.view.presentation.customgraphics.CyCustomGraphics2Factory;

public class LinearGradientFactoryListener {
	private static final String FACTORY_ID = "org.cytoscape.LinearGradient";
	private CyCustomGraphics2Factory<?> factory;

	public void addCustomGraphicsFactory(final CyCustomGraphics2Factory<?> factory,
			final Map<Object, Object> serviceProps) {
		if (FACTORY_ID.equals(factory.getId())) {
			this.factory = factory;
		}
	}

	public void removeCustomGraphicsFactory(final CyCustomGraphics2Factory<?> factory,
			final Map<Object, Object> serviceProps) {
		this.factory = null;
	}

	public CyCustomGraphics2Factory<?> getFactory() {
		return this.factory;
	}
}