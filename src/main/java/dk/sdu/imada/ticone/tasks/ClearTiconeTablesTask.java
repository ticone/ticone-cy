/**
 * 
 */
package dk.sdu.imada.ticone.tasks;

import java.util.Set;
import java.util.stream.Collectors;

import org.cytoscape.model.CyTable;
import org.cytoscape.model.CyTableManager;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.util.ServiceHelper;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 2, 2019
 *
 */
public class ClearTiconeTablesTask extends AbstractTask {

	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setTitle("Clearing TiCoNE tables");
		final CyTableManager tableManager = ServiceHelper.getService(CyTableManager.class);
		Set<CyTable> allTables = tableManager.getAllTables(true);

		Set<CyTable> toDelete = allTables.stream().filter(t -> t.getTitle().startsWith("TiCoNE"))
				.collect(Collectors.toSet());
		for (CyTable t : toDelete)
			tableManager.deleteTable(t.getSUID());
		taskMonitor.setStatusMessage(String.format("%d tables deleted", toDelete.size()));
	}

}
