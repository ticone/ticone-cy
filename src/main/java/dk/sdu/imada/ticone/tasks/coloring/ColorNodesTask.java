package dk.sdu.imada.ticone.tasks.coloring;

import java.awt.Color;
import java.util.List;
import java.util.Map;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork.TiconeCytoscapeNetworkNode;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.util.ColorUtility;
import dk.sdu.imada.ticone.util.CyNetworkUtil;

/**
 * Created by christian on 3/24/15.
 */
public class ColorNodesTask extends AbstractTask {

	private final CyNetwork cyNetwork;
	private final CyNetworkView cyNetworkView;
	private final Map<String, ISimilarityValue> toColor;
	private final Color color;

	public ColorNodesTask(final CyNetwork cyNetwork, final CyNetworkView cyNetworkView,
			final Map<String, ISimilarityValue> toColor, final Color color) {
		super();
		;
		this.cyNetwork = cyNetwork;
		this.cyNetworkView = cyNetworkView;
		this.toColor = toColor;
		this.color = color;
	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		tm.setTitle("Coloring nodes..");
		tm.setProgress(0.1);

		final TiconeCytoscapeNetwork net = new TiconeCytoscapeNetwork(this.cyNetwork);

		final List<TiconeCytoscapeNetworkNode> nodes = CyNetworkUtil.getToBeColoredNodes(net, this.toColor);
		for (final TiconeCytoscapeNetworkNode node : nodes) {
			this.cyNetworkView.getNodeView(node.getCyNode()).setVisualProperty(BasicVisualLexicon.NODE_FILL_COLOR,
					this.color);
			final Color textColor = ColorUtility.calculateTextColor(this.color);
			this.cyNetworkView.getNodeView(node.getCyNode()).setVisualProperty(BasicVisualLexicon.NODE_LABEL_COLOR,
					textColor);
		}
		this.cyNetworkView.updateView();
		tm.setProgress(1.0);
		tm.setStatusMessage(" ... ok ");

	}
}
