package dk.sdu.imada.ticone.tasks.preprocessing;

import java.util.Date;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskManager;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.TaskMonitor.Level;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.ObjectPairSimilarityFeature;
import dk.sdu.imada.ticone.feature.scale.IScaler;
import dk.sdu.imada.ticone.feature.scale.MinMaxScalerBuilder;
import dk.sdu.imada.ticone.io.ILoadDataMethod;
import dk.sdu.imada.ticone.io.LoadDataFromFile;
import dk.sdu.imada.ticone.io.LoadDataFromTable;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.preprocessing.IPreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.PreprocessingException;
import dk.sdu.imada.ticone.similarity.ICompositeSimilarityFunction;
import dk.sdu.imada.ticone.similarity.INetworkBasedSimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimpleSimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.statistics.AssignObjectsToMostSimilarRandomCluster;
import dk.sdu.imada.ticone.tasks.clustering.ClusterObjectsTaskFactory;
import dk.sdu.imada.ticone.util.AggregationException;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.ExtractData;
import dk.sdu.imada.ticone.util.FactoryException;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.IIdMapMethod;
import dk.sdu.imada.ticone.util.IProgress;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;
import dk.sdu.imada.ticone.util.Utility;
import dk.sdu.imada.ticone.variance.IObjectSetVariance;
import dk.sdu.imada.ticone.variance.StandardVariance;

/**
 * Created by christian on 11-03-16.
 */
public class PreprocessingTask extends AbstractTask {

	private final int numberOfInitialPatterns;
	private TaskMonitor taskMonitor;

	protected IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> clusteringProcess;

	protected IPreprocessingSummary<ClusterObjectMapping> summary;
	private IProgress progress;

	public PreprocessingTask(final IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> process,
			final int numberOfInitialPatterns) {
		super();
		this.clusteringProcess = process;
		this.summary = process.getPreprocessingSummary();
		this.numberOfInitialPatterns = numberOfInitialPatterns;
	}

	@Override
	public void run(final TaskMonitor taskMonitor) throws Exception {
		try {
			this.taskMonitor = taskMonitor;
			taskMonitor.setTitle("Preprocessing Data");
			taskMonitor.setProgress(0.05);
			taskMonitor.setStatusMessage("Validating and applying parameters");
			this.progress = Utility.getProgress();
			this.checkOptions();

			this.clusteringProcess.getClusteringResult().setDate(new Date());

			this.preprocessTimeSeriesData();

			try {
				this.mapIds();
			} catch (final Exception e) {
				e.printStackTrace();
				return;
			}
			if (!Utility.getProgress().getStatus())
				throw new InterruptedException();

			final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
			final ClusterObjectsTaskFactory clusterObjectsTaskFactory = new ClusterObjectsTaskFactory(
					clusteringProcess);
			final TaskIterator taskIterator = clusterObjectsTaskFactory.createTaskIterator();
			taskManager.execute(taskIterator);
		} catch (final InterruptedException e) {
		}
	}

	public void mapIds() {
		final IIdMapMethod idMapMethod = this.clusteringProcess.getIdMapMethod();
		if (idMapMethod.isActive()) {
			this.taskMonitor.setStatusMessage("Mapping ids");
			final ITimeSeriesObjectList tsds = this.clusteringProcess.getTimeSeriesPreprocessor().getObjects();
			int i = 0;
			for (final ITimeSeriesObject tsd : tsds) {
				tsd.setAlternativeName(idMapMethod.getAlternativeId(tsd.getName()));
				this.taskMonitor.setProgress((double) i / tsds.size());
				i++;
			}
		}
	}

	public void preprocessTimeSeriesData() throws InterruptedException, SimilarityCalculationException,
			CreateInstanceFactoryException, AggregationException, TiconeUnloadingException, PreprocessingException,
			FactoryException, IncompatibleSimilarityFunctionException, IncompatibleObjectTypeException {
		this.clusteringProcess.getTimeSeriesPreprocessor().getObjects().size();
		this.taskMonitor.setProgress(0.2);

		this.taskMonitor.setStatusMessage("Filtering objects");
		if (this.summary.isRemoveObjectsNotInNetwork()) {
			final ITiconeNetwork network = this.summary.getNetwork();
			if (network != null) {
				this.clusteringProcess.calculateNumberOfMappedObjects(network);

				this.taskMonitor.setStatusMessage("Removing objects not in network");
				this.summary.addRemovedObjectsNotInNetwork(this.clusteringProcess.removeDataNotInNetwork(network));
			}
		}

		this.taskMonitor.setStatusMessage("Initializing similarity function");
		this.initializeSimilarityFunction();

		this.filterObjects();

		new ClusterObjectsTaskFactory(this.clusteringProcess);
	}

	/**
	 * @throws AggregationException
	 * @throws CreateInstanceFactoryException
	 * @throws InterruptedException
	 * @throws FactoryException
	 * @throws IncompatibleSimilarityFunctionException
	 * 
	 */
	private void initializeSimilarityFunction() throws CreateInstanceFactoryException, AggregationException,
			InterruptedException, FactoryException, IncompatibleSimilarityFunctionException {
		final ITimeSeriesObjectList objects = this.clusteringProcess.getTimeSeriesPreprocessor().getObjects();
		final ISimilarityFunction simFunc = this.clusteringProcess.getSimilarityFunction();
		simFunc.initialize();
		// initialize scalers of similarity functions
		if (simFunc instanceof ICompositeSimilarityFunction) {
			final ISimpleSimilarityFunction[] childSimFuncs = ((ICompositeSimilarityFunction) simFunc)
					.getSimilarityFunctions();

			ITiconeNetwork network = null;
			for (final ISimpleSimilarityFunction childSF : childSimFuncs) {
				if (childSF instanceof INetworkBasedSimilarityFunction) {
					network = ((INetworkBasedSimilarityFunction) childSF).getNetwork();
				}
			}
			int i = 0;
			for (final ISimpleSimilarityFunction childSF : childSimFuncs) {
				this.taskMonitor.setStatusMessage(String.format("Initializing %s", childSF));
				// use the same clustering for all scaler initializations of this similarity
				// function
				final ClusterObjectMapping randomClustering = new AssignObjectsToMostSimilarRandomCluster(
						this.clusteringProcess.getSeed(), this.clusteringProcess.getPrototypeBuilder(),
						this.numberOfInitialPatterns, simFunc, network).generateFrom(objects);

				final IScaler quantileScaler = new MinMaxScalerBuilder<>(new ObjectPairSimilarityFeature(childSF),
						randomClustering, 1000, this.clusteringProcess.getSeed()).setMapNanToZero(true).build();
				((ICompositeSimilarityFunction) simFunc).setScalerForAnyPair(i, quantileScaler);

				if (childSF instanceof INetworkBasedSimilarityFunction) {
					this.clusteringProcess.getTimeSeriesPreprocessor().getObjects()
							.mapToNetwork(((INetworkBasedSimilarityFunction) childSF).getNetwork());
				}
				i++;
			}
		}
	}

	public void checkOptions() throws InterruptedException, TiconeUnloadingException {
		final ILoadDataMethod loadDataFrom = GUIUtility.getLoadDataMethod();
		if (loadDataFrom instanceof LoadDataFromFile && !(GUIUtility.getTimeSeriesTextString().length() > 0)) {
			this.taskMonitor.showMessage(Level.ERROR, "You have not selected a time series data file.");
			throw new InterruptedException();
		}
		if (loadDataFrom instanceof LoadDataFromTable && (GUIUtility.getNumberOfTables() < 1
				|| GUIUtility.getSelectedTableName() == null || GUIUtility.getSelectedTableName().length() < 1)) {
			this.taskMonitor.showMessage(Level.ERROR, "You must load a table before performing a clustering.");
			throw new InterruptedException();
		}

		if (this.numberOfInitialPatterns < 1) {
			this.taskMonitor.showMessage(Level.ERROR, "Number of initial clusters must be a positive integer number");
			throw new InterruptedException();
		}

		// TODO: Preprocessing. Dissagreeing sets and low variance.
		// setPermutationMethod();

//		GUIUtility.updateMappedObjectLabels();
//		GUIUtility.updateDiscretizationSlider(); // This also updates the
//													// discretization method

		final ISimilarityFunction similarity = this.clusteringProcess.getSimilarityFunction();

		double pairwiseSimilarityThreshold;
		try {
			pairwiseSimilarityThreshold = GUIUtility.getPairwiseSimilarityThreshold();
			if (pairwiseSimilarityThreshold < 0 || pairwiseSimilarityThreshold > 1) {
				throw new NumberFormatException();
			}
		} catch (final NumberFormatException nfe) {
			this.taskMonitor.showMessage(Level.ERROR, "Pairwise similarity threshold must be a value between 0 and 1");
			throw new InterruptedException();
		}
	}

	private void filterObjects() throws InterruptedException, SimilarityCalculationException, TiconeUnloadingException,
			IncompatibleObjectTypeException {
		final ITimeSeriesObjects objects = this.clusteringProcess.getTimeSeriesPreprocessor().getObjects();
		ITimeSeriesObjects leastConserved = new TimeSeriesObjectList();
		ITimeSeriesObjects lowVariance = new TimeSeriesObjectList();
		if (this.clusteringProcess.getPreprocessingSummary().isRemoveObjectsLeastConserved()) {
			final ISimilarityValue threshold = this.clusteringProcess.getPreprocessingSummary()
					.getRemoveObjectsLeastConservedThreshold();
			final ISimilarityFunction similarity = this.clusteringProcess.getSimilarityFunction();
			if (GUIUtility.getRemoveLeastConservedOption().equals("Threshold")) {
				leastConserved = ExtractData.findLeastConservedObjectsWithThreshold(objects, threshold,
						(ISimilarityFunction) similarity);
			} else if (GUIUtility.getRemoveLeastConservedOption().equals("Percent")) {
				leastConserved = ExtractData.findLeastConservedObjects(objects, (int) threshold.get(),
						(ISimilarityFunction) similarity);
			}
			objects.removeAll(leastConserved);
			this.summary.addRemovedObjectsLeastConserved(leastConserved);
		}
		this.taskMonitor.setProgress(0.5);
		if (GUIUtility.isRemoveVarianceButtonSelected()) {
			final double varThreshold = this.clusteringProcess.getPreprocessingSummary()
					.getRemoveObjectsLowVarianceThreshold();
			final IObjectSetVariance variance = new StandardVariance();
			if (GUIUtility.getRemoveLowVariance().equals("Threshold")) {
				lowVariance = ExtractData.findObjectsWithVarianceThreshold(objects, varThreshold, variance);
			} else if (GUIUtility.getRemoveLowVariance().equals("Percent")) {
				lowVariance = ExtractData.findLeastVaryingObjects(objects, (int) varThreshold, variance);
			}
			objects.removeAll(lowVariance);
			this.summary.addRemovedObjectsLowVariance(lowVariance);
		}
		this.taskMonitor.setProgress(0.8);
	}

	@Override
	public void cancel() {
		this.progress.stop();
	}
}
