package dk.sdu.imada.ticone.tasks.merge;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 13, 2019
 *
 */
public class MergeClustersAboveSimilarityThresholdTaskFactory extends AbstractTaskFactory {

	private final ISimilarityValue threshold;
	protected TiconeClusteringResultPanel resultPanel;

	public MergeClustersAboveSimilarityThresholdTaskFactory(final ISimilarityValue threshold,
			final TiconeClusteringResultPanel resultPanel) {
		super();
		this.threshold = threshold;
		this.resultPanel = resultPanel;
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(new MergeClustersAboveSimilarityThresholdTask(this.threshold, this.resultPanel));
		return taskIterator;
	}
}
