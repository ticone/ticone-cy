package dk.sdu.imada.ticone.tasks.kpm;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.gui.IKPMResultPanel;
import dk.sdu.imada.ticone.gui.panels.kpm.KPMResultFormPanel;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.network.kpm.main.KPM;

/**
 * Created by Christian Wiwie on 20-04-16.
 */
public class KPMBatchTask extends AbstractTask {

	private final CyNetwork network;
	private final List<CyTable> kpmTables;
	private final int kValue;
	private final int resultsWanted;
	private final boolean unmappedNodesOnPositiveList;
	private final int numberOfProcessors;
	private final boolean benFree;
	protected IKPMResultPanel resultPanel;
	private final List<Collection<IClusters>> selectedClusters;
	private final String kpmModel;

	public KPMBatchTask(final CyNetwork network, final List<Collection<IClusters>> selectedClusters,
			final List<CyTable> kpmTables, final int kValue, final int resultsWanted,
			final boolean unmappedNodesOnPositiveList, final int numberOfProcessors, final boolean benFree,
			final IKPMResultPanel resultPanel, final String kpmModel) {
		super();
		this.network = network;
		this.kpmTables = kpmTables;
		this.kValue = kValue;
		this.resultsWanted = resultsWanted;
		this.unmappedNodesOnPositiveList = unmappedNodesOnPositiveList;
		this.numberOfProcessors = numberOfProcessors;
		this.benFree = benFree;
		this.resultPanel = resultPanel;
		this.selectedClusters = selectedClusters;
		this.kpmModel = kpmModel;
	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		tm.setTitle("Batch Network Enrichment");
		tm.setStatusMessage("Finding enriched areas in network for each cluster separately.");
		final IClusteringProcess<ClusterObjectMapping, ?> clusteringProcess = this.resultPanel.getClusteringResult()
				.getClusteringProcess();

		Set<String> unmappedNodes = clusteringProcess.getUnmappedNodes();
		final TiconeCytoscapeNetwork net = new TiconeCytoscapeNetwork(this.network);

		if (unmappedNodes == null) {
			clusteringProcess.calculateNumberOfMappedObjects(net);
			unmappedNodes = clusteringProcess.getUnmappedNodes();
			if (unmappedNodes == null) {
				unmappedNodes = new HashSet<>();
			}
		}
		int i = 0;
		for (final CyTable kpmTable : this.kpmTables) {
			KPM.run(this.network, kpmTable, this.kValue, this.resultsWanted, this.unmappedNodesOnPositiveList,
					unmappedNodes, this.numberOfProcessors, this.benFree, this.kpmModel);
			new KPMResultFormPanel(this.resultPanel, this.network, this.selectedClusters.get(i), KPM.getResults(),
					this.kpmModel);
			i++;
			tm.setProgress((double) i / this.kpmTables.size());
			if (this.cancelled) {
				return;
			}
		}
	}
}
