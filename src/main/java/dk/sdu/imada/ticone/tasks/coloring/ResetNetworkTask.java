package dk.sdu.imada.ticone.tasks.coloring;

import java.awt.Color;
import java.util.List;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.network.NetworkUtil;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork.TiconeCytoscapeNetworkNode;

/**
 * Created by christian on 22-10-15.
 */
public class ResetNetworkTask extends AbstractTask {

	private final CyNetwork cyNetwork;
	private final CyNetworkView cyNetworkView;
	private final Color color = Color.black;

	public ResetNetworkTask(final CyNetwork cyNetwork, final CyNetworkView cyNetworkView) {
		super();
		;
		this.cyNetwork = cyNetwork;
		this.cyNetworkView = cyNetworkView;

	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		tm.setTitle("Coloring nodes..");
		tm.setProgress(0.1);

		final TiconeCytoscapeNetwork net = new TiconeCytoscapeNetwork(this.cyNetwork);

		final List<TiconeCytoscapeNetworkNode> nodes = NetworkUtil.getAllNodes(net);
		for (final TiconeCytoscapeNetworkNode node : nodes) {
			this.cyNetworkView.getNodeView(node.getCyNode()).setVisualProperty(BasicVisualLexicon.NODE_FILL_COLOR,
					this.color);
		}
		tm.setProgress(1.0);
		tm.setStatusMessage(" ... ok ");

	}
}
