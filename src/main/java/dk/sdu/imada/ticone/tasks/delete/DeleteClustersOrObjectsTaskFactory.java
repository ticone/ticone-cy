package dk.sdu.imada.ticone.tasks.delete;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 10, 2019
 *
 */
public class DeleteClustersOrObjectsTaskFactory extends AbstractTaskFactory {

	private final DeleteClustersOrObjectsTask deleteTask;

	public DeleteClustersOrObjectsTaskFactory(final IClusterObjectMapping.DELETE_METHOD deleteMethod,
			final IClusters clusters, final TiconeClusteringResultPanel resultPanel, final boolean showPreview) {
		this.deleteTask = new DeleteClustersOrObjectsTask(deleteMethod, clusters, resultPanel, showPreview);

	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.deleteTask);
		return taskIterator;
	}
}
