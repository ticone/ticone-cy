package dk.sdu.imada.ticone.tasks.coloring;

import java.awt.Color;
import java.util.List;
import java.util.Map;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.similarity.ISimilarityValue;

/**
 * Created by christian on 3/24/15.
 */
public class ColorNodesTaskFactory extends AbstractTaskFactory {

	private final CyNetwork cyNetwork;
	private final CyNetworkView cyNetworkView;
	private final List<Map<String, ISimilarityValue>> toColor;
	private final List<Color> colors;

	public ColorNodesTaskFactory(final CyNetwork cyNetwork, final CyNetworkView cyNetworkView,
			final List<Map<String, ISimilarityValue>> toColor, final List<Color> colors) {

		this.cyNetwork = cyNetwork;
		this.cyNetworkView = cyNetworkView;
		this.toColor = toColor;
		this.colors = colors;
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		// Resetting the coloring of all nodes in the network.
		taskIterator.append(new ResetNetworkTask(this.cyNetwork, this.cyNetworkView));
		for (int i = 0; i < this.colors.size(); i++) {
			taskIterator.append(
					new ColorNodesTask(this.cyNetwork, this.cyNetworkView, this.toColor.get(i), this.colors.get(i)));
		}
		return taskIterator;
	}
}
