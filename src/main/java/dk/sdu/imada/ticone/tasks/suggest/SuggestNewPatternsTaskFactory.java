package dk.sdu.imada.ticone.tasks.suggest;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;

/**
 * Created by christian on 8/25/15.
 */
public class SuggestNewPatternsTaskFactory extends AbstractTaskFactory {

	private final SuggestNewPatternsTask suggestNewPatternsTask;

	public SuggestNewPatternsTaskFactory(final int numberOfPatterns, final int percentLeastFittingObjects,
			final TiconeClusteringResultPanel resultPanel) {
		this.suggestNewPatternsTask = new SuggestNewPatternsTask(numberOfPatterns, percentLeastFittingObjects,
				resultPanel);
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.suggestNewPatternsTask);

		return taskIterator;
	}
}
