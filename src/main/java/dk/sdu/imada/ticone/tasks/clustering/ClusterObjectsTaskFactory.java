package dk.sdu.imada.ticone.tasks.clustering;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;

/**
 * Created by christian on 5/26/15.
 */
public class ClusterObjectsTaskFactory extends AbstractTaskFactory {

	ClusterObjectsTask clusterObjectsTask;

	public ClusterObjectsTaskFactory(
			final IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> process) {
		super();

		this.clusterObjectsTask = new ClusterObjectsTask(process);
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.clusterObjectsTask);
		return taskIterator;
	}
}
