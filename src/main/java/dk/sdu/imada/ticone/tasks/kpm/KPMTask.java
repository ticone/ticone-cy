package dk.sdu.imada.ticone.tasks.kpm;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.gui.IKPMResultPanel;
import dk.sdu.imada.ticone.gui.panels.kpm.KPMResultFormPanel;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.network.kpm.main.KPM;

/**
 * Created by christian on 25-01-16.
 */
public class KPMTask extends AbstractTask {

	private final CyNetwork network;
	private final CyTable kpmTable;
	private final int kValue;
	private final int resultsWanted;
	private final boolean unmappedNodesOnPositiveList;
	private final int numberOfProcessors;
	private final boolean benFree;
	private final IKPMResultPanel resultPanel;
	private final Collection<IClusters> selectedClusters;
	private final String kpmModel;

	public KPMTask(final CyNetwork network, final Collection<IClusters> selectedClusters, final CyTable kpmTable,
			final int kValue, final int resultsWanted, final boolean unmappedNodesOnPositiveList,
			final int numberOfProcessors, final boolean benFree, final IKPMResultPanel resultPanel,
			final String kpmModel) {
		super();
		this.network = network;
		this.kpmTable = kpmTable;
		this.kValue = kValue;
		this.resultsWanted = resultsWanted;
		this.unmappedNodesOnPositiveList = unmappedNodesOnPositiveList;
		this.numberOfProcessors = numberOfProcessors;
		this.benFree = benFree;
		this.resultPanel = resultPanel;
		this.selectedClusters = selectedClusters;
		this.kpmModel = kpmModel;
	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		tm.setTitle("Network enrichment");
		tm.setStatusMessage("Finding enriched areas in network.");

		final IClusteringProcess<ClusterObjectMapping, ?> clusteringProcess = this.resultPanel.getClusteringResult()
				.getClusteringProcess();

		Set<String> unmappedNodes = clusteringProcess.getUnmappedNodes();

		if (unmappedNodes == null) {
			final TiconeCytoscapeNetwork net = new TiconeCytoscapeNetwork(this.network);

			clusteringProcess.calculateNumberOfMappedObjects(net);
			unmappedNodes = clusteringProcess.getUnmappedNodes();
			if (unmappedNodes == null) {
				unmappedNodes = new HashSet<>();
			}
		}
		KPM.run(this.network, this.kpmTable, this.kValue, this.resultsWanted, this.unmappedNodesOnPositiveList,
				unmappedNodes, this.numberOfProcessors, this.benFree, this.kpmModel);
		new KPMResultFormPanel(this.resultPanel, this.network, this.selectedClusters, KPM.getResults(), this.kpmModel);
	}
}
