package dk.sdu.imada.ticone.tasks;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 22, 2019
 *
 */
public class ObjectSimilarityNetworkTaskFactory extends AbstractTaskFactory {

	private final ObjectSimilarityNetworkTask task;

	public ObjectSimilarityNetworkTaskFactory(final TiconeClusteringResultPanel resultPanel)
			throws InterruptedException {
		this.task = new ObjectSimilarityNetworkTask(resultPanel);
	}

	public ObjectSimilarityNetworkTaskFactory(final TiconeClusteringResultPanel resultPanel,
			final ITimeSeriesObjects objects) throws InterruptedException {
		this.task = new ObjectSimilarityNetworkTask(resultPanel, objects);
	}

	/**
	 * @param onlyObjectClusterEdges the onlyObjectClusterEdges to set
	 */
	public ObjectSimilarityNetworkTaskFactory setOnlyObjectClusterEdges(boolean onlyObjectClusterEdges) {
		this.task.onlyObjectClusterEdges = onlyObjectClusterEdges;
		return this;
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.task);
		return taskIterator;
	}
}
