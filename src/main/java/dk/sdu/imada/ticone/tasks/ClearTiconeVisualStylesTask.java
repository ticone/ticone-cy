/**
 * 
 */
package dk.sdu.imada.ticone.tasks;

import java.util.Set;
import java.util.stream.Collectors;

import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.util.ServiceHelper;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 2, 2019
 *
 */
public class ClearTiconeVisualStylesTask extends AbstractTask {

	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setTitle("Clearing TiCoNE visual styles");
		final VisualMappingManager manager = ServiceHelper.getService(VisualMappingManager.class);
		Set<VisualStyle> allStyles = manager.getAllVisualStyles();

		Set<VisualStyle> toDelete = allStyles.stream().filter(t -> t.getTitle().startsWith("TiCoNE"))
				.collect(Collectors.toSet());
		for (VisualStyle vs : toDelete)
			manager.removeVisualStyle(vs);
		taskMonitor.setStatusMessage(String.format("%d visual styles deleted", toDelete.size()));
	}

}
