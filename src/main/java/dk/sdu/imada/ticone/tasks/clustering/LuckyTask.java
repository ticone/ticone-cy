package dk.sdu.imada.ticone.tasks.clustering;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.LocalSearchClustering;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.Progress;
import dk.sdu.imada.ticone.util.Utility;

/**
 * Created by christian on 26-01-16.
 */
public class LuckyTask extends AbstractTask {

	protected TiconeClusteringResultPanel resultPanel;

	public LuckyTask(final TiconeClusteringResultPanel resultPanel) {
		super();
		;
		this.resultPanel = resultPanel;
	}

	@Override
	public void run(final TaskMonitor taskMonitor) throws Exception {
		final IClusteringProcess timeSeriesClusteringWithOverrepresentedPatterns = this.resultPanel
				.getClusteringResult().getClusteringProcess();
		final Progress progress = (Progress) timeSeriesClusteringWithOverrepresentedPatterns.getProgress();
		final ClusterObjectsObserver clusterObjectsObserver = new ClusterObjectsObserver(progress, taskMonitor);
		progress.addObserver(clusterObjectsObserver);

		taskMonitor.setTitle("Clustering Iterations until Convergence");
		taskMonitor.setStatusMessage(
				"Performing clustering iterations. Cluster charts and tables will be refreshed after each iteration. This process stops if a stable clustering solution has been found, or 100 iterations have been performed.");
		try {
			this.resultPanel.getClusteringResult().getSimilarityFunction().initialize();
			new LocalSearchClustering(100)
					.computeIterationsUntilConvergence(timeSeriesClusteringWithOverrepresentedPatterns);
		} catch (InterruptedException e) {
		} finally {
			Utility.getProgress().start();
			this.resultPanel.getClusteringResult().setupPatternStatusMapping();
			GUIUtility.updateGraphPanel(this.resultPanel);
			taskMonitor.setStatusMessage("Done!");
		}
	}

	@Override
	public void cancel() {
		Utility.getProgress().stop();
		super.cancel();
	}
}
