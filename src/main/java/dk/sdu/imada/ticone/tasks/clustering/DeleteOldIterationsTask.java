package dk.sdu.imada.ticone.tasks.clustering;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since May 11, 2017
 *
 */
public class DeleteOldIterationsTask extends AbstractTask {

	protected TiconeCytoscapeClusteringResult utils;

	protected int firstIterationToKeep;

	public DeleteOldIterationsTask(final TiconeCytoscapeClusteringResult utils, final int firstIterationToKeep) {
		super();
		this.utils = utils;
		this.firstIterationToKeep = firstIterationToKeep;
	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		this.utils.getClusteringProcess().deleteOldIterations(this.firstIterationToKeep);
	}
}
