package dk.sdu.imada.ticone.tasks;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 8, 2019
 *
 */
public class ClusterSimilarityNetworkTaskFactory extends AbstractTaskFactory {

	private final ClusterSimilarityNetworkTask task;

	public ClusterSimilarityNetworkTaskFactory(final TiconeClusteringResultPanel resultPanel)
			throws InterruptedException {
		this.task = new ClusterSimilarityNetworkTask(resultPanel);
	}

	public ClusterSimilarityNetworkTaskFactory(final TiconeClusteringResultPanel resultPanel, final IClusters clusters)
			throws InterruptedException {
		this.task = new ClusterSimilarityNetworkTask(resultPanel, clusters);
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.task);
		return taskIterator;
	}
}
