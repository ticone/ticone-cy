package dk.sdu.imada.ticone.tasks.filter;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.clustering.filter.IFilter;
import dk.sdu.imada.ticone.feature.FeaturePvalue;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.gui.panels.MyDialogPanel;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since May 9, 2017
 *
 */
public class FilterClusterTask extends AbstractTask {

	protected TiconeCytoscapeClusteringResult utils;
	protected IArithmeticFeature<?> feature;
	protected IFilter.FILTER_OPERATOR operator;
	protected double filterInput;

	public FilterClusterTask(final TiconeCytoscapeClusteringResult utils, final IArithmeticFeature<?> feature,
			final IFilter.FILTER_OPERATOR operator, final double filterInput) {
		super();
		this.utils = utils;
		this.feature = feature;
		this.operator = operator;
		this.filterInput = filterInput;
	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		tm.setTitle("Applying Filter");
		tm.setStatusMessage("Evaluating filter condition for each cluster ...");

		final IFilter<ICluster> clusterFilter = this.utils.getClusterFilter();

		if (!this.utils.getFeatureStore().featureSet().contains(this.feature)) {
			if (this.feature instanceof FeaturePvalue)
				MyDialogPanel.showMessageDialog("Error applying filter",
						"Cluster p-values have not yet been calculated. Please do so first and then reapply the filter afterwards");
			else
				MyDialogPanel.showMessageDialog("Error applying filter",
						"Your filter settings could not be applied. Please try again");
			// this.filterClusters.doClick();
			return;
		}

		clusterFilter.setActive(true);
		clusterFilter.setFeature(this.feature);
		clusterFilter.setFilterOperator(this.operator);
		clusterFilter.setFilterInput(this.filterInput);

		if (this.feature instanceof FeaturePvalue) {
			((FeaturePvalue) this.feature).setFeatureValueProvider(this.utils.getPvalueCalculationResult());
			// TODO: do we need to initialize more attributes?
		}

		clusterFilter.fireFilterChanged();

		// GUIUtility.setGraphTabInFocus();
		tm.setStatusMessage("Done!");
	}
}
