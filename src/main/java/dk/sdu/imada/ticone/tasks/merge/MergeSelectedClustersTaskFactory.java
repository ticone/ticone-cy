package dk.sdu.imada.ticone.tasks.merge;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;

/**
 * Created by christian on 22-10-15.
 */
public class MergeSelectedClustersTaskFactory extends AbstractTaskFactory {

	private final int[] patternList;
	protected TiconeClusteringResultPanel resultPanel;

	public MergeSelectedClustersTaskFactory(final int[] patternList, final TiconeClusteringResultPanel resultPanel) {
		super();
		this.patternList = patternList;
		this.resultPanel = resultPanel;
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(new MergeSelectedClustersTask(this.patternList, this.resultPanel));
		return taskIterator;
	}
}
