package dk.sdu.imada.ticone.tasks.network;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.network.TiconeNetwork;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Apr 13, 2019
 *
 */
public class CalculateShortestPathsTaskFactory extends AbstractTaskFactory {

	private final CalculateShortestPathsTask task;

	public CalculateShortestPathsTaskFactory(final TiconeNetwork<?, ?> network, final boolean isDirected)
			throws InterruptedException {
		this.task = new CalculateShortestPathsTask(network, isDirected);
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.task);
		return taskIterator;
	}
}
