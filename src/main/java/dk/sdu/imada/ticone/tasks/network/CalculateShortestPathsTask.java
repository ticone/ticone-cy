package dk.sdu.imada.ticone.tasks.network;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Observable;
import java.util.Observer;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.network.TiconeNetwork;
import dk.sdu.imada.ticone.network.TiconeNetwork.ConnectedComponent;
import dk.sdu.imada.ticone.util.Progress;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Apr 13, 2019
 *
 */
public class CalculateShortestPathsTask extends AbstractTask implements Observer {

	protected TaskMonitor tm;

	protected TiconeNetwork<?, ?> network;

	private final boolean isDirected;

	private Progress progress;

	public CalculateShortestPathsTask(final TiconeNetwork<?, ?> network, final boolean isDirected)
			throws InterruptedException {
		super();
		this.network = network;
		this.isDirected = isDirected;
	}

	@Override
	public void update(Observable o, Object arg) {
		if (progress.getTitle() != null)
			this.tm.setTitle(progress.getTitle());
		if (progress.getProgressMessage() != null)
			this.tm.setStatusMessage(progress.getProgressMessage());
		this.tm.setProgress(progress.getProgress());
	}

	@Override
	public void cancel() {
		super.cancel();
		if (this.progress != null)
			this.progress.stop();
	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		try {
			this.tm = tm;

			this.progress = new Progress();
			this.progress.addObserver(this);

			tm.setTitle("Calculating Shortest Paths for Network");
			if (network.getConnectedComponents() == null) {
				tm.setStatusMessage("Identifying connected components ...");
				network.calculateConnectedComponents(progress);
			}

			for (ConnectedComponent component : network.getConnectedComponents()) {
				tm.setStatusMessage(String.format("Processing '%s'", component.toString()));
				network.calculateShortestPaths(isDirected, progress);
			}
		} catch (InterruptedException e) {
		}
	}

	public static double round(final double value, final int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		if (Double.isNaN(value) || Double.isInfinite(value))
			return value;

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
}
