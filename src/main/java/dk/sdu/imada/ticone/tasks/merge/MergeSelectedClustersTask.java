package dk.sdu.imada.ticone.tasks.merge;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.util.GUIUtility;

/**
 * Created by christian on 22-10-15.
 */
public class MergeSelectedClustersTask extends AbstractTask {

	private final int[] clusterNumbers;

	protected TiconeClusteringResultPanel resultPanel;

	public MergeSelectedClustersTask(final int[] patternList, final TiconeClusteringResultPanel resultPanel) {
		super();
		this.clusterNumbers = patternList;
		this.resultPanel = resultPanel;
	}

	@Override
	public void run(final TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setStatusMessage("Merging " + this.clusterNumbers.length + " clusters.");
		taskMonitor.setProgress(0.1);
		this.resultPanel.getClusteringResult().getSimilarityFunction().initialize();
		final IClusteringProcess timeSeriesClusteringWithOverrepresentedPatterns = this.resultPanel
				.getClusteringResult().getClusteringProcess();
		timeSeriesClusteringWithOverrepresentedPatterns.mergeClusters(this.clusterNumbers);
		taskMonitor.setProgress(1.0);
		this.resultPanel.getClusteringResult().setupPatternStatusMapping();
		GUIUtility.updateGraphTable(this.resultPanel);
	}
}
