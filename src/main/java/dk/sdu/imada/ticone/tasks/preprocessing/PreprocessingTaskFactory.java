package dk.sdu.imada.ticone.tasks.preprocessing;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;

/**
 * Created by christian on 11-03-16.
 */
public class PreprocessingTaskFactory extends AbstractTaskFactory {

	protected IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> process;
	private final int numberOfInitialPatterns;

	public PreprocessingTaskFactory(
			final IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> process,
			final int numberOfInitialPatterns) {
		super();
		this.process = process;
		this.numberOfInitialPatterns = numberOfInitialPatterns;
	}

	@Override
	public TaskIterator createTaskIterator() {
		final PreprocessingTask preprocessingTask = new PreprocessingTask(this.process, this.numberOfInitialPatterns);
		final TaskIterator iterator = new TaskIterator();
		iterator.append(preprocessingTask);
		return iterator;
	}
}
