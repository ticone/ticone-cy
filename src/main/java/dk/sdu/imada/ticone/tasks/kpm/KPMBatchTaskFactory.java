package dk.sdu.imada.ticone.tasks.kpm;

import java.util.Collection;
import java.util.List;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.gui.IKPMResultPanel;

/**
 * Created by christian on 25-01-16.
 */
public class KPMBatchTaskFactory extends AbstractTaskFactory {

	private final KPMBatchTask kpmTask;

	public KPMBatchTaskFactory(final CyNetwork network, final List<Collection<IClusters>> selectedClusters,
			final List<CyTable> kpmTables, final int kValue, final int resultsWanted,
			final boolean unmappedNodesOnPositiveList, final int numberOfProcessors, final boolean benFree,
			final IKPMResultPanel resultPanel, final String kpmModel) {
		this.kpmTask = new KPMBatchTask(network, selectedClusters, kpmTables, kValue, resultsWanted,
				unmappedNodesOnPositiveList, numberOfProcessors, benFree, resultPanel, kpmModel);
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.kpmTask);
		return taskIterator;
	}
}
