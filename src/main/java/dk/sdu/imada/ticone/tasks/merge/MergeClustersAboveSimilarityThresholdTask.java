package dk.sdu.imada.ticone.tasks.merge;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.util.GUIUtility;

/**
 * Merges clusters transitively together with a similarity larger than a
 * threshold.
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 13, 2019
 *
 */
public class MergeClustersAboveSimilarityThresholdTask extends AbstractTask {

	private final ISimilarityValue threshold;

	protected TiconeClusteringResultPanel resultPanel;

	public MergeClustersAboveSimilarityThresholdTask(final ISimilarityValue threshold,
			final TiconeClusteringResultPanel resultPanel) {
		super();
		this.threshold = threshold;
		this.resultPanel = resultPanel;
	}

	@Override
	public void run(final TaskMonitor taskMonitor) throws Exception {

		taskMonitor.setTitle("Merging clusters above similarity threshold");
		taskMonitor.setProgress(0.1);
		taskMonitor.setStatusMessage("Calculating cluster similarities and merging clusters ...");
		this.resultPanel.getClusteringResult().getSimilarityFunction().initialize();
		final IClusteringProcess timeSeriesClusteringWithOverrepresentedPatterns = this.resultPanel
				.getClusteringResult().getClusteringProcess();
		timeSeriesClusteringWithOverrepresentedPatterns.mergeClusters(this.threshold);
		taskMonitor.setProgress(1.0);
		this.resultPanel.getClusteringResult().setupPatternStatusMapping();
		GUIUtility.updateGraphTable(this.resultPanel);

	}
}
