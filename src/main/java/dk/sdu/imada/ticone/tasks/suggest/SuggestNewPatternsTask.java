package dk.sdu.imada.ticone.tasks.suggest;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringMethod;
import dk.sdu.imada.ticone.clustering.IClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.suggestclusters.IClusterSuggestion;
import dk.sdu.imada.ticone.clustering.suggestclusters.ISuggestNewCluster;
import dk.sdu.imada.ticone.clustering.suggestclusters.SuggestClustersBasedOnLeastFitting;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.panels.popup.SuggestPatternsAcceptPanel;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.util.Utility;

/**
 * Created by christian on 8/25/15.
 */
public class SuggestNewPatternsTask extends AbstractTask {

	private final int numberOfPatterns;
	private final int percentLeastFittingObjects;
	protected TiconeClusteringResultPanel resultPanel;

	public SuggestNewPatternsTask(final int numberOfPatterns, final int percentLeastFittingObjects,
			final TiconeClusteringResultPanel resultPanel) {
		super();
		;
		this.resultPanel = resultPanel;
		this.numberOfPatterns = numberOfPatterns;
		this.percentLeastFittingObjects = percentLeastFittingObjects;
	}

	@Override
	public void run(final TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setTitle("Finding new clusters.");
		this.resultPanel.getClusteringResult().getSimilarityFunction().initialize();
		final IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethod = this.resultPanel
				.getClusteringResult().getClusteringMethodBuilder();
		final ISuggestNewCluster suggestNewPatterns = new SuggestClustersBasedOnLeastFitting(this.numberOfPatterns,
				this.percentLeastFittingObjects, clusteringMethod,
				new PearsonCorrelationFunction(this.resultPanel.getClusteringResult().getTimePointWeighting()), 42);

		final IClusteringProcess timeSeriesClusteringWithOverrepresentedPatterns = this.resultPanel
				.getClusteringResult().getClusteringProcess();
		final IClusterSuggestion suggestPatternsContainer = timeSeriesClusteringWithOverrepresentedPatterns
				.suggestNewClusters(suggestNewPatterns);
		if (!Utility.getProgress().getStatus()) {
			return;
		}
		final JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		final SuggestPatternsAcceptPanel suggestPatternAcceptPanel = new SuggestPatternsAcceptPanel(frame,
				suggestNewPatterns, suggestPatternsContainer, this.resultPanel);
		final JScrollPane jScrollPane = new JScrollPane(suggestPatternAcceptPanel);
		jScrollPane.getVerticalScrollBar().setUnitIncrement(28);
		frame.add(jScrollPane);
		frame.pack();
		frame.setVisible(true);
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		final int height = Math.min(frame.getHeight(), (int) screenSize.getHeight() - 200);
		frame.setSize(new Dimension(frame.getWidth() + 50, height));
		// GUIUtility.getTsvizGui().setUpGraphPanel();
		taskMonitor.setProgress(1.0);

	}

	@Override
	public void cancel() {
		Utility.getProgress().stop();
		super.cancel();

	}
}
