package dk.sdu.imada.ticone.connectivity;

import java.util.Set;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.pair.IShuffleClusteringPair;
import dk.sdu.imada.ticone.connectivity.TiconeClusterConnectivityTask.ConnectivityType;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.network.IShuffleNetwork;

/**
 * Created by christian on 25-01-16.
 */
public class ClusterConnectivityTaskFactory extends AbstractTaskFactory {

	private final ClusterConnectivityTask kpmTask;

	public ClusterConnectivityTaskFactory(final TiconeClusteringResultPanel resultPanel, final CyNetwork network,
			final ClusterObjectMapping pom, final IClusters clusters, final int numberPermutations,
			final Set<ConnectivityType> connectivityTypes, final boolean twoSidedPermutationTest,
			final IShuffleNetwork permutationFunction) throws InterruptedException {
		this.kpmTask = new ClusterConnectivityTask(resultPanel, network, pom, clusters, numberPermutations,
				connectivityTypes, twoSidedPermutationTest, permutationFunction);
	}

	public ClusterConnectivityTaskFactory(final TiconeClusteringResultPanel resultPanel, final CyNetwork network,
			final ClusterObjectMapping pom, final IClusters clusters, final int numberPermutations,
			final Set<ConnectivityType> connectivityTypes, final boolean twoSidedPermutationTest,
			final IShuffleClusteringPair permutationFunction) throws InterruptedException {
		this.kpmTask = new ClusterConnectivityTask(resultPanel, network, pom, clusters, numberPermutations,
				connectivityTypes, twoSidedPermutationTest, permutationFunction);
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.kpmTask);
		return taskIterator;
	}
}
