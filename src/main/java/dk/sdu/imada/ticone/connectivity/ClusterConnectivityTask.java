package dk.sdu.imada.ticone.connectivity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.pair.IShuffleClusteringPair;
import dk.sdu.imada.ticone.connectivity.TiconeClusterConnectivityTask.ConnectivityType;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.panels.connectivity.TiconeClusteringConnectivityResultPanel;
import dk.sdu.imada.ticone.network.IShuffleNetwork;
import dk.sdu.imada.ticone.network.NetworkUtil;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.util.CyNetworkUtil;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.TiconeProgressEvent;
import dk.sdu.imada.ticone.util.TiconeTaskProgressListener;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

/**
 * 
 * @author wiwiec
 *
 */
public class ClusterConnectivityTask extends AbstractTask implements TiconeTaskProgressListener {

	private final TiconeClusteringResultPanel resultPanel;

	protected TiconeClusterConnectivityTask ticoneConnectivityTask;

	protected Set<ConnectivityType> connectivityTypes;

	protected TaskMonitor tm;

	protected int iterationNumber;

	protected boolean twoSidedPermutationTest;

	protected TiconeNetworkImpl network;

	protected TiconeCytoscapeNetwork undirectedConnectivityNetwork, directedConnectivityNetwork;

	public ClusterConnectivityTask(final TiconeClusteringResultPanel resultPanel, final CyNetwork network,
			final ClusterObjectMapping pom, final IClusters clusters, final int numberPermutations,
			final Set<ConnectivityType> connectivityTypes, final boolean twoSidedPermutationTest,
			final IShuffleNetwork networkShuffle) throws InterruptedException {
		super();
		this.resultPanel = resultPanel;
		this.iterationNumber = resultPanel.getClusteringResult().getClusterHistory().getIterationNumber();
		this.connectivityTypes = connectivityTypes;
		this.twoSidedPermutationTest = twoSidedPermutationTest;
		this.network = TiconeNetworkImpl.getInstance(new TiconeCytoscapeNetwork(network), false);
		this.ticoneConnectivityTask = new TiconeClusterConnectivityTask(this.network, resultPanel.getClusteringResult(),
				pom, clusters, numberPermutations, twoSidedPermutationTest, networkShuffle);
		this.ticoneConnectivityTask.addProgressListener(this);
	}

	public ClusterConnectivityTask(final TiconeClusteringResultPanel resultPanel, final CyNetwork network,
			final ClusterObjectMapping pom, final IClusters clusters, final int numberPermutations,
			final Set<ConnectivityType> connectivityTypes, final boolean twoSidedPermutationTest,
			final IShuffleClusteringPair shuffleClusteringPair) throws InterruptedException {
		super();
		this.resultPanel = resultPanel;
		this.iterationNumber = resultPanel.getClusteringResult().getClusterHistory().getIterationNumber();
		this.connectivityTypes = connectivityTypes;
		this.twoSidedPermutationTest = twoSidedPermutationTest;
		this.network = TiconeNetworkImpl.getInstance(new TiconeCytoscapeNetwork(network), false);
		this.ticoneConnectivityTask = new TiconeClusterConnectivityTask(this.network, resultPanel.getClusteringResult(),
				pom, clusters, numberPermutations, twoSidedPermutationTest, shuffleClusteringPair);
		this.ticoneConnectivityTask.addProgressListener(this);
	}

	@Override
	public void progressUpdated(final TiconeProgressEvent event) {
		if (event.getTitle() != null)
			this.tm.setTitle(event.getTitle());
		if (event.getStatusMessage() != null)
			this.tm.setStatusMessage(event.getStatusMessage());
		if (event.getPercent() != null)
			this.tm.setProgress(event.getPercent());
	}

	@Override
	public void cancel() {
		super.cancel();
		this.ticoneConnectivityTask.cancel();
	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		try {
			this.tm = tm;

			tm.setTitle("Cluster Connectivity");
			tm.setStatusMessage("Preparing connectivity analysis.");

			this.ticoneConnectivityTask.removeNodesNotInDataset(this.network);

			this.network.initNodeDegreesUndirected();
			this.ticoneConnectivityTask.initEntrezToNetworks();

			this.ticoneConnectivityTask.removeObjectsNotInNetwork(this.network);

			ConnectivityResult directedConnectivityResult = null, undirectedConnectivityResult = null;
			ConnectivityPValueResult edgeCrossoverConnectivityResult = null;

			if (this.cancelled)
				return;
//		if (connectivityTypes.contains(ConnectivityType.UNDIRECTED)) {
//			tm.setStatusMessage("Assessing undirected connectivity between selected clusters.");
//			undirectedConnectivityResult = getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees();
//		}
//		if (!this.cancelled && connectivityTypes.contains(ConnectivityType.DIRECTED)) {
//			tm.setStatusMessage("Assessing directed connectivity between selected clusters.");
//			directedConnectivityResult = getEnrichmentDirectedEdgesBetweenClustersNodeDegrees();
//		}
			if (!this.cancelled) {// &&
									// connectivityTypes.contains(ConnectivityType.PERMUTATION_TEST))
									// {
				tm.setStatusMessage("Permuting network and calculating p-values.");
				edgeCrossoverConnectivityResult = this.edgeCrossoverLogOddsPValues();
				// changed
				undirectedConnectivityResult = this.network.getUndirectedEnrichment();
				directedConnectivityResult = this.network.getDirectedEnrichment();
			}

			final ConnectivityResultWrapper wrapper = new ConnectivityResultWrapper(
					this.resultPanel.getClusteringResult(), directedConnectivityResult, undirectedConnectivityResult,
					edgeCrossoverConnectivityResult, this.iterationNumber);

			if (this.connectivityTypes.contains(ConnectivityType.DIRECTED)) {
				this.directedConnectivityNetwork = CyNetworkUtil.createDirectedConnectivityNetwork(wrapper);
			}
			if (this.connectivityTypes.contains(ConnectivityType.UNDIRECTED)) {
				this.undirectedConnectivityNetwork = CyNetworkUtil.createUndirectedConnectivityNetwork(wrapper);
			}

			if (this.connectivityTypes.contains(ConnectivityType.PERMUTATION_TEST)) {
				// add p-values to edge attributes
				if (this.directedConnectivityNetwork != null) {
					CyNetworkUtil.addPvaluesToDirectedConnectivityNetwork(wrapper, this.directedConnectivityNetwork);
				}

				if (this.undirectedConnectivityNetwork != null) {
					CyNetworkUtil.addPvaluesToUndirectedConnectivityNetwork(wrapper,
							this.undirectedConnectivityNetwork);
				}
			}

			final TiconeClusteringConnectivityResultPanel connectivityResultPanel = new TiconeClusteringConnectivityResultPanel(
					this.resultPanel.getClusteringResult(), wrapper);
			if (this.directedConnectivityNetwork != null)
				connectivityResultPanel.addConnectivityNetwork(this.directedConnectivityNetwork);
			if (this.undirectedConnectivityNetwork != null)
				connectivityResultPanel.addConnectivityNetwork(this.undirectedConnectivityNetwork);

		} catch (InterruptedException e) {
		}
	}

	protected ConnectivityResult getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees() {
		final ConnectivityResult result = this.ticoneConnectivityTask
				.getEnrichmentUndirectedEdgesBetweenClustersNodeDegrees(this.network);
		return result;
	}

	protected int getTheoreticallyPossibleDirectedEdges(final CyNetwork network) {
		return (network.getNodeCount() * (network.getNodeCount() - 1));
	}

	protected int getTheoreticallyPossibleDirectedEdgesBetweenNodesWithDegrees(final CyNetwork network,
			final Map<Integer, Integer> nodeDegreeCounts, final int degree1, final int degree2) {
		if (degree1 != degree2)
			return nodeDegreeCounts.get(degree1) * nodeDegreeCounts.get(degree2) * 2;
		return nodeDegreeCounts.get(degree1) * (nodeDegreeCounts.get(degree1) - 1);
	}

	protected int getTheoreticallyPossibleUndirectedEdgesBetweenNodesWithDegrees(final CyNetwork network,
			final Map<Integer, Integer> nodeDegreeCounts, final int degree1, final int degree2) {
		if (degree1 != degree2)
			return nodeDegreeCounts.get(degree1) * nodeDegreeCounts.get(degree2);
		return nodeDegreeCounts.get(degree1) * (nodeDegreeCounts.get(degree1) - 1) / 2;
	}

	protected int getTheoreticallyPossibleUndirectedEdges(final CyNetwork network) {
		return (network.getNodeCount() * (network.getNodeCount() - 1)) / 2;
	}

	protected Map<ICluster, Map<ICluster, Integer>> getDirectedEdgesBetweenClusters(
			final Map<String, ICluster> entrezToNetworks, final Map<String, List<String>> edgeMap,
			final Set<ICluster> selectedClusters) {
		final Map<ICluster, Map<ICluster, Integer>> counts = new HashMap<>();

		for (final String sourceEntrezId : edgeMap.keySet()) {
			for (final String targetEntrezId : edgeMap.get(sourceEntrezId)) {
				final ICluster sourceNet = entrezToNetworks.get(sourceEntrezId);
				final ICluster targetNet = entrezToNetworks.get(targetEntrezId);
				if (sourceNet == null || targetNet == null)
					continue;
				if (!(selectedClusters.contains(sourceNet)))
					continue;
				if (!(selectedClusters.contains(targetNet)))
					continue;
				if (!counts.containsKey(sourceNet)) {
					counts.put(sourceNet, new HashMap<ICluster, Integer>());
				}
				if (!counts.get(sourceNet).containsKey(targetNet)) {
					counts.get(sourceNet).put(targetNet, 0);
				}
				if (!counts.containsKey(targetNet)) {
					counts.put(targetNet, new HashMap<ICluster, Integer>());
				}
				if (!counts.get(targetNet).containsKey(sourceNet)) {
					counts.get(targetNet).put(sourceNet, 0);
				}
				counts.get(sourceNet).put(targetNet, counts.get(sourceNet).get(targetNet) + 1);
			}
		}

		// we counted edges from cluster X -> cluster X twice;
		for (final ICluster p : counts.keySet()) {
			if (counts.get(p).containsKey(p)) {
				counts.get(p).put(p, counts.get(p).get(p) / 2);
			}
		}

		return counts;
	}

	protected Map<ICluster, Map<ICluster, Integer>> getDirectedEdgesBetweenClusters(
			final Map<String, ICluster> entrezToNetworks, final CyNetwork network,
			final Set<ICluster> selectedClusters) {
		final Map<ICluster, Map<ICluster, Integer>> counts = new HashMap<>();

		for (final CyEdge e : network.getEdgeList()) {
			final CyNode source = e.getSource();
			final String sourceEntrezId = (String) network.getRow(source)
					.getRaw(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME);
			final ICluster sourceNet = entrezToNetworks.get(sourceEntrezId);
			final CyNode target = e.getTarget();
			final String targetEntrezId = (String) network.getRow(target)
					.getRaw(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME);
			final ICluster targetNet = entrezToNetworks.get(targetEntrezId);
			if (sourceNet == null || targetNet == null)
				continue;
			if (!(selectedClusters.contains(sourceNet)))
				continue;
			if (!(selectedClusters.contains(targetNet)))
				continue;
			if (!counts.containsKey(sourceNet)) {
				counts.put(sourceNet, new HashMap<ICluster, Integer>());
			}
			if (!counts.get(sourceNet).containsKey(targetNet)) {
				counts.get(sourceNet).put(targetNet, 0);
			}
			if (!counts.containsKey(targetNet)) {
				counts.put(targetNet, new HashMap<ICluster, Integer>());
			}
			if (!counts.get(targetNet).containsKey(sourceNet)) {
				counts.get(targetNet).put(sourceNet, 0);
			}
			counts.get(sourceNet).put(targetNet, counts.get(sourceNet).get(targetNet) + 1);
		}

		// we counted edges from cluster X -> cluster X twice;
		for (final ICluster p : counts.keySet()) {
			if (counts.get(p).containsKey(p)) {
				counts.get(p).put(p, counts.get(p).get(p) / 2);
			}
		}

		return counts;
	}

	protected ConnectivityResult getEnrichmentDirectedEdgesBetweenClustersNodeDegrees() {
		final ConnectivityResult result = this.ticoneConnectivityTask
				.getEnrichmentDirectedEdgesBetweenClustersNodeDegrees(this.network);

		return result;

	}

	protected ConnectivityPValueResult edgeCrossoverLogOddsPValues() throws ClusterConnectivityException,
			InterruptedException, NotAnArithmeticFeatureValueException, ToNumberConversionException {
		final ConnectivityPValueResult result = this.ticoneConnectivityTask.calculatePValues(this.network);

		return result;
	}

	public static double round(final double value, final int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		if (Double.isNaN(value) || Double.isInfinite(value))
			return value;

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
}
