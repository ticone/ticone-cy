package dk.sdu.imada.ticone.io;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.MultipleTimeSeriesSignalsForSameSampleParseException;
import dk.sdu.imada.ticone.data.TimeSeries;
import dk.sdu.imada.ticone.data.TimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.table.TiconeCytoscapeTable;
import dk.sdu.imada.ticone.util.CyTableUtil;
import dk.sdu.imada.ticone.util.ObjectUtils;

public class LoadDataFromTable extends LoadColumnDataMethod {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8738217729123095794L;
	protected TiconeCytoscapeTable importTable;
	protected String importTableName;

	public LoadDataFromTable() {
	}

	public LoadDataFromTable(LoadDataFromTable other) {
		super(other);
		this.importTable = other.importTable;
		this.importTableName = other.importTableName;
	}

	@Override
	public LoadDataFromTable copy() {
		return new LoadDataFromTable(this);
	}

	public void setImportTable(final CyTable importTable) throws LoadDataException {
		if (ObjectUtils.bothNullOrNone(this.importTable, importTable)
				|| (this.importTable != null && !this.importTable.getCyTable().getSUID().equals(importTable.getSUID())))
			this.isDataLoaded = false;
		this.isDataLoaded = false;
		this.importTable = new TiconeCytoscapeTable(importTable);
		if (importTable == null)
			return;
		this.importTableName = importTable.getTitle();

		this.setDefaultColumnMapping();
	}

	@Override
	protected void setDefaultColumnMapping() throws LoadDataException {
		this.isDataLoaded = false;
		try {
			final List<CyTableInputColumn> columns = CyTableUtil
					.getLengthAlphabeticallySortedColumns(this.importTable.getCyTable());
			final int[] timePoints = new int[Math.max(columns.size() - 1, 0)];
			final String[] tpColumnNames = new String[timePoints.length];
			for (int i = 0; i < timePoints.length; i++) {
				timePoints[i] = columns.get(i + 1).getIndex();
				tpColumnNames[i] = columns.get(i + 1).getName();
			}
			this.columnMapping.setTimePointsColumns(timePoints, tpColumnNames);
			this.columnMapping.setObjectIdColumn(columns.get(0).getIndex(), columns.get(0).getName());
		} catch (final IndexOutOfBoundsException e) {
			throw new LoadDataException("Table has too few columns");
		}
	}

	public CyTable getImportTable() {
		return this.importTable.getCyTable();
	}

	@Override
	protected void loadDataInternal(final ITimeSeriesPreprocessor preprocessor) throws LoadDataException {
		this.isDataLoaded = false;
		if (this.getImportTable() == null)
			return;
		final ITimeSeriesObjectList tsds = loadTimeSeriesDataFromTable(this.getImportTable(), this.columnMapping);
		preprocessor.initializeObjects(tsds);
		this.isDataLoaded = true;
	}

	@Override
	public String sourceAsString() {
		return this.getImportTable().getTitle();
	}

	private ITimeSeriesObjectList createObjects(final Map<String, Map<String, double[]>> objectTmp)
			throws LoadDataException {
		final ITimeSeriesObjectList timeSeriesDatas = new TimeSeriesObjectList();
		for (final String objName : objectTmp.keySet()) {
			final Map<String, double[]> samples = objectTmp.get(objName);
			final ITimeSeriesObject tsd = new TimeSeriesObject(objName, samples.size());

			for (final String sampleName : samples.keySet()) {
				try {
					tsd.addOriginalTimeSeriesToList(new TimeSeries(samples.get(sampleName)), sampleName);
				} catch (final MultipleTimeSeriesSignalsForSameSampleParseException e) {
					throw new LoadDataException(e);
				}
			}

			timeSeriesDatas.add(tsd);
		}
		return timeSeriesDatas;
	}

	public ITimeSeriesObjectList loadTimeSeriesDataFromTable(final CyTable cyTable,
			final ImportColumnMapping columnMapping) throws LoadDataException {
		Objects.requireNonNull(cyTable);
		// Find out if multiple samples, or single samples
		final Map<String, Map<String, double[]>> objectTmp = readMultipleSampleTable(cyTable,
				columnMapping.getObjectIdColumnIndex(), columnMapping.getReplicateColumnIndex(),
				columnMapping.getTimePointsColumnIndices());
		return createObjects(objectTmp);
	}

	private Map<String, Map<String, double[]>> readMultipleSampleTable(final CyTable cyTable,
			final int objectIdColumnIndex, final int replicateColumnIndex, final int[] timePointColumnIndices)
			throws LoadDataException {
		Objects.requireNonNull(cyTable);
		final Map<String, Map<String, double[]>> objectTmp = new LinkedHashMap<>();

		final List<CyColumn> columnList = new ArrayList<>(cyTable.getColumns());
		final String objectIdColumnName = columnList.get(objectIdColumnIndex).getName();
		final List<String> timePointColumnNames = new ArrayList<>();
		for (final int i : timePointColumnIndices)
			timePointColumnNames.add(columnList.get(i).getName());

		final List<CyRow> rows = cyTable.getAllRows();
		for (int i = 0; i < rows.size(); i++) {
			readRow(objectTmp, rows.get(i), objectIdColumnName, timePointColumnNames,
					replicateColumnIndex > -1 ? columnList.get(replicateColumnIndex).getName() : null);
		}
		return objectTmp;
	}

	private void readRow(final Map<String, Map<String, double[]>> objectTmp, final CyRow cyRow, final String name,
			final List<String> timePoints, final String sample) throws LoadDataException {
		final Map<String, Object> columns = cyRow.getAllValues();

		if (!columns.containsKey(name))
			return;

		final String objName = "" + (cyRow.getRaw(name) != null ? cyRow.get(name, columns.get(name).getClass()) : "");
		String sampleName = "";
		if (sample != null) {
			if (cyRow.getRaw(sample) != null)
				sampleName += cyRow.get(sample, columns.get(sample).getClass());
		}

		final double[] signal = new double[timePoints.size()];
		for (int i = 0; i < signal.length; i++) {
			Object value = columns.get(timePoints.get(i));
			if (value == null)
				continue;
			if (value.getClass() == Integer.class) {
				signal[i] = (Integer) columns.get(timePoints.get(i));
			} else if (value.getClass() == Double.class) {
				signal[i] = (Double) columns.get(timePoints.get(i));
			} else if (value.getClass() == Float.class) {
				signal[i] = (Float) columns.get(timePoints.get(i));
			} else {
				throw new LoadDataException(
						new NonNumericalColumnLoadDataException("Non-numerical time-point column selected."));
			}
		}
		if (!objectTmp.containsKey(objName))
			objectTmp.put(objName, new LinkedHashMap<String, double[]>());
		objectTmp.get(objName).put(sampleName, signal);
	}
}
