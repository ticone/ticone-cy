/**
 * 
 */
package dk.sdu.imada.ticone.io;

import org.cytoscape.model.CyColumn;

import dk.sdu.imada.ticone.table.TiconeCytoscapeTable;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 14, 2019
 *
 */
public class CyTableInputColumn extends InputColumn {
	protected TiconeCytoscapeTable table;
	protected transient CyColumn cyColumn;

	public CyTableInputColumn(TiconeCytoscapeTable table, int index, CyColumn cyColumn) {
		super(cyColumn.getName(), index);
		this.table = table;
		this.cyColumn = cyColumn;
	}

	/**
	 * @return the table
	 */
	public TiconeCytoscapeTable getTable() {
		return this.table;
	}

	/**
	 * @return the cyColumn
	 */
	public CyColumn getCyColumn() {
		if (this.cyColumn == null) {
			this.cyColumn = getTable().getCyTable().getColumn(name);
		}
		return this.cyColumn;
	}

	public Class<?> getType() {
		return getCyColumn().getType();
	}
}
