package dk.sdu.imada.ticone.io;

import java.io.Serializable;

import org.cytoscape.model.CyTable;

import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.table.TiconeCytoscapeTable;

public class ClusteringImportFromTableConfig implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7794658555511142340L;

	protected TiconeCytoscapeTable table;
	protected String objectIdColumnName, clusterIdColumnName;
	protected int objectIdColumnIndex, clusterIdColumnIndex;
	protected boolean collectMissingObjectsIntoCluster;
	protected PrototypeBuilder prototypeFactory;

	public ClusteringImportFromTableConfig() {
		super();
	}

	public ClusteringImportFromTableConfig(final CyTable table, final String objectIdColumnName,
			final int objectIdColumnIndex, final String clusterIdColumnName, final int clusterIdColumnIndex,
			final boolean collectMissingObjectsIntoCluster, final PrototypeBuilder prototypeFactory) {
		super();
		this.setTable(table);
		this.objectIdColumnIndex = clusterIdColumnIndex;
		this.objectIdColumnName = objectIdColumnName;
		this.clusterIdColumnIndex = clusterIdColumnIndex;
		this.clusterIdColumnName = clusterIdColumnName;
		this.collectMissingObjectsIntoCluster = collectMissingObjectsIntoCluster;
		this.prototypeFactory = prototypeFactory;
	}

	public int getClusterIdColumnIndex() {
		return this.clusterIdColumnIndex;
	}

	public String getClusterIdColumnName() {
		return this.clusterIdColumnName;
	}

	public int getObjectIdColumnIndex() {
		return this.objectIdColumnIndex;
	}

	public String getObjectIdColumnName() {
		return this.objectIdColumnName;
	}

	public CyTable getTable() {
		return this.table.getCyTable();
	}

	public String getTableName() {
		return this.table.getName();
	}

	public void setTable(final CyTable table) {
		this.table = new TiconeCytoscapeTable(table);
	}

	public void setClusterIdColumn(final int clusterIdColumnIndex, final String clusterIdColumnName) {
		this.clusterIdColumnIndex = clusterIdColumnIndex;
		this.clusterIdColumnName = clusterIdColumnName;
	}

	public void setObjectIdColumn(final int objectIdColumnIndex, final String objectIdColumnName) {
		this.objectIdColumnIndex = objectIdColumnIndex;
		this.objectIdColumnName = objectIdColumnName;
	}

	public void setCollectMissingObjectsIntoCluster(final boolean collectMissingObjectsIntoCluster) {
		this.collectMissingObjectsIntoCluster = collectMissingObjectsIntoCluster;
	}

	public boolean isCollectMissingObjectsIntoCluster() {
		return this.collectMissingObjectsIntoCluster;
	}

	/**
	 * @return the prototypeFactory
	 */
	public PrototypeBuilder getPrototypeFactory() {
		return this.prototypeFactory;
	}

	/**
	 * @param prototypeFactory the prototypeFactory to set
	 */
	public void setPrototypeFactory(final PrototypeBuilder prototypeFactory) {
		this.prototypeFactory = prototypeFactory;
	}
}
