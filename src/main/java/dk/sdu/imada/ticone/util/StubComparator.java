package dk.sdu.imada.ticone.util;

import java.util.Comparator;

/**
 * This comparator should just leave the objects in the same order.
 * 
 * @author wiwiec
 *
 */
public class StubComparator implements Comparator<Object> {
	@Override
	public int compare(final Object o1, final Object o2) {
		return 0;
	}
}