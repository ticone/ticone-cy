package dk.sdu.imada.ticone.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyTable;

import dk.sdu.imada.ticone.io.CyTableInputColumn;
import dk.sdu.imada.ticone.table.TiconeCytoscapeTable;

public class CyTableUtil {

	public static List<CyTableInputColumn> getLengthAlphabeticallySortedColumns(final CyTable table) {
		TiconeCytoscapeTable tableWrapper = new TiconeCytoscapeTable(table);
		final List<CyTableInputColumn> columns = new ArrayList<>();
		int idx = 0;
		for (CyColumn c : table.getColumns()) {
			columns.add(new CyTableInputColumn(tableWrapper, idx++, c));
		}
		columns.sort(new Comparator<CyTableInputColumn>() {
			@Override
			public int compare(final CyTableInputColumn o1, final CyTableInputColumn o2) {
				if (o1.getName().length() > o2.getName().length()) {
					return 1;
				} else if (o1.getName().length() < o2.getName().length()) {
					return -1;
				}
				return o1.getName().compareTo(o2.getName());
			}
		});
		return columns;
	}
}
