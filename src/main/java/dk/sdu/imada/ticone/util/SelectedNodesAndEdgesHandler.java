/**
 * 
 */
package dk.sdu.imada.ticone.util;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.events.SelectedNodesAndEdgesEvent;
import org.cytoscape.model.events.SelectedNodesAndEdgesListener;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 16, 2019
 *
 */
public class SelectedNodesAndEdgesHandler implements SelectedNodesAndEdgesListener {

	@Override
	public void handleEvent(SelectedNodesAndEdgesEvent event) {
		try {
			if (!event.nodesChanged())
				return;

			final CyNetwork network = event.getNetwork();
			if (!CyNetworkUtil.isNetworkForCurrentlyVisibleClustering(network))
				return;

			final TiconeClusteringResultPanel clusteringResultPanel = GUIUtility
					.getCurrentlySelectedClusteringResultPanel();
			final ClusterObjectMapping visibleClustering = clusteringResultPanel.getResult().getClusteringProcess()
					.getLatestClustering();

			final IClusters selectedClusters = CyNetworkUtil.getClustersForSelectedNodesInNetwork(network,
					event.getSelectedNodes(), visibleClustering);

			GUIUtility.getCurrentlySelectedClusteringResultPanel().setSelectedClusters(selectedClusters.asList());
		} catch (TiconeUnloadingException e) {
		}
	}

}
