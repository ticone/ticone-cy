package dk.sdu.imada.ticone.util;

import java.util.Comparator;

import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.clustering.compare.ClusterCompareException;
import dk.sdu.imada.ticone.clustering.compare.ClusterPrototypeComparator;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartWithButtonsPanel;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;

public class ClusterChartWithButtonsComparator implements Comparator<ClusterChartWithButtonsPanel> {

	protected ClusterPrototypeComparator prototypeComparator;

	public ClusterChartWithButtonsComparator(final IClusterList patterns, final ISimilarityFunction similarity)
			throws ClusterCompareException, InterruptedException {
		super();
		this.prototypeComparator = new ClusterPrototypeComparator(patterns, similarity);
	}

	@Override
	public int compare(ClusterChartWithButtonsPanel o1, ClusterChartWithButtonsPanel o2) {
		return this.prototypeComparator.compare(o1.getCluster(), o2.getCluster());
	}

	/**
	 * @param clusterStatusMapping
	 * @return
	 */
	public Comparator<?> setStatusMapping(IClusterStatusMapping statusMapping) {
		this.prototypeComparator.setStatusMapping(statusMapping);
		return this;
	}

}