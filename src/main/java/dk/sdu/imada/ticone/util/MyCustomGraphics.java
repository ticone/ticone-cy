package dk.sdu.imada.ticone.util;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.cytoscape.model.CyIdentifiable;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.customgraphics.CyCustomGraphics;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.jfree.chart.ChartPanel;

import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;

public class MyCustomGraphics implements CyCustomGraphics<MyImageCustomGraphicLayer>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 372427785745525660L;
	private final ClusterChartContainer clusterChartContainer;
	private Long identifier;
	private String displayName;
	private int lastWidth, lastHeight, lastBorderWidth;
	private transient List<MyImageCustomGraphicLayer> lastLayers;
	private float ratio;
	private ChartPanel chartPanel;
	private int lastChartImageWidth;
	private int lastChartImageHeight;
	private BufferedImage lastBufferedImage;

	public MyCustomGraphics(final ClusterChartContainer clusterChartContainer) {
		super();
		this.clusterChartContainer = clusterChartContainer;
		this.lastLayers = new ArrayList<>();
	}

	@Override
	public Long getIdentifier() {
		return this.identifier;
	}

	@Override
	public void setIdentifier(final Long id) {
		this.identifier = id;
	}

	@Override
	public String getDisplayName() {
		return this.displayName;
	}

	@Override
	public void setDisplayName(final String displayName) {
		this.displayName = displayName;
	}

	@Override
	public String toSerializableString() {
		return null;
	}

	@Override
	public List<MyImageCustomGraphicLayer> getLayers(final CyNetworkView networkView,
			final View<? extends CyIdentifiable> view) {
		final int nodeWidth = (int) Math.round(view.getVisualProperty(BasicVisualLexicon.NODE_WIDTH));
		final int nodeHeight = (int) Math.round(view.getVisualProperty(BasicVisualLexicon.NODE_HEIGHT));
		final int borderWidth = (int) Math.round(view.getVisualProperty(BasicVisualLexicon.NODE_BORDER_WIDTH));

		if (this.lastLayers == null || this.lastWidth != nodeWidth || this.lastHeight != nodeHeight
				|| this.lastBorderWidth != borderWidth) {
			try {
				lastChartImageWidth = nodeWidth - borderWidth;
				lastChartImageHeight = nodeHeight - borderWidth;
				chartPanel = this.clusterChartContainer.getChartPanel(this.lastChartImageWidth,
						this.lastChartImageHeight, true);
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException | InterruptedException e1) {
			}
			this.lastLayers = new ArrayList<>();

			this.lastWidth = nodeWidth;
			this.lastHeight = nodeHeight;
			this.lastBorderWidth = borderWidth;

			if (nodeWidth <= 0 || nodeHeight <= 0)
				return new ArrayList<>();

			if (chartPanel != null)
				this.lastBufferedImage = chartPanel.getChart().createBufferedImage(this.lastChartImageWidth,
						this.lastChartImageHeight);

			if (this.lastBufferedImage != null) {
				final int x = -this.lastBufferedImage.getWidth() / 2;
				final int y = -this.lastBufferedImage.getHeight() / 2;
				final MyImageCustomGraphicLayer micgl = new MyImageCustomGraphicLayer(this.lastBufferedImage, x, y);
				micgl.convertBufferedImageToRectangle();
				this.lastLayers.add(micgl);
			}
		}
		return this.lastLayers;
	}

	@Override
	public int getWidth() {
		return this.lastWidth;
	}

	@Override
	public int getHeight() {
		return this.lastHeight;
	}

	@Override
	public void setWidth(final int width) {
		this.lastWidth = width;
	}

	@Override
	public void setHeight(final int height) {
		this.lastHeight = height;
	}

	@Override
	public float getFitRatio() {
		return this.ratio;
	}

	@Override
	public void setFitRatio(final float ratio) {
		this.ratio = ratio;
	}

	@Override
	public BufferedImage getRenderedImage() {
		return this.lastBufferedImage;
	}
}