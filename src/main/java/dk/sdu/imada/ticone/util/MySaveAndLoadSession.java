package dk.sdu.imada.ticone.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.session.events.SessionAboutToBeLoadedEvent;
import org.cytoscape.session.events.SessionAboutToBeLoadedListener;
import org.cytoscape.session.events.SessionAboutToBeSavedEvent;
import org.cytoscape.session.events.SessionAboutToBeSavedListener;
import org.cytoscape.session.events.SessionLoadedEvent;
import org.cytoscape.session.events.SessionLoadedListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.sdu.imada.ticone.clustering.ClusterBuilder;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.comparison.ClusteringComparisonResult;
import dk.sdu.imada.ticone.comparison.CytoscapeClusteringComparisonResult;
import dk.sdu.imada.ticone.connectivity.ConnectivityResultWrapper;
import dk.sdu.imada.ticone.gui.TiconeResultPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.panels.comparison.TiconeClusteringComparisonResultPanel;
import dk.sdu.imada.ticone.gui.panels.connectivity.TiconeClusteringConnectivityResultPanel;
import dk.sdu.imada.ticone.gui.panels.kpm.KPMResultFormPanel;
import dk.sdu.imada.ticone.gui.util.AppProperties;

/**
 * @author Christian Wiwie
 * @author Christian Norskov
 * @since 14-12-15
 */
public class MySaveAndLoadSession
		implements SessionAboutToBeSavedListener, SessionLoadedListener, SessionAboutToBeLoadedListener {

	protected <T extends Serializable> void writeObjectToFile(final T object, final File file) throws IOException {
		ObjectOutputStream oos = null;
		try {
			final FileOutputStream fos = new FileOutputStream(file);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(object);
		} catch (Exception e) {
			if (oos != null)
				oos.close();
		}
	}

	@Override
	public void handleEvent(final SessionAboutToBeSavedEvent sessionAboutToBeSavedEvent) {

		final String tmpDir = System.getProperty("java.io.tmpdir");

		final List<File> myFileList = new ArrayList<>();
		final File file = new File(tmpDir, "app.data");
		try {
			final ProgramState state = new ProgramState();
			state.setNextClusteringNumber(TiconeClusteringResult.getNextClusteringNumber());
			state.setNextConnectivityNumber(ConnectivityResultWrapper.getNextConnectivityNumber());
			state.setNextComparisonNumber(ClusteringComparisonResult.getNextComparisonNumber());
			state.setGlobalNumberOfClusters(ClusterBuilder.getGlobalNumberOfClusters());
//			state.setNumberOfClusters(ClusterObjectMapping.getClusterCount());

			for (int i = 0; i < GUIUtility.getTiconeClusteringsResultsPanel().getTabCount(); i++) {
				try {
					final TiconeClusteringResultPanel resultPanel = (TiconeClusteringResultPanel) GUIUtility
							.getTiconeClusteringsResultsPanel().getComponentAt(i);
					resultPanel.saveState(i, state);
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
			for (int i = 0; i < GUIUtility.getTiconeComparisonResultsPanel().getTabCount(); i++) {
				try {
					final TiconeResultPanel resultPanel = (TiconeResultPanel) GUIUtility
							.getTiconeComparisonResultsPanel().getComponentAt(i);
					resultPanel.saveState(i, state);
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
			for (int i = 0; i < GUIUtility.getTiconeConnectivityResultsPanel().getTabCount(); i++) {
				try {
					final TiconeResultPanel resultPanel = (TiconeResultPanel) GUIUtility
							.getTiconeConnectivityResultsPanel().getComponentAt(i);
					resultPanel.saveState(i, state);
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
			final FileOutputStream fos = new FileOutputStream(file);
			final ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(state);
			oos.close();

			myFileList.add(file);

			sessionAboutToBeSavedEvent.addAppFiles(AppProperties.APP_NAME, myFileList);
		} catch (final IOException ioe) {
			ioe.printStackTrace();
		} catch (final NullPointerException npe) {
			npe.printStackTrace();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handleEvent(final SessionLoadedEvent sessionLoadedEvent) {
		try {
			final List<File> fileList = sessionLoadedEvent.getLoadedSession().getAppFileListMap()
					.get(AppProperties.APP_NAME);
			if (fileList == null || fileList.size() < 1) {
				GUIUtility.resetAction();
				TiconeClusteringResult.setNextClusteringNumber(1);
				ClusteringComparisonResult.setNextComparisonNumber(1);
				ConnectivityResultWrapper.setNextConnectivityNumber(1);
				return;
			}

			for (final File file : fileList) {
				FileInputStream fis = null;
				ObjectInputStream ois = null;
				try {
					// check if it is a cluster chart graphics file
					if (file.getName().matches("cluster.*\\.png")) {
						// write images of clusters from session file to temp
						// directory
//					TiCoNEVisualClusteringResult.getAllChartPngFiles().add(file.getAbsolutePath());
					} else if (file.getName().matches("program.state")) {
						fis = new FileInputStream(file);
						ois = new ObjectInputStream(fis);
						final ProgramState state = (ProgramState) ois.readObject();

						TiconeClusteringResult.setNextClusteringNumber(state.getNextClusteringNumber());
						TiconeClusteringResult.setNextClusteringNumber(state.getNextClusteringNumber());
						ClusteringComparisonResult.setNextComparisonNumber(state.getNextComparisonNumber());
						ConnectivityResultWrapper.setNextConnectivityNumber(state.getNextConnectivityNumber());
						ClusterBuilder.setGlobalNumberOfClusters(state.globalNumberOfClusters);
					} else if (file.getName().equals("app.data")) {
						fis = new FileInputStream(file);
						ois = new ObjectInputStream(fis);
						final ProgramState state = (ProgramState) ois.readObject();

						final Map<Integer, List<KPMResultWrapper>> clusteringKpmResults = state
								.getClusteringKpmResults();
						final Map<Integer, List<KPMResultWrapper>> comparisonKpmResults = state
								.getComparisonKpmResults();
						final Map<Integer, IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult>> utils = state.utils;
						final Map<Integer, CytoscapeClusteringComparisonResult> comparisonResults = state
								.getComparisonResults();
						final Map<Integer, ConnectivityResultWrapper> connectivityResults = state
								.getConnectivityResults();

						for (int i : utils.keySet()) {
							try {
								IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> util = utils
										.get(i);
								List<KPMResultWrapper> kpmResults = clusteringKpmResults.get(i);
								// ConnectivityResultWrapper connResults =
								// connectivityResults.get(i);

								util.getClusteringResult().clearListener();
//								util.initChartPngMap();
								TiconeClusteringResultPanel resultPanel = new TiconeClusteringResultPanel(util);
								GUIUtility.updateGraphPanel(resultPanel);

								for (KPMResultWrapper kpmResult : kpmResults) {
									// take a network with the same name
									Set<CyNetwork> networks = ServiceHelper.getService(CyNetworkManager.class)
											.getNetworkSet();
									for (CyNetwork network : networks) {
										String otherName = network.getRow(network).get(CyNetwork.NAME, String.class);
										if (otherName.equals(kpmResult.getNetworkName())) {
											new KPMResultFormPanel(resultPanel, network, kpmResult.getClusters(),
													kpmResult.getResults(), kpmResult.getKpmModel());
											break;
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						for (int i : comparisonResults.keySet()) {
							try {
								CytoscapeClusteringComparisonResult comparisonResult = comparisonResults.get(i);
								TiconeClusteringComparisonResultPanel resultPanel = new TiconeClusteringComparisonResultPanel(
										comparisonResult);

								List<KPMResultWrapper> kpmResults = comparisonKpmResults.get(i);

								if (kpmResults != null) {
									for (KPMResultWrapper kpmResult : kpmResults) {
										// take a network with the same name
										Set<CyNetwork> networks = ServiceHelper.getService(CyNetworkManager.class)
												.getNetworkSet();
										for (CyNetwork network : networks) {
											String otherName = network.getRow(network).get(CyNetwork.NAME,
													String.class);
											if (otherName.equals(kpmResult.getNetworkName())) {
												new KPMResultFormPanel(resultPanel, network, kpmResult.getClusters(),
														kpmResult.getResults(), kpmResult.getKpmModel());
												break;
											}
										}
									}
								}

								// init visual style and network
								comparisonResult.initClusteringComparisonVisualStyle();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						for (int i : connectivityResults.keySet()) {
							try {
								ConnectivityResultWrapper comparisonResult = connectivityResults.get(i);
								TiconeClusteringConnectivityResultPanel resultPanel = new TiconeClusteringConnectivityResultPanel(
										(TiconeCytoscapeClusteringResult) comparisonResult.getClusteringResult(),
										comparisonResult);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						ois.close();

						TiconeClusteringResult.setNextClusteringNumber(state.getNextClusteringNumber());
						TiconeClusteringResult.setNextClusteringNumber(state.getNextClusteringNumber());
						ClusteringComparisonResult.setNextComparisonNumber(state.getNextComparisonNumber());
						ConnectivityResultWrapper.setNextConnectivityNumber(state.getNextConnectivityNumber());
						ClusterBuilder.setGlobalNumberOfClusters(state.globalNumberOfClusters);
					}
				} catch (final Throwable e) {
					e.printStackTrace();
				} finally {
					if (ois != null)
						try {
							ois.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
				}
			}
		} catch (final TiconeUnloadingException e) {
		}
	}

	@Override
	public void handleEvent(final SessionAboutToBeLoadedEvent e) {
		try {
			while (GUIUtility.getTiconeClusteringsResultsPanel().getTabCount() > 0) {
				GUIUtility.getTiconeClusteringsResultsPanel().removeTabAt(0);
			}
			while (GUIUtility.getTiconeComparisonResultsPanel().getTabCount() > 0) {
				GUIUtility.getTiconeComparisonResultsPanel().removeTabAt(0);
			}
			while (GUIUtility.getTiconeConnectivityResultsPanel().getTabCount() > 0) {
				GUIUtility.getTiconeConnectivityResultsPanel().removeTabAt(0);
			}
		} catch (final TiconeUnloadingException e1) {

			Logger logger = LoggerFactory.getLogger(this.getClass());
			logger.error(e1.getMessage(), e1);
			logger.warn("Interrupted current operation to unload TiCoNE app");
		}
	}

}
