package dk.sdu.imada.ticone.util;

import java.awt.AWTException;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JPanel;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.gui.summary.ClusterSummaryPanel;
import dk.sdu.imada.ticone.gui.util.ScreenImage;

/**
 * Created by christian on 29-01-16.
 */
public class ExportToPDFUtil {

	public static ClusterSummaryPanel createClusterSummaryPanel(final IFeatureStore featureStore,
			final IClusterObjectMapping pom, final TiconeCytoscapeClusteringResult utils) {
		return new ClusterSummaryPanel(pom, utils, featureStore);
	}

	public static void printPanelToPNG(final JPanel panel, final String filename) {
		final BufferedImage bi = new BufferedImage(panel.getWidth(), panel.getHeight(), BufferedImage.TYPE_INT_ARGB);
		final Graphics g = bi.createGraphics();
		panel.paint(g);
		g.dispose();
		try {
			ImageIO.write(bi, "png", new File(filename));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public static void printComponentsToPDF(final List<Component> components, final String filename) {
		final List<BufferedImage> images = getImagesFromComponents(components);

		try {
			printToPDF(images, filename);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public static void printToPDF(final List<BufferedImage> images, final String fileName) {
		try {
			if (images.isEmpty())
				return;
			final Document d = new Document(PageSize.A4.rotate());
			d.setMargins(0, 0, 0, 0);
			final PdfWriter writer = PdfWriter.getInstance(d, new FileOutputStream(fileName));
			d.open();
			for (final java.awt.Image image : images) {
				final Image iTextImage = Image.getInstance(writer, image, 1);
				// iTextImage.setAbsolutePosition(50, 50);
				// iTextImage.scalePercent(100);
				iTextImage.scaleToFit(PageSize.A4.getHeight(), PageSize.A4.getWidth());
				d.add(iTextImage);
				d.newPage();
			}

			d.close();

		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public static List<BufferedImage> getImagesFromComponents(final List<Component> components) {
		final List<BufferedImage> images = new ArrayList<>();
		for (final Component component : components) {
			// BufferedImage image = new BufferedImage(component.getWidth(),
			// component.getHeight(),
			// BufferedImage.TYPE_INT_ARGB);
			// component.paint(image.getGraphics());
			BufferedImage image;
			try {
				if (component instanceof JComponent)
					image = ScreenImage.createImage((JComponent) component);
				else
					image = ScreenImage.createImage(component);
				images.add(image);
			} catch (final AWTException e) {
				e.printStackTrace();
			}
		}
		return images;
	}
}
