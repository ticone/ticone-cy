package dk.sdu.imada.ticone.util;

import java.util.Collection;
import java.util.Objects;

import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;

import dk.sdu.imada.ticone.table.TiconeCytoscapeTable;

public class TableIdMapMethod extends IdMapMethod {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1228923447934943376L;
	protected TiconeCytoscapeTable idMapTable;
	protected String idMapTableName;
	protected String keyColumnName;
	protected String valueColumnName;

	@Override
	public boolean equals(Object obj) {
		boolean equals = super.equals(obj);
		if (!equals)
			return false;
		if (obj instanceof TableIdMapMethod) {
			TableIdMapMethod other = (TableIdMapMethod) obj;
			return Objects.equals(idMapTable, other.idMapTable) && Objects.equals(idMapTableName, other.idMapTableName)
					&& Objects.equals(keyColumnName, other.keyColumnName)
					&& Objects.equals(valueColumnName, other.valueColumnName);
		}
		return false;
	}

	@Override
	public IIdMapMethod copy() {
		final TableIdMapMethod copy = new TableIdMapMethod();
		copy.idMapTable = this.idMapTable;
		copy.idMapTableName = idMapTableName;
		copy.keyColumnName = keyColumnName;
		copy.valueColumnName = valueColumnName;
		return copy;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), idMapTable, idMapTableName, keyColumnName, valueColumnName);
	}

	public void setIdMapTable(final CyTable idMapTable) {
		this.idMapTable = new TiconeCytoscapeTable(idMapTable);
		this.idMapTableName = idMapTable.getTitle();
	}

	public void setKeyColumnName(final String keyColumnName) {
		this.keyColumnName = keyColumnName;
	}

	public String getKeyColumnName() {
		return this.keyColumnName;
	}

	public void setValueColumnName(final String valueColumnName) {
		this.valueColumnName = valueColumnName;
	}

	public String getValueColumnName() {
		return this.valueColumnName;
	}

	public CyTable getIdMapTable() {
		return this.idMapTable.getCyTable();
	}

	@Override
	public String getAlternativeId(final String objectId) {
		// we assume this returns one row;
		Object castObjId = objectId;
		if (this.getIdMapTable().getColumn(this.keyColumnName).getType().isAssignableFrom(Integer.class)) {
			try {
				castObjId = Integer.parseInt(objectId);
			} catch (final Exception e) {
				// we ignore the exception and use the original string as key
			}
		}
		final Collection<CyRow> rows = this.getIdMapTable().getMatchingRows(this.keyColumnName, castObjId);
		if (rows.size() > 0)
			return this.getIdMapTable().getMatchingRows(this.keyColumnName, castObjId).iterator().next()
					.get(this.valueColumnName, String.class);
		return "";
	}

}
