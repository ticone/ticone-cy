package dk.sdu.imada.ticone.util;

import java.awt.Component;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.CySwingApplication;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;
import org.cytoscape.model.CyTableManager;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.VisualClusteringManager;
import dk.sdu.imada.ticone.clustering.filter.IFilter;
import dk.sdu.imada.ticone.gui.IKPMResultPanel;
import dk.sdu.imada.ticone.gui.TiconeClusteringResultsPanel;
import dk.sdu.imada.ticone.gui.TiconeComparisonResultsPanel;
import dk.sdu.imada.ticone.gui.TiconeConnectivityResultsPanel;
import dk.sdu.imada.ticone.gui.TiconePanel;
import dk.sdu.imada.ticone.gui.TiconeResultPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer.CHART_Y_LIMITS_TYPE;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.panels.comparison.TiconeClusteringComparisonResultPanel;
import dk.sdu.imada.ticone.gui.panels.connectivity.TiconeClusteringConnectivityResultPanel;
import dk.sdu.imada.ticone.io.ILoadDataMethod;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;

/**
 * @author Christian Wiwie
 * @author Christian Nørskov
 * @since 5/26/15.
 */
public class GUIUtility {

	private static VisualClusteringManager visualClusteringManager = new VisualClusteringManager();
	private static WeakReference<TiconePanel> ticonePanel;
	private static WeakReference<TiconeClusteringResultsPanel> ticoneClusteringResultsPanel;
	private static WeakReference<TiconeComparisonResultsPanel> ticoneComparisonResultsPanel;
	private static WeakReference<TiconeConnectivityResultsPanel> ticoneConnectivityResultsPanel;
	private static List<WeakReference<TiconeResultPanel>> ticoneResultPanels = new ArrayList<>();

	public enum TICONE_RESULTS_TAB {
		CLUSTERINGS, COMPARISONS, CONNECTIVITY
	}

	public static void updateGraphPanel(final TiconeClusteringResultPanel resultPanel)
			throws InterruptedException, TiconeUnloadingException {
		GUIUtility.updateGraphTable(resultPanel);
		GUIUtility.setVisualizeButtonEnabled(true);
		GUIUtility.updateComputeButtonString(false);
		GUIUtility.updateClusterChartLimitType(resultPanel.getClusteringResult().getChartLimits());
	}

	public static TiconePanel getTiconePanel() throws TiconeUnloadingException {
		if (ticonePanel == null || ticonePanel.isEnqueued())
			throw new TiconeUnloadingException();
		return ticonePanel.get();
	}

	public static TiconeClusteringResultsPanel getTiconeClusteringsResultsPanel() throws TiconeUnloadingException {
		if (ticoneClusteringResultsPanel == null || ticoneClusteringResultsPanel.isEnqueued())
			throw new TiconeUnloadingException();
		return ticoneClusteringResultsPanel.get();
	}

	public static TiconeComparisonResultsPanel getTiconeComparisonResultsPanel() throws TiconeUnloadingException {
		if (ticoneComparisonResultsPanel == null || ticoneComparisonResultsPanel.isEnqueued())
			throw new TiconeUnloadingException();
		return ticoneComparisonResultsPanel.get();
	}

	public static TiconeConnectivityResultsPanel getTiconeConnectivityResultsPanel() throws TiconeUnloadingException {
		if (ticoneConnectivityResultsPanel == null || ticoneConnectivityResultsPanel.isEnqueued())
			throw new TiconeUnloadingException();
		return ticoneConnectivityResultsPanel.get();
	}

	public static void setGraphTabInFocus() throws TiconeUnloadingException {
		getTiconePanel().setGraphTabInFocus();
	}

	public static void resetAction() throws TiconeUnloadingException {
		getTiconePanel().getDataFormPanel().resetAction();
	}

	public static void setTiconePanel(final TiconePanel ticonePanel) {
		GUIUtility.ticonePanel = new WeakReference<>(ticonePanel);
	}

	public static void setTiconeClusteringResultsPanel(final TiconeClusteringResultsPanel ticoneResultsPanel) {
		GUIUtility.ticoneClusteringResultsPanel = new WeakReference<>(ticoneResultsPanel);
	}

	public static void setTiconeComparisonResultsPanel(
			final TiconeComparisonResultsPanel ticoneComparisonResultsPanel) {
		GUIUtility.ticoneComparisonResultsPanel = new WeakReference<>(ticoneComparisonResultsPanel);
	}

	public static void setTiconeConnectivityResultsPanel(
			final TiconeConnectivityResultsPanel ticoneConnectivityResultsPanel) {
		GUIUtility.ticoneConnectivityResultsPanel = new WeakReference<>(ticoneConnectivityResultsPanel);
	}

	public static void addTiconeResultsPanel(final TiconeResultPanel ticoneResultsPanel) {
		GUIUtility.ticoneResultPanels.add(new WeakReference<>(ticoneResultsPanel));
	}

	public static void removeTiconeResultsPanel(final TiconeResultPanel resultPanel) {
		if (resultPanel instanceof TiconeClusteringResultPanel)
			((TiconeClusteringResultPanel) resultPanel).onRemove();
		GUIUtility.ticoneResultPanels.removeIf(ref -> ref.get().equals(resultPanel));
	}

	public static List<TiconeResultPanel> getTiconeResultPanels() {
		return ticoneResultPanels.stream().filter(r -> !r.isEnqueued()).map(r -> r.get()).collect(Collectors.toList());
	}

	public static void disableGraphTab() throws TiconeUnloadingException {
		getTiconePanel().updateGraphTab(false);
	}

	public static CyNetwork getSelectedNetwork() throws TiconeUnloadingException {
		return getTiconePanel().getDataFormPanel().getSelectedNetwork();
	}

	public static final int INITIAL_CLUSTER_PAMK = 0;
	public static final int INITIAL_CLUSTER_TRANSCLUST = 1;
	public static final int INITIAL_CLUSTER_CLARA = 2;
	public static final int INITIAL_CLUSTER_STEM = 3;
	public static final int INITIAL_CLUSTER_KMEANS = 4;

	public static double getPairwiseSimilarityThreshold() throws NumberFormatException, TiconeUnloadingException {
		return getTiconePanel().getDataFormPanel().getPairwiseSimilarityThreshold();
	}

	/**
	 * Discretization panel
	 * 
	 * @throws TiconeUnloadingException
	 */
	public static void updateDiscretizationSlider() throws InterruptedException, TiconeUnloadingException {
		getTiconePanel().getDataFormPanel().updateDiscretizationSlider();
	}

	/**
	 * Preprocessing
	 */
	public static boolean isRemoveVarianceButtonSelected() throws TiconeUnloadingException {
		return getTiconePanel().getDataFormPanel().isRemoveVarianceButtonSelected();
	}

	public static double getLowVarianceThreshold() throws InterruptedException, TiconeUnloadingException {
		return getTiconePanel().getDataFormPanel().getLowVarianceThreshold();
	}

	public static String getRemoveLowVariance() throws TiconeUnloadingException {
		return getTiconePanel().getDataFormPanel().getRemoveLowVarianceOption();
	}

	public static boolean isRemoveLeastConservedButtonSelected() throws TiconeUnloadingException {
		return getTiconePanel().getDataFormPanel().isRemoveLeastConservedButtonSelected();
	}

	public static String getRemoveLeastConservedOption() throws TiconeUnloadingException {
		return getTiconePanel().getDataFormPanel().getRemoveLeastConservedOption();
	}

	public static ISimilarityValue getDissagreeingThreshold() throws InterruptedException, TiconeUnloadingException {
		return getTiconePanel().getDataFormPanel().getDissagreeingThreshold();
	}

	public static void setVisualizeButtonEnabled(final boolean enabled) throws TiconeUnloadingException {
		getTiconePanel().getKpmFormPanel().setVisualizeButtonEnabled(enabled);
	}

	public static void updateComputeButtonString(final boolean firstIteration) throws TiconeUnloadingException {
		getTiconePanel().getClustersTabPanel().updateComputeButtonString(firstIteration);
	}

	public static void updateClusterChartLimitType(final CHART_Y_LIMITS_TYPE limitType)
			throws TiconeUnloadingException {
		getTiconePanel().getClustersTabPanel().updateClusterChartLimitType(limitType);
	}

	public static void updateClusterFilter(final IFilter<ICluster> clusterFilter) throws TiconeUnloadingException {
		getTiconePanel().getClustersTabPanel().updateClusterFilter(clusterFilter);
	}

	public static void showActionPanel(final TiconeClusteringResultPanel resultPanel) {
		resultPanel.getClustersTabPanel().showActionPanel();
	}

	public static void hideActionPanel(final TiconeClusteringResultPanel resultPanel) {
		resultPanel.getClustersTabPanel().hideActionPanel();
	}

	public static String getSelectedTableName() throws TiconeUnloadingException {
		return getTiconePanel().getDataFormPanel().getSelectedTable();
	}

	public static CyTable getSelectedTable(final CyTableManager cyTableManager) throws TiconeUnloadingException {
		final String name = getSelectedTableName();
		if (name == null)
			return null;
		final Set<CyTable> tables = cyTableManager.getAllTables(true);
		final Iterator<CyTable> tableIterator1 = tables.iterator();
		while (tableIterator1.hasNext()) {
			final CyTable cyTable = tableIterator1.next();
			if (cyTable.getTitle().equals(name)) {
				return cyTable;
			}
		}
		return null;
	}

	public static String getTimeSeriesTextString() throws TiconeUnloadingException {
		return getTiconePanel().getDataFormPanel().getTimeSeriesTextString();
	}

	public static ILoadDataMethod getLoadDataMethod() throws TiconeUnloadingException {
		return getTiconePanel().getDataFormPanel().getLoadDataMethod();
	}

	public static int getNumberOfTables() throws TiconeUnloadingException {
		return getTiconePanel().getDataFormPanel().getNumberOfTables();
	}

	public static boolean getChangeColumnMapping() throws TiconeUnloadingException {
		return getTiconePanel().getDataFormPanel().getChangeColumnMapping();
	}

	/**
	 * GraphTablePanel
	 * 
	 * @throws InterruptedException
	 */
	// public static GraphTablePanel graphTablePanel;

	// public static void setGraphTablePanel(GraphTablePanel graphTablePanel) {
	// GUIUtility.graphTablePanel = graphTablePanel;
	// }

	public static void updateGraphTable(final TiconeClusteringResultPanel p) throws InterruptedException {
		final IClusterStatusMapping patternStatusMapping = p.getClusteringResult().getClusterStatusMapping();
		p.getClustersTabPanel().getClusterChartTablePanel().updateGraphTable(patternStatusMapping);
	}

	public static Map<IClusters, Map<String, Map<String, Object>>> getSelectedKpmObjects(
			final IKPMResultPanel resultPanel) {
		return resultPanel.getSelectedKpmObjects();
	}

	public static int getCurrentlySelectedClusteringResultIndex() throws TiconeUnloadingException {
		return getTiconeClusteringsResultsPanel().getSelectedIndex();
	}

	public static int getCurrentlySelectedConnectivityResultIndex() throws TiconeUnloadingException {
		return getTiconeConnectivityResultsPanel().getSelectedIndex();
	}

	public static int getCurrentlySelectedComparisonResultIndex() throws TiconeUnloadingException {
		return getTiconeComparisonResultsPanel().getSelectedIndex();
	}

	public static TiconeResultPanel getCurrentlySelectedResultPanel() throws TiconeUnloadingException {
		final TICONE_RESULTS_TAB tab = getCurrentlySelectedResultsTab();
		if (tab.equals(TICONE_RESULTS_TAB.CLUSTERINGS)) {
			final int ind = getCurrentlySelectedClusteringResultIndex();
			return ind > -1 ? (TiconeClusteringResultPanel) getTiconeClusteringsResultsPanel().getComponentAt(ind)
					: null;
		} else if (tab.equals(TICONE_RESULTS_TAB.COMPARISONS)) {
			final int ind = getCurrentlySelectedComparisonResultIndex();
			return ind > -1
					? (TiconeClusteringComparisonResultPanel) getTiconeComparisonResultsPanel().getComponentAt(ind)
					: null;
		} else {
			final int ind = getCurrentlySelectedConnectivityResultIndex();
			return ind > -1
					? (TiconeClusteringConnectivityResultPanel) getTiconeConnectivityResultsPanel().getComponentAt(ind)
					: null;
		}
	}

	public static CyNetwork getCurrentlyVisualizedNetwork() {
		return ServiceHelper.getService(CyApplicationManager.class).getCurrentNetwork();
	}

	public static TiconeClusteringResultPanel getCurrentlySelectedClusteringResultPanel()
			throws TiconeUnloadingException {
		final int ind = getCurrentlySelectedClusteringResultIndex();
		return ind > -1 ? (TiconeClusteringResultPanel) getTiconeClusteringsResultsPanel().getComponentAt(ind) : null;
	}

	public static TiconeClusteringConnectivityResultPanel getCurrentlySelectedConnectivityResultPanel()
			throws TiconeUnloadingException {
		final int ind = getCurrentlySelectedConnectivityResultIndex();
		return ind > -1
				? (TiconeClusteringConnectivityResultPanel) getTiconeConnectivityResultsPanel().getComponentAt(ind)
				: null;
	}

	public static TiconeClusteringComparisonResultPanel getCurrentlySelectedComparisonResultPanel()
			throws TiconeUnloadingException {
		final int ind = getCurrentlySelectedComparisonResultIndex();
		return ind > -1 ? (TiconeClusteringComparisonResultPanel) getTiconeComparisonResultsPanel().getComponentAt(ind)
				: null;
	}

	public static TICONE_RESULTS_TAB getCurrentlySelectedResultsTab() {
		final Component c = ServiceHelper.getService(CySwingApplication.class).getCytoPanel(CytoPanelName.EAST)
				.getSelectedComponent();
		if (c instanceof TiconeClusteringResultsPanel)
			return TICONE_RESULTS_TAB.CLUSTERINGS;
		else if (c instanceof TiconeComparisonResultsPanel)
			return TICONE_RESULTS_TAB.COMPARISONS;
		else if (c instanceof TiconeConnectivityResultsPanel)
			return TICONE_RESULTS_TAB.CONNECTIVITY;
		return null;
	}

	/**
	 * @return the visualClusteringManager
	 */
	public static VisualClusteringManager getVisualClusteringManager() {
		return visualClusteringManager;
	}
}
