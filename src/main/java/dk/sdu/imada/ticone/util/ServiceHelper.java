/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.Collection;
import java.util.Hashtable;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

import dk.sdu.imada.ticone.CyTiconeActivator;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 2, 2019
 *
 */
public final class ServiceHelper {

	public static <S> S getService(final Class<S> c) {
		return getService(c, null);
	}

	public static <S> S getService(final Class<S> c, final String filter) {
		try {
			final BundleContext context = getCytoscapeBundleContext();
			final ServiceReference<S> ref;
			if (filter != null) {
				Collection<ServiceReference<S>> serviceReferences = context.getServiceReferences(c, filter);
				if (serviceReferences.isEmpty())
					return null;
				ref = serviceReferences.iterator().next();
			} else
				ref = context.getServiceReference(c);
			return context.getService(ref);
		} catch (InvalidSyntaxException e) {
			return null;
		}
	}

	public static <S> ServiceRegistration<S> registerService(final Class<S> c, S service) {
		final BundleContext context = getCytoscapeBundleContext();
		return context.registerService(c, service, new Hashtable<>());
	}

	private static BundleContext getCytoscapeBundleContext() {
		return FrameworkUtil.getBundle(CyTiconeActivator.class).getBundleContext();
	}
}
