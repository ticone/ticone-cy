package dk.sdu.imada.ticone.util;

import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.Serializable;

import org.cytoscape.view.presentation.customgraphics.CustomGraphicLayer;
import org.cytoscape.view.presentation.customgraphics.ImageCustomGraphicLayer;

public class MyImageCustomGraphicLayer implements ImageCustomGraphicLayer, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9172359611465650476L;
	private BufferedImage bufferedImage;
	private Rectangle2D rectangle2D;
	private final int x;
	private final int y;
	private TexturePaint paint;

	public MyImageCustomGraphicLayer(final BufferedImage bufferedImage, final int x, final int y) {
		this.bufferedImage = bufferedImage;
		this.x = x;
		this.y = y;
	}

	public BufferedImage getBufferedImage() {
		return this.bufferedImage;
	}

	public void setBufferedImage(final BufferedImage bufferedImage) {
		this.bufferedImage = bufferedImage;
	}

	public void convertBufferedImageToRectangle() {
		if (rectangle2D == null)
			this.rectangle2D = new Rectangle(this.x, this.y, this.bufferedImage.getWidth(),
					this.bufferedImage.getHeight());
	}

	@Override
	public Rectangle2D getBounds2D() {
		return this.rectangle2D;
	}

	@Override
	public TexturePaint getPaint(final Rectangle2D rectangle2D) {
		if (paint == null)
			paint = new TexturePaint(this.getBufferedImage(), new Rectangle(this.x, this.y,
					this.getBufferedImage().getWidth(), this.getBufferedImage().getHeight()));
		return paint;
	}

	@Override
	public CustomGraphicLayer transform(final AffineTransform affineTransform) {
		return this;
	}
}