package dk.sdu.imada.ticone.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.CyNetworkViewFactory;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskManager;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusterSet;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair.ClusterPairsFactory;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.connectivity.ConnectivityResult;
import dk.sdu.imada.ticone.connectivity.ConnectivityResultWrapper;
import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObjectSet;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;
import dk.sdu.imada.ticone.network.NetworkUtil;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork.TiconeCytoscapeNetworkEdge;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork.TiconeCytoscapeNetworkNode;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetworkFactory;
import dk.sdu.imada.ticone.network.TiconeEdgeType;
import dk.sdu.imada.ticone.network.TiconeNetwork.TiconeNetworkEdge;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.similarity.ICompositeSimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityValuesAllPairs;
import dk.sdu.imada.ticone.similarity.ISimpleSimilarityFunction;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.statistics.IPvalue;

/**
 * @author Christian Wiwie
 * @author Christian Norskov
 * @since 3/24/15
 */
public class CyNetworkUtil {

	public static TiconeCytoscapeNetwork createNewNetwork(final String networkName,
			final Map<String, Object> networkAttributes, final List<String> nodeNames,
			final Map<String, List<String>> edgeMapList, final boolean createView, final boolean registerNetwork,
			final Map<String, Map<String, Object>> nodeAttributes) {
		return createNewNetwork(networkName, networkAttributes, nodeNames, edgeMapList, createView, registerNetwork,
				nodeAttributes, null);
	}

	public static TiconeCytoscapeNetwork createNewNetwork(final String networkName,
			final Map<String, Object> networkAttributes, final Iterable<String> nodeNames,
			final Map<String, ? extends Iterable<String>> edgeMapList, final boolean createView,
			final boolean registerNetwork, final Map<String, Map<String, Object>> nodeAttributes,
			final Map<String, Map<String, Map<String, Object>>> edgeAttributes) {
		final TiconeCytoscapeNetworkFactory ticoneNetworkFactory = new TiconeCytoscapeNetworkFactory();
		final TiconeCytoscapeNetwork network = NetworkUtil.createNewNetwork(ticoneNetworkFactory, networkName,
				networkAttributes, nodeNames, edgeMapList, registerNetwork, nodeAttributes, edgeAttributes);

		if (registerNetwork) {
			ServiceHelper.getService(CyNetworkManager.class).addNetwork(network.getCyNetwork());
			ServiceHelper.getService(CyApplicationManager.class).setCurrentNetwork(network.getCyNetwork());
		}

		if (createView) {
			final CyNetworkView networkView = ServiceHelper.getService(CyNetworkViewFactory.class)
					.createNetworkView(network.getCyNetwork());
			final VisualMappingManager visualMappingManager = ServiceHelper.getService(VisualMappingManager.class);
			final VisualStyle defaultStyle = visualMappingManager.getDefaultVisualStyle();
			visualMappingManager.setVisualStyle(defaultStyle, networkView);

			ServiceHelper.getService(CyNetworkViewManager.class).addNetworkView(networkView);
			ServiceHelper.getService(CyApplicationManager.class).setCurrentNetworkView(networkView);

			applyLayoutToShowAllNodes(networkView);
			networkView.updateView();
		}

		return network;
	}

	public static TiconeCytoscapeNetwork getNodeInducedNetwork(final TiconeCytoscapeNetworkFactory networkFactory,
			final TiconeCytoscapeNetwork network, final String networkName, final Map<String, Object> networkAttributes,
			final Map<String, Map<String, Object>> nodesWithAttributes, final boolean createView,
			final boolean registerNetwork, final IIdMapMethod idMapMethod) {
		final TiconeCytoscapeNetwork newNet = NetworkUtil.getNodeInducedNetwork(networkFactory, network, networkName,
				networkAttributes, nodesWithAttributes, registerNetwork, idMapMethod);

		if (registerNetwork) {
			ServiceHelper.getService(CyNetworkManager.class).addNetwork(newNet.getCyNetwork());
			ServiceHelper.getService(CyApplicationManager.class).setCurrentNetwork(newNet.getCyNetwork());
		}

		if (createView) {
			final CyNetworkView networkView = ServiceHelper.getService(CyNetworkViewFactory.class)
					.createNetworkView(newNet.getCyNetwork());
			final VisualMappingManager visualMappingManager = ServiceHelper.getService(VisualMappingManager.class);
			final VisualStyle defaultStyle = visualMappingManager.getDefaultVisualStyle();
			visualMappingManager.setVisualStyle(defaultStyle, networkView);

			ServiceHelper.getService(CyNetworkViewManager.class).addNetworkView(networkView);
			ServiceHelper.getService(CyApplicationManager.class).setCurrentNetworkView(networkView);

			applyLayoutToShowAllNodes(networkView);
			networkView.updateView();
		}

		return newNet;
	}

	private static void applyLayoutToShowAllNodes(final CyNetworkView networkView) {
		final CyLayoutAlgorithmManager layoutAlgorithmManager = ServiceHelper
				.getService(CyLayoutAlgorithmManager.class);
		final CyLayoutAlgorithm layout = layoutAlgorithmManager.getDefaultLayout();
		final Object context = layout.createLayoutContext();

		final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
		final TaskIterator taskIterator = layoutAlgorithmManager.getDefaultLayout().createTaskIterator(networkView,
				context, CyLayoutAlgorithm.ALL_NODE_VIEWS, null);
		taskManager.execute(taskIterator);

	}

	public static void selectNodesOnNetwork(final CyTable nodeTable, final CyNetworkView networkView,
			final CyNetwork network, final Set<String> nodesToSelect, final Boolean select) {
		if (networkView == null || network == null) {
			return;
		}
		final List<CyNode> nodeList = network.getNodeList();
		for (int i = 0; i < nodeList.size(); i++) {
			final CyNode node = nodeList.get(i);
			final String nodeID = nodeTable.getRow(node.getSUID()).get(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME,
					String.class);
			if (nodesToSelect == null || nodesToSelect.contains(nodeID)) {
				network.getRow(node).set(CyNetwork.SELECTED, select);
			}
		}
		networkView.updateView();
	}

	public static void creatNodeAttribute(final String col, final CyNetwork cyNetwork, final Class c) {
		final CyTable nodeTable = cyNetwork.getDefaultNodeTable();
		if (nodeTable.getColumn(col) == null) {
			nodeTable.createColumn(col, c, true);
		}
	}

	// map Nodes by the Name instead of the SUID
	public static Map<String, CyNode> getMappingByID(final CyNetwork cyNetwork) {
		final Map<String, CyNode> map = new HashMap<>();
		final CyTable nodeTable = cyNetwork.getDefaultNodeTable();
		for (final CyNode n : cyNetwork.getNodeList()) {
			final String nodeName = nodeTable.getRow(n.getSUID()).get(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME,
					String.class);
			map.put(nodeName, n);
		}
		return map;
	}

	// set a value for a cell in the cyTable
	public static void setNodeRowColumn(final CyNetwork cyNetwork, final CyNode cyNode, final String col,
			final String value, final Class c) {
		final CyTable nodeTable = cyNetwork.getDefaultNodeTable();
		final CyRow row = nodeTable.getRow(cyNode.getSUID());
		if (c.equals(String.class)) {
			row.set(col, value);
			//
		} else if (c.equals(Integer.class)) {
			row.set(col, Integer.parseInt(value));
			//
		} else if (c.equals(Double.class)) {
			row.set(col, Double.parseDouble(value));
			//
		} else if (c.equals(Boolean.class)) {
			row.set(col, Boolean.valueOf(value));
		}
	}

	public static Object getValue(final CyNetwork cyNetwork, final CyNode cyNode, final String col, final Class c) {
		final CyTable nodeTable = cyNetwork.getDefaultNodeTable();
		final CyRow row = nodeTable.getRow(cyNode.getSUID());
		return row.get(col, c);
	}

	// map edges by node pairs source -> target
	public static Map<CyNode[], CyEdge> getEdgeByNodes(final CyNetwork cyNetwork) {
		final Map<CyNode[], CyEdge> m = new HashMap<>();
		for (final CyEdge e : cyNetwork.getEdgeList()) {
			final CyNode[] pair = new CyNode[2];
			pair[0] = e.getSource();
			pair[1] = e.getTarget();
			m.put(pair, e);
		}
		//
		return m;
	}

	public static <TICONE_NODE extends ITiconeNetworkNode> List<TICONE_NODE> getToBeColoredNodes(
			final ITiconeNetwork<TICONE_NODE, ? extends TiconeNetworkEdge> network,
			final Map<String, ISimilarityValue> toColor) {
		final List<TICONE_NODE> nodes = new ArrayList<>();
		for (final TICONE_NODE node : network.getNodeList()) {
			final String nodeName = node.getName();
			if (toColor.containsKey(nodeName)) {
				nodes.add(node);
			}
		}
		return nodes;
	}

	public static <TICONE_NODE extends ITiconeNetworkNode> List<TICONE_NODE> getToBeColoredNodes(
			final ITiconeNetwork<TICONE_NODE, ? extends TiconeNetworkEdge> TiCoNENetwork, final Set<String> toColor) {
		final List<TICONE_NODE> nodes = new ArrayList<>();

		for (final TICONE_NODE node : TiCoNENetwork.getNodeList()) {
			final String nodeName = node.getName();
			if (toColor.contains(nodeName)) {
				nodes.add(node);
			}
		}
		return nodes;
	}

	public static CyNode getNodeByName(final CyNetwork net, final CyTable table, final String name) {
		final Set<CyRow> matchingRows = new HashSet<>(
				table.getMatchingRows(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME, name));
		// TODO: throw exception?
		if (matchingRows.isEmpty())
			return null;
		// TODO: throw exception?
		if (matchingRows.size() > 1)
			return null;
		final CyRow row = matchingRows.iterator().next();
		final String primaryKeyColname = table.getPrimaryKey().getName();
		final Long nodeId = row.get(primaryKeyColname, Long.class);
		if (nodeId == null)
			return null;
		final CyNode node = net.getNode(nodeId);
		return node;
	}

	public static boolean isNetworkForCurrentlyVisibleClustering(final CyNetwork network)
			throws TiconeUnloadingException {
		if (network == null)
			return false;
		final TiconeClusteringResultPanel clusteringResultPanel = GUIUtility
				.getCurrentlySelectedClusteringResultPanel();
		if (clusteringResultPanel == null)
			return false;

		// get corresponding clustering of this network
		final Long clusteringId = network.getRow(network).get("clustering_id", Long.class);
		final Integer clusteringIteration = network.getRow(network).get("clustering_iteration", Integer.class);
		if (clusteringId == null || clusteringIteration == null)
			return false;

		final ClusterObjectMapping visibleClustering = clusteringResultPanel.getResult().getClusteringProcess()
				.getLatestClustering();
		final int visibleIteration = clusteringResultPanel.getResult().getClusterHistory().getIterationNumber();

		return visibleClustering.getClusteringId() == clusteringId.longValue()
				&& visibleIteration == clusteringIteration.intValue();
	}

	public static boolean isObjectSimilarityNetwork(final CyNetwork network) {
		return network.getRow(network).get(CyNetwork.NAME, String.class).startsWith("Object Similarity Network");
	}

	public static boolean isTotalObjectSimilarityNetwork(final CyNetwork network) {
		final Boolean isTotal = network.getRow(network)
				.get(NetworkUtil.NETWORK_ATTRIBUTE_OBJECT_SIMILARITY_IS_TOTAL_NETWORK, Boolean.class);
		if (isTotal == null)
			return false;
		return isTotal;
	}

	public static boolean isClusterSimilarityNetwork(final CyNetwork network) {
		return network.getRow(network).get(CyNetwork.NAME, String.class).startsWith("Cluster Similarity Network");
	}

	public static void selectNodesInNetworkForSelectedClusters(final CyNetwork network,
			final IClusters selectedClusters) {
		final Set<Integer> clusterNumbers = selectedClusters.stream().map(c -> c.getClusterNumber())
				.collect(Collectors.toSet());
		final CyTable defaultNodeTable = network.getDefaultNodeTable();
		List<CyRow> allRows = defaultNodeTable.getAllRows();
		allRows.stream().forEach(row -> {
			boolean isSelected = clusterNumbers
					.contains(row.get(NetworkUtil.NODE_ATTRIBUTE_CLUSTER_NUMBER, Integer.class));
			row.set(CyNetwork.SELECTED, isSelected);
		});
	}

	public static IClusters getClustersForSelectedNodesInNetwork(final CyNetwork network,
			final IClusterObjectMapping visibleClustering) {
		final List<CyNode> selectedNodes = org.cytoscape.model.CyTableUtil.getNodesInState(network, "selected", true);
		return getClustersForSelectedNodesInNetwork(network, selectedNodes, visibleClustering);
	}

	public static IClusters getClustersForSelectedNodesInNetwork(final CyNetwork network,
			final Collection<CyNode> selectedNodes, final IClusterObjectMapping visibleClustering) {
		final Set<Integer> selectedClusterNumbers = new HashSet<>();
		for (CyNode node : selectedNodes) {
			if (isClusterNode(network, node)) {
				int clusterNumber = network.getDefaultNodeTable().getRow(node.getSUID())
						.get(NetworkUtil.NODE_ATTRIBUTE_CLUSTER_NUMBER, Integer.class);
				selectedClusterNumbers.add(clusterNumber);
			}
		}

		IClusters selectedClusters = new ClusterSet();
		for (ICluster c : visibleClustering.getClusters()) {
			if (selectedClusterNumbers.contains(c.getClusterNumber()))
				selectedClusters.add(c);
		}
		return selectedClusters;
	}

	public static ITimeSeriesObjects getObjectsForSelectedNodesInNetwork(final CyNetwork network,
			final IClusterObjectMapping visibleClustering) {
		final List<CyNode> selectedNodes = org.cytoscape.model.CyTableUtil.getNodesInState(network, "selected", true);
		return getObjectsForSelectedNodesInNetwork(network, selectedNodes, visibleClustering);
	}

	public static ITimeSeriesObjects getObjectsForSelectedNodesInNetwork(final CyNetwork network,
			final Collection<CyNode> selectedNodes, final IClusterObjectMapping visibleClustering) {
		final Set<String> selectedObjectIds = new HashSet<>();
		for (CyNode node : selectedNodes) {
			if (isObjectNode(network, node)) {
				String nodeId = network.getDefaultNodeTable().getRow(node.getSUID())
						.get(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME, String.class);
				selectedObjectIds.add(nodeId);
			}
		}

		final ITimeSeriesObjects selectedObjects = new TimeSeriesObjectSet();
		for (ITimeSeriesObject o : visibleClustering.getAllObjects()) {
			if (selectedObjectIds.contains(o.getName()))
				selectedObjects.add(o);
		}
		return selectedObjects;
	}

	public static TiconeCytoscapeNetwork createDirectedConnectivityNetwork(final ConnectivityResultWrapper wrapper) {
		final ConnectivityResult result = wrapper.getDirectedConnectivityResult();
		final List<String> nodeList = new ArrayList<>();
		for (final IClusterPair clusterPair : result.getEdgeCount().keySet()) {
			if (!nodeList.contains(clusterPair.getFirst().getName()))
				nodeList.add(clusterPair.getFirst().getName());
			if (!nodeList.contains(clusterPair.getSecond().getName()))
				nodeList.add(clusterPair.getSecond().getName());
		}
		final Map<String, Map<String, Object>> nodeAttributes = new HashMap<>();
		final IClusterObjectMapping pom = wrapper.getClusteringResult()
				.getClusteringOfIteration(wrapper.getClusteringIteration());
		for (final ICluster c : pom.getClusters()) {
			final String nodeId = c.getName();
			if (nodeList.contains(nodeId)) {
				if (!nodeAttributes.containsKey(nodeId))
					nodeAttributes.put(nodeId, new HashMap<String, Object>());
				nodeAttributes.get(nodeId).put("nodeLabel", String.format("%s (%d)", nodeId, c.getObjects().size()));
			}
		}

		final Map<String, List<String>> edgeMapList = new HashMap<>();
		for (final String node : nodeList)
			edgeMapList.put(node, nodeList);
		final Map<String, Map<String, Map<String, Object>>> edgeAttributes = new HashMap<>();
		for (final IClusterPair patternPair : result.getEdgeCount().keySet()) {
			final String sourceName = patternPair.getFirst().getName();
			final String targetName = patternPair.getSecond().getName();
			if (!edgeAttributes.containsKey(sourceName))
				edgeAttributes.put(sourceName, new HashMap<String, Map<String, Object>>());
			if (!edgeAttributes.get(sourceName).containsKey(targetName))
				edgeAttributes.get(sourceName).put(targetName, new HashMap<String, Object>());
			edgeAttributes.get(sourceName).get(targetName).put("Source cluster", sourceName);
			edgeAttributes.get(sourceName).get(targetName).put("Target cluster", targetName);
			edgeAttributes.get(sourceName).get(targetName).put("count", result.getEdgeCount().get(patternPair));
			edgeAttributes.get(sourceName).get(targetName).put("expected",
					result.getExpectedEdgeCount().get(patternPair));
			edgeAttributes.get(sourceName).get(targetName).put("log-odds",
					result.getEdgeCountEnrichment().get(patternPair));
			edgeAttributes.get(sourceName).get(targetName).put("log-odds",
					result.getEdgeCountEnrichment().get(patternPair));

			final int observed = result.getEdgeCount().get(patternPair);
			final int expected = (int) Math.round(result.getExpectedEdgeCount().get(patternPair));
			final float OR = (float) observed / expected;

			edgeAttributes.get(sourceName).get(targetName).put("edgeLabel",
					String.format("%d/%d≈%.2f", observed, expected, OR));
		}
		final TiconeCytoscapeNetwork directedConnectivityNetwork = CyNetworkUtil.createNewNetwork(
				String.format("Connectivity %d (Directed)", wrapper.getConnectivityNumber()), new HashMap<>(), nodeList,
				edgeMapList, true, true, nodeAttributes, edgeAttributes);

		final VisualStyle vs = ((TiconeCytoscapeClusteringResult) result.getUtil()).getConnectivityVisualStyle(wrapper,
				true);

		final Collection<CyNetworkView> views = ServiceHelper.getService(CyNetworkViewManager.class)
				.getNetworkViews(directedConnectivityNetwork.getCyNetwork());
		for (final CyNetworkView view : views)
			vs.apply(view);

		ServiceHelper.getService(VisualMappingManager.class).setCurrentVisualStyle(vs);

		wrapper.addOnDestroyListener(new ITiconeResultDestroyedListener() {

			@Override
			public void ticoneResultDestroyed(TiconeResultDestroyedEvent event) {
				directedConnectivityNetwork.destroy();
			}
		});

		return directedConnectivityNetwork;
	}

	public static void addPvaluesToDirectedConnectivityNetwork(final ConnectivityResultWrapper wrapper,
			final TiconeCytoscapeNetwork directedConnectivityNetwork) {
		final Map<IClusterPair, IPvalue> pvals = wrapper.getConnectivityPValueResult().getDirectedEdgeCountPvalues();

		directedConnectivityNetwork.createEdgeAttribute("p", Double.class, false);

		for (final IClusterPair clusterPair : pvals.keySet()) {
			final TiconeCytoscapeNetworkNode n1 = directedConnectivityNetwork.getNode(clusterPair.getFirst().getName());
			final TiconeCytoscapeNetworkNode n2 = directedConnectivityNetwork
					.getNode(clusterPair.getSecond().getName());
			final double pval = pvals.get(clusterPair).getDouble();
			Set<TiconeCytoscapeNetworkEdge> edges;
			if (n1.equals(n2))
				edges = directedConnectivityNetwork.getEdges(n1, n2, TiconeEdgeType.SELF);
			else
				edges = directedConnectivityNetwork.getEdges(n1, n2, TiconeEdgeType.OUTGOING);
			if (edges.isEmpty())
				continue;
			final TiconeCytoscapeNetworkEdge edge = edges.iterator().next();
			if (edge != null && edge.getSource().equals(n1) && edge.getTarget().equals(n2)) {
				directedConnectivityNetwork.setEdgeAttribute(edge, "p", pval);

				directedConnectivityNetwork.setEdgeAttribute(edge, "edgeLabel", String.format("%s p:%.2f",
						directedConnectivityNetwork.getValue(edge, "edgeLabel", String.class), pval));
			}
			// if the edge is not directed the correct we use the other cluster
			// pair to annotate it
			// shouldn't happen here in the directed network case, but we leave
			// it in anyways
		}
	}

	public static void addPvaluesToUndirectedConnectivityNetwork(final ConnectivityResultWrapper wrapper,
			final TiconeCytoscapeNetwork undirectedConnectivityNetwork) {
		final Map<IClusterPair, IPvalue> pvals = wrapper.getConnectivityPValueResult().getUndirectedEdgeCountPvalues();

		undirectedConnectivityNetwork.createEdgeAttribute("p", Double.class, false);

		for (final IClusterPair clusterPair : pvals.keySet()) {
			final TiconeCytoscapeNetworkNode n1 = undirectedConnectivityNetwork
					.getNode(clusterPair.getFirst().getName());
			final TiconeCytoscapeNetworkNode n2 = undirectedConnectivityNetwork
					.getNode(clusterPair.getSecond().getName());
			final double pval = pvals.get(clusterPair).getDouble();
			final Set<TiconeCytoscapeNetworkEdge> edges = undirectedConnectivityNetwork.getEdges(n1, n2,
					TiconeEdgeType.ANY);
			if (edges.isEmpty())
				continue;
			final TiconeCytoscapeNetworkEdge edge = edges.iterator().next();
			if (edge.getSource().equals(n1) && edge.getTarget().equals(n2)) {
				final Object fc = undirectedConnectivityNetwork.getValue(edge, "log-odds", Object.class);
				if (fc != null && !fc.toString().equals("")) {
					undirectedConnectivityNetwork.setEdgeAttribute(edge, "p", pval);

					undirectedConnectivityNetwork.setEdgeAttribute(edge, "edgeLabel", String.format("%s p:%.2f",
							undirectedConnectivityNetwork.getValue(edge, "edgeLabel", String.class), pval));
				}
			}
			// if the edge is not directed the correct we use the other cluster
			// pair to annotate it
		}
	}

	public static TiconeCytoscapeNetwork createUndirectedConnectivityNetwork(final ConnectivityResultWrapper wrapper) {
		final ConnectivityResult result = wrapper.getUndirectedConnectivityResult();
		final Map<IClusterPair, Integer> counts = result.getEdgeCount();
		final Map<IClusterPair, Double> exp = result.getExpectedEdgeCount();
		final Map<IClusterPair, Double> enrichments = result.getEdgeCountEnrichment();

		final List<String> nodeList = new ArrayList<>();
		for (final IClusterPair clusterPair : result.getEdgeCount().keySet()) {
			if (!nodeList.contains(clusterPair.getFirst().getName()))
				nodeList.add(clusterPair.getFirst().getName());
			if (!nodeList.contains(clusterPair.getSecond().getName()))
				nodeList.add(clusterPair.getSecond().getName());
		}

		final Map<String, Map<String, Object>> nodeAttributes = new HashMap<>();
		final IClusterObjectMapping pom = wrapper.getClusteringResult()
				.getClusteringOfIteration(wrapper.getClusteringIteration());
		for (final ICluster c : pom.getClusters()) {
			final String nodeId = c.getName();
			if (nodeList.contains(nodeId)) {
				if (!nodeAttributes.containsKey(nodeId))
					nodeAttributes.put(nodeId, new HashMap<String, Object>());
				nodeAttributes.get(nodeId).put("nodeLabel", String.format("%s (%d)", nodeId, c.getObjects().size()));
			}
		}

		final Map<String, List<String>> edgeMapList = new HashMap<>();
		for (final IClusterPair clusterPair : result.getEdgeCount().keySet()) {
			if (clusterPair.getFirst().getClusterNumber() > clusterPair.getSecond().getClusterNumber())
				continue;
			final String clusterId1 = clusterPair.getFirst().getName(), clusterId2 = clusterPair.getSecond().getName();
			if (!nodeList.contains(clusterId1) || !nodeList.contains(clusterId2))
				continue;
			if (!edgeMapList.containsKey(clusterId1))
				edgeMapList.put(clusterId1, new ArrayList<String>());
			edgeMapList.get(clusterId1).add(clusterId2);
		}
		final Map<String, Map<String, Map<String, Object>>> edgeAttributes = new HashMap<>();
		for (final IClusterPair patternPair : counts.keySet()) {
			// insert each pair of clusters only once;
			if (patternPair.getFirst().getClusterNumber() > patternPair.getSecond().getClusterNumber())
				continue;
			final String sourceName = patternPair.getFirst().getName();
			final String targetName = patternPair.getSecond().getName();
			if (!edgeAttributes.containsKey(sourceName))
				edgeAttributes.put(sourceName, new HashMap<String, Map<String, Object>>());
			if (!edgeAttributes.get(sourceName).containsKey(targetName))
				edgeAttributes.get(sourceName).put(targetName, new HashMap<String, Object>());
			edgeAttributes.get(sourceName).get(targetName).put("Cluster 1", sourceName);
			edgeAttributes.get(sourceName).get(targetName).put("Cluster 2", targetName);
			edgeAttributes.get(sourceName).get(targetName).put("count", counts.get(patternPair));
			edgeAttributes.get(sourceName).get(targetName).put("expected", exp.get(patternPair));
			edgeAttributes.get(sourceName).get(targetName).put("log-odds", enrichments.get(patternPair));

			final int observed = result.getEdgeCount().get(patternPair);
			final int expected = (int) Math.round(result.getExpectedEdgeCount().get(patternPair));
			final float OR = (float) observed / expected;

			edgeAttributes.get(sourceName).get(targetName).put("edgeLabel",
					String.format("%d/%d≈%.2f", observed, expected, OR));
		}
		final TiconeCytoscapeNetwork undirectedConnectivityNetwork = CyNetworkUtil.createNewNetwork(
				String.format("Connectivity %d (Undirected)", wrapper.getConnectivityNumber()), new HashMap<>(),
				nodeList, edgeMapList, true, true, nodeAttributes, edgeAttributes);

		final VisualStyle vs = ((TiconeCytoscapeClusteringResult) result.getUtil()).getConnectivityVisualStyle(wrapper,
				false);

		final Collection<CyNetworkView> views = ServiceHelper.getService(CyNetworkViewManager.class)
				.getNetworkViews(undirectedConnectivityNetwork.getCyNetwork());
		for (final CyNetworkView view : views)
			vs.apply(view);

		ServiceHelper.getService(VisualMappingManager.class).setCurrentVisualStyle(vs);

		wrapper.addOnDestroyListener(new ITiconeResultDestroyedListener() {

			@Override
			public void ticoneResultDestroyed(TiconeResultDestroyedEvent event) {
				undirectedConnectivityNetwork.destroy();
			}
		});

		return undirectedConnectivityNetwork;
	}

	public static TiconeCytoscapeNetwork createClusterSimilarityNetwork(
			final TiconeCytoscapeClusteringResult clusteringWrapper,
			final ISimilarityValuesAllPairs<ICluster, ICluster, IClusterPair> similarities,
			final IClusters selectedClusters)
			throws SimilarityCalculationException, SimilarityValuesException, NoComparableSimilarityValuesException,
			InterruptedException, IncompatiblePrototypeComponentException, MissingPrototypeException {
		final int iteration = clusteringWrapper.getClusteringProcess().getHistory().getIterationNumber();

		final List<String> nodeList = new ArrayList<>();
		for (final ICluster c : selectedClusters) {
			if (!nodeList.contains(c.getName()))
				nodeList.add(c.getName());
		}

		final Map<String, Map<String, Object>> nodeAttributes = new HashMap<>();
		for (final ICluster c : selectedClusters) {
			final String nodeId = c.getName();
			if (nodeList.contains(nodeId)) {
				if (!nodeAttributes.containsKey(nodeId))
					nodeAttributes.put(nodeId, new HashMap<String, Object>());
				nodeAttributes.get(nodeId).put("nodeLabel", String.format("%s (%d)", nodeId, c.getObjects().size()));
				nodeAttributes.get(nodeId).put(NetworkUtil.NODE_ATTRIBUTE_CLUSTER_NUMBER, c.getClusterNumber());
				nodeAttributes.get(nodeId).put(NetworkUtil.NODE_ATTRIBUTE_IS_CLUSTER, true);
				nodeAttributes.get(nodeId).put(NetworkUtil.NODE_ATTRIBUTE_IS_OBJECT, false);
			}
		}

		final Map<String, List<String>> edgeMapList = new HashMap<>();
		for (final IClusterPair clusterPair : new ClusterPairsFactory().createList(selectedClusters,
				selectedClusters)) {
			if (clusterPair.getFirst().getClusterNumber() >= clusterPair.getSecond().getClusterNumber())
				continue;
			final String clusterId1 = clusterPair.getFirst().getName(), clusterId2 = clusterPair.getSecond().getName();
			if (!nodeList.contains(clusterId1) || !nodeList.contains(clusterId2))
				continue;
			if (!edgeMapList.containsKey(clusterId1))
				edgeMapList.put(clusterId1, new ArrayList<String>());
			edgeMapList.get(clusterId1).add(clusterId2);
		}
		final Map<String, Map<String, Map<String, Object>>> edgeAttributes = new HashMap<>();
		for (final int[] pairIdx : similarities.pairIndices()) {
			final ICluster c1 = similarities.getObject1(pairIdx[0]);
			if (!selectedClusters.contains(c1))
				continue;
			final ICluster c2 = similarities.getObject2(pairIdx[1]);
			if (!selectedClusters.contains(c2))
				continue;
			// insert each pair of clusters only once;
			if (c1.getClusterNumber() >= c2.getClusterNumber())
				continue;
			final String sourceName = c1.getName();
			final String targetName = c2.getName();
			if (!edgeAttributes.containsKey(sourceName))
				edgeAttributes.put(sourceName, new HashMap<String, Map<String, Object>>());
			if (!edgeAttributes.get(sourceName).containsKey(targetName))
				edgeAttributes.get(sourceName).put(targetName, new HashMap<String, Object>());
			Map<String, Object> attributeMap = edgeAttributes.get(sourceName).get(targetName);
			attributeMap.put("Cluster 1", sourceName);
			attributeMap.put("Cluster 2", targetName);
			ISimilarityValue simValue = similarities.get(pairIdx[0], pairIdx[1]);
			attributeMap.put("similarity", simValue.get());
			if (simValue instanceof ICompositeSimilarityValue) {
				ISimpleSimilarityFunction[] childFunctions = ((ICompositeSimilarityValue) simValue).getChildFunctions();
				for (int i = 0; i < childFunctions.length; i++) {
					final ISimilarityFunction sf = childFunctions[i];
					attributeMap.put(sf.toString(),
							((ICompositeSimilarityValue) simValue).getChildSimilarityValue(i).get());
					attributeMap.put(String.format("%s (scaled)", sf.toString()),
							((ICompositeSimilarityValue) simValue).getScaledChildSimilarityValue(i).get());
				}
			}

			edgeAttributes.get(sourceName).get(targetName).put("edgeLabel",
					String.format("%.2f", similarities.get(pairIdx[0], pairIdx[1]).get()));
		}
		final TiconeCytoscapeNetwork clusterSimilarityNetwork = CyNetworkUtil.createNewNetwork(
				String.format("Cluster Similarity Network (Clustering %d, Iteration %d)",
						clusteringWrapper.getClusteringNumber(), iteration),
				new HashMap<>(), nodeList, edgeMapList, true, true, nodeAttributes, edgeAttributes);

		final CyTable networkTable = clusterSimilarityNetwork.getCyNetwork().getDefaultNetworkTable();
		networkTable.createColumn("clustering_id", Long.class, true);
		networkTable.getAllRows().get(0).set("clustering_id",
				clusteringWrapper.getClusteringProcess().getLatestClustering().getClusteringId());
		networkTable.createColumn("clustering_iteration", Integer.class, true);
		networkTable.getAllRows().get(0).set("clustering_iteration",
				clusteringWrapper.getClusteringProcess().getHistory().getIterationNumber());

		final VisualStyle vs = clusteringWrapper.getClusterSimilarityVisualStyle(iteration, similarities);

		final Collection<CyNetworkView> views = ServiceHelper.getService(CyNetworkViewManager.class)
				.getNetworkViews(clusterSimilarityNetwork.getCyNetwork());
		for (final CyNetworkView view : views)
			vs.apply(view);

		ServiceHelper.getService(VisualMappingManager.class).setCurrentVisualStyle(vs);

		clusteringWrapper.addOnDestroyListener(new ITiconeResultDestroyedListener() {

			@Override
			public void ticoneResultDestroyed(TiconeResultDestroyedEvent event) {
				clusterSimilarityNetwork.destroy();
			}
		});

		return clusterSimilarityNetwork;
	}

	public static TiconeCytoscapeNetwork createObjectClusterSimilarityNetwork(
			final TiconeCytoscapeClusteringResult clusteringWrapper,
			final ISimilarityValuesAllPairs<ITimeSeriesObject, ICluster, IObjectClusterPair> objectClusterSimilarities,
			final ISimilarityValuesAllPairs<ICluster, ICluster, IClusterPair> clusterSimilarities,
			final Iterable<? extends ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair>> objectSimilaritiesIterable,
			final ITimeSeriesObjects selectedObjects)
			throws SimilarityCalculationException, SimilarityValuesException, NoComparableSimilarityValuesException,
			InterruptedException, IncompatiblePrototypeComponentException, MissingPrototypeException {
		final int iteration = clusteringWrapper.getClusteringProcess().getHistory().getIterationNumber();
		final ClusterObjectMapping clustering = clusteringWrapper.getClusteringProcess().getLatestClustering();

		final Set<String> objectNameSet = selectedObjects.stream().map(o -> o.getName()).collect(Collectors.toSet());
		final IClusters clusters = new ClusterSet();
		final Set<String> clusterNameSet = new HashSet<>();

		final Map<String, Object> networkAttributes = new HashMap<>();
		networkAttributes.put(NetworkUtil.NETWORK_ATTRIBUTE_OBJECT_SIMILARITY_IS_TOTAL_NETWORK,
				objectSimilaritiesIterable != null && Iterables.size(objectSimilaritiesIterable) == 1);

		final Map<String, Map<String, Object>> nodeAttributes = new HashMap<>();
		for (final ITimeSeriesObject o : selectedObjects) {
			final String nodeId = o.getName();
			if (objectNameSet.contains(nodeId)) {
				// part of the clustering?
				final Map<ICluster, ISimilarityValue> coefficients = clustering.getSimilarities(o);
				if (coefficients.isEmpty())
					continue;

				// assume one cluster
				final ICluster cluster = coefficients.keySet().iterator().next();
				final int clusterNumber = cluster.getClusterNumber();

				nodeAttributes.putIfAbsent(nodeId, new HashMap<>());
				nodeAttributes.get(nodeId).put("nodeLabel", o.hasAlternativeName() ? o.getAlternativeName() : nodeId);

				nodeAttributes.get(nodeId).put(NetworkUtil.NODE_ATTRIBUTE_CLUSTER_NUMBER, clusterNumber);
				nodeAttributes.get(nodeId).put(NetworkUtil.NODE_ATTRIBUTE_IS_CLUSTER, false);
				nodeAttributes.get(nodeId).put(NetworkUtil.NODE_ATTRIBUTE_IS_OBJECT, true);

				final String clusterNodeId = cluster.getName();
				boolean isNewCluster = clusterNameSet.add(clusterNodeId);
				if (isNewCluster) {
					clusters.add(cluster);
					nodeAttributes.putIfAbsent(clusterNodeId, new HashMap<>());
					nodeAttributes.get(clusterNodeId).put("nodeLabel", clusterNodeId);
					nodeAttributes.get(clusterNodeId).put(NetworkUtil.NODE_ATTRIBUTE_CLUSTER_NUMBER, clusterNumber);
					nodeAttributes.get(clusterNodeId).put(NetworkUtil.NODE_ATTRIBUTE_IS_CLUSTER, true);
					nodeAttributes.get(clusterNodeId).put(NetworkUtil.NODE_ATTRIBUTE_IS_OBJECT, false);
				}
			}
		}

		final Map<String, List<String>> edgeMapList = new HashMap<>();
		final Map<String, Map<String, Map<String, Object>>> edgeAttributes = new HashMap<>();

		if (objectClusterSimilarities != null) {
			for (final int[] pairIdx : objectClusterSimilarities.pairIndices()) {
				final ITimeSeriesObject o = objectClusterSimilarities.getObject1(pairIdx[0]);
				final ICluster c = objectClusterSimilarities.getObject2(pairIdx[1]);
				// only insert edges for selected clusters / objects
				if (!selectedObjects.contains(o) || !clusters.contains(c))
					continue;
				final String objectName = o.getName();
				final String clusterName = c.getName();

				edgeMapList.putIfAbsent(objectName, new ArrayList<String>());
				edgeMapList.get(objectName).add(clusterName);
				edgeAttributes.putIfAbsent(objectName, new HashMap<>());
				edgeAttributes.get(objectName).putIfAbsent(clusterName, new HashMap<>());
				Map<String, Object> attributeMap = edgeAttributes.get(objectName).get(clusterName);
				attributeMap.put("Object", objectName);
				attributeMap.put("Cluster", clusterName);
				ISimilarityValue simValue = objectClusterSimilarities.get(pairIdx[0], pairIdx[1]);
				attributeMap.put("similarity", simValue.get());
				if (simValue instanceof ICompositeSimilarityValue) {
					ISimpleSimilarityFunction[] childFunctions = ((ICompositeSimilarityValue) simValue)
							.getChildFunctions();
					for (int i = 0; i < childFunctions.length; i++) {
						final ISimilarityFunction sf = childFunctions[i];
						attributeMap.put(sf.toString(),
								((ICompositeSimilarityValue) simValue).getChildSimilarityValue(i).get());
						attributeMap.put(String.format("%s (scaled)", sf.toString()),
								((ICompositeSimilarityValue) simValue).getScaledChildSimilarityValue(i).get());
					}
				}

				edgeAttributes.get(objectName).get(clusterName).put("edgeLabel",
						String.format("%.2f", objectClusterSimilarities.get(pairIdx[0], pairIdx[1]).get()));
			}
		}

		if (clusterSimilarities != null) {
			for (final int[] pairIdx : clusterSimilarities.pairIndices()) {
				final ICluster c1 = clusterSimilarities.getObject1(pairIdx[0]);
				final ICluster c2 = clusterSimilarities.getObject2(pairIdx[1]);
				if (c1 == c2)
					continue;
				// only insert edges for selected clusters
				if (!clusters.contains(c1) || !clusters.contains(c2))
					continue;
				final String cluster1Name = c1.getName();
				final String cluster2Name = c2.getName();
				edgeMapList.putIfAbsent(cluster1Name, new ArrayList<String>());
				edgeMapList.get(cluster1Name).add(cluster2Name);
				edgeAttributes.putIfAbsent(cluster1Name, new HashMap<>());
				edgeAttributes.get(cluster1Name).putIfAbsent(cluster2Name, new HashMap<>());
				Map<String, Object> attributeMap = edgeAttributes.get(cluster1Name).get(cluster2Name);
				attributeMap.put("Cluster 1", cluster1Name);
				attributeMap.put("Cluster 2", cluster2Name);
				ISimilarityValue simValue = clusterSimilarities.get(pairIdx[0], pairIdx[1]);
				attributeMap.put("similarity", simValue.get());

				edgeAttributes.get(cluster1Name).get(cluster2Name).put("edgeLabel",
						String.format("%.2f", clusterSimilarities.get(pairIdx[0], pairIdx[1]).get()));
			}
		}

		if (objectSimilaritiesIterable != null) {
			for (ISimilarityValuesAllPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair> objectSimilarities : objectSimilaritiesIterable) {
				for (final int[] pairIdx : objectSimilarities.pairIndices()) {
					final ITimeSeriesObject c1 = objectSimilarities.getObject1(pairIdx[0]);
					if (!selectedObjects.contains(c1))
						continue;
					final ITimeSeriesObject c2 = objectSimilarities.getObject2(pairIdx[1]);
					if (!selectedObjects.contains(c2))
						continue;
					if (c1 == c2)
						continue;
					final String sourceName = c1.getName();
					final String targetName = c2.getName();
					// only insert edges that are selected
					if (!objectNameSet.contains(sourceName) || !objectNameSet.contains(targetName))
						continue;
					// insert each pair of clusters only once;
					if (sourceName.compareTo(targetName) >= 0)
						continue;

					edgeMapList.putIfAbsent(sourceName, new ArrayList<String>());
					edgeMapList.get(sourceName).add(targetName);
					edgeAttributes.putIfAbsent(sourceName, new HashMap<String, Map<String, Object>>());
					edgeAttributes.get(sourceName).putIfAbsent(targetName, new HashMap<String, Object>());
					Map<String, Object> attributeMap = edgeAttributes.get(sourceName).get(targetName);
					attributeMap.put("Object 1", sourceName);
					attributeMap.put("Object 2", targetName);
					ISimilarityValue simValue = objectSimilarities.get(pairIdx[0], pairIdx[1]);
					attributeMap.put("similarity", simValue.get());
					if (simValue instanceof ICompositeSimilarityValue) {
						ISimpleSimilarityFunction[] childFunctions = ((ICompositeSimilarityValue) simValue)
								.getChildFunctions();
						for (int i = 0; i < childFunctions.length; i++) {
							final ISimilarityFunction sf = childFunctions[i];
							attributeMap.put(sf.toString(),
									((ICompositeSimilarityValue) simValue).getChildSimilarityValue(i).get());
							attributeMap.put(String.format("%s (scaled)", sf.toString()),
									((ICompositeSimilarityValue) simValue).getScaledChildSimilarityValue(i).get());
						}
					}

					edgeAttributes.get(sourceName).get(targetName).put("edgeLabel",
							String.format("%.2f", objectSimilarities.get(pairIdx[0], pairIdx[1]).get()));
				}
			}
		}

		final Set<String> nodeNames = clusterNameSet;
		nodeNames.addAll(objectNameSet);
		final TiconeCytoscapeNetwork clusterSimilarityNetwork = CyNetworkUtil.createNewNetwork(
				String.format("Object Similarity Network (Clustering %d, Iteration %d)",
						clusteringWrapper.getClusteringNumber(), iteration),
				networkAttributes, nodeNames, edgeMapList, true, true, nodeAttributes, edgeAttributes);

		final CyTable networkTable = clusterSimilarityNetwork.getCyNetwork().getDefaultNetworkTable();
		networkTable.createColumn("clustering_id", Long.class, true);
		networkTable.getAllRows().get(0).set("clustering_id",
				clusteringWrapper.getClusteringProcess().getLatestClustering().getClusteringId());
		networkTable.createColumn("clustering_iteration", Integer.class, true);
		networkTable.getAllRows().get(0).set("clustering_iteration",
				clusteringWrapper.getClusteringProcess().getHistory().getIterationNumber());

		final VisualStyle vs = clusteringWrapper.getObjectSimilarityVisualStyle(iteration, objectClusterSimilarities);

		final Collection<CyNetworkView> views = ServiceHelper.getService(CyNetworkViewManager.class)
				.getNetworkViews(clusterSimilarityNetwork.getCyNetwork());
		for (final CyNetworkView view : views)
			vs.apply(view);

		ServiceHelper.getService(VisualMappingManager.class).setCurrentVisualStyle(vs);

		clusteringWrapper.addOnDestroyListener(new ITiconeResultDestroyedListener() {

			@Override
			public void ticoneResultDestroyed(TiconeResultDestroyedEvent event) {
				clusterSimilarityNetwork.destroy();
			}
		});

		return clusterSimilarityNetwork;
	}

	static boolean isClusterNode(CyNetwork network, CyNode node) {
		final Boolean isCluster = network.getDefaultNodeTable().getRow(node.getSUID())
				.get(NetworkUtil.NODE_ATTRIBUTE_IS_CLUSTER, Boolean.class);
		if (isCluster == null)
			return false;
		return isCluster;
	}

	static boolean isObjectNode(CyNetwork network, CyNode node) {
		final Boolean isObject = network.getDefaultNodeTable().getRow(node.getSUID())
				.get(NetworkUtil.NODE_ATTRIBUTE_IS_OBJECT, Boolean.class);
		if (isObject == null)
			return false;
		return isObject;
	}

}
