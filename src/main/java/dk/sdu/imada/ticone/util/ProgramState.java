package dk.sdu.imada.ticone.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.comparison.CytoscapeClusteringComparisonResult;
import dk.sdu.imada.ticone.connectivity.ConnectivityResultWrapper;

public class ProgramState implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3814948590253231022L;
	protected Map<Integer, IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult>> utils;
	protected Map<Integer, CytoscapeClusteringComparisonResult> comparisonResults;
	protected Map<Integer, ConnectivityResultWrapper> connectivityResults;
	/**
	 * A map holding long ids of the network and the kpm results for each tab index.
	 */
	protected Map<Integer, List<KPMResultWrapper>> clusteringKpmResults, comparisonKpmResults;
	protected int nextClusteringNumber, nextComparisonNumber, nextConnectivityNumber;
	protected long globalNumberOfClusters;
	protected int numberOfClusters;

	public ProgramState() {
		super();
		this.utils = new HashMap<>();
		this.clusteringKpmResults = new HashMap<>();
		this.comparisonKpmResults = new HashMap<>();
		this.comparisonResults = new HashMap<>();
		this.connectivityResults = new HashMap<>();
	}

	public void addOverrepresentedPatternUtil(final int tabIndex,
			final IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> utils) {
		this.utils.put(tabIndex, utils);
	}

	public void addClusteringKPMResults(final int tabIndex, final List<KPMResultWrapper> kpmResult) {
		this.clusteringKpmResults.put(tabIndex, kpmResult);
	}

	public void addComparisonKPMResults(final int tabIndex, final List<KPMResultWrapper> kpmResult) {
		this.comparisonKpmResults.put(tabIndex, kpmResult);
	}

	public void addClusteringComparisonResult(final int tabIndex,
			final CytoscapeClusteringComparisonResult comparisonResult) {
		this.comparisonResults.put(tabIndex, comparisonResult);
	}

	public void addConnectivityResult(final int tabIndex, final ConnectivityResultWrapper connectivityResult) {
		this.connectivityResults.put(tabIndex, connectivityResult);
	}

	public Map<Integer, List<KPMResultWrapper>> getClusteringKpmResults() {
		return this.clusteringKpmResults;
	}

	public Map<Integer, List<KPMResultWrapper>> getComparisonKpmResults() {
		return this.comparisonKpmResults;
	}

	public void setNextClusteringNumber(final int nextClusteringNumber) {
		this.nextClusteringNumber = nextClusteringNumber;
	}

	public int getNextClusteringNumber() {
		return this.nextClusteringNumber;
	}

	public Map<Integer, CytoscapeClusteringComparisonResult> getComparisonResults() {
		return this.comparisonResults;
	}

	public void setNextComparisonNumber(final int nextComparisonNumber) {
		this.nextComparisonNumber = nextComparisonNumber;
	}

	public int getNextComparisonNumber() {
		return this.nextComparisonNumber;
	}

	public Map<Integer, ConnectivityResultWrapper> getConnectivityResults() {
		return this.connectivityResults;
	}

	public void setNextConnectivityNumber(final int nextConnectivityNumber) {
		this.nextConnectivityNumber = nextConnectivityNumber;
	}

	public int getNextConnectivityNumber() {
		return this.nextConnectivityNumber;
	}

	public void setGlobalNumberOfClusters(final long globalNumberOfClusters) {
		this.globalNumberOfClusters = globalNumberOfClusters;
	}

	public long getGlobalNumberOfClusters() {
		return this.globalNumberOfClusters;
	}

	public void setNumberOfClusters(final int numberOfClusters) {
		this.numberOfClusters = numberOfClusters;
	}

	public int getNumberOfClusters() {
		return this.numberOfClusters;
	}
}
