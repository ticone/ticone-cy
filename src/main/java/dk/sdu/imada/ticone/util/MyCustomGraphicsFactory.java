package dk.sdu.imada.ticone.util;

import java.net.URL;

import org.cytoscape.view.presentation.customgraphics.CyCustomGraphics;
import org.cytoscape.view.presentation.customgraphics.CyCustomGraphicsFactory;

import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer;

public class MyCustomGraphicsFactory implements CyCustomGraphicsFactory<MyImageCustomGraphicLayer> {

	protected ClusterChartContainer clusterChartContainer;

	public MyCustomGraphicsFactory(final ClusterChartContainer clusterChartContainer) {
		super();
		this.clusterChartContainer = clusterChartContainer;
	}

	@Override
	public String getPrefix() {
		return null;
	}

	@Override
	public boolean supportsMime(final String mimeType) {
		return false;
	}

	@Override
	public MyCustomGraphics getInstance(final URL url) {
		final MyCustomGraphics myCustomGraphics = new MyCustomGraphics(this.clusterChartContainer);
		return myCustomGraphics;
	}

	@Override
	public CyCustomGraphics getInstance(final String input) {
		return null;
	}

	@Override
	public MyCustomGraphics parseSerializableString(final String string) {
//		if (string == null)
//			return null;
//		try {
//			string = string.substring(string.indexOf(","));
//			byte b[] = string.getBytes();
//			ByteArrayInputStream bi = new ByteArrayInputStream(b);
//			ObjectInputStream si;
//			si = new ObjectInputStream(bi);
//			ClusterChartContainer obj = (ClusterChartContainer) si.readObject();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		}
		return null;
	}

	@Override
	public Class<? extends CyCustomGraphics> getSupportedClass() {
		return MyCustomGraphics.class;
	}
}