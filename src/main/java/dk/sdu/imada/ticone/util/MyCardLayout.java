package dk.sdu.imada.ticone.util;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;

/**
 * Created by christian on 8/7/15.
 */
public class MyCardLayout extends CardLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6902493485143593084L;

	@Override
	public Dimension preferredLayoutSize(final Container parent) {

		final Component current = this.findCurrentComponent(parent);
		if (current != null) {
			final Insets insets = parent.getInsets();
			final Dimension pref = current.getPreferredSize();
			pref.width += insets.left + insets.right;
			pref.height += insets.top + insets.bottom;
			return pref;
		}
		return super.preferredLayoutSize(parent);
	}

	public Component findCurrentComponent(final Container parent) {
		for (final Component comp : parent.getComponents()) {
			if (comp.isVisible()) {
				return comp;
			}
		}
		return null;
	}

}
