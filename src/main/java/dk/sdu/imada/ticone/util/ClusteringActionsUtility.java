package dk.sdu.imada.ticone.util;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskManager;
import org.cytoscape.work.TaskObserver;

import dk.sdu.imada.ticone.clustering.AbstractNamedTiconeResult;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.gui.TiconeResultPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.panels.comparison.TiconeClusteringComparisonResultPanel;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.tasks.coloring.ColorClusterPairsTaskFactory;
import dk.sdu.imada.ticone.tasks.coloring.ColorNodesTaskFactory;

/**
 * TODO: do we still need this? Created by christian on 9/9/15.
 */
public class ClusteringActionsUtility {

	public static void visualizeAction(final TiconeResultPanel resultPanel) {
		if (resultPanel instanceof TiconeClusteringResultPanel) {
			final TiconeCytoscapeClusteringResult utils = resultPanel.getClusteringResult();
			final CyApplicationManager cyApplicationManager = ServiceHelper.getService(CyApplicationManager.class);

			final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
			final CyNetwork cyNetwork = cyApplicationManager.getCurrentNetwork();
			final CyNetworkView currentNetworkView = cyApplicationManager.getCurrentNetworkView();
			if (cyNetwork == null || currentNetworkView == null) {
				return;
			}

			final IClusterObjectMapping patternObjectMapping = utils.getClusteringProcess().getLatestClustering();
			final List<Map<String, ISimilarityValue>> toColor = new ArrayList<>();
			final List<Color> colors = new ArrayList<>();
			final IClusters patternList = ((TiconeClusteringResultPanel) resultPanel).getClustersTabPanel()
					.getClusterChartTablePanel().getSelectedClusters();
			for (final ICluster p : patternList) {
				final Color gColor = Color.decode(ColorUtility.getColor(p));
				colors.add(gColor);
				final Map<String, ISimilarityValue> m = new HashMap<>();
				for (final ITimeSeriesObject o : p.getSimilarities().keySet())
					m.put(o.getName(), p.getSimilarities().get(o));
				toColor.add(m);
			}
			taskManager.execute(
					new ColorNodesTaskFactory(cyNetwork, currentNetworkView, toColor, colors).createTaskIterator());
		} else if (resultPanel instanceof TiconeClusteringComparisonResultPanel) {
			final TiconeClusteringResult clustering1 = ((TiconeClusteringComparisonResultPanel) resultPanel)
					.getClusteringComparisonResult().getClustering1();
			final AbstractNamedTiconeResult clustering2 = ((TiconeClusteringComparisonResultPanel) resultPanel)
					.getClusteringComparisonResult().getClustering2();

			final CyApplicationManager cyApplicationManager = ServiceHelper.getService(CyApplicationManager.class);
			final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
			final CyNetwork cyNetwork = cyApplicationManager.getCurrentNetwork();
			final CyNetworkView currentNetworkView = cyApplicationManager.getCurrentNetworkView();
			if (cyNetwork == null || currentNetworkView == null) {
				return;
			}

			final IClusterObjectMapping patternObjectMapping = clustering1.getClusteringProcess().getLatestClustering();
			final Set<Pair<ICluster, ICluster>> clusterPairs = new HashSet<>();
			final List<Triple<ICluster, ICluster, Set<String>>> clusterPairsAndObjects = ((TiconeClusteringComparisonResultPanel) resultPanel)
					.getSelectedClusterPairsToVisualizeOnNetwork();
			for (int i = 0; i < clusterPairsAndObjects.size(); i++) {
				final Triple<ICluster, ICluster, Set<String>> clusterPairObjects = clusterPairsAndObjects.get(i);
				// Pair<Color, Color> gColor =
				// Pair.of(Color.decode(ColorUtility.getColor(clusterPair.getLeft())),
				// Color.decode(ColorUtility.getColor(clusterPair.getMiddle())));
				clusterPairs.add(
						Pair.of(clusterPairsAndObjects.get(i).getLeft(), clusterPairsAndObjects.get(i).getMiddle()));
				// toColor.add(clusterPair.getRight());
			}
			taskManager.execute(
					new ColorClusterPairsTaskFactory(cyNetwork, currentNetworkView, patternObjectMapping, clusterPairs,
							"TiCoNE Node-Coloring Comparison " + ((TiconeClusteringComparisonResultPanel) resultPanel)
									.getClusteringComparisonResult().getName()).createTaskIterator());
		}
	}

	public static void startTask(final AbstractTaskFactory taskFactory, final TaskObserver taskObserver) {
		final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
		final TaskIterator taskIterator = taskFactory.createTaskIterator();
		Utility.getProgress().start();
		if (taskObserver != null) {
			taskManager.execute(taskIterator, taskObserver);
		} else {
			taskManager.execute(taskIterator);
		}
	}
}
