package dk.sdu.imada.ticone.comparison;

import java.awt.Color;
import java.awt.Paint;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.view.model.VisualProperty;
import org.cytoscape.view.presentation.RenderingEngineManager;
import org.cytoscape.view.presentation.customgraphics.CyCustomGraphics;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.view.presentation.property.LineTypeVisualProperty;
import org.cytoscape.view.presentation.property.NodeShapeVisualProperty;
import org.cytoscape.view.presentation.property.values.LineType;
import org.cytoscape.view.vizmap.VisualMappingFunction;
import org.cytoscape.view.vizmap.VisualMappingFunctionFactory;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualPropertyDependency;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.view.vizmap.VisualStyleFactory;
import org.cytoscape.view.vizmap.mappings.BoundaryRangeValues;
import org.cytoscape.view.vizmap.mappings.ContinuousMapping;
import org.cytoscape.view.vizmap.mappings.DiscreteMapping;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.clustering.TiconeVisualClusteringResult;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureNumberCommonObjects;
import dk.sdu.imada.ticone.feature.ClusterPairFeaturePrototypeSimilarity;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer;
import dk.sdu.imada.ticone.network.NetworkUtil;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.util.ITiconeResultDestroyedListener;
import dk.sdu.imada.ticone.util.MyCustomGraphics;
import dk.sdu.imada.ticone.util.MyCustomGraphicsFactory;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeResultDestroyedEvent;

public class CytoscapeClusteringComparisonResult extends ClusteringComparisonResult<TiconeCytoscapeClusteringResult>
		implements ITiconeResultDestroyedListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1630922093928986560L;
	private transient VisualStyle visualStyle;
	private String visualStyleName;
	private TiconeCytoscapeNetwork comparisonNetwork;
	private String comparisonNetworkName;

	public CytoscapeClusteringComparisonResult(final ClusteringComparisonResult comparisonResult) {
		super(TiconeCytoscapeClusteringResult
				.getInstance(TiconeVisualClusteringResult.getInstance(comparisonResult.clustering1)),
				TiconeCytoscapeClusteringResult
						.getInstance(TiconeVisualClusteringResult.getInstance(comparisonResult.clustering2)),
				comparisonResult.clustering1iteration, comparisonResult.clustering2iteration, comparisonResult.pom1,
				comparisonResult.pom2, comparisonResult.similarityFunction, comparisonResult.permutations,
				comparisonResult.pvalues);
		ClusteringComparisonResult.nextComparisonNumber--;
		this.comparisonNumber--;

		this.clustering1.addOnDestroyListener(this);
		this.clustering2.addOnDestroyListener(this);
	}

	public void setComparisonNetwork(final CyNetwork comparisonNetwork) {
		this.comparisonNetwork = new TiconeCytoscapeNetwork(comparisonNetwork);
		this.comparisonNetworkName = comparisonNetwork.getRow(comparisonNetwork).get(CyNetwork.NAME, String.class);
	}

	public VisualStyle getVisualStyle() {
		return this.visualStyle;
	}

	public void clearComparisonVisualStyle() {
		if (this.getVisualStyle() != null) {
			ServiceHelper.getService(VisualMappingManager.class).removeVisualStyle(this.getVisualStyle());
		}
	}

	public void clearComparisonNetwork() {
		if (this.getComparisonNetwork() != null) {
			this.comparisonNetwork.destroy();
			this.comparisonNetwork = null;
		}
	}

	public CyNetwork getComparisonNetwork() {
		if (this.comparisonNetwork == null && this.comparisonNetworkName != null) {
			for (final CyNetwork net : ServiceHelper.getService(CyNetworkManager.class).getNetworkSet()) {
				if (net.getRow(net).get(CyNetwork.NAME, String.class).equals(this.comparisonNetworkName)) {
					this.comparisonNetwork = new TiconeCytoscapeNetwork(net);
					break;
				}
			}
		}
		if (this.comparisonNetwork != null)
			return this.comparisonNetwork.getCyNetwork();
		return null;
	}

	public void initClusteringComparisonVisualStyle() throws SimilarityCalculationException {
		// we have had a visual style before; delete it (possibly after loading
		// session);
		if (this.visualStyleName != null) {
			final VisualMappingManager visManager = ServiceHelper.getService(VisualMappingManager.class);
			for (final VisualStyle style : visManager.getAllVisualStyles()) {
				if (style.getTitle().equals(this.visualStyleName)) {
					visManager.removeVisualStyle(style);
					break;
				}
			}
		}

		final VisualStyle vs = ServiceHelper.getService(VisualStyleFactory.class)
				.createVisualStyle("TiCoNE Comparison '" + this.getName() + "'");

		vs.setDefaultValue(BasicVisualLexicon.NODE_WIDTH, 250.0);
		vs.setDefaultValue(BasicVisualLexicon.NODE_HEIGHT, 150.0);
		vs.setDefaultValue(BasicVisualLexicon.NODE_BORDER_WIDTH, 10.0);
		vs.setDefaultValue(BasicVisualLexicon.NODE_SHAPE, NodeShapeVisualProperty.RECTANGLE);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_FONT_SIZE, 20);

		for (final VisualPropertyDependency<?> dependency : vs.getAllVisualPropertyDependencies()) {
			if (dependency.getIdString().equals("nodeSizeLocked")) {
				dependency.setDependency(false);
			} else if (dependency.getIdString().equals("arrowColorMatchesEdge")) {
				dependency.setDependency(true);
			}
		}

		final VisualMappingFunctionFactory vmfDiscreteFactoryP = ServiceHelper
				.getService(VisualMappingFunctionFactory.class, "(mapping.type=discrete)");
		final VisualMappingFunctionFactory vmfContinuousFactoryP = ServiceHelper
				.getService(VisualMappingFunctionFactory.class, "(mapping.type=continuous)");
		final VisualMappingFunctionFactory vmfPassthroughFactoryP = ServiceHelper
				.getService(VisualMappingFunctionFactory.class, "(mapping.type=passthrough)");

		final VisualProperty<CyCustomGraphics> property = (VisualProperty<CyCustomGraphics>) ServiceHelper
				.getService(RenderingEngineManager.class).getDefaultVisualLexicon()
				.lookup(CyNode.class, "NODE_CUSTOMGRAPHICS_1");
		final DiscreteMapping<String, CyCustomGraphics> pMapping = (DiscreteMapping<String, CyCustomGraphics>) vmfDiscreteFactoryP
				.createVisualMappingFunction(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME, String.class, property);

		for (final ICluster p : this.clustering1.getClusterHistory().getClusterObjectMapping().getClusters()) {
			try {
				final ITimeSeriesObjectList timeSeriesDatas = p.getObjects();
				ClusterChartContainer container = this.clustering1.getDefaultClusterChartContainer(p);
				if (container == null) {
					container = new ClusterChartContainer(this.clustering1, p, timeSeriesDatas);
					this.clustering1.addClusterChartContainer(p, timeSeriesDatas, container);
				}
				final MyCustomGraphicsFactory factory = new MyCustomGraphicsFactory(container);
				final MyCustomGraphics mcg = factory.getInstance((URL) null);
				pMapping.putMapValue("(1) " + p.getName(), mcg);
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
				e.printStackTrace();
			}
		}
		for (final ICluster p : this.clustering2.getClusterHistory().getClusterObjectMapping().getClusters()) {
			try {
				final ITimeSeriesObjectList timeSeriesDatas = p.getObjects();
				ClusterChartContainer container = this.clustering2.getDefaultClusterChartContainer(p);
				if (container == null) {
					container = new ClusterChartContainer(this.clustering2, p, timeSeriesDatas);
					this.clustering2.addClusterChartContainer(p, timeSeriesDatas, container);
				}
				final MyCustomGraphicsFactory factory = new MyCustomGraphicsFactory(container);
				final MyCustomGraphics mcg = factory.getInstance((URL) null);
				pMapping.putMapValue("(2) " + p.getName(), mcg);
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
				e.printStackTrace();
			}
		}

		vs.addVisualMappingFunction(pMapping);

		// node label
		final VisualProperty<String> nodeLabelProperty = BasicVisualLexicon.NODE_LABEL;
		final VisualMappingFunction<String, String> nodeLabelMapping = vmfPassthroughFactoryP
				.createVisualMappingFunction(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME, String.class, nodeLabelProperty);
		vs.addVisualMappingFunction(nodeLabelMapping);

		// border color selected
		final VisualProperty<Paint> borderColorProperty = BasicVisualLexicon.NODE_BORDER_PAINT;
		final VisualMappingFunction<Boolean, Paint> borderColorMapping = vmfDiscreteFactoryP
				.createVisualMappingFunction("selected", Boolean.class, borderColorProperty);
		((DiscreteMapping<Boolean, Paint>) borderColorMapping).putMapValue(false, Color.BLACK);
		((DiscreteMapping<Boolean, Paint>) borderColorMapping).putMapValue(true, Color.YELLOW);
		vs.addVisualMappingFunction(borderColorMapping);

		// border line type
		final VisualProperty<LineType> borderLineTypeProperty = BasicVisualLexicon.NODE_BORDER_LINE_TYPE;
		final VisualMappingFunction<String, LineType> borderLineTypeMapping = vmfDiscreteFactoryP
				.createVisualMappingFunction("clustering", String.class, borderLineTypeProperty);
		((DiscreteMapping<String, LineType>) borderLineTypeMapping).putMapValue("1", LineTypeVisualProperty.SOLID);
		((DiscreteMapping<String, LineType>) borderLineTypeMapping).putMapValue("2", LineTypeVisualProperty.EQUAL_DASH);
		vs.addVisualMappingFunction(borderLineTypeMapping);

		// edge width
		final Collection<ISimilarityValue> vals = this.getFeatureStore()
				.valuesRaw(new ClusterPairFeaturePrototypeSimilarity());
		final double minSim = Collections.min(vals).get();
		final double maxSim = Collections.max(vals).get();
		double minObs = Double.MAX_VALUE;
		double maxObs = Double.MIN_VALUE;
		for (final IClusterPair patternPair : this.getFeatureStore().keySet(ObjectType.CLUSTER_PAIR)) {
			final int common = this.getFeatureStore()
					.getFeatureValue(patternPair, new ClusterPairFeatureNumberCommonObjects()).getValue();
			final double n = (double) common / (patternPair.getFirst().getObjects().size()
					+ patternPair.getSecond().getObjects().size() - common);
			if (n > maxObs)
				maxObs = n;
			if (n < minObs)
				minObs = n;
		}

		final VisualProperty<Double> edgeWidthProperty = BasicVisualLexicon.EDGE_WIDTH;
		final VisualMappingFunction<Double, Double> edgeWidthMapping = vmfContinuousFactoryP
				.createVisualMappingFunction("objects", Double.class, edgeWidthProperty);

		final BoundaryRangeValues<Double> minEdgeWidth = new BoundaryRangeValues<>(0.0, 0.0, 0.0);
		final BoundaryRangeValues<Double> medEdgeWidth = new BoundaryRangeValues<>(10.0, 10.0, 10.0);
		final BoundaryRangeValues<Double> maxEdgeWidth = new BoundaryRangeValues<>(40.0, 40.0, 40.0);

		((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(0.0, minEdgeWidth);
		((ContinuousMapping<Double, Double>) edgeWidthMapping).addPoint(maxObs, maxEdgeWidth);
		vs.addVisualMappingFunction(edgeWidthMapping);

		// edge color
		final VisualProperty<Paint> edgeColorProperty = BasicVisualLexicon.EDGE_UNSELECTED_PAINT;
		final VisualMappingFunction<Double, Paint> edgeColorMapping = vmfContinuousFactoryP
				.createVisualMappingFunction("sim", Double.class, edgeColorProperty);

		final BoundaryRangeValues<Paint> minEdgeColor = new BoundaryRangeValues<>(Color.RED, Color.RED, Color.RED);
		final BoundaryRangeValues<Paint> maxEdgeColor = new BoundaryRangeValues<>(Color.GREEN, Color.GREEN,
				Color.GREEN);

		((ContinuousMapping<Double, Paint>) edgeColorMapping).addPoint(minSim, minEdgeColor);
		((ContinuousMapping<Double, Paint>) edgeColorMapping).addPoint(maxSim, maxEdgeColor);
		vs.addVisualMappingFunction(edgeColorMapping);

		ServiceHelper.getService(VisualMappingManager.class).addVisualStyle(vs);
		this.visualStyle = vs;
		this.visualStyleName = vs.getTitle();

		final Collection<CyNetworkView> views = ServiceHelper.getService(CyNetworkViewManager.class)
				.getNetworkViews(this.comparisonNetwork.getCyNetwork());
		for (final CyNetworkView view : views)
			vs.apply(view);

		ServiceHelper.getService(VisualMappingManager.class).setCurrentVisualStyle(vs);
	}

	@Override
	public void ticoneResultDestroyed(TiconeResultDestroyedEvent event) {
		if (event.getResult().equals(this.getClustering1()) || event.getResult().equals(this.getClustering2()))
			this.destroy();
	}
}
