package dk.sdu.imada.ticone.comparison;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskManager;
import org.cytoscape.work.TaskMonitor;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureNumberCommonObjects;
import dk.sdu.imada.ticone.feature.ClusterPairFeaturePrototypeSimilarity;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.gui.panels.comparison.TiconeClusteringComparisonResultPanel;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.util.CyNetworkUtil;
import dk.sdu.imada.ticone.util.ITiconeResultDestroyedListener;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeProgressEvent;
import dk.sdu.imada.ticone.util.TiconeResultDestroyedEvent;
import dk.sdu.imada.ticone.util.TiconeTaskProgressListener;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

/**
 * 
 * @author wiwiec
 *
 */
public class ComparisonTask extends AbstractTask implements TiconeTaskProgressListener {

	protected TiconeComparisonTask ticoneComparisonTask;
	protected TaskMonitor tm;

	public ComparisonTask(final TiconeClusteringResult clustering1, final TiconeClusteringResult clustering2,
			final int clustering1Iteration, final int clustering2Iteration, final IClusters clustering1clusters,
			final IClusters clustering2clusters, final ISimilarityFunction similarity,
			final IShuffle shuffleClusteringPair, final int permutations) throws InterruptedException {
		super();

		this.ticoneComparisonTask = new TiconeComparisonTask(clustering1, clustering2, shuffleClusteringPair,
				clustering1Iteration, clustering2Iteration, clustering1clusters, clustering2clusters, similarity,
				permutations);
		this.ticoneComparisonTask.addProgressListener(this);
	}

	@Override
	public void cancel() {
		super.cancel();
		this.ticoneComparisonTask.cancel();
	}

	@Override
	public void run(final TaskMonitor tm) throws Exception {
		try {
			this.tm = tm;
			tm.setTitle("Clustering Comparison");
			tm.setStatusMessage("Comparing clusterings");
			this.ticoneComparisonTask.similarityFunction.initialize();

			this.calculateClusterComparisonMatrix();
		} catch (InterruptedException e) {
		} finally {
			this.ticoneComparisonTask.removeProgressListener(this);
		}
	}

	protected void calculateClusterComparisonMatrix() throws InterruptedException, ClusteringComparisonException,
			ToNumberConversionException, SimilarityCalculationException {
		final ClusteringComparisonResult<?> compResult = this.ticoneComparisonTask.calculateClusterComparisonMatrix();

		final CytoscapeClusteringComparisonResult comparisonResult = new CytoscapeClusteringComparisonResult(
				compResult);
		this.tm.setTitle("Creating network visualization");
		// create network visualization of clustering comparison
		final List<String> nodeList1 = new ArrayList<>();
		final Map<String, Map<String, Object>> nodeAttributes = new HashMap<>();
		for (final ICluster p : compResult.pom1.getClusters()) {
			final String clusterName = "(1) " + p.getName();
			nodeList1.add(clusterName);
			nodeAttributes.put(clusterName, new HashMap<String, Object>());
			nodeAttributes.get(clusterName).put("clustering", "1");
		}
		final List<String> nodeList2 = new ArrayList<>();
		for (final ICluster p : compResult.pom2.getClusters()) {
			final String clusterName = "(2) " + p.getName();
			nodeList2.add(clusterName);
			nodeAttributes.put(clusterName, new HashMap<String, Object>());
			nodeAttributes.get(clusterName).put("clustering", "2");
		}

		final List<String> nodeList = new ArrayList<>(nodeList1);
		nodeList.addAll(nodeList2);

		final Map<String, List<String>> edgeMapList = new HashMap<>();
		for (final String node : nodeList1)
			edgeMapList.put(node, nodeList2);
		final Map<String, Map<String, Map<String, Object>>> edgeAttributes = new HashMap<>();
		for (final IClusterPair patternPair : comparisonResult.getFeatureStore().keySet(ObjectType.CLUSTER_PAIR)) {
			final String sourceName = "(1) " + patternPair.getFirst().getName();
			final String targetName = "(2) " + patternPair.getSecond().getName();
			if (!edgeAttributes.containsKey(sourceName))
				edgeAttributes.put(sourceName, new HashMap<String, Map<String, Object>>());
			if (!edgeAttributes.get(sourceName).containsKey(targetName))
				edgeAttributes.get(sourceName).put(targetName, new HashMap<String, Object>());
			final int common = comparisonResult.getFeatureStore()
					.getFeatureValue(patternPair, new ClusterPairFeatureNumberCommonObjects()).getValue();
			edgeAttributes.get(sourceName).get(targetName).put("objects",
					(double) common / (patternPair.getFirst().getObjects().size()
							+ patternPair.getSecond().getObjects().size() - common));

			final double sim = comparisonResult.getFeatureStore()
					.getFeatureValue(patternPair, new ClusterPairFeaturePrototypeSimilarity()).getValue().toNumber()
					.doubleValue();

			edgeAttributes.get(sourceName).get(targetName).put("sim", sim);
		}
		final TiconeCytoscapeNetwork net = CyNetworkUtil.createNewNetwork(
				"Clustering Comparison " + comparisonResult.getComparisonNumber(), new HashMap<>(), nodeList,
				edgeMapList, true, true, nodeAttributes, edgeAttributes);

		comparisonResult.setComparisonNetwork(net.getCyNetwork());
		comparisonResult.initClusteringComparisonVisualStyle();
		final CyLayoutAlgorithmManager layoutManager = ServiceHelper.getService(CyLayoutAlgorithmManager.class);
		final CyLayoutAlgorithm layout = layoutManager.getLayout("clusteringComparisonLayout");

		final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);

		final Collection<CyNetworkView> views = ServiceHelper.getService(CyNetworkViewManager.class)
				.getNetworkViews(net.getCyNetwork());
		for (final CyNetworkView view : views) {
			taskManager
					.execute(layout.createTaskIterator(view, null, new HashSet<>(view.getNodeViews()), "clustering"));
		}
		this.tm.setTitle("Connectivity Comparison");
		try {
			comparisonResult.setComparisonNetwork(net.getCyNetwork());

			new TiconeClusteringComparisonResultPanel(comparisonResult);
		} catch (final Exception e) {
			e.printStackTrace();
		}

		comparisonResult.addOnDestroyListener(new ITiconeResultDestroyedListener() {

			@Override
			public void ticoneResultDestroyed(TiconeResultDestroyedEvent event) {
				net.destroy();
			}
		});

	}

	public static double round(final double value, final int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		if (Double.isNaN(value) || Double.isInfinite(value))
			return value;

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	@Override
	public void progressUpdated(final TiconeProgressEvent event) {
		if (event.getTitle() != null)
			this.tm.setTitle(event.getTitle());
		if (event.getStatusMessage() != null)
			this.tm.setStatusMessage(event.getStatusMessage());
		if (event.getPercent() != null)
			this.tm.setProgress(event.getPercent());
	}
}
