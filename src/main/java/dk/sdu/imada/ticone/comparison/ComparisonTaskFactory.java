package dk.sdu.imada.ticone.comparison;

import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;

/**
 * Created by christian on 25-01-16.
 */
public class ComparisonTaskFactory extends AbstractTaskFactory {

	private final ComparisonTask kpmTask;

	public ComparisonTaskFactory(final TiconeClusteringResult clustering1, final TiconeClusteringResult clustering2,
			final int clustering1Iteration, final int clustering2Iteration, final IClusters clustering1Clusters,
			final IClusters clustering2Clusters, final ISimilarityFunction similarity,
			final IShuffle shuffleClusteringPair, final int permutations) throws InterruptedException {
		this.kpmTask = new ComparisonTask(clustering1, clustering2, clustering1Iteration, clustering2Iteration,
				clustering1Clusters, clustering2Clusters, similarity, shuffleClusteringPair, permutations);
	}

	@Override
	public TaskIterator createTaskIterator() {
		final TaskIterator taskIterator = new TaskIterator();
		taskIterator.append(this.kpmTask);
		return taskIterator;
	}
}
