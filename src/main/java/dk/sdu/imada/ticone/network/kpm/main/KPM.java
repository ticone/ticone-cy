/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package dk.sdu.imada.ticone.network.kpm.main;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.lang3.tuple.Pair;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.sdu.imada.ticone.network.NetworkUtil;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.network.kpm.Algo;
import dk.sdu.imada.ticone.network.kpm.BenRemover;
import dk.sdu.imada.ticone.network.kpm.Globals;
import dk.sdu.imada.ticone.network.kpm.KPMGraph;
import dk.sdu.imada.ticone.network.kpm.Parser;
import dk.sdu.imada.ticone.network.kpm.Result;
import dk.sdu.imada.ticone.util.ColorUtility;
import dk.sdu.imada.ticone.util.CyNetworkUtil;
import dk.sdu.imada.ticone.util.IIdMapMethod;

/**
 *
 * @author nalcaraz
 */
public class KPM {

	private static KPMGraph kpmGraph;
	private static List<Result> results;

	public static void run(final CyNetwork network, final CyTable kpmTable, final int K, final int resultsWanted,
			final boolean positiveList, final Set<String> unmappedNodes, final int numberOfProcessors,
			final boolean benFree, final String strategy) throws IOException, InterruptedException {

		Globals.GENE_EXCEPTIONS = K;

		Globals.NUM_SOLUTIONS = resultsWanted;

		Globals.NUMBER_OF_PROCESSORS = numberOfProcessors;

		Globals.FILTER_BENS = benFree;

		// Some preprocessing that is needed
		final HashMap<String, Integer> id2param = new HashMap<>();
		id2param.put("L0", 0);
		Globals.CASE_EXCEPTIONS_MAP = id2param;

		// Register starting time
		Globals.STARTING_TIME = System.nanoTime();

		Logger logger = LoggerFactory.getLogger(KPM.class);
		logger.info("\n*********** CREATING GRAPH ***************\n");

		// Parse the graph and matrix files
		final Parser p = new Parser();

		// Create KPM graph
		kpmGraph = p.createGraph(network, kpmTable);

		/**
		 * Add nodes to priority lists
		 */
		if (positiveList) {
			kpmGraph.setPositiveList(unmappedNodes);
		} else {
			kpmGraph.setNegativeList(unmappedNodes);
		}

		// IMPORTANT: Graph must be refreshed before each new run of an
		// algorithm
		kpmGraph.refreshGraph();

		// Check which strategy and algorithm to use
		final String algorithm = "GREEDY";
		if (strategy.equals("INES"))
			Globals.ALGO = Algo.GREEDY;
		else if (strategy.equals("GLONE"))
			Globals.ALGO = Algo.EXCEPTIONSUMGREEDY;

		logger.info("\n********* RUNNING " + strategy + " (" + algorithm + ") " + "... *************\n");
		long start, end, time;

		// Run the algorithm
		start = System.currentTimeMillis();
		results = Globals.ALGO.run(kpmGraph);
		end = System.currentTimeMillis();
		time = (end - start) / 1000;

		// Do stats and post-processing stuff after obtaining the results
		logger.info("TOTAL RUNNING TIME: " + time + " seconds");

		Globals.TOTAL_RUNNING_TIME = time;
		if (strategy.equals("INES") && Globals.FILTER_BENS) {
			logger.info("REMOVING BENs...");
			final List<Result> benFiltered = new ArrayList<>();

			final ExecutorService pool = Executors.newFixedThreadPool(Globals.NUMBER_OF_PROCESSORS);
			final List<Future<Result>> futures = new ArrayList<>(results.size());
			for (final Result result : results) {
				final BenRemover benr = new BenRemover(kpmGraph);
				futures.add(pool.submit(new Callable<Result>() {
					@Override
					public Result call() throws Exception {
						return benr.filterBENs(result);
					}
				}));
			}
			for (final Future<Result> future : futures) {
				try {
					final Result filtered = future.get();
					if (!benFiltered.contains(filtered) && filtered.getVisitedNodes().size() >= Globals.MIN_PATH_SIZE) {
						benFiltered.add(filtered);
					}
				} catch (final ExecutionException ex) {
					logger.error(ex.getMessage(), ex);
				}
			}
			pool.shutdown();
			results = benFiltered;
		}

		if (results.size() > 1) {
			logger.info("PERFORMING POST-PROCESSING OPERATIONS...");
			if (!Globals.DOUBLE_SOLUTIONS_ALLOWED) {
				results = removeDoubleSolutions(results);
			}
			Collections.sort(results);

			if (Globals.NUM_SOLUTIONS >= 0) {
				final List<Result> filter = new ArrayList<>();
				int toAdd = 1;
				for (final Result result : results) {
					if (toAdd > Globals.NUM_SOLUTIONS) {
						break;
					} else {
						filter.add(result);
						toAdd++;
					}
				}
				results = filter;
			}
			logger.info("BEST FITNESS: " + results.get(0).getFitness());
		} else {
			logger.info("NO PATHWAYS EXIST FOR THE GIVEN PARAMETERS !");
			return;
		}
		logger.info("FINISHED !!");
	}

	public static KPMGraph getKPMGraph() {
		return kpmGraph;
	}

	public static List<Result> getResults() {
		return results;
	}

	public static TiconeCytoscapeNetwork createNetworkWithResults(final CyTable nodeTable, final String networkName,
			final CyNetwork network, final List<Result> resultList, final boolean union, final boolean createView,
			final IIdMapMethod idMapMethod) {
		// int numberOfResults = Math.min(resultList.size(), 10);
		final int numberOfResults = resultList.size();
		// List<Result> topResults = resultList.subList(0, numberOfResults);
		// Map<String, Integer> unionMap = findComponentsUnion(topResults);
		final Map<String, Integer> unionMap = findComponentsUnion(network, resultList);

		final List<CyNode> nodeList = network.getNodeList();
		final Map<String, List<String>> edgeMapList = new HashMap<>();
		final Set<String> alreadyThere = new HashSet<>();
		final List<String> nodeNameList = new ArrayList<>();

		final Map<String, Map<String, Map<String, Object>>> edgeAttributes = new HashMap<>();

		for (int i = 0; i < nodeList.size(); i++) {
			final CyNode node = nodeList.get(i);
			final String nodeID = nodeTable.getRow(node.getSUID()).get(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME,
					String.class);
			// View<CyNode> nodeView = networkView.getNodeView(nodeList.get(i));
			if (unionMap.containsKey(nodeID) && (union || unionMap.get(nodeID) == numberOfResults)) {

				alreadyThere.add(nodeID);
				// List<CyNode> neighbors = network.getNeighborList(node,
				// CyEdge.Type.ANY);
				final List<CyEdge> neighbors = network.getAdjacentEdgeList(node, CyEdge.Type.ANY);
				final Pair<List<String>, List<CyEdge>> edgeList = addNeighborsToList(alreadyThere, nodeID, neighbors,
						network);
				edgeMapList.put(nodeID, edgeList.getKey());
				nodeNameList.add(nodeID);

				// copy edge attributes over
				for (int n = 0; n < edgeList.getKey().size(); n++) {
					final String neighborName = edgeList.getKey().get(n);
					final CyEdge neighborEdge = edgeList.getValue().get(n);
					if (!edgeAttributes.containsKey(nodeID))
						edgeAttributes.put(nodeID, new HashMap<String, Map<String, Object>>());
					edgeAttributes.get(nodeID).put(neighborName, network.getRow(neighborEdge).getAllValues());
				}

			} else {
				// nodeView.setVisualProperty(BasicVisualLexicon.NODE_VISIBLE,
				// false);
			}
		}
		final Map<String, Map<String, Object>> nodeAttributes = new HashMap<>();
		if (idMapMethod.isActive()) {
			for (final String nodeId : nodeNameList) {
				final String altId = idMapMethod.getAlternativeId(nodeId);
				nodeAttributes.put(nodeId, new HashMap<String, Object>());
				nodeAttributes.get(nodeId).put(NetworkUtil.NODE_ATTRIBUTE_OBJECT_ALTERNATIVE_NAME, altId);
			}
		}
		final TiconeCytoscapeNetwork newNetwork = CyNetworkUtil.createNewNetwork(networkName, new HashMap<>(),
				nodeNameList, edgeMapList, createView, true, nodeAttributes, edgeAttributes);
		return newNetwork;
	}

	private static Pair<List<String>, List<CyEdge>> addNeighborsToList(final Set<String> alreadyThere,
			final String node, final List<CyEdge> neighborEdges, final CyNetwork network) {
		final List<String> neighborList = new ArrayList<>();
		final List<CyEdge> resultEdges = new ArrayList<>();
		for (int i = 0; i < neighborEdges.size(); i++) {
			final CyEdge neighborEdge = neighborEdges.get(i);
			final String sourceName = network.getRow(neighborEdge.getSource()).get(CyNetwork.NAME, String.class);
			final String targetName = network.getRow(neighborEdge.getTarget()).get(CyNetwork.NAME, String.class);
			// Only add edges to nodes that should be in the network
			final String neighborName = sourceName.equals(node) ? targetName : sourceName;
			if (alreadyThere.contains(neighborName)) {
				neighborList.add(neighborName);
				resultEdges.add(neighborEdge);
			}
		}
		return Pair.of(neighborList, resultEdges);
	}

	public static void showCoreOnNetwork(final CyTable nodeTable, final CyNetworkView networkView,
			final CyNetwork network, final List<Result> resultList) {

		final int numberOfResults = resultList.size();
		final Map<String, Integer> union = findComponentsUnion(network, resultList);

		final List<CyNode> nodeList = network.getNodeList();
		for (int i = 0; i < nodeList.size(); i++) {
			final CyNode node = nodeList.get(i);
			final String nodeID = nodeTable.getRow(node.getSUID()).get(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME,
					String.class);
			final View<CyNode> nodeView = networkView.getNodeView(nodeList.get(i));
			if (union.containsKey(nodeID) && union.get(nodeID) == numberOfResults) {
				final Color nodeColor = (Color) nodeView.getVisualProperty(BasicVisualLexicon.NODE_FILL_COLOR);
				final int timesRepresented = union.get(nodeID);
				final int alpha = (int) (((double) timesRepresented / (double) resultList.size()) * 255);
				final int r = nodeColor.getRed();
				final int g = nodeColor.getGreen();
				final int b = nodeColor.getBlue();
				final Color newNodeColor = new Color(r, g, b, alpha);
				nodeView.setVisualProperty(BasicVisualLexicon.NODE_VISIBLE, true);
				nodeView.setVisualProperty(BasicVisualLexicon.NODE_FILL_COLOR, newNodeColor);
				final Color textColor = ColorUtility.calculateTextColor(nodeColor);
				nodeView.setVisualProperty(BasicVisualLexicon.NODE_LABEL_COLOR, textColor);
			} else {
				nodeView.setVisualProperty(BasicVisualLexicon.NODE_VISIBLE, false);
			}
		}
		networkView.updateView();
	}

	public static Map<String, Integer> findComponentsUnion(final CyNetwork network, final List<Result> results) {
		final Map<String, Integer> nodesRepresented = new HashMap<>();
		Set<String> nodesAlreadyCountedInResult;

		for (int i = 0; i < results.size(); i++) {
			final Result result = results.get(i);
			nodesAlreadyCountedInResult = new HashSet<>();
			for (final CyEdge edge : network.getEdgeList()) {
				final String node1 = network.getRow(edge.getSource()).get(CyNetwork.NAME, String.class);
				if (!result.getVisitedNodes().containsKey(node1))
					continue;
				final String node2 = network.getRow(edge.getTarget()).get(CyNetwork.NAME, String.class);
				if (!result.getVisitedNodes().containsKey(node2))
					continue;
				addNodeToUnion(node1, nodesAlreadyCountedInResult, nodesRepresented);
				addNodeToUnion(node2, nodesAlreadyCountedInResult, nodesRepresented);
			}
		}
		return nodesRepresented;
	}

	private static void addNodeToUnion(final String node, final Set<String> nodesAlreadyCountedInResult,
			final Map<String, Integer> nodesRepresented) {
		if (!nodesAlreadyCountedInResult.contains(node)) {
			nodesAlreadyCountedInResult.add(node);
			if (nodesRepresented.containsKey(node)) {
				nodesRepresented.put(node, nodesRepresented.get(node) + 1);
			} else {
				nodesRepresented.put(node, 1);
			}
		}

	}

	/**
	 * Removes double results, if there are any. More specifically, for each element
	 * e1 in the list, it removes all other elements e2 where e1.equals(e2) yields
	 * true.
	 *
	 * @param results the list where the double entries are to be removed. List has
	 *                to be sorted.
	 * @return the list without double entries. the input list is not changed.
	 */
	private static List<Result> removeDoubleSolutions(final List<Result> results) {
		final List<Result> toReturn = new ArrayList<>();
		int i = 0;

		while (i < results.size()) {
			final Result r = results.get(i);
			if (!toReturn.contains(r)) {
				toReturn.add(r);
			}
			i++;
		}

		return toReturn;
	}
}
