package dk.sdu.imada.ticone.network;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;

import de.wiwie.wiutils.utils.Triple;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.TimeSeriesObject.NetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork.TiconeCytoscapeNetworkEdge;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork.TiconeCytoscapeNetworkNode;
import dk.sdu.imada.ticone.util.CyNetworkUtil;
import dk.sdu.imada.ticone.util.ServiceHelper;

public class TiconeCytoscapeNetwork extends TiconeNetwork<TiconeCytoscapeNetworkNode, TiconeCytoscapeNetworkEdge> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5973983430397382448L;

	protected transient CyNetwork cyNetwork;

	protected transient Map<CyEdge, TiconeCytoscapeNetworkEdge> cyEdges;
	protected transient Map<CyNode, TiconeCytoscapeNetworkNode> cyNodes;

	public TiconeCytoscapeNetwork(final CyNetwork cyNetwork) {
		super(cyNetwork.getRow(cyNetwork).get(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME, String.class));
		this.cyNetwork = cyNetwork;
		this.cyNodes = new HashMap<>();
		this.cyEdges = new HashMap<>();
		for (final CyNode node : cyNetwork.getNodeList())
			this.addNode(node);
		for (final CyEdge edge : cyNetwork.getEdgeList())
			this.addEdge(edge);
	}

	public CyNetwork getCyNetwork() {
		if (this.cyNetwork == null && this.name != null) {
			final CyNetworkManager cyNetworkManager = ServiceHelper.getService(CyNetworkManager.class);
			final Set<CyNetwork> networks = cyNetworkManager.getNetworkSet();
			final Iterator<CyNetwork> tableIterator1 = networks.iterator();
			while (tableIterator1.hasNext()) {
				final CyNetwork network = tableIterator1.next();
				if (network.getRow(network).get(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME, String.class)
						.equals(this.name)) {
					this.cyNetwork = network;
				}
			}
		}
		return this.cyNetwork;
	}

	@Override
	public void setName(final String name) {
		this.getCyNetwork().getRow(this.cyNetwork).set(CyNetwork.NAME, name);
		super.setName(name);
	}

	@Override
	public List<TiconeCytoscapeNetworkNode> getNodeList() {
		if (this.cyNodes == null) {
			this.cyNodes = new HashMap<>();

			final List<CyNode> nodeList = this.getCyNetwork().getNodeList();

			final List<TiconeCytoscapeNetworkNode> result = new ArrayList<>();
			for (final CyNode node : nodeList) {
				result.add(this.wrap(node));
			}
		}
		return new ArrayList<>(this.cyNodes.values());
	}

	public TiconeCytoscapeNetworkEdge wrap(final CyEdge cyEdge) {
		Objects.requireNonNull(cyEdge);
		if (this.cyEdges.containsKey(cyEdge))
			return this.cyEdges.get(cyEdge);
		final TiconeCytoscapeNetworkEdge newEdge = new TiconeCytoscapeNetworkEdge(cyEdge);
		this.addEdgeToMap(cyEdge, newEdge);
		return newEdge;
	}

	protected void addEdgeToMap(final CyEdge cyEdge, final TiconeCytoscapeNetworkEdge newEdge) {
		if (this.cyEdges.containsKey(cyEdge))
			return;
		this.cyEdges.put(cyEdge, newEdge);
	}

	@Override
	public Set<TiconeCytoscapeNetworkNode> getNodeSet() {
		return new HashSet<>(this.getNodeList());
	}

	@Override
	protected TiconeCytoscapeNetworkNode getNode(final ITiconeNetworkNode node) {
		if (node instanceof TiconeCytoscapeNetworkNode)
			return (TiconeCytoscapeNetworkNode) node;

		return TiconeCytoscapeNetwork.this.getNode(node.getName());
	}

	@Override
	protected TiconeCytoscapeNetworkNode getNode(final ITimeSeriesObject object) {
		TiconeCytoscapeNetworkNode result;
		if (object instanceof NetworkMappedTimeSeriesObject && ((NetworkMappedTimeSeriesObject) object)
				.getNode(TiconeCytoscapeNetwork.this) instanceof TiconeCytoscapeNetworkNode)
			result = (TiconeCytoscapeNetworkNode) ((NetworkMappedTimeSeriesObject) object)
					.getNode(TiconeCytoscapeNetwork.this);
		else
			result = TiconeCytoscapeNetwork.this.getNode(object.getName());
		return result;
	}

	@Override
	public List<TiconeCytoscapeNetworkEdge> getEdgeList() {
		if (this.cyEdges == null) {
			this.cyEdges = new HashMap<>();

			final List<CyEdge> edgeList = this.getCyNetwork().getEdgeList();

			final List<TiconeCytoscapeNetworkEdge> result = new ArrayList<>();
			for (final CyEdge edge : edgeList) {
				result.add(this.wrap(edge));
			}
		}
		return new ArrayList<>(this.cyEdges.values());
	}

	@Override
	public Set<TiconeCytoscapeNetworkEdge> getEdgeSet() {
		return new HashSet<>(this.getEdgeList());
	}

	@Override
	public Object getValue(final TiconeCytoscapeNetworkNode node, final String col, final Class c) {
		final CyTable nodeTable = this.getCyNetwork().getDefaultNodeTable();
		final CyRow row = nodeTable.getRow(node.getUID());
		return row.get(col, c);
	}

	@Override
	public Object getValue(final TiconeCytoscapeNetworkEdge e, final String col, final Class c) {
		final CyTable edgeTable = this.getCyNetwork().getDefaultEdgeTable();
		final CyRow row = edgeTable.getRow(e.getUID());
		return row.get(col, c);
	}

	// @Override
	// public boolean containsEdge(TiCoNECytoscapeNetworkNode n1,
	// TiCoNECytoscapeNetworkNode n2) {
	// return getCyNetwork().containsEdge(n1.cyNode, n2.cyNode);
	// }
	//

	@Override
	public <N extends TiconeCytoscapeNetworkNode> Set<TiconeCytoscapeNetworkEdge> getEdges(final N n1, final N n2,
			final TiconeEdgeType type) {
		final Set<TiconeCytoscapeNetworkEdge> edges = new HashSet<>();
		// TODO: how to handle this exception?
		if (type.equals(TiconeEdgeType.OUTGOING)) {
			try {
				for (final CyEdge e : this.getCyNetwork().getConnectingEdgeList(n1.cyNode, n2.cyNode,
						CyEdge.Type.OUTGOING))
					edges.add(this.wrap(e));
			} catch (final IllegalArgumentException ex) {
				ex.printStackTrace();
			}
		} else if (type.equals(TiconeEdgeType.INCOMING)) {
			try {
				for (final CyEdge e : this.getCyNetwork().getConnectingEdgeList(n1.cyNode, n2.cyNode,
						CyEdge.Type.INCOMING))
					edges.add(this.wrap(e));
			} catch (final IllegalArgumentException ex) {
				ex.printStackTrace();
			}
		} else if (type.equals(TiconeEdgeType.SELF)) {
			try {
				// TODO: we just ignore the second node for now
				for (final CyEdge e : this.getCyNetwork().getConnectingEdgeList(n1.cyNode, n1.cyNode, CyEdge.Type.ANY))
					edges.add(this.wrap(e));
			} catch (final IllegalArgumentException ex) {
				ex.printStackTrace();
			}
		} else if (type.equals(TiconeEdgeType.ANY)) {
			try {
				for (final CyEdge e : this.getCyNetwork().getConnectingEdgeList(n1.cyNode, n2.cyNode, CyEdge.Type.ANY))
					edges.add(this.wrap(e));
			} catch (final IllegalArgumentException ex) {
				ex.printStackTrace();
			}
		}

		return edges;
	}

	@Override
	public int getEdgeCount() {
		return this.getCyNetwork().getEdgeCount();
	}

	@Override
	public int getNodeCount() {
		return this.getCyNetwork().getNodeCount();
	}

	@Override
	public boolean hasNodeAttribute(final String attribute) {
		return this.getCyNetwork().getDefaultNodeTable().getColumn(attribute) != null;
	}

	@Override
	public void createNodeAttribute(final String attribute, final Class c, final boolean isImmutable) {
		this.getCyNetwork().getDefaultNodeTable().createColumn(attribute, c, isImmutable);
	}

	public void destroy() {
		if (cyNetwork != null) {
			if (ServiceHelper.getService(CyNetworkManager.class).networkExists(cyNetwork.getSUID()))
				ServiceHelper.getService(CyNetworkManager.class).destroyNetwork(cyNetwork);
			cyNetwork = null;
		}
	}

	@Override
	public boolean hasEdgeAttribute(final String attribute) {
		return this.getCyNetwork().getDefaultEdgeTable().getColumn(attribute) != null;
	}

	@Override
	public void createEdgeAttribute(final String attribute, final Class c, final boolean isImmutable) {
		this.getCyNetwork().getDefaultEdgeTable().createColumn(attribute, c, isImmutable);
	}

	@Override
	public boolean hasNetworkAttribute(String attribute) {
		return this.getCyNetwork().getDefaultNetworkTable().getColumn(attribute) != null;
	}

	@Override
	public void createNetworkAttribute(String attribute, Class c, boolean isImmutable) {
		this.getCyNetwork().getDefaultNetworkTable().createColumn(attribute, c, isImmutable);
	}

	@Override
	public TiconeCytoscapeNetworkNode addNode(final String name) {
		final CyNode node = this.getCyNetwork().addNode();
		this.getCyNetwork().getDefaultNodeTable().getRow(node.getSUID()).set(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME,
				name);
		return this.addNode(node);
	}

	protected TiconeCytoscapeNetworkNode addNode(final CyNode node) {
		final TiconeCytoscapeNetworkNode newNode = this.wrap(node);

		this.insertNodeIntoNodeDatastructures(newNode);
		this.insertNodeIntoEdgeDatastructures(newNode);
		return newNode;
	}

	@Override
	public TiconeCytoscapeNetworkNode[] addNodes(final String[] names) {
		final TiconeCytoscapeNetworkNode[] result = new TiconeCytoscapeNetworkNode[names.length];
		for (int p = 0; p < names.length; p++)
			result[p] = this.addNode(names[p]);
		return result;
	}

	@Override
	public TiconeCytoscapeNetworkNode removeNode(final String name) {
		final TiconeCytoscapeNetworkNode node = this.nodeIdToNode.get(name);
		this.removeNode(node);
		return node;
	}

	@Override
	public boolean removeNode(final TiconeCytoscapeNetworkNode node) {
		this.nodeIdToNode.remove(node.getName());
		this.getCyNetwork().removeNodes(Arrays.asList(node.cyNode));
		return true;
	}

	@Override
	public <N extends TiconeCytoscapeNetworkNode> boolean removeNodes(final TiconeCytoscapeNetworkNode[] nodes) {
		boolean result = true;
		for (int p = 0; p < nodes.length; p++)
			result &= this.removeNode(nodes[p]);
		return result;
	}

	@Override
	public TiconeCytoscapeNetworkNode[] removeNodesByIds(final String[] names) {
		final TiconeCytoscapeNetworkNode[] result = new TiconeCytoscapeNetworkNode[names.length];
		for (int p = 0; p < names.length; p++)
			result[p] = this.removeNode(names[p]);
		return result;
	}

	@Override
	public TiconeCytoscapeNetworkEdge addEdge(final String sourceNodeId, final String targetNodeId,
			final boolean isDirected) {
		if (sourceNodeId == null || targetNodeId == null)
			return null;
		return this.addEdge(this.nodeIdToNode.get(sourceNodeId), this.nodeIdToNode.get(targetNodeId), isDirected);
	}

	@Override
	public TiconeCytoscapeNetworkEdge addEdge(final TiconeCytoscapeNetworkNode source,
			final TiconeCytoscapeNetworkNode target, final boolean isDirected) {
		if (source == null || target == null)
			return null;
		return this.addEdge(this.getCyNetwork().addEdge(source.cyNode, target.cyNode, isDirected));
	}

	protected TiconeCytoscapeNetworkEdge addEdge(final CyEdge cyEdge) {
		if (cyEdge == null)
			return null;
		return this.insertEdgeIntoEdgeDatastructures(this.wrap(cyEdge));
	}

	@Override
	public void setNodeAttribute(final TiconeCytoscapeNetworkNode node, final String attribute, final Object value) {
		this.getCyNetwork().getRow(node.cyNode).set(attribute, value);
	}

	@Override
	public void setEdgeAttribute(final TiconeCytoscapeNetworkEdge edge, final String attribute, final Object value) {
		this.getCyNetwork().getRow(edge.cyEdge).set(attribute, value);
	}

	@Override
	public void setNetworkAttribute(String attribute, Object value) {
		this.getCyNetwork().getRow(this.getCyNetwork()).set(attribute, value);
	}

	@Override
	public <N extends TiconeCytoscapeNetworkNode> List<TiconeCytoscapeNetworkEdge> getAdjacentEdgeList(final N node,
			final TiconeEdgeType types) {
		// TODO: doesn't work yet
		CyEdge.Type type = null;
		if (types.equals(TiconeEdgeType.ANY))
			type = CyEdge.Type.ANY;
		else if (types.equals(TiconeEdgeType.INCOMING))
			type = CyEdge.Type.INCOMING;
		else if (types.equals(TiconeEdgeType.OUTGOING))
			type = CyEdge.Type.OUTGOING;
		final List<CyEdge> cyEdges = this.getCyNetwork().getAdjacentEdgeList(node.cyNode, type);

		final List<TiconeCytoscapeNetworkEdge> result = new ArrayList<>();
		for (final CyEdge cyEdge : cyEdges)
			result.add(this.wrap(cyEdge));
		return result;
	}

	@Override
	public Set<Triple<String, Class, Boolean>> getNodeAttributes() {
		final Set<Triple<String, Class, Boolean>> result = new HashSet<>();
		for (final CyColumn col : this.cyNetwork.getDefaultNodeTable().getColumns()) {
			result.add(Triple.getTriple(col.getName(), (Class) col.getType(), col.isImmutable()));
		}
		return result;
	}

	@Override
	public Set<Triple<String, Class, Boolean>> getEdgeAttributes() {
		final Set<Triple<String, Class, Boolean>> result = new HashSet<>();
		for (final CyColumn col : this.cyNetwork.getDefaultEdgeTable().getColumns()) {
			result.add(Triple.getTriple(col.getName(), (Class) col.getType(), col.isImmutable()));
		}
		return result;
	}

	@Override
	public Set<Triple<String, Class, Boolean>> getNetworkAttributes() {
		final Set<Triple<String, Class, Boolean>> result = new HashSet<>();
		for (final CyColumn col : this.cyNetwork.getDefaultNetworkTable().getColumns()) {
			result.add(Triple.getTriple(col.getName(), (Class) col.getType(), col.isImmutable()));
		}
		return result;
	}

	@Override
	public void performEdgeCrossovers(final double factorEdgeSwaps, final boolean store, final long seed) {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public ITiconeNetwork<TiconeCytoscapeNetworkNode, TiconeCytoscapeNetworkEdge> copy() {
		throw new UnsupportedOperationException("Not implemented");
	}
//
//	@Override
//	protected void finalize() throws Throwable {
//		super.finalize();
//		final CyNetwork cyNet = getCyNetwork();
//		if (cyNet != null) {
//			final CyNetworkManager cyNetworkManager = ServiceHelper.getService(CyNetworkManager.class);
//			cyNetworkManager.destroyNetwork(cyNet);
//		}
//	}

	@Override
	public int getAdjacentEdgeCount(final TiconeCytoscapeNetworkNode node, final TiconeEdgeType types) {
		return this.getAdjacentEdgeList(node, types).size();
	}

	public TiconeCytoscapeNetworkNode wrap(final CyNode cyNode) {
		Objects.requireNonNull(cyNode);
		if (this.cyNodes.containsKey(cyNode))
			return this.cyNodes.get(cyNode);
		final TiconeCytoscapeNetworkNode newNode = new TiconeCytoscapeNetworkNode(cyNode);
		this.addNodeToMap(this.getCyNetwork(), cyNode, newNode);
		return newNode;
	}

	protected void addNodeToMap(final CyNetwork network, final CyNode cyNode,
			final TiconeCytoscapeNetworkNode newNode) {
		if (this.cyNodes.containsKey(cyNode))
			return;
		this.cyNodes.put(cyNode, newNode);
	}

	public class TiconeCytoscapeNetworkEdge
			extends TiconeNetwork<TiconeCytoscapeNetworkNode, TiconeCytoscapeNetworkEdge>.TiconeNetworkEdge {

		/**
		 * 
		 */
		private static final long serialVersionUID = 7661870618897445369L;

		protected transient CyEdge cyEdge;

		public TiconeCytoscapeNetworkEdge(final CyEdge cyEdge) {
			super(TiconeCytoscapeNetwork.this.wrap(cyEdge.getSource()),
					TiconeCytoscapeNetwork.this.wrap(cyEdge.getTarget()), cyEdge.isDirected(), cyEdge.getSUID());
			this.cyEdge = cyEdge;
		}

		protected CyEdge getCyEdge() {
			if (this.cyEdge == null) {
				// find the cytoscape edge, given the two cytoscape nodes
				final CyNode source = this.getSource().getCyNode();
				final CyNode target = this.getTarget().getCyNode();
				List<CyEdge> edges;
				if (this.isDirected)
					edges = TiconeCytoscapeNetwork.this.getCyNetwork().getConnectingEdgeList(source, target,
							CyEdge.Type.DIRECTED);
				else
					edges = TiconeCytoscapeNetwork.this.getCyNetwork().getConnectingEdgeList(source, target,
							CyEdge.Type.UNDIRECTED);
				this.cyEdge = edges.get(0);
				TiconeCytoscapeNetwork.this.addEdgeToMap(this.cyEdge, this);
			}
			return this.cyEdge;
		}

		@Override
		public long getUID() {
			return this.getCyEdge().getSUID();
		}
	}

	public class TiconeCytoscapeNetworkNode
			extends TiconeNetwork<TiconeCytoscapeNetworkNode, TiconeCytoscapeNetworkEdge>.TiconeNetworkNode {

		/**
		 * 
		 */
		private static final long serialVersionUID = -4751213510316967857L;
		protected transient CyNode cyNode;

		private TiconeCytoscapeNetworkNode(final CyNode cyNode) {
			super(TiconeCytoscapeNetwork.this.getCyNetwork().getDefaultNodeTable().getRow(cyNode.getSUID())
					.get(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME, String.class));

			this.cyNode = cyNode;
		}

		@Override
		public long getUID() {
			return this.getCyNode().getSUID();
		}

		public CyNode getCyNode() {
			if (this.cyNode == null && this.name != null) {
				this.cyNode = CyNetworkUtil.getNodeByName(TiconeCytoscapeNetwork.this.getCyNetwork(),
						TiconeCytoscapeNetwork.this.getCyNetwork().getDefaultNodeTable(), this.name);
				TiconeCytoscapeNetwork.this.addNodeToMap(TiconeCytoscapeNetwork.this.getCyNetwork(), this.cyNode, this);
			}
			return this.cyNode;
		}
	}

}
