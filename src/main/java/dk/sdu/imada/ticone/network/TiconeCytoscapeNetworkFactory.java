package dk.sdu.imada.ticone.network;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkFactory;
import org.cytoscape.model.SavePolicy;
import org.cytoscape.model.subnetwork.CyRootNetwork;
import org.cytoscape.model.subnetwork.CyRootNetworkManager;

import dk.sdu.imada.ticone.util.ServiceHelper;

public class TiconeCytoscapeNetworkFactory extends TiconeNetworkFactory<TiconeCytoscapeNetwork> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3659822257144277603L;

	private CyNetwork parentNetwork;

	/**
	 * @param parentNetwork the rootNetwork to set
	 */
	public void setParentNetwork(CyNetwork parentNetwork) {
		this.parentNetwork = parentNetwork;
	}

	@Override
	public TiconeCytoscapeNetwork getInstance() {
		return this.getInstance(true);
	}

	@Override
	public TiconeCytoscapeNetwork getInstance(final boolean registerNetwork) {
		CyNetwork network;
		if (parentNetwork == null) {
			CyNetworkFactory networkFactory = ServiceHelper.getService(CyNetworkFactory.class);
			if (registerNetwork)
				network = networkFactory.createNetwork();
			else
				network = networkFactory.createNetworkWithPrivateTables(SavePolicy.DO_NOT_SAVE);
		} else {
			CyRootNetworkManager rootNetworkManager = ServiceHelper.getService(CyRootNetworkManager.class);
			CyRootNetwork rootNetwork = rootNetworkManager.getRootNetwork(parentNetwork);
			if (registerNetwork)
				network = rootNetwork.addSubNetwork();
			else
				network = rootNetwork.addSubNetwork(SavePolicy.DO_NOT_SAVE);
		}
		return new TiconeCytoscapeNetwork(network);
	}
}
