/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.imada.ticone.network.kpm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;

import dk.sdu.imada.ticone.network.NetworkUtil;

/**
 *
 * @author nalcaraz
 */
public class Parser {

	Map<String, Set<String>> backNodesMap;
	Map<String, Set<String>> backNodesByExpMap;
	Map<String, Set<String>> backGenesMap;
	Map<String, Integer> numCasesMap;
	Map<String, Integer> numGenesMap;
	Map<String, Double> avgExpressedCasesMap;
	Map<String, Double> avgExpressedGenesMap;
	Map<String, Integer> totalExpressedMap;

	public Parser() {
		this.backNodesMap = new HashMap<>();
		this.backNodesByExpMap = new HashMap<>();
		this.backGenesMap = new HashMap<>();
		this.numCasesMap = new HashMap<>();
		this.numGenesMap = new HashMap<>();
		this.avgExpressedCasesMap = new HashMap<>();
		this.avgExpressedGenesMap = new HashMap<>();
		this.totalExpressedMap = new HashMap<>();
		this.backGenesMap.put("L0", new HashSet());
		this.numCasesMap.put("L0", 0);

	}

	public KPMGraph createGraph(final CyNetwork network, final CyTable kpmTable) {
		final HashMap<String, String> nodeId2Symbol = new HashMap<>();
		final Map<String, Map<String, int[]>> expressionMap = new HashMap<>();
		final LinkedList<String[]> edgeList = new LinkedList<>();
		final HashMap<String, Integer> without_exp = new HashMap<>();
		final HashSet<String> inNetwork = new HashSet<>();
		final String fileId = "L0";
		this.numCasesMap.put(fileId, 0);
		without_exp.put(fileId, 0);

		// Read CyNetwork instead of graphfile
		final List<CyEdge> edges = network.getEdgeList();
		final CyTable nodeTable = network.getDefaultNodeTable();

		for (int i = 0; i < edges.size(); i++) {
			final CyEdge edge = edges.get(i);

			// Source node
			final CyNode source = edge.getSource();
			final String sourceID = nodeTable.getRow(source.getSUID()).get(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME,
					String.class);
			nodeId2Symbol.put(sourceID, sourceID);
			inNetwork.add(sourceID);

			// Target node
			final CyNode target = edge.getTarget();
			final String targetID = nodeTable.getRow(target.getSUID()).get(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME,
					String.class);
			nodeId2Symbol.put(targetID, targetID);
			inNetwork.add(targetID);

			edgeList.add(new String[] { sourceID, targetID });
		}

		final String line = "";

		// MY CODE START HERE, lolo
		final List<CyRow> rowList = kpmTable.getAllRows();

		final Set<String> inExp = new HashSet<>();
		final int totalExp = 0;
		int numCases = 0;
		final int numGenes = 0;
		for (int i = 0; i < rowList.size(); i++) {
			final String nodeId = rowList.get(i).get(NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME, String.class);
			final int[] exp = new int[1];
			final int kpmVal = rowList.get(i).get("kpm", Integer.class);
			exp[0] = kpmVal;
			inExp.add(nodeId);

			if (expressionMap.containsKey(nodeId)) {
				expressionMap.get(nodeId).put(fileId, exp);
			} else {
				final Map<String, int[]> aux = new HashMap<>();
				aux.put(fileId, exp);
				expressionMap.put(nodeId, aux);
			}
			numCases = exp.length;
			this.numCasesMap.put(fileId, numCases);
		}
		this.totalExpressedMap.put(fileId, totalExp);
		double avgExpCases = 0;
		double avgExpGenes = 0;
		if (totalExp > 0) {
			avgExpCases = (double) numCases / (double) totalExp;
			avgExpGenes = (double) numGenes / (double) totalExp;
		}
		this.numGenesMap.put(fileId, inExp.size());
		this.avgExpressedCasesMap.put(fileId, avgExpCases);
		this.avgExpressedGenesMap.put(fileId, avgExpGenes);
		final Set<String> bckN = new HashSet(inNetwork);
		final Set<String> bckG = new HashSet(inExp);
		for (final String id : inNetwork) {
			if (inExp.contains(id)) {
				bckN.remove(id);
			}
		}
		for (final String id : inExp) {
			if (inNetwork.contains(id)) {
				bckG.remove(id);
			}
		}

		this.backNodesByExpMap.put(fileId, bckN);
		this.backGenesMap.put(fileId, bckG);

		// MY CODE END HERE, lolo

		for (final String nodeId : inNetwork) {
			if (expressionMap.containsKey(nodeId)) {
				final Map<String, int[]> expMap = expressionMap.get(nodeId);
				if (!expMap.containsKey(fileId)) {
					if (this.backNodesMap.containsKey(nodeId)) {
						this.backNodesMap.get(nodeId).add(fileId);
					} else {
						final HashSet<String> aux = new HashSet<>();
						aux.add(fileId);
						this.backNodesMap.put(nodeId, aux);
					}
				}
			} else {
				final Set<String> temp = new HashSet<>();
				temp.add(fileId);
				if (this.backNodesMap.containsKey(nodeId)) {
					this.backNodesMap.get(nodeId).addAll(temp);
				} else {
					final HashSet<String> aux = new HashSet<>();
					aux.addAll(temp);
					this.backNodesMap.put(nodeId, aux);
				}
			}
		}

		Globals.NUM_CASES_MAP = this.numCasesMap;
		Globals.NUM_STUDIES = this.numCasesMap.size();
		return new KPMGraph(expressionMap, edgeList, nodeId2Symbol, this.backNodesMap, this.backGenesMap);
	}

	public boolean isNumber(final String input) {
		return input.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+");
	}

	public Map<String, Double> getAvgExpressedCasesMap() {
		return this.avgExpressedCasesMap;
	}

	public Map<String, Double> getAvgExpressedGenesMap() {
		return this.avgExpressedGenesMap;
	}

	public Map<String, Integer> getTotalExpressedMap() {
		return this.totalExpressedMap;
	}

	public Map<String, Set<String>> getBackNodesByExpMap() {
		return this.backNodesByExpMap;
	}

	public Map<String, Integer> getNumGenesMap() {
		return this.numGenesMap;
	}

}
