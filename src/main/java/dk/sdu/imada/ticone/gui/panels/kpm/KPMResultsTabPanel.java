package dk.sdu.imada.ticone.gui.panels.kpm;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import dk.sdu.imada.ticone.gui.HidableTabbedPane;
import dk.sdu.imada.ticone.gui.IKPMResultPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel.TAB_INDEX;

/**
 * 
 * @author wiwiec
 *
 */
public class KPMResultsTabPanel extends HidableTabbedPane {

	protected int nextKpmNumber;

	protected List<KPMResultFormPanel> kpmResultPanels;

	/**
	 * 
	 */
	private static final long serialVersionUID = -4894711667214407905L;
	protected IKPMResultPanel resultPanel;

	public KPMResultsTabPanel(final IKPMResultPanel resultPanel) {
		super();
		this.resultPanel = resultPanel;
		this.nextKpmNumber = 1;
		this.kpmResultPanels = new ArrayList<>();
	}

	public int getNextKpmNumber() {
		return this.nextKpmNumber;
	}

	@Override
	public void removeTabAt(final int index) {
		super.removeTabAt(index);
		this.kpmResultPanels.remove(index);
		if (this.getTabCount() == 0) {
			this.resultPanel.getjTabbedPane().setEnabledAt(TAB_INDEX.NETWORK_ENRICHMENT.ordinal(), false);
			this.resultPanel.getjTabbedPane().setSelectedIndex(0);
		}
	}

	public void addKPMResultPanel(final String title, final KPMResultFormPanel resultPanel) {
		this.kpmResultPanels.add(resultPanel);
		this.addTab(title, resultPanel.getMainPanel());
		this.nextKpmNumber++;
	}

	public List<KPMResultFormPanel> getKpmResultPanels() {
		return this.kpmResultPanels;
	}

	@Override
	protected void paintComponent(Graphics g) {
		try {
			super.paintComponent(g);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
