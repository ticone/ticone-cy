package dk.sdu.imada.ticone.gui.panels.connectivity;

import javax.swing.table.DefaultTableModel;

import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartWithTitle;

public class ConnectivityTableModel extends DefaultTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5422376559504186988L;

	public ConnectivityTableModel(final String[] columnNames) {
		super();

		for (final String col : columnNames)
			this.addColumn(col);
	}

	@Override
	public boolean isCellEditable(final int row, final int column) {
		return false;
	}

	@Override
	public Class<?> getColumnClass(final int columnIndex) {
		if (columnIndex == 0 || columnIndex == 1) {
			return ClusterChartWithTitle.class;
		} else if (this.getRowCount() == 0) {
			return Object.class;
		}
		return this.getValueAt(0, columnIndex).getClass();
	}
}