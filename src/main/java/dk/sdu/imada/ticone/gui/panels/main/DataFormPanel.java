package dk.sdu.imada.ticone.gui.panels.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;
import org.cytoscape.model.CyTableManager;
import org.cytoscape.model.events.ColumnCreatedEvent;
import org.cytoscape.model.events.ColumnCreatedListener;
import org.cytoscape.model.events.ColumnDeletedEvent;
import org.cytoscape.model.events.ColumnDeletedListener;
import org.cytoscape.model.events.ColumnNameChangedEvent;
import org.cytoscape.model.events.ColumnNameChangedListener;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskManager;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;

import dk.sdu.imada.ticone.clustering.BasicClusteringProcessBuilder;
import dk.sdu.imada.ticone.clustering.CLARAClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringMethod;
import dk.sdu.imada.ticone.clustering.IClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.IInitialClusteringProvider;
import dk.sdu.imada.ticone.clustering.InitialClusteringFromTable;
import dk.sdu.imada.ticone.clustering.InitialClusteringMethod;
import dk.sdu.imada.ticone.clustering.KMeansClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.PAMKClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.STEMClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResultFactory;
import dk.sdu.imada.ticone.clustering.TransClustClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMeanTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedianTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedoidNode;
import dk.sdu.imada.ticone.clustering.aggregate.AggregateClusterMedoidTimeSeries;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateClusterIntoNetworkLocation;
import dk.sdu.imada.ticone.clustering.aggregate.IAggregateClusterIntoTimeSeries;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.clustering.validity.DunnIndex;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.ClusteringFeatureNumberClusters;
import dk.sdu.imada.ticone.feature.ClusteringFeatureValidity;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.gui.JRuler;
import dk.sdu.imada.ticone.gui.panels.CollapsiblePanel;
import dk.sdu.imada.ticone.gui.panels.MyDialogPanel;
import dk.sdu.imada.ticone.gui.panels.leastfitting.HistogramDialog;
import dk.sdu.imada.ticone.gui.panels.leastfitting.PreprocesingContainer;
import dk.sdu.imada.ticone.gui.panels.leastfitting.PreprocessingDialog;
import dk.sdu.imada.ticone.gui.util.CyColumnComboBox;
import dk.sdu.imada.ticone.gui.util.CyNetworkComboBox;
import dk.sdu.imada.ticone.gui.util.CyNetworkComboBoxModel;
import dk.sdu.imada.ticone.gui.util.CyTableComboBox;
import dk.sdu.imada.ticone.io.ClusteringImportFromTableConfig;
import dk.sdu.imada.ticone.io.CyTableInputColumn;
import dk.sdu.imada.ticone.io.ILoadColumnDataMethod;
import dk.sdu.imada.ticone.io.ILoadDataMethod;
import dk.sdu.imada.ticone.io.InputColumn;
import dk.sdu.imada.ticone.io.LoadDataException;
import dk.sdu.imada.ticone.io.LoadDataFromFile;
import dk.sdu.imada.ticone.io.LoadDataFromTable;
import dk.sdu.imada.ticone.network.INetworkLocationPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.NetworkLocationPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.network.TiconeNetwork;
import dk.sdu.imada.ticone.network.TiconeNetworkImpl;
import dk.sdu.imada.ticone.network.TiconeNetworkImplFactory;
import dk.sdu.imada.ticone.preprocessing.PreprocessingException;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeBuilder;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.similarity.ICompositeSimilarityFunction;
import dk.sdu.imada.ticone.similarity.INetworkBasedSimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimpleSimilarityFunction;
import dk.sdu.imada.ticone.similarity.ITimeSeriesSimilarityFunction;
import dk.sdu.imada.ticone.similarity.IWeightedTimeSeriesSimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.similarity.InverseShortestPathSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.WeightedAverageCompositeSimilarityFunction;
import dk.sdu.imada.ticone.tasks.network.CalculateShortestPathsTaskFactory;
import dk.sdu.imada.ticone.tasks.preprocessing.PreprocessingTaskFactory;
import dk.sdu.imada.ticone.util.CyTableUtil;
import dk.sdu.imada.ticone.util.ExtractData;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.IIdMapMethod;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TableIdMapMethod;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;
import dk.sdu.imada.ticone.util.TimePointWeighting;
import dk.sdu.imada.ticone.util.Utility;
import dk.sdu.imada.ticone.variance.IObjectSetVariance;
import dk.sdu.imada.ticone.variance.StandardVariance;

/**
 * Created by christian on 20-01-16.
 */
public class DataFormPanel implements ColumnCreatedListener, ColumnDeletedListener, ColumnNameChangedListener {

	private JPanel mainPanel;
	private JPanel dataPanel;
	private JPanel similarityPanel;
	private JPanel prototypePanel;
	private JPanel otherPanel;

	// similarities
	private JComboBox<ITimeSeriesSimilarityFunction> timeSeriesSimilarityFunctionComboBox;
	private JComboBox<INetworkBasedSimilarityFunction> networkLocationSimilarityFunctionComboBox;
	private JComboBox shortestPathNetworkComboBox;

	private JRadioButton fromFileRadioButton;
	private JRadioButton fromTableRadioButton;
	// private JPanel loadTimeSeriesOptionsPanel;
	private JSpinner negativeStepsTextField;
	private JSpinner positiveStepsTextField;
	private JPanel discretizationRulerPanel;
	private JSpinner initialClustersWantedSpinner;
	private JTextField similarityThresholdTextField;
	private JLabel similarityThresholdLabel, loadDataErrorLabel;
	private JButton startButton;
	private JComboBox dataTableComboBox;
	private JPanel loadDataPanel;
	private JTextField dataFileTextField;
	private JCheckBox firstRowIsHeader;
	private JButton chooseDataFileButton;
	private JTextField patternFileTextField;
	private JButton choosePatternFileButton;
	private JPanel discretizationPanel;
	private JPanel clusteringPanel;
	private JPanel preprocessingOptionsPanel;
	private JLabel methodLabel;
	private JPanel dissagreeingSetsPanel;
	private JCheckBox removeDissagreeingObjectSetsCheckBox;
	private JTextField dissagreeingSetsTextField;
	private JPanel lowVariancePanel;

	private JCheckBox removeObjectsNotInNetworkCheckBox;
	private JPanel removeObjectsNotInNetworkPanel;
	private JComboBox networkCombobox;

	private JCheckBox removeSetsWithLowCheckBox;
	private JTextField lowVarianceTextField;
	private JButton visualizeDissagreeingButton;
	private JButton visualizeVarianceButton;
	private JButton histDissagreeingButton;
	private JButton histVarianceButton;
	private JComboBox clusteringMethodComboBox;
	private JComboBox dissagreeingCombobox;
	private JComboBox varianceCombobox;
	private JButton updateDiscretizedSteps;

	private JRuler jRuler;

	private String varianceThresholdText = "1";
	private String variancePercentText = "10";
	private String variancePreviousItem = "Percent";
	private String dissagreeingThresholdText = "0.5";
	private String dissagreeingPercentText = "10";
	private String dissagreeingPreviousItem = "Percent";

	// column mapping components
	private JPanel columnMappingPanel;
	private JButton columnMappingUpdateButton;
	private JPanel columnMappingFirstRowPanel;
	private JLabel firstRowTimePointsLabel;
	private JLabel firstRowReplicateLabel;
	private JLabel firstRowObjectIdLabel;
	private JCheckBox changeColumnMappingCheckbox;
	private JComboBox<InputColumn> replicateColumnSpinner;
	private DefaultComboBoxModel<InputColumn> replicateColumnSpinnerModel;
	private JComboBox<InputColumn> objectIdColumnSpinner;
	private DefaultComboBoxModel<InputColumn> objectIdColumnSpinnerModel;
	private JList<InputColumn> timePointColumnsList;
	private DefaultListModel<InputColumn> timePointColumnsListModel;

	// id mapping
	private JCheckBox idMappingCheckBox;
	private JComboBox<CyTable> idMappingTableComboBox;
	private JComboBox<CyColumn> idMappingKeyColumnComboBox, idMappingValueColumnComboBox;

	// time point weights
	private JCheckBox timePointWeightcheckBox;
	private JPanel timePointWeightPanel;
	private DefaultTableModel timePointWeightTableModel;
	private JTable timePointWeightTable;

	// clustering import from table
	private JPanel initialClusteringMethodPanel, initialClusteringTableImportPanel;
	private JComboBox clusteringImportTableComboBox;
	private JComboBox clusteringImportObjectIdColumnComboBox, clusteringImportClusterIdColumnComboBox;
	private JLabel clusteringImportErrorLabel;
	private JCheckBox importClusteringCollectMissingObjectsCheckBox;

	// cluster aggregate
	private JComboBox<IAggregateClusterIntoTimeSeries> clusterAggregateTimeSeriesFunctionComboBox;
	private JComboBox<IAggregateClusterIntoNetworkLocation> clusterAggregateNetworkFunctionComboBox;
	private JCheckBox includeNetworkLocationInPrototypeCheckBox;
	private JLabel networkLocationSimilarityFunctionLabel;
	private JComboBox<IInitialClusteringProvider> initialClusteringComboBox;
	private JFormattedTextField randomSeedTextField;
	private JPanel clusteringMethodParameterPanel;
	private JSpinner maxIterationsSpinner;
	private JSpinner nStartsSpinner;
	private JPanel initialClusteringPanel;
	private JSpinner samplesSpinner;
	private JSpinner sampleSizeSpinner;
	private JLabel samplesLabel;
	private JLabel sampleSizeLabel;
	private JLabel maxIterationsLabel;
	private JLabel nStartsLabel;

	protected void updateComponentsColumnMapping() throws TiconeUnloadingException {
		// update components for column mapping
		final ILoadDataMethod loadDataFrom = GUIUtility.getLoadDataMethod();
		if (loadDataFrom instanceof LoadDataFromFile) {
			final boolean firstRowHeader = ((LoadDataFromFile) loadDataFrom).isFirstRowHeaderFile();
			// read first line of file and count columns
			final String filePath = GUIUtility.getTimeSeriesTextString();
			try {
				final File timeSeriesData = new File(filePath);
				final Scanner scan = new Scanner(timeSeriesData);
				if (scan.hasNextLine()) {
					final String[] lineSplit = scan.nextLine().split("\t");

					this.timePointColumnsListModel.removeAllElements();
					this.objectIdColumnSpinnerModel.removeAllElements();
					this.replicateColumnSpinnerModel.removeAllElements();
					final int[] indices = new int[lineSplit.length - 1];
					for (int c = 0; c < lineSplit.length; c++) {
						final InputColumn col = new InputColumn(firstRowHeader ? lineSplit[c] : "Column " + (c + 1), c);
						this.timePointColumnsListModel.addElement(col);
						this.objectIdColumnSpinnerModel.addElement(col);
						this.replicateColumnSpinnerModel.addElement(col);
						if (c > 0)
							indices[c - 1] = c;
					}
					this.timePointColumnsList.setSelectedIndices(indices);
					this.objectIdColumnSpinner.setSelectedIndex(0);
					this.replicateColumnSpinner.setSelectedIndex(-1);
				}
				scan.close();
			} catch (final Exception ex) {
				ex.printStackTrace();
			}
		} else if (loadDataFrom instanceof LoadDataFromTable) {
			if (this.dataTableComboBox.getSelectedIndex() > -1) {
				final CyTable selectedTable = GUIUtility
						.getSelectedTable(ServiceHelper.getService(CyTableManager.class));
				if (selectedTable != null) {
					this.timePointColumnsListModel.removeAllElements();
					this.objectIdColumnSpinnerModel.removeAllElements();
					this.replicateColumnSpinnerModel.removeAllElements();
					this.replicateColumnSpinnerModel.addElement(InputColumn.NO_COLUMN);
					// timePointColumnsListModel.addElement(selectedTable.getPrimaryKey().getName());
					// TODO: workaround in case column order is messed up after
					// loading session
					final List<CyTableInputColumn> columns = CyTableUtil
							.getLengthAlphabeticallySortedColumns(selectedTable);
					for (final CyTableInputColumn c : columns) {
						if (Number.class.isAssignableFrom(c.getType()))
							this.timePointColumnsListModel.addElement(c);
						this.objectIdColumnSpinnerModel.addElement(c);
						this.replicateColumnSpinnerModel.addElement(c);
					}
					this.objectIdColumnSpinner.setSelectedIndex(0);// Column 1 // Column 2 // Column 3
					this.replicateColumnSpinner.setSelectedIndex(0); // -- // Column 1 // Column 2

					final List<Integer> tpIndices = new ArrayList<>();
					for (int i = 0; i < this.timePointColumnsListModel.size(); i++) {
						final InputColumn tpCandidate = this.timePointColumnsListModel.getElementAt(i);
						if (tpCandidate != this.objectIdColumnSpinner.getSelectedItem()
								&& tpCandidate != this.replicateColumnSpinner.getSelectedItem())
							tpIndices.add(i);
					}

					this.timePointColumnsList.setSelectedIndices(tpIndices.stream().mapToInt(in -> in).toArray());
				}
			}
		}
	}

	protected void updateFirstRowData() throws TiconeUnloadingException {
		boolean parsingValid = false;

		String objectId = "-";
		String replicate = "-";
		double[] timeSeries = null;

		final ILoadDataMethod loadDataFrom = GUIUtility.getLoadDataMethod();
		if (loadDataFrom instanceof LoadDataFromFile) {
			// read first line of file and count columns
			final String filePath = GUIUtility.getTimeSeriesTextString();
			try {
				final File timeSeriesData = new File(filePath);
				final Scanner scan = new Scanner(timeSeriesData);

				if (((LoadDataFromFile) loadDataFrom).isFirstRowHeaderFile()) {
					// burn header line
					if (scan.hasNextLine())
						scan.nextLine();
				}

				if (scan.hasNextLine()) {
					final String[] lineSplit = scan.nextLine().split("\t");

					objectId = lineSplit[((ILoadColumnDataMethod) this.clusteringProcessBuilder.getLoadDataMethod())
							.getColumnMapping().getObjectIdColumnIndex()];
					int replicateColumnIndex = ((ILoadColumnDataMethod) this.clusteringProcessBuilder
							.getLoadDataMethod()).getColumnMapping().getReplicateColumnIndex();
					if (replicateColumnIndex > -1)
						replicate = lineSplit[replicateColumnIndex];
					final int[] timePointColumns = ((ILoadColumnDataMethod) this.clusteringProcessBuilder
							.getLoadDataMethod()).getColumnMapping().getTimePointsColumnIndices();
					timeSeries = new double[timePointColumns.length];
					for (int c = 0; c < timePointColumns.length; c++) {
						timeSeries[c] = Double.valueOf(lineSplit[timePointColumns[c]]);
					}

					parsingValid = true;
				}
				scan.close();
			} catch (final Exception ex) {
				ex.printStackTrace();
				this.loadDataErrorLabel.setText(ex.getMessage());
			}
		} else if (loadDataFrom instanceof LoadDataFromTable) {
			if (this.dataTableComboBox.getSelectedIndex() > -1) {
				final ITimeSeriesObjectList tsData = this.clusteringProcessBuilder.getTimeSeriesPreprocessor()
						.getObjects();
				if (tsData != null && tsData.size() > 0) {
					// update labels showing parsed information from first
					// row in input
					final ITimeSeriesObject firstRow = tsData.get(0);
					objectId = firstRow.getName();
					replicate = firstRow.getSampleNameList().get(0);
					timeSeries = firstRow.getOriginalTimeSeriesList()[0].asArray();
					parsingValid = true;
				}
			}
		}

		this.firstRowObjectIdLabel.setText(objectId);
		this.firstRowReplicateLabel.setText(replicate);

		if (parsingValid) {
			final StringBuilder sb = new StringBuilder();
			sb.append("<html><body>");
			int i;
			for (i = 0; i < timeSeries.length; i++) {
				final double d = timeSeries[i];
				sb.append(String.format("%.3f", d));
				// not last element
				if (i < timeSeries.length - 1) {
					sb.append(", ");
					if ((i + 1) % 5 == 0)
						sb.append("<br>");
				}
				if (sb.length() > 100)
					break;
			}

			sb.append("</body></html>");
			this.firstRowTimePointsLabel.setText(sb.toString());
		} else {
			this.firstRowTimePointsLabel.setText("-");
		}
	}

	protected PrototypeBuilder prototypeFactory;

	protected TimeSeriesPrototypeComponentBuilder tsPrototypeComponentFactory;

	protected NetworkLocationPrototypeComponentBuilder nlPrototypeComponentFactory;

	protected TiconeNetworkImplFactory networkFactory;

	protected BasicClusteringProcessBuilder<TiconeCytoscapeClusteringResult> clusteringProcessBuilder;

	public DataFormPanel() throws InterruptedException {
		super();
		this.$$$setupUI$$$();

		this.setProcessBuilder(new BasicClusteringProcessBuilder<TiconeCytoscapeClusteringResult>());
		ServiceHelper.registerService(ColumnCreatedListener.class, this);
		ServiceHelper.registerService(ColumnDeletedListener.class, this);
		ServiceHelper.registerService(ColumnNameChangedListener.class, this);
	}

	public BasicClusteringProcessBuilder<TiconeCytoscapeClusteringResult> getClusteringProcess() {
		return this.clusteringProcessBuilder;
	}

	public void setProcessBuilder(final BasicClusteringProcessBuilder<TiconeCytoscapeClusteringResult> utils) {
		IIdMapMethod newIdMapMethod = null;
		if (this.clusteringProcessBuilder != null && this.clusteringProcessBuilder.getIdMapMethod() != null) {
			newIdMapMethod = this.clusteringProcessBuilder.getIdMapMethod().copy();
			utils.setIdMapMethod(newIdMapMethod);
		} else {
			newIdMapMethod = new TableIdMapMethod();
		}
		utils.setInitialClusteringProvider(
				(IInitialClusteringProvider<ClusterObjectMapping>) this.initialClusteringComboBox.getSelectedItem());
		utils.setLoadDataMethod(new LoadDataFromTable());
		utils.setIdMapMethod(newIdMapMethod);
		utils.setTimePointWeighting(new TimePointWeighting(0));
		this.prototypeFactory = new PrototypeBuilder();
		this.tsPrototypeComponentFactory = new TimeSeriesPrototypeComponentBuilder();
		this.tsPrototypeComponentFactory.setAggregationFunction(
				(IAggregateCluster<ITimeSeries>) this.clusterAggregateTimeSeriesFunctionComboBox.getSelectedItem());
		this.prototypeFactory.addPrototypeComponentFactory(this.tsPrototypeComponentFactory);

		if (this.includeNetworkLocationInPrototypeCheckBox.isSelected()) {
			this.addShortestPathPrototypeComponentFactory();
		} else {
			this.removeShortestPathPrototypeComponentFactory();
		}

		// reuse previous factory to keep cache of created network instances (to not
		// recalculate shortest paths every time)
		if (this.networkFactory == null)
			this.networkFactory = new TiconeNetworkImplFactory();

		utils.setPrototypeBuilder(this.prototypeFactory);
		utils.setSeed(Long.valueOf(randomSeedTextField.getText()));
		utils.setTimeSeriesPreprocessor(new AbsoluteValues());

		this.clusteringProcessBuilder = utils;
	}

	private void setupButtons() {
		this.setupRadioButtonGroups();
		this.setupActionListeners();
	}

	private void updateLoadDataMethod() throws TiconeUnloadingException, InterruptedException {
		try {
			this.loadDataErrorLabel.setText("");
			if (this.clusteringProcessBuilder.getLoadDataMethod() instanceof LoadDataFromFile) {
				final LoadDataFromFile loadData = (LoadDataFromFile) this.clusteringProcessBuilder.getLoadDataMethod();

				if (this.dataFileTextField.getText().isEmpty())
					return;
				loadData.setImportFile(new File(this.dataFileTextField.getText()));
				loadData.setFirstRowHeaderFile(this.firstRowIsHeader.isSelected());

			} else if (this.clusteringProcessBuilder.getLoadDataMethod() instanceof LoadDataFromTable) {
				final LoadDataFromTable loadData = (LoadDataFromTable) this.clusteringProcessBuilder
						.getLoadDataMethod();
				loadData.setImportTable(GUIUtility.getSelectedTable(ServiceHelper.getService(CyTableManager.class)));
			}

			if (!this.getLoadDataMethod().isDataLoaded()) {
				this.getLoadDataMethod().loadData(this.clusteringProcessBuilder.getTimeSeriesPreprocessor());
				if (this.getLoadDataMethod().isDataLoaded())
					this.clusteringProcessBuilder.getTimeSeriesPreprocessor().process();
			}
		} catch (final LoadDataException | PreprocessingException e) {
			this.loadDataErrorLabel.setText(e.getMessage());
		}

		this.updateComponentsAfterLoadMethodUpdate();

		// we cannot make this dependent on whether
		// LoadDataMethod.isDataLoaded() returns true, because also
		// in case of loading errors we need to be able to change the columns.
		if (this.clusteringProcessBuilder.getLoadDataMethod() instanceof LoadDataFromFile) {
			this.changeColumnMappingCheckbox.setEnabled(!this.dataFileTextField.getText().isEmpty());
			if (!this.dataFileTextField.getText().isEmpty()) {
				this.updateComponentsColumnMapping();
			}
		} else {
			this.changeColumnMappingCheckbox.setEnabled(this.dataTableComboBox.getSelectedIndex() > -1);
			if (this.dataTableComboBox.getSelectedIndex() > -1) {
				this.updateComponentsColumnMapping();
			}
		}
	}

	private void updateComponentsAfterLoadMethodUpdate() throws TiconeUnloadingException {
		this.startButton.setEnabled(this.clusteringProcessBuilder.getLoadDataMethod().isDataLoaded());
		this.columnMappingFirstRowPanel.setVisible(this.clusteringProcessBuilder.getLoadDataMethod().isDataLoaded());
		this.discretizationPanel.setVisible(this.clusteringProcessBuilder.getLoadDataMethod().isDataLoaded());
		this.histDissagreeingButton.setEnabled(this.clusteringProcessBuilder.getLoadDataMethod().isDataLoaded());
		this.histVarianceButton.setEnabled(this.clusteringProcessBuilder.getLoadDataMethod().isDataLoaded());
		this.visualizeVarianceButton.setEnabled(this.removeSetsWithLowCheckBox.isSelected()
				&& this.clusteringProcessBuilder.getLoadDataMethod().isDataLoaded());
		this.visualizeDissagreeingButton.setEnabled(this.removeDissagreeingObjectSetsCheckBox.isSelected()
				&& this.clusteringProcessBuilder.getLoadDataMethod().isDataLoaded());
		if (this.clusteringProcessBuilder.getLoadDataMethod().isDataLoaded()) {
			try {
				this.updateDiscretizationSlider();
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
			this.updateFirstRowData();
		}

	}

	private void setupActionListeners() {

		/**
		 * Data table
		 */
		this.dataTableComboBox.addActionListener(new DataComboBoxActionListener());
		/**
		 * Load data actions
		 */
		this.fromTableRadioButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				DataFormPanel.this.dataTableComboBox.setVisible(true);
				DataFormPanel.this.dataFileTextField.setVisible(false);
				DataFormPanel.this.firstRowIsHeader.setVisible(false);
				DataFormPanel.this.chooseDataFileButton.setVisible(false);

				DataFormPanel.this.clusteringProcessBuilder.setLoadDataMethod(new LoadDataFromTable());
				try {
					DataFormPanel.this.updateLoadDataMethod();
				} catch (final TiconeUnloadingException | InterruptedException e1) {
				}
			}
		});
		this.fromFileRadioButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				DataFormPanel.this.dataTableComboBox.setVisible(false);
				DataFormPanel.this.dataFileTextField.setVisible(true);
				DataFormPanel.this.firstRowIsHeader.setVisible(true);
				DataFormPanel.this.chooseDataFileButton.setVisible(true);

				DataFormPanel.this.clusteringProcessBuilder.setLoadDataMethod(new LoadDataFromFile());
				try {
					DataFormPanel.this.updateLoadDataMethod();
				} catch (final TiconeUnloadingException | InterruptedException e1) {
				}
			}
		});
		this.firstRowIsHeader.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					DataFormPanel.this.updateLoadDataMethod();
				} catch (final TiconeUnloadingException | InterruptedException e1) {
				}
			}
		});

		/**
		 * Data options
		 */
		this.chooseDataFileButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					DataFormPanel.this.openTimeSeriesDataAction();
				} catch (final TiconeUnloadingException | InterruptedException e1) {
				}
			}
		});
		this.choosePatternFileButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				DataFormPanel.this.openPatternFileAction();
			}
		});

		/**
		 * Initial clustering method actions
		 */
		this.clusteringMethodComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				String selectedMethod = (String) DataFormPanel.this.clusteringMethodComboBox.getSelectedItem();
				if (selectedMethod.equals("TransClust")) {
					DataFormPanel.this.similarityThresholdLabel.setVisible(true);
					DataFormPanel.this.similarityThresholdTextField.setVisible(true);
				} else {
					DataFormPanel.this.similarityThresholdLabel.setVisible(false);
					DataFormPanel.this.similarityThresholdTextField.setVisible(false);
				}

				// hide / show method parameters
				final boolean showSamplesSpinner = selectedMethod.equals("CLARA");
				final boolean showSampleSizeSpinner = selectedMethod.equals("CLARA");
				final boolean showMaxIterationsSpinner = selectedMethod.equals("K-Means")
						|| selectedMethod.equals("PAMK") || selectedMethod.equals("CLARA");
				final boolean showNstartSpinner = showMaxIterationsSpinner;

				samplesLabel.setVisible(showSamplesSpinner);
				samplesSpinner.setVisible(showSamplesSpinner);
				sampleSizeLabel.setVisible(showSampleSizeSpinner);
				sampleSizeSpinner.setVisible(showSampleSizeSpinner);
				maxIterationsLabel.setVisible(showMaxIterationsSpinner);
				maxIterationsSpinner.setVisible(showMaxIterationsSpinner);
				nStartsLabel.setVisible(showNstartSpinner);
				nStartsSpinner.setVisible(showNstartSpinner);

				boolean showParameterPanel = showSamplesSpinner || showSampleSizeSpinner || showMaxIterationsSpinner
						|| showNstartSpinner;

				clusteringMethodParameterPanel.setVisible(showParameterPanel);
			}
		});

		/**
		 * Start button
		 */
		this.startButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				DataFormPanel.this.startAction();
			}
		});

		/**
		 * Visualize dissagreeing and low variance
		 */
		this.visualizeDissagreeingButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					DataFormPanel.this.visualizeDissagreeingAction();
				} catch (SimilarityCalculationException | IncompatiblePrototypeComponentException
						| MissingPrototypeFactoryException | IncompatibleSimilarityFunctionException
						| IncompatibleObjectTypeException e1) {
					MyDialogPanel.showMessageDialog(e1.getMessage());
				} catch (final InterruptedException e1) {
				}
			}
		});

		this.visualizeVarianceButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					DataFormPanel.this.visualizeVarianceAction();
				} catch (final SimilarityCalculationException | IncompatibleObjectTypeException e1) {
					MyDialogPanel.showMessageDialog(e1.getMessage());
				} catch (final InterruptedException e1) {
				}
			}
		});

		/**
		 * Histogram actions
		 */
		this.histDissagreeingButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					DataFormPanel.this.showHistogram(false);
				} catch (SimilarityCalculationException | IncompatiblePrototypeComponentException
						| MissingPrototypeFactoryException | IncompatibleSimilarityFunctionException
						| IncompatibleObjectTypeException e1) {
					MyDialogPanel.showMessageDialog(e1.getMessage());
				} catch (final InterruptedException e1) {
				}
			}
		});

		this.histVarianceButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					DataFormPanel.this.showHistogram(true);
				} catch (SimilarityCalculationException | IncompatiblePrototypeComponentException
						| MissingPrototypeFactoryException | IncompatibleSimilarityFunctionException
						| IncompatibleObjectTypeException e1) {
					MyDialogPanel.showMessageDialog(e1.getMessage());
				} catch (final InterruptedException e1) {
				}
			}
		});

		/**
		 * Combobox for variance and dissagreeing sets
		 */
		this.varianceCombobox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					DataFormPanel.this.variancePreviousItem = (String) e.getItem();
					if (DataFormPanel.this.variancePreviousItem.equals("Threshold")) {
						DataFormPanel.this.varianceThresholdText = DataFormPanel.this.lowVarianceTextField.getText();
					} else if (DataFormPanel.this.variancePreviousItem.equals("Percent")) {
						DataFormPanel.this.variancePercentText = DataFormPanel.this.lowVarianceTextField.getText();
					}
				}
				if (e.getStateChange() == ItemEvent.SELECTED) {
					final String item = (String) e.getItem();
					if (item.equals("Threshold")) {
						DataFormPanel.this.lowVarianceTextField.setText(DataFormPanel.this.varianceThresholdText);
					} else if (item.equals("Percent")) {
						DataFormPanel.this.lowVarianceTextField.setText(DataFormPanel.this.variancePercentText);
					}

				}
			}
		});
		this.dissagreeingCombobox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					DataFormPanel.this.dissagreeingPreviousItem = (String) e.getItem();
					if (DataFormPanel.this.dissagreeingPreviousItem.equals("Threshold")) {
						DataFormPanel.this.dissagreeingThresholdText = DataFormPanel.this.dissagreeingSetsTextField
								.getText();
					} else if (DataFormPanel.this.dissagreeingPreviousItem.equals("Percent")) {
						DataFormPanel.this.dissagreeingPercentText = DataFormPanel.this.dissagreeingSetsTextField
								.getText();
					}
				}
				if (e.getStateChange() == ItemEvent.SELECTED) {
					final String item = (String) e.getItem();
					if (item.equals("Threshold")) {
						DataFormPanel.this.dissagreeingSetsTextField
								.setText(DataFormPanel.this.dissagreeingThresholdText);
					} else if (item.equals("Percent")) {
						DataFormPanel.this.dissagreeingSetsTextField
								.setText(DataFormPanel.this.dissagreeingPercentText);
					}

				}
			}
		});

		/**
		 * Update discretized steps button
		 */
		this.updateDiscretizedSteps.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					DataFormPanel.this.updateDiscretizationSlider();
				} catch (final InterruptedException ie) {

				}
			}
		});

		this.removeObjectsNotInNetworkCheckBox.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(final ChangeEvent e) {
				DataFormPanel.this.networkCombobox
						.setEnabled(DataFormPanel.this.removeObjectsNotInNetworkCheckBox.isSelected());
			}
		});

		this.removeDissagreeingObjectSetsCheckBox.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(final ChangeEvent e) {
				DataFormPanel.this.dissagreeingCombobox
						.setEnabled(DataFormPanel.this.removeDissagreeingObjectSetsCheckBox.isSelected());
				DataFormPanel.this.dissagreeingSetsTextField
						.setEnabled(DataFormPanel.this.removeDissagreeingObjectSetsCheckBox.isSelected());
				DataFormPanel.this.visualizeDissagreeingButton
						.setEnabled(DataFormPanel.this.removeDissagreeingObjectSetsCheckBox.isSelected()
								&& DataFormPanel.this.clusteringProcessBuilder.getLoadDataMethod().isDataLoaded());
			}
		});

		this.removeSetsWithLowCheckBox.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(final ChangeEvent e) {
				DataFormPanel.this.varianceCombobox
						.setEnabled(DataFormPanel.this.removeSetsWithLowCheckBox.isSelected());
				DataFormPanel.this.lowVarianceTextField
						.setEnabled(DataFormPanel.this.removeSetsWithLowCheckBox.isSelected());
				DataFormPanel.this.visualizeVarianceButton
						.setEnabled(DataFormPanel.this.removeSetsWithLowCheckBox.isSelected()
								&& DataFormPanel.this.clusteringProcessBuilder.getLoadDataMethod().isDataLoaded());
			}
		});

		// column mapping
		this.changeColumnMappingCheckbox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					DataFormPanel.this.columnMappingPanel
							.setVisible(DataFormPanel.this.changeColumnMappingCheckbox.isSelected());
					DataFormPanel.this.objectIdColumnSpinner
							.setEnabled(DataFormPanel.this.changeColumnMappingCheckbox.isSelected());
					DataFormPanel.this.replicateColumnSpinner
							.setEnabled(DataFormPanel.this.changeColumnMappingCheckbox.isSelected());
					DataFormPanel.this.timePointColumnsList
							.setEnabled(DataFormPanel.this.changeColumnMappingCheckbox.isSelected());

					// if we have unchecked this, we have to update the first row
					if (!DataFormPanel.this.changeColumnMappingCheckbox.isSelected()) {
						try {
							DataFormPanel.this.loadDataErrorLabel.setText("");
							if (!DataFormPanel.this.getLoadDataMethod().isDataLoaded()) {
								DataFormPanel.this.getLoadDataMethod().loadData(
										DataFormPanel.this.clusteringProcessBuilder.getTimeSeriesPreprocessor());
								if (DataFormPanel.this.getLoadDataMethod().isDataLoaded())
									DataFormPanel.this.clusteringProcessBuilder.getTimeSeriesPreprocessor().process();
							}

							updateComponentsAfterLoadMethodUpdate();
						} catch (final LoadDataException | PreprocessingException e1) {
							DataFormPanel.this.loadDataErrorLabel.setText(e1.getMessage());
						}
						// restore default columns
						DataFormPanel.this.updateComponentsColumnMapping();
					} else {
					}
				} catch (final TiconeUnloadingException | InterruptedException e1) {
				}
			}
		});

		this.columnMappingUpdateButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					DataFormPanel.this.loadDataErrorLabel.setText("");
					if (!DataFormPanel.this.getLoadDataMethod().isDataLoaded()) {
						DataFormPanel.this.getLoadDataMethod()
								.loadData(DataFormPanel.this.clusteringProcessBuilder.getTimeSeriesPreprocessor());
						if (DataFormPanel.this.getLoadDataMethod().isDataLoaded())
							DataFormPanel.this.clusteringProcessBuilder.getTimeSeriesPreprocessor().process();
					}

					updateComponentsAfterLoadMethodUpdate();
				} catch (final LoadDataException | PreprocessingException e1) {
					DataFormPanel.this.loadDataErrorLabel.setText(e1.getMessage());
				} catch (final TiconeUnloadingException | InterruptedException e1) {
				}
			}
		});

		this.timePointColumnsList.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(final ListSelectionEvent e) {
				final InputColumn[] columns = DataFormPanel.this.timePointColumnsList.getSelectedValuesList()
						.toArray(new InputColumn[0]);
				final int[] columnIndices = new int[columns.length];
				final String[] columnNames = new String[columns.length];

				for (int i = 0; i < columns.length; i++) {
					final InputColumn col = columns[i];
					columnIndices[i] = col.getIndex();
					columnNames[i] = col.getName();
				}
				DataFormPanel.this.getLoadDataMethod().getColumnMapping().setTimePointsColumns(columnIndices,
						columnNames);
				DataFormPanel.this.updateTimePointWeightTable();
			}
		});

		this.objectIdColumnSpinner.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				if (DataFormPanel.this.objectIdColumnSpinner.getSelectedIndex() == -1)
					return;
				final InputColumn selectedItem = (InputColumn) DataFormPanel.this.objectIdColumnSpinner
						.getSelectedItem();
				DataFormPanel.this.getLoadDataMethod().getColumnMapping().setObjectIdColumn(selectedItem.getIndex(),
						selectedItem.getName());
			}
		});

		this.replicateColumnSpinner.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				if (DataFormPanel.this.replicateColumnSpinner.getSelectedIndex() == -1)
					return;
				final InputColumn selectedItem = (InputColumn) DataFormPanel.this.replicateColumnSpinner
						.getSelectedItem();
				if (selectedItem.equals(InputColumn.NO_COLUMN))
					DataFormPanel.this.getLoadDataMethod().getColumnMapping().setReplicateColumn(-1, null);
				else
					DataFormPanel.this.getLoadDataMethod().getColumnMapping()
							.setReplicateColumn(selectedItem.getIndex(), selectedItem.getName());
			}
		});

		this.idMappingCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				DataFormPanel.this.clusteringProcessBuilder.getIdMapMethod()
						.setActive(DataFormPanel.this.idMappingCheckBox.isSelected());

				DataFormPanel.this.idMappingTableComboBox.setEnabled(DataFormPanel.this.idMappingCheckBox.isSelected());
				DataFormPanel.this.idMappingKeyColumnComboBox
						.setEnabled(DataFormPanel.this.idMappingCheckBox.isSelected());
				DataFormPanel.this.idMappingValueColumnComboBox
						.setEnabled(DataFormPanel.this.idMappingCheckBox.isSelected());
			}
		});

		this.idMappingTableComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				final CyTable table = (CyTable) DataFormPanel.this.idMappingTableComboBox.getSelectedItem();
				if (table == null)
					return;
				((CyColumnComboBox) DataFormPanel.this.idMappingKeyColumnComboBox).setTable(table);
				((CyColumnComboBox) DataFormPanel.this.idMappingValueColumnComboBox).setTable(table);

				final TableIdMapMethod idMapMethod = ((TableIdMapMethod) DataFormPanel.this.clusteringProcessBuilder
						.getIdMapMethod());
				idMapMethod.setIdMapTable(table);
			}
		});

		this.idMappingKeyColumnComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				final CyColumn col = (CyColumn) DataFormPanel.this.idMappingKeyColumnComboBox.getSelectedItem();
				if (col == null)
					return;
				final TableIdMapMethod idMapMethod = ((TableIdMapMethod) DataFormPanel.this.clusteringProcessBuilder
						.getIdMapMethod());
				idMapMethod.setKeyColumnName(col.getName());
			}
		});

		this.idMappingValueColumnComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				final CyColumn col = (CyColumn) DataFormPanel.this.idMappingValueColumnComboBox.getSelectedItem();
				if (col == null)
					return;
				final TableIdMapMethod idMapMethod = ((TableIdMapMethod) DataFormPanel.this.clusteringProcessBuilder
						.getIdMapMethod());
				idMapMethod.setValueColumnName(col.getName());
			}
		});

		this.timePointWeightcheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				DataFormPanel.this.timePointWeightPanel
						.setVisible(DataFormPanel.this.timePointWeightcheckBox.isSelected());
				if (!DataFormPanel.this.timePointWeightcheckBox.isSelected()) {
					for (int r = 0; r < DataFormPanel.this.timePointWeightTableModel.getRowCount(); r++) {
						DataFormPanel.this.timePointWeightTableModel.setValueAt(100, r, 2);
					}
				}
			}
		});

		this.randomSeedTextField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				try {
					clusteringProcessBuilder.setSeed(Long.valueOf(randomSeedTextField.getText()));
				} catch (Exception e1) {
				}
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				try {
					clusteringProcessBuilder.setSeed(Long.valueOf(randomSeedTextField.getText()));
				} catch (Exception e1) {
				}
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				try {
					clusteringProcessBuilder.setSeed(Long.valueOf(randomSeedTextField.getText()));
				} catch (Exception e1) {
				}
			}
		});
	}

	protected void updateTimePointWeightTable() {
		this.timePointWeightTableModel.setRowCount(0);
		final int[] colIdx = ((ILoadColumnDataMethod) this.clusteringProcessBuilder.getLoadDataMethod())
				.getColumnMapping().getTimePointsColumnIndices();
		this.clusteringProcessBuilder.getTimePointWeighting().setTimePointColumns(colIdx.length);
		final String[] colNames = ((ILoadColumnDataMethod) this.clusteringProcessBuilder.getLoadDataMethod())
				.getColumnMapping().getTimePointsColumnNames();
		for (int i = 0; i < colIdx.length; i++) {
			this.timePointWeightTableModel.addRow(new Object[] { colIdx[i], colNames[i], 100, 100 });
		}
	}

	protected void updateTimePointWeighting() {
		final TimePointWeighting timePointWeighting = new TimePointWeighting(
				this.timePointWeightTableModel.getRowCount());
		for (int r = 0; r < this.timePointWeightTableModel.getRowCount(); r++) {
			timePointWeighting.setWeightForTimePointColumn(r, (int) this.timePointWeightTableModel.getValueAt(r, 2));
		}
		this.clusteringProcessBuilder.setTimePointWeighting(timePointWeighting);
	}

	private void showHistogram(final boolean varianceMode) throws SimilarityCalculationException,
			IncompatiblePrototypeComponentException, MissingPrototypeFactoryException, InterruptedException,
			IncompatibleSimilarityFunctionException, IncompatibleObjectTypeException {
		String title;
		String xAxisLabel;
		final ITimeSeriesObjectList objects = this.clusteringProcessBuilder.getTimeSeriesPreprocessor().getObjects();
		final double[] values = new double[objects.size()];
		if (varianceMode) {
			title = "Variance";
			xAxisLabel = "Average Standard Deviation";
			final IObjectSetVariance variance = new StandardVariance();
			double maxVariance = -1;
			for (int i = 0; i < objects.size(); i++) {
				values[i] = variance.calculateObjectSetVariance(objects.get(i));
				if (values[i] > maxVariance) {
					maxVariance = values[i];
				}
			}
		} else {
			this.updateSimilarityFunction();
			final ISimilarityFunction similarity = this.clusteringProcessBuilder.getSimilarityFunction();
			title = "Disagreeing objects";
			xAxisLabel = "Average Similarity";
			for (int i = 0; i < objects.size(); i++) {
				values[i] = ExtractData.findObjectConformity(objects.get(i), (ISimilarityFunction) similarity).get();
			}
		}
		final HistogramDialog dialog = new HistogramDialog(values, title, xAxisLabel);
		dialog.pack();
		dialog.setVisible(true);
	}

	private void visualizeDissagreeingAction() throws SimilarityCalculationException,
			IncompatiblePrototypeComponentException, MissingPrototypeFactoryException, InterruptedException,
			IncompatibleSimilarityFunctionException, IncompatibleObjectTypeException {
		ISimilarityFunction similarity;
		double threshold;
		try {
			threshold = Double.parseDouble(this.dissagreeingSetsTextField.getText());
		} catch (final NumberFormatException nfe) {
			return;
		}
		try {
			this.loadDataErrorLabel.setText("");
			if (!this.getLoadDataMethod().isDataLoaded()) {
				this.getLoadDataMethod().loadData(this.clusteringProcessBuilder.getTimeSeriesPreprocessor());
				if (this.getLoadDataMethod().isDataLoaded())
					this.clusteringProcessBuilder.getTimeSeriesPreprocessor().process();
			}
		} catch (final Exception e) {
			e.printStackTrace();
			return;
		}
		this.updateSimilarityFunction();
		similarity = (ISimilarityFunction) this.clusteringProcessBuilder.getSimilarityFunction();
		final ITimeSeriesObjectList objects = this.clusteringProcessBuilder.getTimeSeriesPreprocessor().getObjects();
		final PreprocesingContainer container = new PreprocesingContainer();
		final String parm = (String) this.dissagreeingCombobox.getSelectedItem();
		final PreprocessingDialog dialog = new PreprocessingDialog(container, similarity, threshold, objects, true,
				parm);
		dialog.pack();
		dialog.setVisible(true);
		if (container.ok) {
			this.dissagreeingCombobox.setSelectedItem(container.parm);
			this.dissagreeingSetsTextField.setText("" + container.threshold);
		}
	}

	private void visualizeVarianceAction()
			throws SimilarityCalculationException, InterruptedException, IncompatibleObjectTypeException {
		double threshold;
		try {
			threshold = Double.parseDouble(this.lowVarianceTextField.getText());
		} catch (final NumberFormatException nfe) {
			return;
		}
		try {
			this.loadDataErrorLabel.setText("");
			this.getLoadDataMethod().loadData(this.clusteringProcessBuilder.getTimeSeriesPreprocessor());
		} catch (final Exception e) {
			e.printStackTrace();
			return;
		}
		final ITimeSeriesObjectList objects = this.clusteringProcessBuilder.getTimeSeriesPreprocessor().getObjects();
		final PreprocesingContainer container = new PreprocesingContainer();
		final String parm = (String) this.varianceCombobox.getSelectedItem();
		final PreprocessingDialog dialog = new PreprocessingDialog(container, null, threshold, objects, false, parm);
		dialog.pack();
		dialog.setVisible(true);
		if (container.ok) {
			this.varianceCombobox.setSelectedItem(container.parm);
			this.lowVarianceTextField.setText("" + container.threshold);
		}
	}

	private void setupRadioButtonGroups() {
		final ButtonGroup loadDataMethodButtonGroup = new ButtonGroup();
		loadDataMethodButtonGroup.add(this.fromFileRadioButton);
		loadDataMethodButtonGroup.add(this.fromTableRadioButton);
	}

	public JPanel getMainPanel() {
		return this.mainPanel;
	}

	private void createUIComponents() {
		// use collapsible panels to save space
		this.discretizationPanel = new CollapsiblePanel("Discretization", false);
		this.negativeStepsTextField = new JSpinner(new SpinnerNumberModel(10, 1, 100, 1));
		this.positiveStepsTextField = new JSpinner(new SpinnerNumberModel(10, 1, 100, 1));

		this.clusteringPanel = new CollapsiblePanel("Clustering", true);
		this.initialClustersWantedSpinner = new JSpinner(new SpinnerNumberModel(10, 1, Integer.MAX_VALUE, 1));
		this.initialClustersWantedSpinner.setPreferredSize(new Dimension(100, -1));
		this.initialClustersWantedSpinner.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				if (sampleSizeSpinner != null)
					sampleSizeSpinner.setValue(40 + 2 * getNumberOfInitialPatterns());
			}
		});

		this.preprocessingOptionsPanel = new CollapsiblePanel("Preprocessing", false);
		this.similarityPanel = new CollapsiblePanel("Similarities", false);
		this.prototypePanel = new CollapsiblePanel("Prototypes", false);
		this.otherPanel = new CollapsiblePanel("Other", false);

		this.discretizationRulerPanel = new JPanel();

		this.networkCombobox = new CyNetworkComboBox();
		this.removeObjectsNotInNetworkCheckBox = new JCheckBox();
		this.shortestPathNetworkComboBox = new CyNetworkComboBox(
				(CyNetworkComboBoxModel) this.networkCombobox.getModel());
		this.shortestPathNetworkComboBox.setVisible(false);
		this.fromFileRadioButton = new JRadioButton();
		this.fromTableRadioButton = new JRadioButton();
		this.loadDataErrorLabel = new JLabel() {
			@Override
			public void setText(final String text) {
				this.setVisible(text != null && !text.isEmpty());
				super.setText("<html>" + text + "</html>");
			}
		};
		this.loadDataErrorLabel.setVisible(false);
		this.firstRowIsHeader = new JCheckBox("First Row Contains Header");
		this.chooseDataFileButton = new JButton();
		this.choosePatternFileButton = new JButton();
		this.dataTableComboBox = new CyTableComboBox();
		this.startButton = new JButton();
		this.methodLabel = new JLabel();
		this.visualizeDissagreeingButton = new JButton();
		this.visualizeVarianceButton = new JButton();
		this.histDissagreeingButton = new JButton();
		this.histVarianceButton = new JButton();

		// clustering methods selection
		this.clusteringMethodComboBox = new JComboBox<>();
		// additional method parameters
		this.maxIterationsSpinner = new JSpinner(new SpinnerNumberModel(50, 1, Integer.MAX_VALUE, 1));
		this.maxIterationsSpinner.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				int maxIt = (int) maxIterationsSpinner.getValue();
				IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethod = clusteringProcessBuilder
						.getClusteringMethodBuilder();
				if (clusteringMethod instanceof KMeansClusteringMethodBuilder)
					((KMeansClusteringMethodBuilder) clusteringMethod).setMaxIterations(maxIt);
				else if (clusteringMethod instanceof PAMKClusteringMethodBuilder)
					((PAMKClusteringMethodBuilder) clusteringMethod).setSwapIterations(maxIt);
				else if (clusteringMethod instanceof CLARAClusteringMethodBuilder)
					((CLARAClusteringMethodBuilder) clusteringMethod)
							.setPamkBuilder(new PAMKClusteringMethodBuilder().setSwapIterations(maxIt));
			}
		});
		this.nStartsSpinner = new JSpinner(new SpinnerNumberModel(5, 1, Integer.MAX_VALUE, 1));
		this.nStartsSpinner.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				int nstart = (int) nStartsSpinner.getValue();
				IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethod = clusteringProcessBuilder
						.getClusteringMethodBuilder();
				if (clusteringMethod instanceof KMeansClusteringMethodBuilder)
					((KMeansClusteringMethodBuilder) clusteringMethod).setNstart(nstart);
				else if (clusteringMethod instanceof PAMKClusteringMethodBuilder)
					((PAMKClusteringMethodBuilder) clusteringMethod).setNstart(nstart);
				else if (clusteringMethod instanceof CLARAClusteringMethodBuilder)
					((CLARAClusteringMethodBuilder) clusteringMethod)
							.setPamkBuilder(new PAMKClusteringMethodBuilder().setNstart(nstart));
			}
		});
		this.samplesSpinner = new JSpinner(new SpinnerNumberModel(5, 1, Integer.MAX_VALUE, 1));
		this.samplesSpinner.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				int samples = (int) samplesSpinner.getValue();
				IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethod = clusteringProcessBuilder
						.getClusteringMethodBuilder();
				if (clusteringMethod instanceof CLARAClusteringMethodBuilder)
					((CLARAClusteringMethodBuilder) clusteringMethod).setSamples(samples);
			}
		});

		this.sampleSizeSpinner = new JSpinner(
				// TODO: where does this number come from?
				new SpinnerNumberModel(40 + 2 * this.getNumberOfInitialPatterns(), 1, Integer.MAX_VALUE, 1));
		this.sampleSizeSpinner.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				int sampleSize = (int) sampleSizeSpinner.getValue();
				IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethod = clusteringProcessBuilder
						.getClusteringMethodBuilder();
				if (clusteringMethod instanceof CLARAClusteringMethodBuilder)
					((CLARAClusteringMethodBuilder) clusteringMethod).setSampleSize(sampleSize);
			}
		});

		this.dissagreeingCombobox = new JComboBox();
		this.varianceCombobox = new JComboBox();
		this.updateDiscretizedSteps = new JButton();
		this.removeDissagreeingObjectSetsCheckBox = new JCheckBox();
		this.removeSetsWithLowCheckBox = new JCheckBox();
		this.changeColumnMappingCheckbox = new JCheckBox();
		this.firstRowTimePointsLabel = new JLabel();
		this.firstRowReplicateLabel = new JLabel();
		this.firstRowObjectIdLabel = new JLabel();
		this.columnMappingUpdateButton = new JButton();

		this.objectIdColumnSpinnerModel = new DefaultComboBoxModel<>();
		this.objectIdColumnSpinner = new JComboBox(this.objectIdColumnSpinnerModel);
		this.replicateColumnSpinnerModel = new DefaultComboBoxModel<>();
		this.replicateColumnSpinner = new JComboBox(this.replicateColumnSpinnerModel);
		this.timePointColumnsListModel = new DefaultListModel<>();
		this.timePointColumnsList = new JList<>(this.timePointColumnsListModel);
		// id mapping
		this.idMappingCheckBox = new JCheckBox();
		this.idMappingTableComboBox = new CyTableComboBox();
		this.idMappingKeyColumnComboBox = new CyColumnComboBox();
		this.idMappingValueColumnComboBox = new CyColumnComboBox();
		// time point weights
		this.timePointWeightcheckBox = new JCheckBox("Change Weights of Time Points");
		this.timePointWeightPanel = new JPanel();
		this.timePointWeightTableModel = new DefaultTableModel(
				new String[] { "Time Point Index", "Time Point", "Weight", "Weight TextField" }, 0);// {
		this.timePointWeightTableModel.addTableModelListener(new TableModelListener() {

			@Override
			public void tableChanged(final TableModelEvent e) {
				// slider changed
				if (e.getColumn() == 2) {
					DataFormPanel.this.timePointWeightTableModel.setValueAt(
							DataFormPanel.this.timePointWeightTableModel.getValueAt(e.getFirstRow(), e.getColumn()),
							e.getFirstRow(), e.getColumn() + 1);
				}
			}
		});
		this.timePointWeightTable = new JTable(this.timePointWeightTableModel);
		this.timePointWeightTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		this.timePointWeightTable.getColumnModel()
				.removeColumn(this.timePointWeightTable.getColumn("Time Point Index"));
		this.timePointWeightTable.getColumn("Weight")
				.setCellRenderer(new SliderRenderer(SwingConstants.HORIZONTAL, 0, 100, 100));
		this.timePointWeightTable.getColumn("Weight")
				.setCellEditor(new SliderEditor(SwingConstants.HORIZONTAL, 0, 100, 100));
		this.timePointWeightTable.getColumn("Weight TextField").setWidth(50);
		this.timePointWeightTable.setRowHeight(40);
		this.setupJRuler();

		// initial clustering
		this.initialClusteringComboBox = new JComboBox<>();
		this.resetInitialClusteringComboBoxItems();
		this.clusteringImportObjectIdColumnComboBox = new CyColumnComboBox();
		this.clusteringImportClusterIdColumnComboBox = new CyColumnComboBox();
		this.clusteringImportTableComboBox = new CyTableComboBox();
		this.importClusteringCollectMissingObjectsCheckBox = new JCheckBox("Collect missing objects in one cluster");

		// prototypes
		this.includeNetworkLocationInPrototypeCheckBox = new JCheckBox();
		this.includeNetworkLocationInPrototypeCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				clusterAggregateNetworkFunctionComboBox
						.setEnabled(includeNetworkLocationInPrototypeCheckBox.isSelected());
				clusterAggregateNetworkFunctionComboBox
						.setVisible(includeNetworkLocationInPrototypeCheckBox.isSelected());

				networkLocationSimilarityFunctionLabel
						.setEnabled(includeNetworkLocationInPrototypeCheckBox.isSelected());
				networkLocationSimilarityFunctionLabel
						.setVisible(includeNetworkLocationInPrototypeCheckBox.isSelected());

				networkLocationSimilarityFunctionComboBox
						.setEnabled(includeNetworkLocationInPrototypeCheckBox.isSelected());
				networkLocationSimilarityFunctionComboBox
						.setVisible(includeNetworkLocationInPrototypeCheckBox.isSelected());

				if (DataFormPanel.this.includeNetworkLocationInPrototypeCheckBox.isSelected()) {
					DataFormPanel.this.addShortestPathPrototypeComponentFactory();
				} else {
					DataFormPanel.this.removeShortestPathPrototypeComponentFactory();
				}
			}
		});
		// cluster aggregate
		this.clusterAggregateTimeSeriesFunctionComboBox = new JComboBox<>();
		this.resetClusterAggregateTimeSeriesComboBoxItems();

		this.clusterAggregateTimeSeriesFunctionComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					if (DataFormPanel.this.clusterAggregateTimeSeriesFunctionComboBox.getSelectedIndex() > -1
							&& DataFormPanel.this.tsPrototypeComponentFactory != null)
						DataFormPanel.this.tsPrototypeComponentFactory.setAggregationFunction(
								(IAggregateClusterIntoTimeSeries) DataFormPanel.this.clusterAggregateTimeSeriesFunctionComboBox
										.getSelectedItem());
				} catch (final Exception ex) {
					ex.printStackTrace();
				}
			}
		});

		this.clusterAggregateNetworkFunctionComboBox = new JComboBox<>();
		this.resetClusterAggregateNetworkComboBoxItems();
		this.clusterAggregateNetworkFunctionComboBox
				.setEnabled(this.includeNetworkLocationInPrototypeCheckBox.isSelected());
		this.clusterAggregateNetworkFunctionComboBox
				.setVisible(this.includeNetworkLocationInPrototypeCheckBox.isSelected());
		this.networkLocationSimilarityFunctionLabel = new JLabel();
		this.networkLocationSimilarityFunctionLabel
				.setEnabled(this.includeNetworkLocationInPrototypeCheckBox.isSelected());
		this.networkLocationSimilarityFunctionLabel
				.setVisible(this.includeNetworkLocationInPrototypeCheckBox.isSelected());

		this.clusterAggregateNetworkFunctionComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					if (DataFormPanel.this.includeNetworkLocationInPrototypeCheckBox.isSelected()
							&& DataFormPanel.this.clusterAggregateNetworkFunctionComboBox.getSelectedIndex() > -1)
						DataFormPanel.this.nlPrototypeComponentFactory.setAggregationFunction(
								(IAggregateClusterIntoNetworkLocation) DataFormPanel.this.clusterAggregateNetworkFunctionComboBox
										.getSelectedItem());
				} catch (final Exception ex) {
					ex.printStackTrace();
				}
			}
		});

		this.timeSeriesSimilarityFunctionComboBox = new JComboBox<>(new ITimeSeriesSimilarityFunction[] {
				new PearsonCorrelationFunction(), new InverseEuclideanSimilarityFunction() });
		this.timeSeriesSimilarityFunctionComboBox.setSelectedIndex(1);

		this.networkLocationSimilarityFunctionComboBox = new JComboBox<>(
				new INetworkBasedSimilarityFunction[] { new InverseShortestPathSimilarityFunction() });
		this.networkLocationSimilarityFunctionComboBox
				.setEnabled(this.includeNetworkLocationInPrototypeCheckBox.isSelected());
		this.networkLocationSimilarityFunctionComboBox
				.setVisible(this.includeNetworkLocationInPrototypeCheckBox.isSelected());

		this.initialClusteringComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final IInitialClusteringProvider selectedItem = (IInitialClusteringProvider) DataFormPanel.this.initialClusteringComboBox
						.getSelectedItem();
				DataFormPanel.this.clusteringProcessBuilder.setInitialClusteringProvider(selectedItem);

				DataFormPanel.this.initialClusteringMethodPanel
						.setVisible(selectedItem instanceof InitialClusteringMethod);

				final boolean isTable = selectedItem instanceof InitialClusteringFromTable;
				DataFormPanel.this.initialClusteringTableImportPanel.setVisible(isTable);

				if (isTable) {
					try {
						DataFormPanel.this.updateClusteringImportTable();
					} catch (NoTableSelectedException | NoObjectIdColumnSelectedException
							| NoClusterIdColumnSelectedException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		NumberFormat f = NumberFormat.getNumberInstance();
		f.setGroupingUsed(false);
		this.randomSeedTextField = new JFormattedTextField(f);
		this.updateRandomSeed();

		this.clusteringImportObjectIdColumnComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					DataFormPanel.this.updateClusteringImportTable();
				} catch (final NoTableSelectedException e1) {
					e1.printStackTrace();
				} catch (final NoObjectIdColumnSelectedException e1) {
					e1.printStackTrace();
				} catch (final NoClusterIdColumnSelectedException e1) {
					e1.printStackTrace();
				}
			}
		});
		this.clusteringImportClusterIdColumnComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					DataFormPanel.this.updateClusteringImportTable();
				} catch (final NoTableSelectedException e1) {
					e1.printStackTrace();
				} catch (final NoObjectIdColumnSelectedException e1) {
					e1.printStackTrace();
				} catch (final NoClusterIdColumnSelectedException e1) {
					e1.printStackTrace();
				}
			}
		});
		this.clusteringImportTableComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					final CyTable table = (CyTable) DataFormPanel.this.clusteringImportTableComboBox.getSelectedItem();
					if (table == null)
						return;

					((CyColumnComboBox) DataFormPanel.this.clusteringImportObjectIdColumnComboBox).setTable(table);
					((CyColumnComboBox) DataFormPanel.this.clusteringImportClusterIdColumnComboBox).setTable(table);
				} catch (final Exception e1) {
					// Do nothing
					e1.printStackTrace();
				}
			}
		});
		this.importClusteringCollectMissingObjectsCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					DataFormPanel.this.updateClusteringImportTable();
				} catch (final NoTableSelectedException e1) {
					e1.printStackTrace();
				} catch (final NoObjectIdColumnSelectedException e1) {
					e1.printStackTrace();
				} catch (final NoClusterIdColumnSelectedException e1) {
					e1.printStackTrace();
				}
			}
		});
		this.setupButtons();
	}

	/**
	 * 
	 */
	private void updateRandomSeed() {
		this.randomSeedTextField.setText(Long.toString(new Random(System.currentTimeMillis()).nextLong()));
	}

	/**
	 * 
	 */
	private void resetClusterAggregateNetworkComboBoxItems() {
		this.clusterAggregateNetworkFunctionComboBox.removeAllItems();
//		this.clusterAggregateNetworkFunctionComboBox.addItem(new AggregateClusterAllNodes());
		this.clusterAggregateNetworkFunctionComboBox.addItem(new AggregateClusterMedoidNode());
	}

	/**
	 * 
	 */
	private void resetClusterAggregateTimeSeriesComboBoxItems() {
		this.clusterAggregateTimeSeriesFunctionComboBox.removeAllItems();
		this.clusterAggregateTimeSeriesFunctionComboBox.addItem(new AggregateClusterMeanTimeSeries());
		this.clusterAggregateTimeSeriesFunctionComboBox.addItem(new AggregateClusterMedianTimeSeries());
		this.clusterAggregateTimeSeriesFunctionComboBox.addItem(new AggregateClusterMedoidTimeSeries(null));
	}

	/**
	 * 
	 */
	private void resetInitialClusteringComboBoxItems() {
		this.initialClusteringComboBox.removeAllItems();
		this.initialClusteringComboBox.addItem(new InitialClusteringMethod());
		this.initialClusteringComboBox.addItem(new InitialClusteringFromTable());
	}

	protected void updateClusteringImportTable()
			throws NoTableSelectedException, NoObjectIdColumnSelectedException, NoClusterIdColumnSelectedException {
		if (this.initialClusteringComboBox.getSelectedItem() instanceof InitialClusteringFromTable) {
			final InitialClusteringFromTable provider = (InitialClusteringFromTable) this.initialClusteringComboBox
					.getSelectedItem();
			if (this.clusteringImportTableComboBox == null)
				return;
			this.clusteringImportErrorLabel.setVisible(false);
			this.clusteringImportErrorLabel.setText("");
			final ClusteringImportFromTableConfig clusteringImportFromTableConfig;
			if (provider.getConfig() != null)
				clusteringImportFromTableConfig = provider.getConfig();
			else
				clusteringImportFromTableConfig = new ClusteringImportFromTableConfig();
			provider.setConfig(clusteringImportFromTableConfig);

			if (this.clusteringImportTableComboBox.getSelectedIndex() > -1) {
				clusteringImportFromTableConfig
						.setTable((CyTable) this.clusteringImportTableComboBox.getSelectedItem());
			} else {
				clusteringImportFromTableConfig.setTable(null);
				this.clusteringImportErrorLabel.setText("No table selected");
				this.clusteringImportErrorLabel.setVisible(true);
				throw new NoTableSelectedException();
			}
			if (this.clusteringImportObjectIdColumnComboBox.getSelectedIndex() > -1) {
				clusteringImportFromTableConfig.setObjectIdColumn(
						this.clusteringImportObjectIdColumnComboBox.getSelectedIndex(),
						((CyColumn) this.clusteringImportObjectIdColumnComboBox.getSelectedItem()).getName());
			} else {
				clusteringImportFromTableConfig.setObjectIdColumn(-1, null);
				this.clusteringImportErrorLabel.setText("No object id column selected");
				this.clusteringImportErrorLabel.setVisible(true);
				throw new NoObjectIdColumnSelectedException();
			}
			if (this.clusteringImportClusterIdColumnComboBox.getSelectedIndex() > -1) {
				clusteringImportFromTableConfig.setClusterIdColumn(
						this.clusteringImportClusterIdColumnComboBox.getSelectedIndex(),
						((CyColumn) this.clusteringImportClusterIdColumnComboBox.getSelectedItem()).getName());
			} else {
				clusteringImportFromTableConfig.setClusterIdColumn(-1, null);
				this.clusteringImportErrorLabel.setText("No cluster id column selected");
				this.clusteringImportErrorLabel.setVisible(true);
				throw new NoClusterIdColumnSelectedException();
			}
			clusteringImportFromTableConfig.setCollectMissingObjectsIntoCluster(
					this.importClusteringCollectMissingObjectsCheckBox.isSelected());
			clusteringImportFromTableConfig.setPrototypeFactory(this.prototypeFactory);
		}
	}

	/**
	 * Can be computationally expensive, if a network-based similarity function is
	 * involved that needs to be initialized with a network instance which needs to
	 * be created.
	 *
	 * @throws MissingPrototypeFactoryException
	 * @throws IncompatiblePrototypeComponentException
	 * @throws IncompatibleSimilarityFunctionException
	 */
	protected void updateSimilarityFunction() throws IncompatiblePrototypeComponentException,
			MissingPrototypeFactoryException, IncompatibleSimilarityFunctionException {
		final ITimeSeriesSimilarityFunction tsSimilarityFunction = (ITimeSeriesSimilarityFunction) ((ITimeSeriesSimilarityFunction) this.timeSeriesSimilarityFunctionComboBox
				.getSelectedItem()).copy();
		if (tsSimilarityFunction instanceof IWeightedTimeSeriesSimilarityFunction) {
			((IWeightedTimeSeriesSimilarityFunction) tsSimilarityFunction)
					.setTimePointWeights(this.clusteringProcessBuilder.getTimePointWeighting());
			tsSimilarityFunction.setTimeSeriesPreprocessor(this.clusteringProcessBuilder.getTimeSeriesPreprocessor());
		}

		if (!this.includeNetworkLocationInPrototypeCheckBox.isSelected())
			this.clusteringProcessBuilder.setSimilarityFunction(tsSimilarityFunction);
		else {
			final TiconeNetworkImplFactory networkFactory = this.networkFactory;
			networkFactory.setReuseInstanceIfExists(true);

			final TiconeNetworkImpl network = networkFactory.getInstance(
					new TiconeCytoscapeNetwork((CyNetwork) this.shortestPathNetworkComboBox.getSelectedItem()));

			final INetworkBasedSimilarityFunction spSf = (INetworkBasedSimilarityFunction) ((INetworkBasedSimilarityFunction) this.networkLocationSimilarityFunctionComboBox
					.getSelectedItem()).copy();
			spSf.setNetwork(network);
			final WeightedAverageCompositeSimilarityFunction sf = new WeightedAverageCompositeSimilarityFunction(
					(ISimpleSimilarityFunction) tsSimilarityFunction, (ISimpleSimilarityFunction) spSf);
			this.clusteringProcessBuilder.setSimilarityFunction(sf);

			final INetworkLocationPrototypeComponentBuilder componentFactory = PrototypeComponentType.NETWORK_LOCATION
					.getComponentFactory(this.prototypeFactory);
			final IAggregateClusterIntoNetworkLocation aggregationFunction = (IAggregateClusterIntoNetworkLocation) this.clusterAggregateNetworkFunctionComboBox
					.getSelectedItem();
			aggregationFunction.setNetwork(network);
			if (aggregationFunction instanceof AggregateClusterMedoidNode)
				((AggregateClusterMedoidNode) aggregationFunction).setSimilarityFunction(spSf);
			componentFactory.setAggregationFunction(aggregationFunction);
		}
	}

	public CyNetwork getSelectedNetwork() {
		return (CyNetwork) this.networkCombobox.getSelectedItem();
	}

	private void openTimeSeriesDataAction() throws TiconeUnloadingException, InterruptedException {
		final JFileChooser jFileChooser = new JFileChooser();
		String currentDir = ".";
		if (this.dataFileTextField.getText().length() > 0) {
			currentDir = this.dataFileTextField.getText();
		}
		jFileChooser.setCurrentDirectory(new File(currentDir));
		final int returnVal = jFileChooser.showOpenDialog(this.mainPanel);
		if (returnVal == JFileChooser.CANCEL_OPTION) {
			return;
		}
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			final String filePath = jFileChooser.getSelectedFile().getAbsolutePath();
			this.dataFileTextField.setText(filePath);
			this.updateLoadDataMethod();
		}
	}

	private void openPatternFileAction() {
		final JFileChooser jFileChooser = new JFileChooser();
		String currentDir = ".";
		if (this.patternFileTextField.getText().length() > 0) {
			currentDir = this.patternFileTextField.getText();
		}
		jFileChooser.setCurrentDirectory(new File(currentDir));
		final int returnVal = jFileChooser.showOpenDialog(this.mainPanel);
		if (returnVal == JFileChooser.CANCEL_OPTION) {
			return;
		}
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			final String filePath = jFileChooser.getSelectedFile().getAbsolutePath();
			this.patternFileTextField.setText(filePath);
		}
	}

	private void setupJRuler() {
		final double[] valueArray = new double[] { -1, 0, 1 };
		final List<Double> values = new ArrayList<>();
		for (int i = 0; i < valueArray.length; i++) {
			values.add(valueArray[i]);
		}
		this.jRuler = new JRuler(values);
		this.jRuler.setPreferredSize(new Dimension(250, 40));
		this.jRuler.setToolTipText("The discretized values, that clusters can have for each time point.");
		this.discretizationRulerPanel.add(this.jRuler);
	}

	public ILoadColumnDataMethod getLoadDataMethod() {
		return (ILoadColumnDataMethod) this.clusteringProcessBuilder.getLoadDataMethod();
	}

	public String getSelectedTable() {
		if (this.dataTableComboBox.getSelectedItem() != null)
			return ((CyTable) this.dataTableComboBox.getSelectedItem()).getTitle();
		return null;
	}

	public int getNumberOfTables() {
		return this.dataTableComboBox.getItemCount();
	}

	public String getTimeSeriesTextString() {
		return this.dataFileTextField.getText();
	}

	public boolean getRemoveObjectsNotInNetworkSelected() {
		return this.removeObjectsNotInNetworkCheckBox.isSelected();
	}

	private double roundDoubleHalfUp(final double value) {
		final BigDecimal bigDecimal = new BigDecimal(value).setScale(2, BigDecimal.ROUND_HALF_UP);
		return bigDecimal.doubleValue();
	}

	public int getNumberOfInitialPatterns() throws NumberFormatException {
		return (Integer) this.initialClustersWantedSpinner.getValue();
	}

	public double getPairwiseSimilarityThreshold() throws NumberFormatException {
		return Double.parseDouble(this.similarityThresholdTextField.getText());
	}

	public void updateDiscretizationSlider() throws InterruptedException {
		try {
			final int negativeSteps = (Integer) this.negativeStepsTextField.getValue();
			final int positiveSteps = (Integer) this.positiveStepsTextField.getValue();
			if (negativeSteps <= 0 || positiveSteps <= 0) {
				throw new NumberFormatException();
			}
			double min;
			double max;
			try {
				min = this.clusteringProcessBuilder.getTimeSeriesPreprocessor().getMinValue();
				max = this.clusteringProcessBuilder.getTimeSeriesPreprocessor().getMaxValue();
			} catch (final NullPointerException npe) {
				MyDialogPanel.showMessageDialog("Data not initialized", "You must select a valid dataset table/file.");
				npe.printStackTrace();
				throw new InterruptedException();
			}
			final IDiscretizeTimeSeries discretizePattern = new DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax(
					min, max, negativeSteps, positiveSteps);
			this.tsPrototypeComponentFactory.setDiscretizeFunction(discretizePattern);
			final double[] valueArray = this.tsPrototypeComponentFactory.getDiscretizeFunction()
					.getDiscretizationValues();
			final List<Double> values = new ArrayList<>();
			for (int i = 0; i < valueArray.length; i++) {
				values.add(valueArray[i]);
			}
			this.jRuler.updateValues(values);
		} catch (final NumberFormatException nfe) {
			MyDialogPanel.showMessageDialog(null, "Discretization steps must be positive integers");
			throw new InterruptedException();
		}

	}

	public String getRemoveLeastConservedOption() {
		return (String) this.dissagreeingCombobox.getSelectedItem();
	}

	public boolean getChangeColumnMapping() {
		return this.changeColumnMappingCheckbox.isSelected();
	}

	public String getRemoveLowVarianceOption() {
		return (String) this.varianceCombobox.getSelectedItem();
	}

	public void updateStartButton(final boolean started) {
		if (started) {
			final ActionListener[] al = this.startButton.getActionListeners();
			for (int i = 0; i < al.length; i++) {
				this.startButton.removeActionListener(this.startButton.getActionListeners()[i]);
			}
			this.startButton.setText("Reset");
			this.startButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(final ActionEvent e) {
					try {
						DataFormPanel.this.askResetAction();
					} catch (final TiconeUnloadingException e1) {
					}
				}
			});
		} else {
			final ActionListener[] al = this.startButton.getActionListeners();
			for (int i = 0; i < al.length; i++) {
				this.startButton.removeActionListener(this.startButton.getActionListeners()[i]);
			}
			this.startButton.setText("Start");
			this.startButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(final ActionEvent e) {
					DataFormPanel.this.startAction();
				}

			});
		}
	}

	public boolean isRemoveVarianceButtonSelected() {
		return this.removeSetsWithLowCheckBox.isSelected();
	}

	public double getLowVarianceThreshold() throws InterruptedException {
		double threshold;
		try {
			threshold = Double.parseDouble(this.lowVarianceTextField.getText());
			if (this.getRemoveLowVarianceOption().equals("Threshold")) {
				if (threshold <= 0) {
					throw new NumberFormatException();
				}
			} else if (this.getRemoveLowVarianceOption().equals("Percent")) {
				threshold = Integer.parseInt(this.lowVarianceTextField.getText());
				if (threshold <= 0 || threshold >= 100) {
					throw new NumberFormatException();
				}
			}
		} catch (final NumberFormatException nfe) {
			if (this.getRemoveLowVarianceOption().equals("Threshold")) {
				MyDialogPanel.showMessageDialog(null, "Variance threshold must be a number above 0.");
			} else if (this.getRemoveLowVarianceOption().equals("Percent")) {
				MyDialogPanel.showMessageDialog(null,
						"Variance threshold percent must be a integer number above 0 and below 100.");
			}
			throw new InterruptedException();
		}
		return threshold;
	}

	public boolean isRemoveLeastConservedButtonSelected() {
		return this.removeDissagreeingObjectSetsCheckBox.isSelected();
	}

	public ISimilarityValue getDissagreeingThreshold() throws InterruptedException {
		double threshold;
		try {
			threshold = Double.parseDouble(this.dissagreeingSetsTextField.getText());
			if (this.getRemoveLeastConservedOption().equals("Threshold")) {
				if (threshold <= 0 || threshold >= 1) {
					throw new NumberFormatException();
				}
			} else if (this.getRemoveLeastConservedOption().equals("Percent")) {
				threshold = Integer.parseInt(this.dissagreeingSetsTextField.getText());
				if (threshold <= 0 || threshold >= 100) {
					throw new NumberFormatException();
				}
			}
		} catch (final NumberFormatException nfe) {
			if (this.getRemoveLeastConservedOption().equals("Threshold")) {
				MyDialogPanel.showMessageDialog(null,
						"Disagreeing objects threshold must be a double between 0 and 1.");
			} else if (this.getRemoveLeastConservedOption().equals("Percent")) {
				MyDialogPanel.showMessageDialog(null,
						"Dissagreeing objects threshold percent must be a integer between 0 and 100.");
			}
			throw new InterruptedException();
		}
//		return SimpleSimilarityValue.of(threshold);
		// TODO
		return null;
	}

	private void askResetAction() throws TiconeUnloadingException {
		final int choice = JOptionPane.showConfirmDialog(null, "Are you sure you want to reset all your progress?");
		if (choice != JOptionPane.OK_OPTION) {
			return;
		}
		this.resetAction();
	}

	private void updatePreprocessingSummary() throws TiconeUnloadingException, InterruptedException {
		final PreprocessingSummary summary = new PreprocessingSummary();

		if (this.getRemoveObjectsNotInNetworkSelected()) {
			summary.setRemoveObjectsNotInNetwork(true);
			if (GUIUtility.getSelectedNetwork() != null) {
				final CyNetwork cyNetwork = GUIUtility.getSelectedNetwork();
				final TiconeNetworkImpl network = this.networkFactory
						.getInstance(new TiconeCytoscapeNetwork(cyNetwork));
				summary.setNetwork(network);
			}
		}

		summary.setRemoveObjectsLeastConserved(GUIUtility.isRemoveLeastConservedButtonSelected());
		final ISimilarityValue threshold = GUIUtility.getDissagreeingThreshold();
		summary.setRemoveObjectsLeastConservedThreshold(threshold,
				GUIUtility.getRemoveLeastConservedOption().equals("Percent"));
		summary.setRemoveObjectsLowVariance(GUIUtility.isRemoveVarianceButtonSelected());
		final double varThreshold = GUIUtility.getLowVarianceThreshold();
		summary.setRemoveObjectsLowVarianceThreshold(varThreshold, GUIUtility.getRemoveLowVariance().equals("Percent"));

		this.clusteringProcessBuilder.setPreprocessingSummary(summary);
	}

	private void startAction() {
		try {
			this.updateClusteringImportTable();
			this.updateTimePointWeighting();
			this.updateSimilarityFunction();
			this.updatePreprocessingSummary();
			this.updateInitialClusteringMethod();
			this.updateInitialClusteringProvider();

			final BasicClusteringProcessBuilder<TiconeCytoscapeClusteringResult> processBuilder = this.clusteringProcessBuilder;
			processBuilder.setResultFactory(new TiconeCytoscapeClusteringResultFactory());
			processBuilder.setRequestedFeatures(Arrays.asList(new IFeature[] { new ClusterFeatureNumberObjects(),
					new ClusterFeatureAverageSimilarity(processBuilder.getSimilarityFunction()),
					new ClusteringFeatureNumberClusters(), new ClusteringFeatureValidity(new DunnIndex(),
							(ISimilarityFunction) processBuilder.getSimilarityFunction()) }));
			final IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> clusteringProcess = processBuilder
					.build();
			clusteringProcess.start();

			INetworkBasedSimilarityFunction networkSimilarityFunction = null;
			if (processBuilder.getSimilarityFunction() instanceof INetworkBasedSimilarityFunction) {
				networkSimilarityFunction = (INetworkBasedSimilarityFunction) processBuilder.getSimilarityFunction();
			} else if (processBuilder.getSimilarityFunction() instanceof ICompositeSimilarityFunction) {
				for (ISimpleSimilarityFunction<?> csf : ((ICompositeSimilarityFunction) processBuilder
						.getSimilarityFunction()).getSimilarityFunctions()) {
					if (csf instanceof INetworkBasedSimilarityFunction) {
						networkSimilarityFunction = (INetworkBasedSimilarityFunction) csf;
						break;
					}
				}
			}

			TaskIterator taskIterator = null;
			if (networkSimilarityFunction != null) {
				taskIterator = new CalculateShortestPathsTaskFactory(
						(TiconeNetwork<?, ?>) networkSimilarityFunction.getNetwork(), false).createTaskIterator();
			}

			if (taskIterator == null)
				taskIterator = new PreprocessingTaskFactory(clusteringProcess, this.getNumberOfInitialPatterns())
						.createTaskIterator();
			else
				taskIterator.append(new PreprocessingTaskFactory(clusteringProcess, this.getNumberOfInitialPatterns())
						.createTaskIterator());

			final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
			Utility.getProgress().start();
			taskManager.execute(taskIterator);
		} catch (NoTableSelectedException | NoObjectIdColumnSelectedException | NoClusterIdColumnSelectedException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "You have invalid initial clustering settings.",
					"Could not start clustering", JOptionPane.ERROR_MESSAGE);
			return;
		} catch (final Exception e) {
			e.printStackTrace();
			return;
		}
	}

	/**
	 * 
	 */
	private void updateInitialClusteringProvider() {
		final IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = this.clusteringProcessBuilder
				.getInitialClusteringProvider();

		if (initialClusteringProvider instanceof InitialClusteringMethod) {
			((InitialClusteringMethod) initialClusteringProvider)
					.setClusteringMethod(this.clusteringProcessBuilder.getClusteringMethodBuilder());
			((InitialClusteringMethod) initialClusteringProvider)
					.setInitialNumberOfClusters(getNumberOfInitialPatterns());
			((InitialClusteringMethod) initialClusteringProvider).setSeed(this.clusteringProcessBuilder.getSeed());
		}
	}

	/**
	 * @throws IncompatibleSimilarityFunctionException
	 * @throws NumberFormatException
	 *
	 */
	private void updateInitialClusteringMethod() throws NumberFormatException, IncompatibleSimilarityFunctionException {
		IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> result = null;
		final String clusterMethod = (String) this.clusteringMethodComboBox.getSelectedItem();
		if (clusterMethod.equals("PAMK"))
			result = new PAMKClusteringMethodBuilder().setSwapIterations((int) this.maxIterationsSpinner.getValue())
					.setNstart((int) this.nStartsSpinner.getValue())
					.setSimilarityFunction(this.clusteringProcessBuilder.getSimilarityFunction())
					.setPrototypeBuilder(this.prototypeFactory);
		else if (clusterMethod.equals("TransClust"))
			result = new TransClustClusteringMethodBuilder().setSimilarityThreshold(getPairwiseSimilarityThreshold())
					.setPrototypeBuilder(this.prototypeFactory)
					.setSimilarityFunction(this.clusteringProcessBuilder.getSimilarityFunction());
		else if (clusterMethod.equals("CLARA"))
			result = new CLARAClusteringMethodBuilder().setSamples((int) samplesSpinner.getValue())
					.setSampleSize((int) sampleSizeSpinner.getValue())
					.setPamkBuilder(new PAMKClusteringMethodBuilder()
							.setSwapIterations((int) this.maxIterationsSpinner.getValue())
							.setNstart((int) this.nStartsSpinner.getValue()))
					.setSimilarityFunction(this.clusteringProcessBuilder.getSimilarityFunction())
					.setPrototypeBuilder(this.prototypeFactory);
		else if (clusterMethod.equals("STEM")) {
			final int timepoints = this.clusteringProcessBuilder.getTimeSeriesPreprocessor().getObjects().get(0)
					.getPreprocessedTimeSeriesList()[0].getNumberTimePoints();
			result = new STEMClusteringMethodBuilder().setTimepoints(timepoints)
					.setSimilarityFunction(this.clusteringProcessBuilder.getSimilarityFunction())
					.setPrototypeBuilder(this.prototypeFactory);
		} else if (clusterMethod.equals("K-Means"))
			result = new KMeansClusteringMethodBuilder().setMaxIterations((int) this.maxIterationsSpinner.getValue())
					.setNstart((int) this.nStartsSpinner.getValue())
					.setSimilarityFunction(this.clusteringProcessBuilder.getSimilarityFunction())
					.setPrototypeBuilder(this.prototypeFactory);

		this.clusteringProcessBuilder.setClusteringMethodBuilder(result);
	}

	public void resetAction() throws TiconeUnloadingException {
		GUIUtility.disableGraphTab();
		this.updateStartButton(false);
		this.resetComponents();
		try {
			this.loadDataErrorLabel.setText("");
			if (!this.getLoadDataMethod().isDataLoaded()) {
				this.getLoadDataMethod().loadData(this.clusteringProcessBuilder.getTimeSeriesPreprocessor());
				if (this.getLoadDataMethod().isDataLoaded())
					this.clusteringProcessBuilder.getTimeSeriesPreprocessor().process();
			}
			GUIUtility.updateDiscretizationSlider();
		} catch (final LoadDataException | PreprocessingException ie) {
			this.loadDataErrorLabel.setText(ie.getMessage());
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
	}

	// TODO: do we need this?
	public void resetComponents() {
		this.dataTableComboBox.setSelectedIndex(-1);
		if (this.networkCombobox.getItemCount() > 0)
			this.networkCombobox.setSelectedIndex(0);
		this.firstRowObjectIdLabel.setText("-");
		this.firstRowReplicateLabel.setText("-");
		this.firstRowTimePointsLabel.setText("-");
		this.clusteringImportTableComboBox.setSelectedIndex(-1);
		this.updateRandomSeed();

		this.resetInitialClusteringComboBoxItems();
		this.resetClusterAggregateTimeSeriesComboBoxItems();
		this.resetClusterAggregateNetworkComboBoxItems();
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
	 * edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayoutManager(10, 1, new Insets(10, 10, 10, 10), -1, -1));
		dataPanel = new JPanel();
		dataPanel.setLayout(new GridLayoutManager(6, 2, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(dataPanel,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		dataPanel.setBorder(BorderFactory.createTitledBorder("Time Series Data"));
		fromTableRadioButton.setSelected(true);
		fromTableRadioButton.setText("From Table");
		dataPanel.add(fromTableRadioButton,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		fromFileRadioButton.setText("From File");
		dataPanel.add(fromFileRadioButton,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		loadDataPanel = new JPanel();
		loadDataPanel.setLayout(new GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1));
		dataPanel.add(loadDataPanel,
				new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		dataFileTextField = new JTextField();
		dataFileTextField.setVisible(false);
		loadDataPanel.add(dataFileTextField,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		chooseDataFileButton.setText("Choose File");
		chooseDataFileButton.setVisible(false);
		loadDataPanel.add(chooseDataFileButton,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		dataTableComboBox.setToolTipText("Choose which table you want to load your time series data from");
		loadDataPanel.add(dataTableComboBox,
				new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		firstRowIsHeader.setText("First Row Contains Header");
		firstRowIsHeader.setVisible(false);
		loadDataPanel.add(firstRowIsHeader,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		columnMappingPanel = new JPanel();
		columnMappingPanel.setLayout(new GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1));
		columnMappingPanel.setEnabled(true);
		columnMappingPanel.setVisible(false);
		dataPanel.add(columnMappingPanel,
				new GridConstraints(4, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		columnMappingPanel.setBorder(BorderFactory.createTitledBorder(""));
		final JLabel label1 = new JLabel();
		label1.setText("Object ids:");
		columnMappingPanel.add(label1,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label2 = new JLabel();
		label2.setText("Time-points:");
		columnMappingPanel.add(label2,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		objectIdColumnSpinner.setEnabled(false);
		columnMappingPanel.add(objectIdColumnSpinner,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label3 = new JLabel();
		label3.setText("Replicates:");
		columnMappingPanel.add(label3,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		replicateColumnSpinner.setEnabled(false);
		columnMappingPanel.add(replicateColumnSpinner,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		timePointColumnsList.setEnabled(false);
		columnMappingPanel.add(timePointColumnsList,
				new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null,
						new Dimension(150, 50), null, 0, false));
		columnMappingUpdateButton.setText("Update");
		columnMappingPanel.add(columnMappingUpdateButton,
				new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		changeColumnMappingCheckbox.setEnabled(false);
		changeColumnMappingCheckbox.setText("Change column mapping");
		dataPanel.add(changeColumnMappingCheckbox,
				new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		columnMappingFirstRowPanel = new JPanel();
		columnMappingFirstRowPanel.setLayout(new GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1));
		columnMappingFirstRowPanel.setVisible(false);
		dataPanel.add(columnMappingFirstRowPanel,
				new GridConstraints(5, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		columnMappingFirstRowPanel.setBorder(BorderFactory.createTitledBorder("Mapping of first row:"));
		final JLabel label4 = new JLabel();
		label4.setText("Object id:");
		columnMappingFirstRowPanel.add(label4,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		firstRowObjectIdLabel.setText("-");
		columnMappingFirstRowPanel.add(firstRowObjectIdLabel,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label5 = new JLabel();
		label5.setText("Time-points:");
		columnMappingFirstRowPanel.add(label5,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		firstRowTimePointsLabel = new JLabel();
		firstRowTimePointsLabel.setText("-");
		columnMappingFirstRowPanel.add(firstRowTimePointsLabel,
				new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label6 = new JLabel();
		label6.setText("Replicate:");
		columnMappingFirstRowPanel.add(label6,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		firstRowReplicateLabel.setText("-");
		columnMappingFirstRowPanel.add(firstRowReplicateLabel,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		Font loadDataErrorLabelFont = this.$$$getFont$$$(null, Font.BOLD, -1, loadDataErrorLabel.getFont());
		if (loadDataErrorLabelFont != null)
			loadDataErrorLabel.setFont(loadDataErrorLabelFont);
		loadDataErrorLabel.setForeground(new Color(-65536));
		loadDataErrorLabel.setText("");
		loadDataErrorLabel.setVisible(false);
		dataPanel.add(loadDataErrorLabel,
				new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, new Dimension(366, -1), 0, false));
		similarityPanel.setLayout(new GridLayoutManager(7, 1, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(similarityPanel,
				new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		timePointWeightcheckBox.setText("Change Weights of Time Points");
		similarityPanel.add(timePointWeightcheckBox,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		timePointWeightPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		timePointWeightPanel.setVisible(false);
		similarityPanel.add(timePointWeightPanel,
				new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		timePointWeightPanel.add(timePointWeightTable,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null,
						new Dimension(150, 50), null, 0, false));
		shortestPathNetworkComboBox.setVisible(false);
		similarityPanel.add(shortestPathNetworkComboBox,
				new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		similarityPanel.add(timeSeriesSimilarityFunctionComboBox,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		similarityPanel.add(networkLocationSimilarityFunctionComboBox,
				new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label7 = new JLabel();
		label7.setText("Time series:");
		similarityPanel.add(label7,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		networkLocationSimilarityFunctionLabel.setText("Network location:");
		similarityPanel.add(networkLocationSimilarityFunctionLabel,
				new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		startButton.setEnabled(false);
		startButton.setText("Start");
		mainPanel.add(startButton,
				new GridConstraints(8, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		discretizationPanel.setLayout(new GridLayoutManager(3, 5, new Insets(0, 0, 0, 0), -1, -1));
		discretizationPanel.setEnabled(true);
		discretizationPanel.setVisible(true);
		mainPanel.add(discretizationPanel,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		discretizationPanel.add(negativeStepsTextField,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, 1,
						GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(40, -1), null, 0, false));
		discretizationPanel.add(discretizationRulerPanel,
				new GridConstraints(1, 0, 2, 5, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JLabel label8 = new JLabel();
		label8.setText("Negative");
		discretizationPanel.add(label8,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label9 = new JLabel();
		label9.setText("Positive ");
		discretizationPanel.add(label9,
				new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		discretizationPanel.add(positiveStepsTextField,
				new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, 1,
						GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(40, -1), null, 0, false));
		updateDiscretizedSteps.setText("Update");
		discretizationPanel.add(updateDiscretizedSteps,
				new GridConstraints(0, 4, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final Spacer spacer1 = new Spacer();
		mainPanel.add(spacer1, new GridConstraints(9, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
		otherPanel.setLayout(new GridLayoutManager(6, 2, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(otherPanel,
				new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		idMappingCheckBox.setText("Map alternative ids to objects");
		otherPanel.add(idMappingCheckBox,
				new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		idMappingTableComboBox.setEnabled(false);
		otherPanel.add(idMappingTableComboBox,
				new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		idMappingKeyColumnComboBox.setEnabled(false);
		otherPanel.add(idMappingKeyColumnComboBox,
				new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		idMappingValueColumnComboBox.setEnabled(false);
		otherPanel.add(idMappingValueColumnComboBox,
				new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label10 = new JLabel();
		label10.setText("Key Column:");
		otherPanel.add(label10, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label11 = new JLabel();
		label11.setText("Alternative Id Column:");
		otherPanel.add(label11,
				new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(204, 16), null, 0, false));
		final JSeparator separator1 = new JSeparator();
		otherPanel.add(separator1,
				new GridConstraints(4, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0,
						false));
		final JLabel label12 = new JLabel();
		label12.setText("Random seed:");
		label12.setToolTipText(
				"Set the random seed used throughout this clustering. This includes possible randomness when calculating an initial clustering, and shuffling when calculating p-values. To achieve reproducible results, set the random seed to the same value");
		otherPanel.add(label12, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		otherPanel.add(randomSeedTextField,
				new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		clusteringPanel.setLayout(new GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(clusteringPanel,
				new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
		defaultComboBoxModel1.addElement("CLARA");
		defaultComboBoxModel1.addElement("PAMK");
		defaultComboBoxModel1.addElement("K-Means");
		defaultComboBoxModel1.addElement("STEM");
		defaultComboBoxModel1.addElement("TransClust");
		clusteringMethodComboBox.setModel(defaultComboBoxModel1);
		clusteringPanel.add(clusteringMethodComboBox,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label13 = new JLabel();
		label13.setText("Clustering Method:");
		label13.setToolTipText("Supplementary Clustering Method");
		clusteringPanel.add(label13,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		clusteringMethodParameterPanel = new JPanel();
		clusteringMethodParameterPanel.setLayout(new GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1));
		clusteringMethodParameterPanel.setVisible(true);
		clusteringPanel.add(clusteringMethodParameterPanel,
				new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		clusteringMethodParameterPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				"Advanced parameters", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP));
		maxIterationsLabel = new JLabel();
		maxIterationsLabel.setText("Maximal iterations:");
		clusteringMethodParameterPanel.add(maxIterationsLabel,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		nStartsLabel = new JLabel();
		nStartsLabel.setText("Repetitions (nstart):");
		clusteringMethodParameterPanel.add(nStartsLabel,
				new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		clusteringMethodParameterPanel.add(maxIterationsSpinner,
				new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		clusteringMethodParameterPanel.add(nStartsSpinner,
				new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		samplesLabel = new JLabel();
		samplesLabel.setText("Samples:");
		clusteringMethodParameterPanel.add(samplesLabel,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		clusteringMethodParameterPanel.add(samplesSpinner,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		sampleSizeLabel = new JLabel();
		sampleSizeLabel.setText("Sample size:");
		clusteringMethodParameterPanel.add(sampleSizeLabel,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		clusteringMethodParameterPanel.add(sampleSizeSpinner,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		initialClusteringPanel = new JPanel();
		initialClusteringPanel.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
		clusteringPanel.add(initialClusteringPanel,
				new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		initialClusteringPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				"Initial Clustering", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP));
		initialClusteringPanel.add(initialClusteringComboBox,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		initialClusteringMethodPanel = new JPanel();
		initialClusteringMethodPanel.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		initialClusteringPanel.add(initialClusteringMethodPanel,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JLabel label14 = new JLabel();
		label14.setText("Number of clusters:");
		initialClusteringMethodPanel.add(label14,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		initialClusteringMethodPanel.add(initialClustersWantedSpinner,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		initialClusteringTableImportPanel = new JPanel();
		initialClusteringTableImportPanel.setLayout(new GridLayoutManager(6, 2, new Insets(0, 0, 0, 0), -1, -1));
		initialClusteringTableImportPanel.setVisible(false);
		initialClusteringPanel.add(initialClusteringTableImportPanel,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JLabel label15 = new JLabel();
		label15.setText("Table:");
		initialClusteringTableImportPanel.add(label15,
				new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label16 = new JLabel();
		label16.setText("Object id column:");
		initialClusteringTableImportPanel.add(label16,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label17 = new JLabel();
		label17.setText("Cluster id column:");
		initialClusteringTableImportPanel.add(label17,
				new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		initialClusteringTableImportPanel.add(clusteringImportTableComboBox,
				new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		initialClusteringTableImportPanel.add(clusteringImportClusterIdColumnComboBox,
				new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		initialClusteringTableImportPanel.add(clusteringImportObjectIdColumnComboBox,
				new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		clusteringImportErrorLabel = new JLabel();
		Font clusteringImportErrorLabelFont = this.$$$getFont$$$(null, Font.BOLD, -1,
				clusteringImportErrorLabel.getFont());
		if (clusteringImportErrorLabelFont != null)
			clusteringImportErrorLabel.setFont(clusteringImportErrorLabelFont);
		clusteringImportErrorLabel.setForeground(new Color(-65536));
		clusteringImportErrorLabel.setText("");
		clusteringImportErrorLabel.setVisible(false);
		initialClusteringTableImportPanel.add(clusteringImportErrorLabel,
				new GridConstraints(5, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		importClusteringCollectMissingObjectsCheckBox.setSelected(true);
		initialClusteringTableImportPanel.add(importClusteringCollectMissingObjectsCheckBox,
				new GridConstraints(4, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		preprocessingOptionsPanel.setLayout(new BorderLayout(0, 0));
		preprocessingOptionsPanel.setVisible(true);
		mainPanel.add(preprocessingOptionsPanel,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		dissagreeingSetsPanel = new JPanel();
		dissagreeingSetsPanel.setLayout(new GridLayoutManager(2, 4, new Insets(0, 0, 0, 0), -1, -1));
		preprocessingOptionsPanel.add(dissagreeingSetsPanel, BorderLayout.NORTH);
		removeDissagreeingObjectSetsCheckBox.setText("Remove disagreeing objects");
		dissagreeingSetsPanel.add(removeDissagreeingObjectSetsCheckBox,
				new GridConstraints(0, 0, 1, 4, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		dissagreeingSetsTextField = new JTextField();
		dissagreeingSetsTextField.setEnabled(false);
		dissagreeingSetsTextField.setText("10");
		dissagreeingSetsPanel.add(dissagreeingSetsTextField,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(50, 24), null, 0, false));
		histDissagreeingButton.setEnabled(false);
		histDissagreeingButton.setText("Hist.");
		dissagreeingSetsPanel.add(histDissagreeingButton,
				new GridConstraints(1, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		dissagreeingCombobox.setEnabled(false);
		final DefaultComboBoxModel defaultComboBoxModel2 = new DefaultComboBoxModel();
		defaultComboBoxModel2.addElement("Percent");
		defaultComboBoxModel2.addElement("Threshold");
		dissagreeingCombobox.setModel(defaultComboBoxModel2);
		dissagreeingSetsPanel.add(dissagreeingCombobox,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		visualizeDissagreeingButton.setEnabled(false);
		visualizeDissagreeingButton.setText("Visualize");
		dissagreeingSetsPanel.add(visualizeDissagreeingButton,
				new GridConstraints(1, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		similarityThresholdTextField = new JTextField();
		similarityThresholdTextField.setText("0.5");
		similarityThresholdTextField.setVisible(false);
		dissagreeingSetsPanel.add(similarityThresholdTextField,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(150, -1), null, 0, false));
		similarityThresholdLabel = new JLabel();
		similarityThresholdLabel.setText("Similarity Threshold");
		similarityThresholdLabel.setVisible(false);
		dissagreeingSetsPanel.add(similarityThresholdLabel,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		lowVariancePanel = new JPanel();
		lowVariancePanel.setLayout(new GridLayoutManager(2, 4, new Insets(0, 0, 0, 0), -1, -1));
		preprocessingOptionsPanel.add(lowVariancePanel, BorderLayout.CENTER);
		removeSetsWithLowCheckBox.setText("Remove objects with low standard variation");
		lowVariancePanel.add(removeSetsWithLowCheckBox,
				new GridConstraints(0, 0, 1, 4, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		lowVarianceTextField = new JTextField();
		lowVarianceTextField.setEnabled(false);
		lowVarianceTextField.setText("10");
		lowVariancePanel.add(lowVarianceTextField,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(50, 24), null, 0, false));
		histVarianceButton.setEnabled(false);
		histVarianceButton.setText("Hist.");
		lowVariancePanel.add(histVarianceButton,
				new GridConstraints(1, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		varianceCombobox.setEnabled(false);
		final DefaultComboBoxModel defaultComboBoxModel3 = new DefaultComboBoxModel();
		defaultComboBoxModel3.addElement("Percent");
		defaultComboBoxModel3.addElement("Threshold");
		varianceCombobox.setModel(defaultComboBoxModel3);
		lowVariancePanel.add(varianceCombobox,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		visualizeVarianceButton.setEnabled(false);
		visualizeVarianceButton.setText("Visualize");
		lowVariancePanel.add(visualizeVarianceButton,
				new GridConstraints(1, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		removeObjectsNotInNetworkPanel = new JPanel();
		removeObjectsNotInNetworkPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		preprocessingOptionsPanel.add(removeObjectsNotInNetworkPanel, BorderLayout.SOUTH);
		networkCombobox.setEnabled(false);
		networkCombobox
				.setToolTipText("Select a network to work with (only needed for removing objects not in network).");
		removeObjectsNotInNetworkPanel.add(networkCombobox,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		removeObjectsNotInNetworkCheckBox.setText("Remove objects not in network");
		removeObjectsNotInNetworkPanel.add(removeObjectsNotInNetworkCheckBox,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		prototypePanel.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(prototypePanel,
				new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		prototypePanel.add(clusterAggregateTimeSeriesFunctionComboBox,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		prototypePanel.add(clusterAggregateNetworkFunctionComboBox,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		includeNetworkLocationInPrototypeCheckBox.setText("Prototypes should include network location");
		prototypePanel.add(includeNetworkLocationInPrototypeCheckBox,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
	}

	/**
	 * @noinspection ALL
	 */
	private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
		if (currentFont == null)
			return null;
		String resultName;
		if (fontName == null) {
			resultName = currentFont.getName();
		} else {
			Font testFont = new Font(fontName, Font.PLAIN, 10);
			if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
				resultName = fontName;
			} else {
				resultName = currentFont.getName();
			}
		}
		return new Font(resultName, style >= 0 ? style : currentFont.getStyle(),
				size >= 0 ? size : currentFont.getSize());
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return mainPanel;
	}

	class DataComboBoxActionListener implements ActionListener {

		@Override
		public void actionPerformed(final ActionEvent e) {
			try {
				DataFormPanel.this.updateLoadDataMethod();
			} catch (final Exception e1) {
				// Do nothing
				e1.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	private void removeShortestPathPrototypeComponentFactory() {
		this.prototypeFactory.getPrototypeComponentFactories().remove(PrototypeComponentType.NETWORK_LOCATION);

		this.removeObjectsNotInNetworkCheckBox.setEnabled(true);
		this.shortestPathNetworkComboBox.setVisible(false);
	}

	/**
	 * 
	 */
	private void addShortestPathPrototypeComponentFactory() {
		this.removeObjectsNotInNetworkCheckBox.setSelected(true);
		this.nlPrototypeComponentFactory = new NetworkLocationPrototypeComponentBuilder();
		this.prototypeFactory.addPrototypeComponentFactory(this.nlPrototypeComponentFactory);

		this.removeObjectsNotInNetworkCheckBox.setEnabled(false);
		this.shortestPathNetworkComboBox.setVisible(true);
	}

	@Override
	public void handleEvent(ColumnNameChangedEvent e) {
		final CyTable selectedTable = (CyTable) this.dataTableComboBox.getSelectedItem();
		if (e.getSource().equals(selectedTable)) {
			for (int col = 0; col < this.objectIdColumnSpinnerModel.getSize(); col++) {
				final InputColumn inputColumn = this.objectIdColumnSpinnerModel.getElementAt(col);
				if (inputColumn.getName().equals(e.getOldColumnName()))
					inputColumn.setName(e.getNewColumnName());
			}
			this.objectIdColumnSpinner.repaint();
			this.replicateColumnSpinner.repaint();
			this.timePointColumnsList.repaint();
		}
	}

	@Override
	public void handleEvent(ColumnDeletedEvent e) {
		if (e.getSource().equals(this.dataTableComboBox.getSelectedItem())) {
			for (int col = 0; col < this.objectIdColumnSpinnerModel.getSize(); col++)
				if (this.objectIdColumnSpinnerModel.getElementAt(col).getName().equals(e.getColumnName()))
					this.objectIdColumnSpinnerModel.removeElementAt(col);
			for (int col = 0; col < this.replicateColumnSpinnerModel.getSize(); col++)
				if (this.replicateColumnSpinnerModel.getElementAt(col).getName().equals(e.getColumnName()))
					this.replicateColumnSpinnerModel.removeElementAt(col);
			for (int col = 0; col < this.timePointColumnsListModel.getSize(); col++)
				if (this.timePointColumnsListModel.getElementAt(col).getName().equals(e.getColumnName()))
					this.timePointColumnsListModel.removeElementAt(col);
		}
	}

	@Override
	public void handleEvent(ColumnCreatedEvent e) {
		if (e.getSource().equals(this.dataTableComboBox.getSelectedItem())) {
			final InputColumn inputColumn = new InputColumn(e.getColumnName(), e.getSource().getColumns().size());
			this.objectIdColumnSpinnerModel.addElement(inputColumn);
			this.replicateColumnSpinnerModel.addElement(inputColumn);
			if (Number.class.isAssignableFrom(e.getSource().getColumn(e.getColumnName()).getType()))
				this.timePointColumnsListModel.addElement(inputColumn);
		}
	}
}

class SliderRenderer extends JSlider implements TableCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3428603483843493372L;

	public SliderRenderer(final int orientation, final int min, final int max, final int value) {
		super(orientation, min, max, value);
		this.setPaintLabels(true);
		this.setPaintTicks(true);
		this.setMajorTickSpacing(25);
		this.setMinorTickSpacing(10);
	}

	@Override
	public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
			final boolean hasFocus, final int row, final int column) {
		if (isSelected) {
			this.setForeground(table.getSelectionForeground());
			this.setBackground(table.getSelectionBackground());
		} else {
			this.setForeground(table.getForeground());
			this.setBackground(table.getBackground());
		}

		final TableColumnModel columnModel = table.getColumnModel();
		final TableColumn selectedColumn = columnModel.getColumn(column);
		final int columnWidth = selectedColumn.getWidth();
		final int columnHeight = table.getRowHeight();
		this.setSize(new Dimension(columnWidth, columnHeight));

		this.setValue(((Integer) value).intValue());
		return this;
	}
}

class SliderEditor extends DefaultCellEditor {
	/**
	 * 
	 */
	private static final long serialVersionUID = 830356104016406764L;
	protected JSlider slider;

	public SliderEditor(final int orientation, final int min, final int max, final int value) {
		super(new JCheckBox());
		this.slider = new JSlider(orientation, min, max, value);
		this.slider.setPaintLabels(true);
		this.slider.setPaintTicks(true);
		this.slider.setMajorTickSpacing(25);
		this.slider.setMinorTickSpacing(10);
		this.slider.setOpaque(true);
		this.slider.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(final MouseEvent e) {
				super.mouseReleased(e);
				SliderEditor.this.stopCellEditing();
			}
		});
		this.slider.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent e) {
				super.keyReleased(e);
				SliderEditor.this.stopCellEditing();
			}
		});
	}

	@Override
	public Component getTableCellEditorComponent(final JTable table, final Object value, final boolean isSelected,
			final int row, final int column) {
		if (isSelected) {
			this.slider.setForeground(table.getSelectionForeground());
			this.slider.setBackground(table.getSelectionBackground());
		} else {
			this.slider.setForeground(table.getForeground());
			this.slider.setBackground(table.getBackground());
		}
		this.slider.setValue(((Integer) value).intValue());

		return this.slider;
	}

	@Override
	public Object getCellEditorValue() {
		return new Integer(this.slider.getValue());
	}

	@Override
	public boolean stopCellEditing() {
		return super.stopCellEditing();
	}

	@Override
	protected void fireEditingStopped() {
		super.fireEditingStopped();
	}
}
