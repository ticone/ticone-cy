package dk.sdu.imada.ticone.gui.panels.clusterchart;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cytoscape.work.TaskManager;

import dk.sdu.imada.ticone.clustering.ClusterOperationException;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeaturesProxy;
import dk.sdu.imada.ticone.gui.panels.MyDialogPanel;
import dk.sdu.imada.ticone.gui.panels.leastfitting.LeastFittingFromOnePatternDialog;
import dk.sdu.imada.ticone.gui.panels.leastfitting.LeastFittingObjectsFromOnePatternContainer;
import dk.sdu.imada.ticone.gui.panels.popup.SplitPatternPanel;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.tasks.delete.DeleteClustersOrObjectsTaskFactory;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * Created by christian on 9/11/15.
 */
public class ClusterChartWithButtonsPanel extends JPanel implements IObjectWithFeaturesProxy {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1909119706703971210L;

	private final ICluster cluster;
	private final ITimeSeriesObjectList patternsData;
	private Color color;
	private ClusterChartContainer clusterChartContainer;
	private final boolean showOptions;

	protected TiconeClusteringResultPanel resultPanel;

	public ClusterChartWithButtonsPanel(final ICluster p, final ITimeSeriesObjectList patternsData,
			final boolean showOptions, final TiconeClusteringResultPanel resultPanel)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException {
		super();
		this.setLayout(new GridBagLayout());
		this.resultPanel = resultPanel;
		this.cluster = p;
		this.showOptions = showOptions;
		this.patternsData = patternsData;
		this.setOpaque(true);
		this.initComponents();
	}

	public ClusterChartContainer getPatternGraphPanelContainer() {
		return this.clusterChartContainer;
	}

	public Color getColor() {
		return this.color;
	}

	public ICluster getCluster() {
		return this.cluster;
	}

	private void initComponents()
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException {
		final boolean hasNetworkPrototypeComponent = false;
//				this.cluster.getPrototype()
//				.hasPrototypeComponent(PrototypeComponentType.NETWORK_LOCATION);

		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.fill = GridBagConstraints.BOTH;
		this.clusterChartContainer = new ClusterChartContainer(this.resultPanel.getClusteringResult(), this.cluster,
				this.patternsData);
		this.resultPanel.getClusteringResult().setDefaultClusterChartContainer(this.cluster,
				this.clusterChartContainer);

		final JPanel tempChartPanel = this.clusterChartContainer.getChartPanel(150, 100);
		this.color = this.clusterChartContainer.getGraphColor();

		constraints.gridwidth = 1;
		constraints.gridx = 0;

		if (hasNetworkPrototypeComponent) {
			constraints.gridy = 0;
			this.add(new JLabel("<html><b>Time series:</b></html>"), constraints);
		}

		constraints.gridy = 1;
		this.add(tempChartPanel, constraints);

		final JPanel optionsPanel = this.setupButtonsPanel();
		optionsPanel.setOpaque(false);
		constraints.gridy = 2;
		constraints.gridwidth = hasNetworkPrototypeComponent ? 2 : 1;
		this.add(optionsPanel, constraints);

		if (hasNetworkPrototypeComponent) {
			constraints.gridx = 1;
			constraints.gridy = 0;
			constraints.gridwidth = 1;
			this.add(new JLabel("<html><b>Network location:</b></html>"), constraints);
			constraints.gridy = 1;
			String name = "";
			if (this.resultPanel.getResult().getIdMapMethod().isActive())
				name = this.resultPanel.getResult().getIdMapMethod().getAlternativeId(name);
			this.add(new JLabel(name, SwingConstants.CENTER), constraints);
		}
	}

	private JPanel setupButtonsPanel() {
		final JPanel optionsPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;

		final JButton zoomChartButton = this.setupZoomChartButton(this.clusterChartContainer);
		constraints.gridx = 0;
		constraints.gridy = 0;
		optionsPanel.add(zoomChartButton, constraints);

		if (this.showOptions) {
			final JCheckBox keepPatternButton = this.setupKeepPatternButton(this.cluster);
			constraints.gridx = 1;
			constraints.gridy = 0;
			optionsPanel.add(keepPatternButton, constraints);

			final JButton deleteButton = this.setupDeleteButton(this.cluster);
			constraints.gridx = 2;
			constraints.gridy = 0;
			optionsPanel.add(deleteButton, constraints);

			final JButton splitPatternButton = this.setupSplitPatternButton(this.cluster);
			constraints.gridx = 3;
			constraints.gridy = 0;
			optionsPanel.add(splitPatternButton, constraints);
		}

		return optionsPanel;
	}

	private JButton setupZoomChartButton(final ClusterChartContainer patternGraphPanelContainer) {
		final JButton zoomChartButton = new JButton("Zoom");
		zoomChartButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				try {
					ClusterChartWithButtonsPanel.this.zoomAction(patternGraphPanelContainer);
				} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
				}
			}
		});
		return zoomChartButton;
	}

	private void zoomAction(final ClusterChartContainer clusterChartContainer)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException {
		final JFrame newWindow = new JFrame("Large Chart Panel");
		newWindow.add(clusterChartContainer.getLargeChartPanel());
		newWindow.setSize(750, 400);
		newWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		newWindow.setVisible(true);
	}

	private JCheckBox setupKeepPatternButton(final ICluster p) {
		final JCheckBox keepPatternButton = new JCheckBox("Keep");
		keepPatternButton.setSelected(p.isKeep());
		keepPatternButton.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent itemEvent) {
				if (itemEvent.getStateChange() == ItemEvent.SELECTED) {
					p.setKeep(true);
				} else {
					p.setKeep(false);
				}
			}
		});
		return keepPatternButton;
	}

	private JButton setupDeleteButton(final ICluster p) {
		final JButton deleteButton = new JButton("Delete");
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				try {
					ClusterChartWithButtonsPanel.this.deletePatternAction();
				} catch (final ClusterOperationException e) {
					MyDialogPanel.showMessageDialog(e.getMessage());
				} catch (InterruptedException | TiconeUnloadingException e) {
				}
			}
		});
		return deleteButton;
	}

	private void deletePatternAction()
			throws ClusterOperationException, InterruptedException, TiconeUnloadingException {
		final Object[] options = { "Cancel", "Both", "Only cluster", "Only assigned objects", "Least fitting objects" };
		final int n = JOptionPane.showOptionDialog(null, "What would you like to delete?", "Delete cluster",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[3]);
		IClusterObjectMapping.DELETE_METHOD deleteMethod;
		if (n == 1) {
			deleteMethod = dk.sdu.imada.ticone.clustering.ClusterObjectMapping.DELETE_METHOD.BOTH_PROTOTYPE_AND_OBJECTS;
		} else if (n == 2) {
			deleteMethod = dk.sdu.imada.ticone.clustering.ClusterObjectMapping.DELETE_METHOD.ONLY_PROTOTYPE;
		} else if (n == 3) {
			deleteMethod = dk.sdu.imada.ticone.clustering.ClusterObjectMapping.DELETE_METHOD.ONLY_OBJECTS;
		} else if (n == 4) {
			final LeastFittingObjectsFromOnePatternContainer leastFittingObjectsFromOnePatternContainer = new LeastFittingObjectsFromOnePatternContainer();
			leastFittingObjectsFromOnePatternContainer.setPattern(this.cluster);
			final LeastFittingFromOnePatternDialog leastFittingFromOnePatternDialog = new LeastFittingFromOnePatternDialog(
					leastFittingObjectsFromOnePatternContainer, this.resultPanel);
			leastFittingFromOnePatternDialog.pack();
			leastFittingFromOnePatternDialog.setVisible(true);
			if (leastFittingObjectsFromOnePatternContainer.isDeleteData()) {
				final IClusteringProcess tscwo = this.resultPanel.getClusteringResult().getClusteringProcess();
				tscwo.removeObjectsFromClusters(leastFittingObjectsFromOnePatternContainer.getTimeSeriesDataToDelete(),
						this.cluster.getClusterNumber());
				GUIUtility.updateGraphPanel(this.resultPanel);
			}
			return;
		} else {
			return;
		}

		final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
		taskManager.execute(new DeleteClustersOrObjectsTaskFactory(deleteMethod, this.cluster.asSingletonList(),
				this.resultPanel, true).createTaskIterator());
	}

	private JButton setupSplitPatternButton(final ICluster p) {
		final JButton splitPatternButton = new JButton("Split");
		splitPatternButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				ClusterChartWithButtonsPanel.this.splitPatternAction();
			}
		});
		return splitPatternButton;
	}

	private void splitPatternAction() {
		final JFrame window = new JFrame("Split cluster");
		final SplitPatternPanel splitPatternPanel = new SplitPatternPanel(window, this.cluster.getClusterNumber(),
				this.resultPanel);
		window.add(splitPatternPanel);
		window.setVisible(true);
		window.setLocation(200, 200);
		window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		window.pack();
	}

	@Override
	public IObjectWithFeatures getObjectWithFeatures() {
		return this.cluster;
	}

}
