package dk.sdu.imada.ticone.gui.panels.kpm;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.model.CyNetworkView;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.gui.IKPMResultPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.panels.comparison.TiconeClusteringComparisonResultPanel;
import dk.sdu.imada.ticone.network.kpm.Result;
import dk.sdu.imada.ticone.network.kpm.main.KPM;
import dk.sdu.imada.ticone.util.CyNetworkUtil;
import dk.sdu.imada.ticone.util.ServiceHelper;

/**
 * Created by christian on 29-01-16.
 */
public class KPMResultFormPanel {

	private final IKPMResultPanel resultPanel;

	private JPanel mainPanel;
	private JTable resultTable;
	private JButton showUnionButton;
	private JButton showCoreButton;

	private final CyNetwork network;

	private final List<Result> resultList;
	private final Collection<IClusters> selectedPatternForThisResult;
	private final String kpmModel;
	private JLabel selectedClustersLabel, kpmModelLabel;

	public KPMResultFormPanel(final IKPMResultPanel resultPanel, final CyNetwork network,
			final Collection<IClusters> selectedPatternForThisResult, final List<Result> resultList,
			final String kpmModel) {
		super();
		this.resultPanel = resultPanel;
		this.network = network;
		this.selectedPatternForThisResult = selectedPatternForThisResult;
		this.resultList = resultList;
		this.kpmModel = kpmModel;
		this.$$$setupUI$$$();
		this.register();
	}

	public void register() {
		this.updateResultTable();
		final KPMResultsTabPanel kpmTabPanel = resultPanel.getKpmResultsTabPanel();
		kpmTabPanel.addKPMResultPanel("Enrichment " + kpmTabPanel.getNextKpmNumber(), this);
		resultPanel.getKpmResultsTabPanel().setSelectedIndex(resultPanel.getKpmResultsTabPanel().getTabCount() - 1);
		if (resultPanel instanceof TiconeClusteringResultPanel) {
			resultPanel.getjTabbedPane()
					.setEnabledAt(TiconeClusteringResultPanel.TAB_INDEX.NETWORK_ENRICHMENT.ordinal(), true);
			resultPanel.getjTabbedPane()
					.setSelectedIndex(TiconeClusteringResultPanel.TAB_INDEX.NETWORK_ENRICHMENT.ordinal());
		} else if (resultPanel instanceof TiconeClusteringComparisonResultPanel) {
			resultPanel.getjTabbedPane()
					.setEnabledAt(TiconeClusteringComparisonResultPanel.TAB_INDEX.NETWORK_ENRICHMENT.ordinal(), true);
			resultPanel.getjTabbedPane()
					.setSelectedIndex(TiconeClusteringComparisonResultPanel.TAB_INDEX.NETWORK_ENRICHMENT.ordinal());
		}
	}

	private void createUIComponents() {
		this.resultTable = new JTable();
		this.resultTable.setModel(new DefaultTableModel() {
			/**
			 *
			 */
			private static final long serialVersionUID = -8318670796885852560L;

			@Override
			public boolean isCellEditable(final int row, final int column) {
				return false;
			}
		});
		this.resultTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(final ListSelectionEvent e) {
				KPMResultFormPanel.this.showResult();
			}
		});
		this.showUnionButton = new JButton();
		this.showUnionButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				KPMResultFormPanel.this.showUnionAction();
			}
		});
		this.showCoreButton = new JButton();
		this.showCoreButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				KPMResultFormPanel.this.showCoreAction();
			}
		});
		final StringBuilder sb = new StringBuilder();
		for (int l = 0; l < this.selectedPatternForThisResult.size(); l++) {
			final IClusters clusterList = new ArrayList<>(this.selectedPatternForThisResult).get(l);
			int c = 0;
			for (final ICluster cluster : clusterList) {
				sb.append(cluster.getName());
				if (c < clusterList.size() - 1)
					sb.append(" & ");
				c++;
			}
			if (l < this.selectedPatternForThisResult.size() - 1)
				sb.append(", ");
		}
		this.selectedClustersLabel = new JLabel(sb.toString());
		this.kpmModelLabel = new JLabel(this.kpmModel != null ? this.kpmModel : "INES");
		this.updateResultTable();
	}

	private void updateResultTable() {
		int rows = 10;
		if (this.resultList != null) {
			if (this.resultList.size() > 0) {
				rows = this.resultList.size();
				this.showCoreButton.setEnabled(true);
				this.showUnionButton.setEnabled(true);
			}
		}
		((DefaultTableModel) this.resultTable.getModel()).setRowCount(0);
		this.resultTable.setRowHeight(30);
		final DefaultTableModel tableModel = (DefaultTableModel) this.resultTable.getModel();
		tableModel.setColumnCount(3);
		tableModel.setRowCount(rows);
		final TableColumnModel columnModel = this.resultTable.getColumnModel();
		columnModel.getColumn(0).setHeaderValue("ID");
		columnModel.getColumn(0).setPreferredWidth(32);
		columnModel.getColumn(0).setMinWidth(32);
		columnModel.getColumn(1).setHeaderValue("Number of nodes");
		columnModel.getColumn(1).setPreferredWidth(72);
		columnModel.getColumn(1).setMinWidth(72);
		columnModel.getColumn(2).setHeaderValue("Exception nodes");
		columnModel.getColumn(2).setPreferredWidth(72);
		columnModel.getColumn(2).setMinWidth(72);

		for (int i = 0; i < rows; i++) {
			if (this.resultList.size() == rows) {
				final Result result = this.resultList.get(i);
				this.resultTable.setValueAt(i, i, 0);
				this.resultTable.setValueAt(result.getFitness(), i, 1);
				this.resultTable.setValueAt(result.getNumExceptionNodes(), i, 2);
			}
		}
		// tableScrollPane.setPreferredSize(new Dimension(300, 400));
		// tableScrollPane.updateUI();
	}

	private void showCoreAction() {
		final CyTable nodeTable = this.network.getDefaultNodeTable();
		final List<Result> resultCore = this.getSelectedResults();
		if (resultCore.size() > 0) {
			KPM.createNetworkWithResults(nodeTable,
					this.getNameForNewKPMNetwork(false, this.selectedPatternForThisResult), this.network, resultCore,
					false, true, this.resultPanel.getClusteringResult().getIdMapMethod());
		}
	}

	private void showUnionAction() {
		final CyTable nodeTable = this.network.getDefaultNodeTable();
		final List<Result> resultUnion = this.getSelectedResults();
		if (resultUnion.size() > 0) {
			KPM.createNetworkWithResults(nodeTable,
					this.getNameForNewKPMNetwork(true, this.selectedPatternForThisResult), this.network, resultUnion,
					true, true, this.resultPanel.getClusteringResult().getIdMapMethod());
		}
	}

	private String getNameForNewKPMNetwork(final boolean isUnion, final Collection<IClusters> patternsInNetwork) {
		final StringBuilder networkName = new StringBuilder();
		if (!isUnion)
			networkName.append("KPM Core ");
		else
			networkName.append("KPM Union ");
		networkName.append(this.resultPanel.toString());
		networkName.append(": ");
		int i = 0;
		for (final IClusters p : patternsInNetwork) {
			if (p.size() == 1)
				networkName.append(p.iterator().next().getName());
			else {
				networkName.append("(");
				int c = 0;
				for (final ICluster cl : p) {
					networkName.append(cl.getName());
					if (c < p.size() - 1)
						networkName.append(",");
					c++;
				}
				networkName.append(")");
			}
			if (i < patternsInNetwork.size() - 1)
				networkName.append(", ");
			i++;
		}
		return networkName.toString();
	}

	private List<Result> getSelectedResults() {
		final List<Result> selectedResults = new ArrayList<>();
		final int[] selectedRows = this.resultTable.getSelectedRows();
		for (int i = 0; i < selectedRows.length; i++) {
			selectedResults.add(this.resultList.get(selectedRows[i]));
		}
		return selectedResults;
	}

	private void showResult() {
		if (this.resultTable.getSelectedRow() < 0) {
			return;
		}
		final CyApplicationManager applicationManager = ServiceHelper.getService(CyApplicationManager.class);
		final CyNetworkView networkView = applicationManager.getCurrentNetworkView();
		final CyTable nodeTable = this.network.getDefaultNodeTable();
		// Result result = resultList.get(resultTable.getSelectedRow());
		Result result;
		if (networkView == null) {
			return;
		}

		CyNetworkUtil.selectNodesOnNetwork(nodeTable, networkView, this.network, null, false);

		final Set<String> nodesInComponent = new HashSet<>();
		for (int i = 0; i < this.resultTable.getSelectedRowCount(); i++) {
			result = this.resultList.get(this.resultTable.getSelectedRows()[i]);
			nodesInComponent.addAll(result.getVisitedNodes().keySet());
		}
		CyNetworkUtil.selectNodesOnNetwork(nodeTable, networkView, this.network, nodesInComponent, true);
	}

	public JPanel getMainPanel() {
		return this.mainPanel;
	}

	public CyNetwork getNetwork() {
		return this.network;
	}

	public List<Result> getResultList() {
		return this.resultList;
	}

	public Collection<IClusters> getSelectedPatternForThisResult() {
		return this.selectedPatternForThisResult;
	}

	public String getKpmModel() {
		return this.kpmModel;
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
	 * edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayoutManager(3, 1, new Insets(5, 5, 5, 5), -1, -1));
		final JScrollPane scrollPane1 = new JScrollPane();
		mainPanel.add(scrollPane1,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_NORTH, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null,
						0, false));
		resultTable.setAutoCreateRowSorter(false);
		scrollPane1.setViewportView(resultTable);
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(panel1,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		showUnionButton.setEnabled(false);
		showUnionButton.setHorizontalAlignment(2);
		showUnionButton.setText("Show union of selection");
		panel1.add(showUnionButton,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		showCoreButton.setEnabled(false);
		showCoreButton.setHorizontalAlignment(2);
		showCoreButton.setText("Show core of selection");
		panel1.add(showCoreButton,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(panel2,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		panel2.setBorder(BorderFactory.createTitledBorder("Info"));
		final JLabel label1 = new JLabel();
		label1.setText("Selected Clusters:");
		panel2.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, 1,
				GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel2.add(selectedClustersLabel,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label2 = new JLabel();
		label2.setText("Model:");
		label2.setVisible(false);
		panel2.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		kpmModelLabel.setVisible(false);
		panel2.add(kpmModelLabel,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return mainPanel;
	}
}
