package dk.sdu.imada.ticone.gui.panels.leastfitting;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.statistics.HistogramDataset;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;

public class HistogramDialog extends JDialog {
	/**
	 *
	 */
	private static final long serialVersionUID = 427563233970258850L;
	private JPanel contentPane;
	private JButton buttonOK;
	private JPanel histogramPanel;
	private JFreeChart histogram;
	private final double[] values;
	private int buckets;
	private final String title;
	private final String xAxisLabel;

	public HistogramDialog(final double[] values, final String title, final String xAxisLabel) {
		this.values = values;
		this.title = title;
		this.xAxisLabel = xAxisLabel;
		this.$$$setupUI$$$();
		Arrays.sort(values);
		final double q1 = values[(int) (values.length * 0.25)];
		final double q3 = values[(int) (values.length * 0.75)];
		final double IQR = q3 - q1;
		double h = 2 * IQR * Math.pow(values.length, -1.0 / 3.0);
		final double max = values[values.length - 1];
		final double min = values[0];
		if (h == 0) {
			h = 1;
		}
		this.buckets = (int) ((max - min) / h);
		if (this.buckets == 0) {
			this.buckets = 1;
		}

		this.setContentPane(this.contentPane);
		this.setModal(true);
		this.getRootPane().setDefaultButton(this.buttonOK);

		this.buttonOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				HistogramDialog.this.onOK();
			}
		});
		this.setupHistogram();
	}

	private void onOK() {
// add your code here
		this.dispose();
	}

	private void createUIComponents() {
		// TODO: place custom component creation code here
		this.histogramPanel = new JPanel();
	}

	private void setupHistogram() {
		final boolean legend = false;
		final boolean tooltips = false;
		final boolean urls = false;
		final HistogramDataset dataset = new HistogramDataset();
		dataset.addSeries("key", this.values, this.buckets);
		this.histogram = ChartFactory.createHistogram(this.title, this.xAxisLabel, "# Objects", dataset,
				PlotOrientation.VERTICAL, legend, tooltips, urls);
		final ChartPanel chartPanel = new ChartPanel(this.histogram);
		this.histogramPanel.add(chartPanel);
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
	 * edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		contentPane = new JPanel();
		contentPane.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		contentPane.add(panel1,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null,
						null, 0, false));
		final Spacer spacer1 = new Spacer();
		panel1.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel1.add(panel2,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		buttonOK = new JButton();
		buttonOK.setText("OK");
		panel2.add(buttonOK,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		contentPane.add(panel3,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		panel3.add(histogramPanel,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return contentPane;
	}
}
