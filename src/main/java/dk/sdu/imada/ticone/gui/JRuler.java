package dk.sdu.imada.ticone.gui;

/**
 * Created by christian on 7/8/15.
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;

import javax.swing.JPanel;

public class JRuler extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6720414294373400466L;
	private List<Double> values;
	private Graphics2D g2d;

	private final int leftSpace = 25;

	private double negativeStepSize;
	private double positiveStepSize;

	public JRuler(final List<Double> values) {
		this.values = values;

		// Do not paint background
		this.setOpaque(false);
	}

	@Override
	public void paintComponent(final Graphics g) {

		this.g2d = (Graphics2D) g;
		this.g2d.setColor(Color.BLACK);

		final int width = this.getWidth() - this.leftSpace;
		final int height = this.getHeight();
		final int middleY = height / 2;

		this.g2d.drawLine(0, middleY, width, middleY);
		this.calculateStepSizes();
		this.drawIndices();
	}

	private void drawIndices() {
		boolean lower;
		int currentX = 0;
		for (int i = 0; i < this.values.size(); i++) {

			// Upper or lower indice
			if (i % 2 == 0) {
				lower = true;
			} else {
				lower = false;
			}

			if (i == this.values.size() - 1) {
				currentX = this.getWidth() - this.leftSpace;
			}
			this.drawSeperator(this.values.get(i), currentX, lower, true);

			// Increment current x to next position.
			if (this.values.get(i) < 0) {
				currentX += this.negativeStepSize;
			} else {
				currentX += this.positiveStepSize;
			}
		}
	}

	private void calculateStepSizes() {
		int stepsFromMinToZero = 0;
		int stepsFromZeroToMax = 0;
		for (int i = 0; i < this.values.size(); i++) {
			if (this.values.get(i) < 0) {
				stepsFromMinToZero++;
			}
			if (this.values.get(i) > 0) {
				stepsFromZeroToMax++;
			}
		}
		final double minValue = this.values.get(0);
		final double maxValue = this.values.get(this.values.size() - 1);
		final double total = maxValue - minValue;

		// The location of zero on the x-axis
		final int width = this.getWidth() - this.leftSpace;
		final double zeroPercentLocation = Math.abs(minValue / total);
		final double zeroX = width * zeroPercentLocation;

		this.negativeStepSize = zeroX / stepsFromMinToZero;
		this.positiveStepSize = (width - zeroX) / stepsFromZeroToMax;
	}

	private void drawSeperator(final double value, final int x, final boolean lower, final boolean paintNumber) {
		final int middleY = this.getHeight() / 2;
		int y;
		int numberY;
		if (lower) {
			y = 0;
			numberY = middleY / 2 + 3;
		} else {
			y = this.getHeight();
			numberY = (y + middleY) / 2 + 7;
		}
		this.g2d.setColor(Color.BLACK);
		this.g2d.drawLine(x, y, x, middleY);
		String number = "" + value;
		number = number.substring(0, Math.min(4, number.length()));
		if (paintNumber) {
			this.g2d.drawString(number, x + 3, numberY);
		}
	}

	public void updateValues(final List<Double> values) {
		this.values = values;
		this.repaint();
	}
}