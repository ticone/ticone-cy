/**
 * 
 */
package dk.sdu.imada.ticone.gui.util;

import java.io.Serializable;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Apr 28, 2017
 *
 */
public interface IClusterComparatorComboBoxItemsChangedListener extends Serializable {
	void comboBoxItemsChanged(final ClusterComparatorComboBoxItemsChangedEvent e);
}
