package dk.sdu.imada.ticone.gui.panels.clusterchart;

import java.awt.BorderLayout;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.util.IClusterStatusMapping;

/**
 * Created by christian on 16-10-15.
 */
public class ClusterChartTablePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4418199596573389131L;

	private final ClusterChartTable graphTableClass;
	protected TiconeClusteringResultPanel resultPanel;

	public ClusterChartTablePanel(final TiconeClusteringResultPanel resultPanel)
			throws IncompatibleSimilarityFunctionException {
		super();
		this.setLayout(new BorderLayout());
		this.resultPanel = resultPanel;
		this.graphTableClass = new ClusterChartTable(this, true, resultPanel);
	}

	public void updateGraphTable(final IClusterStatusMapping patternStatusMapping) throws InterruptedException {
		this.graphTableClass.updateGraphTable(patternStatusMapping);
	}

	/**
	 * @return the graphTableClass
	 */
	public ClusterChartTable getClusterChartTable() {
		return this.graphTableClass;
	}

	public IClusterList getSelectedClusters() {
		return this.graphTableClass.getSelectedClusters();
	}

	public void setSelectedClusters(final IClusterList selectedClusters) {
		this.graphTableClass.setSelectedClusters(selectedClusters);
	}

	public Map<ICluster, Map<String, Map<String, Object>>> getSelectedKpmObjects() {
		final Map<ICluster, Map<String, Map<String, Object>>> selectedObjects = new HashMap<>();

		final IClusterList selectedPatterns = this.getSelectedClusters().asList();

		for (int i = 0; i < selectedPatterns.size(); i++) {
			final ICluster p = selectedPatterns.get(i);
			final ITimeSeriesObjectList objects = p.getObjects();
			final Map<String, Map<String, Object>> objIds = new HashMap<>();
			for (final ITimeSeriesObject tsd : objects) {
				final Map<String, Object> nodeAttributes = new HashMap<>();
				nodeAttributes.put("cluster", p.getName());
				objIds.put(tsd.getName(), nodeAttributes);
			}
			selectedObjects.put(p, objIds);
		}

		return selectedObjects;
	}

	public IClusters getPatternList() {
		return this.graphTableClass.getPatternList();
	}
}
