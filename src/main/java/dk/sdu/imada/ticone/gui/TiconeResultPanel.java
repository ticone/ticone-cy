package dk.sdu.imada.ticone.gui;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;

import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.util.INamedTiconeResult;
import dk.sdu.imada.ticone.util.ITiconeResultDestroyedListener;
import dk.sdu.imada.ticone.util.ITiconeResultNameChangeListener;
import dk.sdu.imada.ticone.util.ProgramState;

public abstract class TiconeResultPanel<RESULT extends INamedTiconeResult> extends JPanel
		implements CytoPanelComponent, ITiconeResultNameChangeListener, ITiconeResultDestroyedListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4829238276140028569L;

	protected RESULT result;

	public TiconeResultPanel(final RESULT result) {
		super();
		this.result = result;
		this.result.addNameChangeListener(this);
	}

	@Override
	public Icon getIcon() {
		BufferedImage image;
		try {
			image = ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("ticone_logo_square.png"));
			return new ImageIcon(image);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getTitle() {
		return "TiCoNE Result";
	}

	@Override
	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.EAST;
	}

	@Override
	public Component getComponent() {
		return this;
	}

	public abstract void saveState(int tabIndex, ProgramState programState);

	public RESULT getResult() {
		return this.result;
	}

	public abstract TiconeCytoscapeClusteringResult getClusteringResult();

	public void setResultName(final String newName) {
		this.result.setName(newName);
	}

	@Override
	public String toString() {
		return this.result.getName();
	}

	public void onRemove() {
	}

	/**
	 * @return
	 */
	public abstract Serializable asSerializable();
}