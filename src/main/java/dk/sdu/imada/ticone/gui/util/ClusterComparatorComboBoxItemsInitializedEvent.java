/**
 * 
 */
package dk.sdu.imada.ticone.gui.util;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 27, 2017
 *
 */
public class ClusterComparatorComboBoxItemsInitializedEvent {
	protected ClusterComparatorComboBox comboBox;

	public ClusterComparatorComboBoxItemsInitializedEvent(final ClusterComparatorComboBox cb) {
		super();
		this.comboBox = cb;
	}

	/**
	 * @return the cb
	 */
	public ClusterComparatorComboBox getCb() {
		return this.comboBox;
	}

}
