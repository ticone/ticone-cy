/**
 * 
 */
package dk.sdu.imada.ticone.gui.util;

import dk.sdu.imada.ticone.gui.TiconeResultPanel;
import dk.sdu.imada.ticone.gui.TiconeTabbedResultsPanel;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 1, 2019
 *
 */
public class TiconeTabSelectionChangedEvent {
	protected TiconeTabbedResultsPanel resultsPanel;
	protected TiconeResultPanel<?> previouslySelectedPanel, currentlySelectedPanel;

	public TiconeTabSelectionChangedEvent(TiconeTabbedResultsPanel resultsPanel,
			TiconeResultPanel<?> previouslySelectedPanel, TiconeResultPanel<?> currentlySelectedPanel) {
		super();
		this.resultsPanel = resultsPanel;
		this.previouslySelectedPanel = previouslySelectedPanel;
		this.currentlySelectedPanel = currentlySelectedPanel;
	}

	/**
	 * @return the resultsPanel
	 */
	public TiconeTabbedResultsPanel getSource() {
		return this.resultsPanel;
	}

	/**
	 * @return the previouslySelectedPanel
	 */
	public TiconeResultPanel<?> getPreviouslySelectedPanel() {
		return this.previouslySelectedPanel;
	}

	/**
	 * @return the currentlySelectedPanel
	 */
	public TiconeResultPanel<?> getCurrentlySelectedPanel() {
		return this.currentlySelectedPanel;
	}
}
