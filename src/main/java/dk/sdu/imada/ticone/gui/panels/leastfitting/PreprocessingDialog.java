package dk.sdu.imada.ticone.gui.panels.leastfitting;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.gui.JFreeChartBuilder;
import dk.sdu.imada.ticone.gui.panels.MyDialogPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer.CHART_Y_LIMITS_TYPE;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimpleSimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.util.ExtractData;
import dk.sdu.imada.ticone.variance.IObjectSetVariance;
import dk.sdu.imada.ticone.variance.StandardVariance;

public class PreprocessingDialog extends JDialog {
	/**
	 *
	 */
	private static final long serialVersionUID = 3405966596842804891L;
	private JPanel contentPane;
	private JButton buttonOK;
	private JButton buttonCancel;
	private JTextField thresholdTextField;
	private JButton updateButton;
	private JPanel graphPanel;
	private JLabel numberOfObjectsLabel;
	private JLabel thresholdLabel;
	private JComboBox parmCombobox;

	private double threshold;
	private final ISimilarityFunction similarity;
	private final ITimeSeriesObjects objects;
	private ITimeSeriesObjects leastConserved;
	private final boolean leastConservedMode;
	private final PreprocesingContainer preprocesingContainer;

	private final String parm;
	private String thresholdText;
	private String percentText;

	public PreprocessingDialog(final PreprocesingContainer preprocesingContainer, final ISimilarityFunction similarity,
			final double threshold, final ITimeSeriesObjects timeSeriesDataList, final boolean leastConservedMode,
			final String parm)
			throws SimilarityCalculationException, InterruptedException, IncompatibleObjectTypeException {
		super();
		this.parm = parm;
		this.threshold = threshold;
		this.objects = timeSeriesDataList;
		this.similarity = similarity;
		this.leastConservedMode = leastConservedMode;
		this.preprocesingContainer = preprocesingContainer;

		this.$$$setupUI$$$();
		this.setContentPane(this.contentPane);
		this.setModal(true);
		this.getRootPane().setDefaultButton(this.buttonOK);

		this.buttonOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				PreprocessingDialog.this.onOK();
			}
		});

		this.buttonCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				PreprocessingDialog.this.onCancel();
			}
		});

		// call onCancel() when cross is clicked
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				PreprocessingDialog.this.onCancel();
			}
		});

		// call onCancel() on ESCAPE
		this.contentPane.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				PreprocessingDialog.this.onCancel();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		if (parm.equals("Percent")) {
			this.thresholdTextField.setText("" + (int) threshold);
		} else if (parm.equals("Threshold")) {
			this.thresholdTextField.setText("" + threshold);
		}
		this.updateAction();
	}

	private void onOK() {
		// add your code here
		try {
			this.threshold = Double.parseDouble(this.thresholdTextField.getText());
			final String parm = (String) this.parmCombobox.getSelectedItem();
			if (this.leastConservedMode) {
				if (parm.equals("Threshold")) {
					if (this.threshold <= 0 || this.threshold >= 1) {
						throw new NumberFormatException();
					}
				} else if (parm.equals("Percent")) {
					this.threshold = Integer.parseInt(this.thresholdTextField.getText());
					if (this.threshold <= 0 || this.threshold >= 100) {
						throw new NumberFormatException();
					}
				}
			} else {
				if (parm.equals("Threshold")) {
					if (this.threshold <= 0) {
						throw new NumberFormatException();
					}
				} else if (parm.equals("Percent")) {
					this.threshold = Integer.parseInt(this.thresholdTextField.getText());
					if (this.threshold <= 0 || this.threshold >= 100) {
						throw new NumberFormatException();
					}
				}
			}
			this.preprocesingContainer.threshold = this.threshold;
		} catch (final NumberFormatException nfe) {
			if (this.leastConservedMode) {
				JOptionPane.showMessageDialog(this,
						"Threshold textfield must be a double between 0 and 1 (or 0 and 100 if percent)");
			} else {
				JOptionPane.showMessageDialog(this,
						"Threshold textfield must be a number above 0 (or between 0 and 100 if percent)");
			}
			return;
		}
		this.preprocesingContainer.ok = true;
		this.preprocesingContainer.parm = (String) this.parmCombobox.getSelectedItem();
		this.dispose();
	}

	private void onCancel() {
		// add your code here if necessary
		this.dispose();
	}

	private void createUIComponents() {
		// TODO: place custom component creation code here
		this.graphPanel = new JPanel();
		this.updateButton = new JButton();
		this.updateButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					PreprocessingDialog.this.updateAction();
				} catch (final SimilarityCalculationException | IncompatibleObjectTypeException e1) {
					MyDialogPanel.showMessageDialog(e1.getMessage());
				} catch (final InterruptedException e1) {
				}
			}
		});
		this.parmCombobox = new JComboBox();
		this.parmCombobox.addItem("Threshold");
		this.parmCombobox.addItem("Percent");
		this.parmCombobox.setSelectedItem(this.parm);
		this.parmCombobox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					final String previousItem = (String) e.getItem();
					if (previousItem.equals("Threshold")) {
						PreprocessingDialog.this.thresholdText = PreprocessingDialog.this.thresholdTextField.getText();
					} else if (previousItem.equals("Percent")) {
						PreprocessingDialog.this.percentText = PreprocessingDialog.this.thresholdTextField.getText();
					}
				}
				if (e.getStateChange() == ItemEvent.SELECTED) {
					final String item = (String) e.getItem();
					if (item.equals("Threshold")) {
						PreprocessingDialog.this.thresholdTextField.setText(PreprocessingDialog.this.thresholdText);
					} else if (item.equals("Percent")) {
						PreprocessingDialog.this.thresholdTextField.setText(PreprocessingDialog.this.percentText);
					}

				}
			}
		});
	}

	private void updateAction()
			throws SimilarityCalculationException, InterruptedException, IncompatibleObjectTypeException {
		final String parm = (String) this.parmCombobox.getSelectedItem();
		try {
			if (this.thresholdTextField.getText().length() <= 0) {
				throw new NumberFormatException();
			}
			this.threshold = Double.parseDouble(this.thresholdTextField.getText());
			if (this.leastConservedMode) {
				if (parm.equals("Threshold")) {
					if (this.threshold <= 0 || this.threshold >= 1) {
						throw new NumberFormatException();
					}
				} else if (parm.equals("Percent")) {
					this.threshold = Integer.parseInt(this.thresholdTextField.getText());
					if (this.threshold <= 0 || this.threshold >= 100) {
						throw new NumberFormatException();
					}
				}
			} else {
				if (parm.equals("Threshold")) {
					if (this.threshold <= 0) {
						throw new NumberFormatException();
					}
				} else if (parm.equals("Percent")) {
					this.threshold = Integer.parseInt(this.thresholdTextField.getText());
					if (this.threshold <= 0 || this.threshold >= 100) {
						throw new NumberFormatException();
					}
				}

			}
		} catch (final Exception e) {
			if (this.leastConservedMode) {
				JOptionPane.showMessageDialog(this,
						"Threshold textfield must be a double between 0 and 1 (or an integer between 0 and 100 if percent)");
			} else {
				JOptionPane.showMessageDialog(this,
						"Threshold textfield must be a number above 0 (or an integer between 0 and 100 if percent)");
			}
			return;
		}
		if (this.leastConservedMode) {
			if (parm.equals("Threshold")) {
				this.leastConserved = ExtractData.findLeastConservedObjectsWithThreshold(this.objects,
						((ISimpleSimilarityFunction) this.similarity).value(this.threshold, null), this.similarity);
			} else if (parm.equals("Percent")) {
				this.leastConserved = ExtractData.findLeastConservedObjects(this.objects, (int) this.threshold,
						this.similarity);
			}
			this.thresholdLabel.setText("Least conserved");
		} else {
			final IObjectSetVariance variance = new StandardVariance();
			if (parm.equals("Threshold")) {
				this.leastConserved = ExtractData.findObjectsWithVarianceThreshold(this.objects, this.threshold,
						variance);
			} else if (parm.equals("Percent")) {
				this.leastConserved = ExtractData.findObjectsWithVariance(this.objects, (int) this.threshold, variance);
			}
			this.thresholdLabel.setText("Low variance from baseline");
		}

		this.numberOfObjectsLabel.setText("Number of objects: " + this.leastConserved.size());
		this.updateGraphPanel();
	}

	private void updateGraphPanel() {
		try {
			this.graphPanel.removeAll();
			this.graphPanel.updateUI();
			final ClusterChartContainer patternGraphPanelContainer = new ClusterChartContainer(null,
					this.leastConserved, CHART_Y_LIMITS_TYPE.MIN_MAX, new JFreeChartBuilder());
			this.graphPanel.add(patternGraphPanelContainer.getChartPanel());
			this.graphPanel.revalidate();
			this.pack();
		} catch (final Exception e) {
			// Something went wrong setting up the graph. No worries.
		}
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
	 * edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		contentPane = new JPanel();
		contentPane.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		contentPane.add(panel1,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null,
						null, 0, false));
		final Spacer spacer1 = new Spacer();
		panel1.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, true, false));
		panel1.add(panel2,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		buttonOK = new JButton();
		buttonOK.setText("Apply threshold");
		panel2.add(buttonOK,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		buttonCancel = new JButton();
		buttonCancel.setText("Cancel");
		panel2.add(buttonCancel,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayoutManager(4, 3, new Insets(0, 0, 0, 0), -1, -1));
		contentPane.add(panel3,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		panel3.add(graphPanel,
				new GridConstraints(1, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		numberOfObjectsLabel = new JLabel();
		numberOfObjectsLabel.setText("Number of objects");
		panel3.add(numberOfObjectsLabel,
				new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		thresholdLabel = new JLabel();
		thresholdLabel.setText("Least conserved");
		panel3.add(thresholdLabel,
				new GridConstraints(2, 0, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		thresholdTextField = new JTextField();
		thresholdTextField.setText("");
		panel3.add(thresholdTextField,
				new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(150, -1), null, 0, false));
		updateButton.setText("Update");
		panel3.add(updateButton,
				new GridConstraints(3, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel3.add(parmCombobox,
				new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return contentPane;
	}
}
