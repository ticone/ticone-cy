package dk.sdu.imada.ticone.gui.panels.popup;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import dk.sdu.imada.ticone.clustering.ClusterFactoryException;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusterOperationException;
import dk.sdu.imada.ticone.clustering.CreateClusterInstanceFactoryException;
import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.gui.panels.MyDialogPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.prototype.CreatePrototypeInstanceFactoryException;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * Created by christian on 7/2/15.
 */
public class AddPatternPanel {

	private final int numberOfTimePoints;
	private final JSlider[] jSliders;
	private final JPanel panel;
	private final JPanel outerPanel;
	private final JFrame window;

	protected TiconeClusteringResultPanel resultPanel;

	public AddPatternPanel(final int numberOfTimePoints, final JFrame window,
			final TiconeClusteringResultPanel resultPanel)
			throws IncompatiblePrototypeComponentException, MissingPrototypeFactoryException {
		super();
		this.resultPanel = resultPanel;
		this.window = window;
		this.outerPanel = new JPanel();
		this.panel = new JPanel(new GridBagLayout());
		this.outerPanel.add(this.panel);
		this.numberOfTimePoints = numberOfTimePoints;
		this.jSliders = new JSlider[numberOfTimePoints];
		this.initComponents();

	}

	public void initComponents() throws IncompatiblePrototypeComponentException, MissingPrototypeFactoryException {
		final IPrototypeBuilder prototypeFactory = this.resultPanel.getClusteringResult().getPrototypeBuilder();
		final ITimeSeriesPrototypeComponentBuilder tsCompFactory = PrototypeComponentType.TIME_SERIES
				.getComponentFactory(prototypeFactory);

		final IDiscretizeTimeSeries discretizePatternFunction = tsCompFactory.getDiscretizeFunction();
		final double[] values = discretizePatternFunction.getDiscretizationValues();

		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.gridy = 0;
		final Hashtable valueTable = new Hashtable();
		for (int i = 0; i < values.length; i++) {
			BigDecimal tempValue = new BigDecimal(values[i]);
			tempValue = tempValue.setScale(2, BigDecimal.ROUND_HALF_UP);
			valueTable.put(i, new JLabel("" + tempValue.doubleValue()));
		}
		for (int i = 0; i < this.numberOfTimePoints; i++) {
			final JPanel tempPanel = new JPanel();
			tempPanel.setPreferredSize(new Dimension(100, 160));
			this.jSliders[i] = new JSlider(0, values.length - 1);
			this.jSliders[i].setOrientation(1);
			this.jSliders[i].setPaintTicks(true);
			this.jSliders[i].setMajorTickSpacing(1);
			this.jSliders[i].setPreferredSize(new Dimension(200, 140));
			this.jSliders[i].setPaintLabels(true);
			this.jSliders[i].setLabelTable(valueTable);

			constraints.gridx = i;
			tempPanel.add(this.jSliders[i]);
			this.panel.add(tempPanel, constraints);
		}

		final JPanel buttonPanel = new JPanel(new GridBagLayout());

		final JButton addButton = new JButton("Add cluster");
		constraints.gridx = 0;
		buttonPanel.add(addButton, constraints);

		final JButton cancelButton = new JButton("Cancel");
		constraints.gridx = 1;
		buttonPanel.add(cancelButton, constraints);

		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.gridwidth = this.numberOfTimePoints;
		constraints.anchor = GridBagConstraints.CENTER;
		this.panel.add(buttonPanel, constraints);

		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				try {
					AddPatternPanel.this.addPatternButtonAction();
					GUIUtility.updateGraphPanel(AddPatternPanel.this.resultPanel);
				} catch (ClusterOperationException | PrototypeFactoryException | IncompatiblePrototypeComponentException
						| MissingPrototypeFactoryException | CreatePrototypeInstanceFactoryException
						| ClusterFactoryException | CreateClusterInstanceFactoryException e) {
					MyDialogPanel.showMessageDialog(e.getMessage());
				} catch (InterruptedException | TiconeUnloadingException e) {
				}
				AddPatternPanel.this.window.dispose();
			}
		});

		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				AddPatternPanel.this.window.dispose();
			}
		});
	}

	public boolean addPatternButtonAction() throws ClusterOperationException, PrototypeFactoryException,
			IncompatiblePrototypeComponentException, MissingPrototypeFactoryException, InterruptedException,
			CreatePrototypeInstanceFactoryException, ClusterFactoryException, CreateClusterInstanceFactoryException {
		final double[] pattern = new double[this.jSliders.length];

		ClusterObjectMapping clusterObjectMapping = this.resultPanel.getClusteringResult().getClusterHistory()
				.getClusterObjectMapping();

		final IPrototypeBuilder prototypeFactory = this.resultPanel.getClusteringResult().getPrototypeBuilder();
		final ITimeSeriesPrototypeComponentBuilder tsCompFactory = PrototypeComponentType.TIME_SERIES
				.getComponentFactory(prototypeFactory);

		final double[] discretizedValues = tsCompFactory.getDiscretizeFunction().getDiscretizationValues();
		for (int i = 0; i < pattern.length; i++) {
			pattern[i] = discretizedValues[this.jSliders[i].getValue()];
		}
		tsCompFactory.setTimeSeries(pattern);
		return this.resultPanel.getClusteringResult().getClusteringProcess()
				.addCluster(prototypeFactory.build()) != null;
	}

	public JPanel getPanel() {
		return this.outerPanel;
	}

}
