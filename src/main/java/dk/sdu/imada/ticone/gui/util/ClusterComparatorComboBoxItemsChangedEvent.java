/**
 * 
 */
package dk.sdu.imada.ticone.gui.util;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Apr 28, 2017
 *
 */
public class ClusterComparatorComboBoxItemsChangedEvent {
	protected ClusterComparatorComboBox comboBox;

	public ClusterComparatorComboBoxItemsChangedEvent(final ClusterComparatorComboBox cb) {
		super();
		this.comboBox = cb;
	}

	/**
	 * @return the cb
	 */
	public ClusterComparatorComboBox getCb() {
		return this.comboBox;
	}

}
