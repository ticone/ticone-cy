package dk.sdu.imada.ticone.gui.panels.clusterchart;

import java.awt.Component;
import java.util.EventObject;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;

import dk.sdu.imada.ticone.gui.jtable.AbstractTiconeTableCellRenderer;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;

public class ClusterChartWithTitleCellRenderer extends AbstractTiconeTableCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7832659325670281021L;

	@Override
	public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
			final boolean hasFocus, final int row, final int column) {
		final int width = table.getColumnModel().getColumn(column).getWidth();
		final int height = table.getRowHeight(row);
		final ClusterChartWithTitle chartWithTitle = (ClusterChartWithTitle) value;
		try {
			final JPanel panel = chartWithTitle.getChartPanel(width, height);

			this.setColors(table, getObjectFromValue(table, row, column, chartWithTitle), isSelected, hasFocus, row,
					column, panel);

			return panel;
		} catch (IncompatiblePrototypeComponentException | MissingPrototypeException | InterruptedException e) {
			// TODO
			return null;
		}
	}

	@Override
	public Component getTableCellEditorComponent(final JTable jTable, final Object value, final boolean b,
			final int row, final int column) {
		return null;
	}

	@Override
	public boolean isCellEditable(final EventObject eventObject) {
		return false;
	}

	@Override
	public boolean shouldSelectCell(final EventObject eventObject) {
		return true;
	}

	@Override
	public boolean stopCellEditing() {
		return true;
	}

	@Override
	public Object getCellEditorValue() {
		return null;
	}

	@Override
	public void addCellEditorListener(final CellEditorListener cellEditorListener) {

	}

	@Override
	public void cancelCellEditing() {

	}

	@Override
	public void removeCellEditorListener(final CellEditorListener cellEditorListener) {

	}

}