package dk.sdu.imada.ticone.gui.panels.leastfitting;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.util.ExtractData;

public class LeastFittingFromOnePatternDialog extends JDialog {
	/**
	 *
	 */
	private static final long serialVersionUID = 451534459351011039L;
	private JPanel contentPane;
	private JButton buttonOK;
	private JButton buttonCancel;
	private JLabel numberOfObjectsLabel;
	private JPanel graphPanel;
	private JTextField percentTextField;
	private JButton updateButton;
	private JRadioButton shapePearsonRadioButton;
	private JRadioButton magnitudeEuclideanRadioButton;

	private final LeastFittingObjectsFromOnePatternContainer leastFittingObjectsFromOnePatternContainer;

	protected TiconeClusteringResultPanel resultPanel;

	public LeastFittingFromOnePatternDialog(
			final LeastFittingObjectsFromOnePatternContainer leastFittingObjectsFromOnePatternContainer,
			final TiconeClusteringResultPanel resultPanel) throws InterruptedException {
		super();
		this.resultPanel = resultPanel;
		this.leastFittingObjectsFromOnePatternContainer = leastFittingObjectsFromOnePatternContainer;
		this.$$$setupUI$$$();
		this.setContentPane(this.contentPane);
		this.setModal(true);
		this.getRootPane().setDefaultButton(this.buttonOK);

		this.buttonOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				LeastFittingFromOnePatternDialog.this.onOK();
			}
		});

		this.buttonCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				LeastFittingFromOnePatternDialog.this.onCancel();
			}
		});

		// call onCancel() when cross is clicked
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				LeastFittingFromOnePatternDialog.this.onCancel();
			}
		});

		// call onCancel() on ESCAPE
		this.contentPane.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				LeastFittingFromOnePatternDialog.this.onCancel();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		this.updateTimeSeriesData();
		this.updateGraphPanel();
		this.updateNumberLabel();

	}

	private void updateNumberLabel() {
		this.numberOfObjectsLabel.setText("Number of objects: "
				+ this.leastFittingObjectsFromOnePatternContainer.getTimeSeriesDataToDelete().size());
	}

	private void updateTimeSeriesData() {
		try {
			final int percent = Integer.parseInt(this.percentTextField.getText());
			if (percent <= 0 || percent > 100) {
				throw new NumberFormatException();
			}
			final IClusterObjectMapping pom = this.resultPanel.getClusteringResult().getClusteringProcess()
					.getLatestClustering();
			final ICluster p = this.leastFittingObjectsFromOnePatternContainer.getPattern();
			ISimilarityFunction similarityFunction;
			if (this.shapePearsonRadioButton.isSelected()) {
				similarityFunction = new PearsonCorrelationFunction(
						this.resultPanel.getClusteringResult().getTimePointWeighting());
			} else {
				similarityFunction = new InverseEuclideanSimilarityFunction(
						this.resultPanel.getClusteringResult().getTimePointWeighting());
			}

			this.leastFittingObjectsFromOnePatternContainer.setTimeSeriesDataToDelete(
					ExtractData.getLeastFittingDataFromPattern(pom, p, percent, similarityFunction));
		} catch (final NumberFormatException nfe) {
			JOptionPane.showMessageDialog(this, "Percent textfield must be an integer between 1 and 100.");
		} catch (final Exception e) {
			JOptionPane.showMessageDialog(this, "Percent textfield must be an integer between 1 and 100.");
		}
	}

	private void updateGraphPanel() throws InterruptedException {
		this.graphPanel.removeAll();
		this.graphPanel.updateUI();

		if (this.leastFittingObjectsFromOnePatternContainer.getTimeSeriesDataToDelete().size() > 0) {
			try {
				final ClusterChartContainer patternGraphPanelContainer = new ClusterChartContainer(
						this.resultPanel.getClusteringResult(), null,
						this.leastFittingObjectsFromOnePatternContainer.getTimeSeriesDataToDelete());
				this.graphPanel.add(patternGraphPanelContainer.getChartPanel());
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
			}
		}
		this.graphPanel.revalidate();
	}

	private void onOK() {
		this.leastFittingObjectsFromOnePatternContainer.setDeleteData(true);
		// add your code here
		this.dispose();
	}

	private void onCancel() {
		// add your code here if necessary
		this.dispose();
	}

	public static void main(final String[] args) throws InterruptedException {
		final LeastFittingFromOnePatternDialog dialog = new LeastFittingFromOnePatternDialog(null, null);
		dialog.pack();
		dialog.setVisible(true);
		System.exit(0);
	}

	private void createUIComponents() {
		this.graphPanel = new JPanel();
		this.updateButton = new JButton();
		this.updateButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					LeastFittingFromOnePatternDialog.this.updateTimeSeriesData();
					LeastFittingFromOnePatternDialog.this.updateGraphPanel();
					LeastFittingFromOnePatternDialog.this.updateNumberLabel();
					LeastFittingFromOnePatternDialog.this.pack();
				} catch (InterruptedException e1) {
				}
			}
		});
		this.shapePearsonRadioButton = new JRadioButton();
		this.magnitudeEuclideanRadioButton = new JRadioButton();
		final ButtonGroup similarityButtonGroup = new ButtonGroup();
		similarityButtonGroup.add(this.shapePearsonRadioButton);
		similarityButtonGroup.add(this.magnitudeEuclideanRadioButton);
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
	 * edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		contentPane = new JPanel();
		contentPane.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		contentPane.add(panel1,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null,
						null, 0, false));
		final Spacer spacer1 = new Spacer();
		panel1.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, true, false));
		panel1.add(panel2,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		buttonOK = new JButton();
		buttonOK.setText("Delete objects");
		panel2.add(buttonOK,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		buttonCancel = new JButton();
		buttonCancel.setText("Cancel");
		panel2.add(buttonCancel,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayoutManager(4, 3, new Insets(0, 0, 0, 0), -1, -1));
		contentPane.add(panel3,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		panel3.add(graphPanel,
				new GridConstraints(1, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		numberOfObjectsLabel = new JLabel();
		numberOfObjectsLabel.setText("Number of objects: ");
		panel3.add(numberOfObjectsLabel,
				new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		percentTextField = new JTextField();
		percentTextField.setText("10");
		panel3.add(percentTextField,
				new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(150, -1), null, 0, false));
		updateButton.setText("Update");
		panel3.add(updateButton,
				new GridConstraints(3, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label1 = new JLabel();
		label1.setText("Percent:");
		panel3.add(label1, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel4 = new JPanel();
		panel4.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel3.add(panel4,
				new GridConstraints(2, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel5 = new JPanel();
		panel5.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		panel4.add(panel5,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		panel5.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Similarity function"));
		shapePearsonRadioButton.setSelected(true);
		shapePearsonRadioButton.setText("Shape (Pearson)");
		panel5.add(shapePearsonRadioButton,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		magnitudeEuclideanRadioButton.setText("Magnitude (Euclidean)");
		panel5.add(magnitudeEuclideanRadioButton,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return contentPane;
	}
}
