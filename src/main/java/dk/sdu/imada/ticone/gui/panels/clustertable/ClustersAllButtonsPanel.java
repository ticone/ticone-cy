package dk.sdu.imada.ticone.gui.panels.clustertable;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskManager;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusterOperationException;
import dk.sdu.imada.ticone.clustering.ClusteringChangeEvent;
import dk.sdu.imada.ticone.clustering.ClusteringIterationAddedEvent;
import dk.sdu.imada.ticone.clustering.ClusteringIterationDeletedEvent;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringChangeListener;
import dk.sdu.imada.ticone.clustering.IClusteringIterationAddedListener;
import dk.sdu.imada.ticone.clustering.IClusteringIterationDeletionListener;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.ITiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.clustering.filter.IFilter;
import dk.sdu.imada.ticone.clustering.validity.DunnIndex;
import dk.sdu.imada.ticone.data.CreateRandomDataset;
import dk.sdu.imada.ticone.data.CreateRandomTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ShuffleDatasetGlobally;
import dk.sdu.imada.ticone.data.ShuffleDatasetRowwise;
import dk.sdu.imada.ticone.data.ShuffleTimeSeries;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureInformationContent;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.ClusterFeaturePrototypeStandardVariance;
import dk.sdu.imada.ticone.feature.ClusteringFeatureNumberClusters;
import dk.sdu.imada.ticone.feature.ClusteringFeatureValidity;
import dk.sdu.imada.ticone.feature.FeaturePvalue;
import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.ISimilarityFeature;
import dk.sdu.imada.ticone.feature.scale.IScalerBuilder;
import dk.sdu.imada.ticone.feature.scale.TanhNormalizerBuilder;
import dk.sdu.imada.ticone.fitness.BasicFitnessScore;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.gui.panels.CollapsiblePanel;
import dk.sdu.imada.ticone.gui.panels.MyDialogPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer.CHART_Y_LIMITS_TYPE;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.panels.history.HistoryFrame;
import dk.sdu.imada.ticone.gui.panels.leastfitting.FilterObjectsDialog;
import dk.sdu.imada.ticone.gui.panels.popup.AddPatternPanel;
import dk.sdu.imada.ticone.gui.panels.valuesample.FeatureValueHistogramChartPanel;
import dk.sdu.imada.ticone.gui.panels.valuesample.TwoFeatureValueSamplesScatterChartPanel;
import dk.sdu.imada.ticone.gui.summary.ClusterSummaryFrame;
import dk.sdu.imada.ticone.gui.util.ITiconeTabSelectionChangedListener;
import dk.sdu.imada.ticone.gui.util.ShuffleComboBox;
import dk.sdu.imada.ticone.gui.util.TiconeTabManager;
import dk.sdu.imada.ticone.gui.util.TiconeTabSelectionChangedEvent;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimpleSimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.statistics.BasicCalculatePValues;
import dk.sdu.imada.ticone.statistics.BonferroniPValues;
import dk.sdu.imada.ticone.statistics.ICombinePValues;
import dk.sdu.imada.ticone.statistics.IPValueCalculationResult;
import dk.sdu.imada.ticone.statistics.IPValueResultStorageListener;
import dk.sdu.imada.ticone.statistics.MultiplyPValues;
import dk.sdu.imada.ticone.statistics.PValueResultStorageEvent;
import dk.sdu.imada.ticone.tasks.ClusterSimilarityNetworkTaskFactory;
import dk.sdu.imada.ticone.tasks.ObjectSimilarityNetworkTaskFactory;
import dk.sdu.imada.ticone.tasks.clustering.ClusterObjectsTaskFactory;
import dk.sdu.imada.ticone.tasks.clustering.DeleteOldIterationsTaskFactory;
import dk.sdu.imada.ticone.tasks.clustering.LuckyTaskFactory;
import dk.sdu.imada.ticone.tasks.filter.FilterClusterTaskFactory;
import dk.sdu.imada.ticone.tasks.merge.MergeClustersAboveSimilarityThresholdTaskFactory;
import dk.sdu.imada.ticone.tasks.merge.MergeSelectedClustersTaskFactory;
import dk.sdu.imada.ticone.tasks.statistics.ClusterCalculatePValuesTaskFactory;
import dk.sdu.imada.ticone.util.CyNetworkUtil;
import dk.sdu.imada.ticone.util.ExportToPDFUtil;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.ITiconeResultChangeListener;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.Iterables;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeResultChangeEvent;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

/**
 * @author Christian Wiwie
 * @since Aug 5, 2016
 */
public class ClustersAllButtonsPanel extends JPanel
		implements IClusteringChangeListener, ITiconeTabSelectionChangedListener, ITiconeResultChangeListener,
		IPValueResultStorageListener, IClusteringIterationAddedListener, IClusteringIterationDeletionListener {
	/**
	 *
	 */
	private static final long serialVersionUID = -724746668207668079L;
	private JPanel mainPanel, actionPanel;
	private JScrollPane scrollPane;
	private JButton showHistoryButton, exportClustersButton, showClusterSummaryButton, addPredefinedPatternsButton,
			mergePatternsButton, showLestFittingObjectsButton, showLeastConservedButton, computeButton, luckyButton,
			applyActionsButton, discardActionsButton;
	private JComboBox<CHART_Y_LIMITS_TYPE> clusterChartLimitComboBox;
	private boolean ignoreClusterChartComboBoxSelection;

	// features for cluster p-value calculation
	private JPanel permutationPanel;
	private JRadioButton globallyRadioButton;
	private JRadioButton objectwiseRadioButton;
	private JTextField numberOfIterationsRefinementStepsTextField;
	private JComboBox combinePvaluesComboBox;
	private JComboBox<IShuffle> permuteComboBox;
	private JButton calculatePValuesButton;
	private JCheckBox filterClusters;
	private JComboBox filterClustersFeature;
	private JComboBox filterClustersOperator;
	private JTextField filterClustersInput;
	private JButton filterApplyBtn;
	private JButton deleteAllIterationsExceptButton;
	private DefaultListModel<IArithmeticFeature<? extends Comparable<?>>> clusterFeaturesPValueCalculationListModel,
			clusterConditionalFeaturesPValueCalculationListModel;

	private JList<IArithmeticFeature<? extends Comparable<?>>> clusterPValueFeatures;
	private JPanel filterClustersPanel;
	private JPanel cleanUpClusteringPanel;
	private JPanel visHistoryExportPanel;
	private JButton mergeSimilarClustersButton;
	private JPanel modifyClusteringPanel;
	private JList<IArithmeticFeature<? extends Comparable<?>>> clusterPValueConditionalFeatures;
	private JCheckBox storeFeatureDistributionsCheckBox;
	private JButton visualizeFeatureDistributionButton;
	private JButton clearFeatureValuesButton;
	private JTextField permutationSeedTextField;

	private void createUIComponents() {

		this.mainPanel = new JPanel();

		this.scrollPane = new JScrollPane();
		this.scrollPane.getVerticalScrollBar().setUnitIncrement(32);

		this.modifyClusteringPanel = new CollapsiblePanel("Modify Clustering", true);

		this.showHistoryButton = new JButton();
		this.showHistoryButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					ClustersAllButtonsPanel.this.showHistoryAction();
				} catch (InterruptedException | TiconeUnloadingException e1) {
				}
			}
		});
		this.exportClustersButton = new JButton();
		this.exportClustersButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					ClustersAllButtonsPanel.this.saveClustersAction();
				} catch (final TiconeUnloadingException e1) {
				}
			}
		});
		this.showClusterSummaryButton = new JButton();
		this.showClusterSummaryButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					ClustersAllButtonsPanel.this.showClusterSummaryAction();
				} catch (final TiconeUnloadingException | InterruptedException e1) {
				}
			}
		});
		this.addPredefinedPatternsButton = new JButton();
		this.addPredefinedPatternsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					ClustersAllButtonsPanel.this.addPatternAction();
				} catch (IncompatiblePrototypeComponentException | MissingPrototypeFactoryException e1) {
					MyDialogPanel.showMessageDialog(e1.getMessage());
				} catch (final TiconeUnloadingException e1) {
				}
			}
		});
		this.mergePatternsButton = new JButton();
		this.mergePatternsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					ClustersAllButtonsPanel.this.mergePatternsAction();
				} catch (final TiconeUnloadingException | InterruptedException e1) {
				}
			}
		});
		this.mergeSimilarClustersButton = new JButton();
		this.mergeSimilarClustersButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					ClustersAllButtonsPanel.this.mergeSimilarClustersAction();
				} catch (final TiconeUnloadingException | InterruptedException e1) {
				}
			}
		});
		this.showLestFittingObjectsButton = new JButton();
		this.showLestFittingObjectsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					ClustersAllButtonsPanel.this.showLeastFittingObjectsAction();
				} catch (final TiconeUnloadingException e1) {
				}
			}
		});

		this.clusterChartLimitComboBox = new JComboBox<>(CHART_Y_LIMITS_TYPE.values());
		this.clusterChartLimitComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				if (!ClustersAllButtonsPanel.this.ignoreClusterChartComboBoxSelection)
					try {
						GUIUtility.getCurrentlySelectedClusteringResultPanel().getClusteringResult().setChartLimits(
								(CHART_Y_LIMITS_TYPE) ClustersAllButtonsPanel.this.clusterChartLimitComboBox
										.getSelectedItem());
					} catch (final TiconeUnloadingException e1) {
					}
			}
		});

		this.visHistoryExportPanel = new CollapsiblePanel("Visualization, History & Export", false);
		this.filterClustersPanel = new CollapsiblePanel("Filter Clusters", false);
		this.permutationPanel = new CollapsiblePanel("Calculate Cluster P-Values", false);
		this.cleanUpClusteringPanel = new CollapsiblePanel("Clean Up Clustering", false);

		this.setupComputeButton();
		this.setupLuckyButton();
		this.setupActionChangesButtonPanel();

		// combine p-values
		this.combinePvaluesComboBox = new JComboBox<>(
				new ICombinePValues[] { new MultiplyPValues(), new BonferroniPValues() });

		this.permuteComboBox = new ShuffleComboBox(new IShuffle[] {
				// these two lead to biased p-values where alpha is incorrectly controlled.
				new ShuffleTimeSeries(), new CreateRandomTimeSeries(),
				// doesn't make sense here, because it ruins the intra-cluster similarity;
				// new ShuffleClusteringByShufflingClusters(),
				new CreateRandomDataset(), new ShuffleDatasetRowwise(), new ShuffleDatasetGlobally() });

		// features for cluster p-value calculation
		this.clusterFeaturesPValueCalculationListModel = new DefaultListModel<>();
		this.clusterConditionalFeaturesPValueCalculationListModel = new DefaultListModel<>();

		for (IArithmeticFeature<? extends Comparable<?>> f : new IArithmeticFeature[] {
				new ClusterFeatureAverageSimilarity(), new ClusterFeatureNumberObjects(),
				new ClusterFeaturePrototypeStandardVariance(), new ClusterFeatureInformationContent() }) {
			this.clusterFeaturesPValueCalculationListModel.addElement(f);
			this.clusterConditionalFeaturesPValueCalculationListModel.addElement(f);
		}

		this.clusterPValueFeatures = new JList(this.clusterFeaturesPValueCalculationListModel);
		this.clusterPValueConditionalFeatures = new JList(this.clusterConditionalFeaturesPValueCalculationListModel);

		this.clusterPValueFeatures.setSelectedIndices(new int[] { 0 });
		this.clusterPValueConditionalFeatures.setSelectedIndices(new int[] { 1 });

		this.visualizeFeatureDistributionButton = new JButton();
		this.visualizeFeatureDistributionButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				visualizeFeatureDistributionAction();
			}
		});

		this.clearFeatureValuesButton = new JButton();
		this.clearFeatureValuesButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					IPValueCalculationResult pvalueCalculationResult = GUIUtility
							.getCurrentlySelectedClusteringResultPanel().getResult().getPvalueCalculationResult();
					if (pvalueCalculationResult == null)
						return;
					pvalueCalculationResult.clearPermutedFeatureValues();
					pvalueCalculationResult.clearPermutedFeatureValuesObjectSpecific();
					pvalueCalculationResult.clearPermutedFitnessValues();
					pvalueCalculationResult.clearPermutedFitnessValuesObjectSpecific();
				} catch (TiconeUnloadingException e1) {
				}
			}
		});

		final NumberFormat f = NumberFormat.getNumberInstance();
		f.setGroupingUsed(false);
		this.permutationSeedTextField = new JFormattedTextField(f);
		this.updatePermutationsRandomSeed();

		TiconeTabManager.getSingleton().addClusteringTabSelectionListener(this);

		this.calculatePValuesButton = new JButton("Calculate P-values");
		this.calculatePValuesButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				calculatePvaluesAction();
				updatePermutationsRandomSeed();
			}
		});

		// cluster filtering
		this.filterClusters = new JCheckBox("Filter clusters:");
		this.filterClustersFeature = new JComboBox<IFeature<? extends Number>>(
				new IFeature[] { new ClusterFeatureNumberObjects(),
//						new ClusterFeatureAverageSimilarity(new NegativeEuclideanSimilarityFunction()),
//						new ClusterFeatureAverageSimilarity(new PearsonCorrelationFunction()),
						new FeaturePvalue(ObjectType.CLUSTER), new ClusterFeaturePrototypeStandardVariance(),
						new ClusterFeatureInformationContent() });
		this.filterClustersFeature.setEnabled(false);
		this.filterClustersOperator = new JComboBox<>(IFilter.FILTER_OPERATOR.values());
		this.filterClustersOperator.setEnabled(false);
		this.filterClustersInput = new JTextField("0.00001");
		this.filterClustersInput.setEnabled(false);
		this.filterApplyBtn = new JButton("Apply Filter Settings");
		this.filterApplyBtn.setEnabled(false);

		this.filterClusters.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				ClustersAllButtonsPanel.this.filterClustersFeature
						.setEnabled(ClustersAllButtonsPanel.this.filterClusters.isSelected());
				ClustersAllButtonsPanel.this.filterClustersOperator
						.setEnabled(ClustersAllButtonsPanel.this.filterClusters.isSelected());
				ClustersAllButtonsPanel.this.filterClustersInput
						.setEnabled(ClustersAllButtonsPanel.this.filterClusters.isSelected());
				ClustersAllButtonsPanel.this.filterApplyBtn
						.setEnabled(ClustersAllButtonsPanel.this.filterClusters.isSelected());

				TiconeCytoscapeClusteringResult utils;
				try {
					utils = GUIUtility.getCurrentlySelectedClusteringResultPanel().getClusteringResult();
				} catch (final TiconeUnloadingException e1) {
					return;
				}
				utils.getClusterFilter().setActive(ClustersAllButtonsPanel.this.filterClusters.isSelected());
				utils.getClusterFilter().fireFilterChanged();
			}
		});

		this.filterApplyBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				if (ClustersAllButtonsPanel.this.filterClusters.isSelected()) {
					try {
						final TiconeCytoscapeClusteringResult utils = GUIUtility
								.getCurrentlySelectedClusteringResultPanel().getClusteringResult();
						final IArithmeticFeature<?> feature = (IArithmeticFeature) ClustersAllButtonsPanel.this.filterClustersFeature
								.getSelectedItem();
						final IFilter.FILTER_OPERATOR operator = (IFilter.FILTER_OPERATOR) ClustersAllButtonsPanel.this.filterClustersOperator
								.getSelectedItem();
						final double filterInput = Double
								.valueOf(ClustersAllButtonsPanel.this.filterClustersInput.getText());

						final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
						taskManager.execute(new FilterClusterTaskFactory(utils, feature, operator, filterInput)
								.createTaskIterator());
					} catch (final NumberFormatException ex) {
						ex.printStackTrace();
					} catch (final TiconeUnloadingException e1) {
					}
				}
			}
		});

		this.deleteAllIterationsExceptButton = new JButton("Delete all iterations except the last");
		this.deleteAllIterationsExceptButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				TiconeCytoscapeClusteringResult utils;
				try {
					utils = GUIUtility.getCurrentlySelectedClusteringResultPanel().getClusteringResult();
				} catch (final TiconeUnloadingException e1) {
					return;
				}
				final int currentIteration = utils.getClusterHistory().getIterationNumber();
				final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
				taskManager.execute(new DeleteOldIterationsTaskFactory(utils, currentIteration).createTaskIterator());
			}
		});
	}

	protected void visualizeFeatureDistributionAction() {
		try {
			final IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> clusteringProcess = GUIUtility
					.getCurrentlySelectedClusteringResultPanel().getClusteringProcess();
			final TiconeCytoscapeClusteringResult clusteringResult = clusteringProcess.getClusteringResult();
			final IPValueCalculationResult pvalueCalculationResult = clusteringResult.getPvalueCalculationResult();
			if (pvalueCalculationResult == null)
				return;

			final JFrame jFrame = new JFrame(String.format("%s - Iteration %d", clusteringResult.getName(),
					clusteringProcess.getHistory().getIterationNumber()));
			jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			final JPanel mainPanel = new JPanel(new GridLayout(0, 3, 0, 0));

			final FeaturePvalue featurePvalue = new FeaturePvalue(ObjectType.CLUSTER);
			mainPanel.add(new FeatureValueHistogramChartPanel(featurePvalue, Optional.empty(),
					Optional.ofNullable((Iterable) pvalueCalculationResult.getFeatureStore()
							.getFeatureValues(featurePvalue, ObjectType.CLUSTER))));

			for (final ObjectType<?> objectType : new ObjectType[] { ObjectType.CLUSTER, ObjectType.CLUSTERING }) {

				if (!pvalueCalculationResult.hasPermutedFeatureValues())
					return;

				final List<IArithmeticFeature<? extends Comparable<?>>> featureList = pvalueCalculationResult
						.getFeatures(objectType);

				for (IArithmeticFeature<? extends Comparable<?>> f : featureList) {
					mainPanel.add(new FeatureValueHistogramChartPanel(f,
							Optional.ofNullable((List) pvalueCalculationResult.getPermutedFeatureValues(objectType, f)),
							Optional.ofNullable((Iterable) pvalueCalculationResult.getFeatureStore().getFeatureValues(f,
									objectType))));
				}

				int f1idx = 0;
				for (IArithmeticFeature<? extends Comparable<?>> f1 : featureList) {
					int f2idx = 0;
					for (IArithmeticFeature<? extends Comparable<?>> f2 : featureList) {
						if (f2idx >= f1idx + 1 && f1.supportedObjectType().equals(f2.supportedObjectType())) {
							try {
								final JPanel chartPanel = new TwoFeatureValueSamplesScatterChartPanel<>(
										(List) pvalueCalculationResult.getPermutedFeatureValues(objectType, f1),
										(List) pvalueCalculationResult.getPermutedFeatureValues(objectType, f2),
										Optional.ofNullable((List) Iterables.toList(
												pvalueCalculationResult.getFeatureStore().getFeatureValues(f1))),
										Optional.ofNullable((List) Iterables.toList(
												pvalueCalculationResult.getFeatureStore().getFeatureValues(f2))));
								if (chartPanel != null)
									mainPanel.add(chartPanel);
							} catch (Exception e) {

							}
						}
						f2idx++;
					}
					f1idx++;
				}
			}

			jFrame.add(mainPanel);
			Rectangle bounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice()
					.getDefaultConfiguration().getBounds();
			jFrame.setSize(bounds.width * 2 / 3, bounds.height * 2 / 3);
			jFrame.setVisible(true);

			pvalueCalculationResult.addChangeListener(new ITiconeResultChangeListener() {

				@Override
				public void resultChanged(TiconeResultChangeEvent e) {
					if (!pvalueCalculationResult.hasPermutedFeatureValues()) {
						EventQueue.invokeLater(
								() -> jFrame.dispatchEvent(new WindowEvent(jFrame, WindowEvent.WINDOW_CLOSING)));
						pvalueCalculationResult.removeChangeListener(this);
					}
				}
			});
		} catch (HeadlessException | TiconeUnloadingException | IncompatiblePrototypeComponentException
				| MissingPrototypeException | InterruptedException | NotAnArithmeticFeatureValueException
				| ToNumberConversionException | IncompatibleFeatureValueProviderException e1) {
			e1.printStackTrace();
		}
	}

	protected void calculatePvaluesAction() {
		final TiconeCytoscapeClusteringResult utils;
		try {
			ClustersAllButtonsPanel.this.updatePermuteMethod();
			utils = GUIUtility.getCurrentlySelectedClusteringResultPanel().getClusteringResult();

			final int numberOfPermutations = Integer
					.valueOf(ClustersAllButtonsPanel.this.numberOfIterationsRefinementStepsTextField.getText());
			if (numberOfPermutations < 0) {
				throw new NumberFormatException();
			}

			final IShuffle shuffle = ((IShuffle) ClustersAllButtonsPanel.this.permuteComboBox.getSelectedItem()).copy();

			final List<IArithmeticFeature<? extends Comparable<?>>> selectedFeatures = ClustersAllButtonsPanel.this.clusterPValueFeatures
					.getSelectedValuesList();
			for (IFeature<?> f : selectedFeatures)
				if (f instanceof ISimilarityFeature)
					((ISimilarityFeature) f).setSimilarityFunction(utils.getSimilarityFunction());
			final Set<IArithmeticFeature<? extends Comparable<?>>> selectedFeatureCopies = selectedFeatures.stream()
					.map(f -> {
						f.setStoreFeatureInValues(storeFeatureDistributionsCheckBox.isSelected());
						f.setStoreObjectInValues(storeFeatureDistributionsCheckBox.isSelected());
						return (IArithmeticFeature<? extends Comparable<?>>) f.copy();
					}).collect(Collectors.toCollection(() -> new HashSet<>()));

			final List<IArithmeticFeature<? extends Comparable<?>>> selectedConditionalFeatures = ClustersAllButtonsPanel.this.clusterPValueConditionalFeatures
					.getSelectedValuesList();
			for (IFeature<?> f : selectedConditionalFeatures)
				if (f instanceof ISimilarityFeature)
					((ISimilarityFeature) f).setSimilarityFunction(utils.getSimilarityFunction());

			final List<IArithmeticFeature<? extends Comparable<?>>> conditionalFeatures = selectedConditionalFeatures
					.stream().map(f -> (IArithmeticFeature<? extends Comparable<?>>) f.copy())
					.collect(Collectors.toCollection(() -> new ArrayList<>()));

			final List<IFitnessScore> fitnessScores = new ArrayList<>();
			final List<Boolean> fitnessScoreTwoSided = new ArrayList<>();

			List<IScalerBuilder> scaler = new ArrayList<>();
			double[] weights = new double[selectedFeatureCopies.size()];

			int fc = 0;
			for (final IArithmeticFeature<? extends Comparable<?>> f : selectedFeatureCopies) {
				scaler.add(new TanhNormalizerBuilder().setFeatureToScale(f));
				weights[fc++] = 1.0;
			}

			final IFitnessScore fitnessScore = new BasicFitnessScore(ObjectType.CLUSTER,
					selectedFeatureCopies.toArray(new IArithmeticFeature[0]), scaler.toArray(new IScalerBuilder[0]),
					weights);
			fitnessScores.add(fitnessScore);
			fitnessScoreTwoSided.add(false);

			final IFitnessScore fitnessScoreClustering = new BasicFitnessScore(ObjectType.CLUSTERING,
					new IArithmeticFeature[] {
							new ClusteringFeatureValidity(new DunnIndex(), utils.getSimilarityFunction()) },
					null, null);
			fitnessScores.add(fitnessScoreClustering);
			fitnessScoreTwoSided.add(false);

			final BasicCalculatePValues calculateClusterPValues = new BasicCalculatePValues(
					Arrays.asList(ObjectType.CLUSTER, ObjectType.CLUSTERING), shuffle, fitnessScores,
					new List[] { conditionalFeatures,
							new ArrayList<>(Arrays.asList(new ClusteringFeatureNumberClusters())) },
					fitnessScoreTwoSided,
					(ICombinePValues) ClustersAllButtonsPanel.this.combinePvaluesComboBox.getSelectedItem(),
					numberOfPermutations);
			calculateClusterPValues.setStorePermutedFeatureValues(storeFeatureDistributionsCheckBox.isSelected());
			calculateClusterPValues.setStorePermutedFitnessValues(storeFeatureDistributionsCheckBox.isSelected());

			final AbstractTaskFactory tf = new ClusterCalculatePValuesTaskFactory(utils, calculateClusterPValues,
					Long.parseLong(this.permutationSeedTextField.getText()));
			final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
			taskManager.execute(tf.createTaskIterator());

		} catch (final TiconeUnloadingException e1) {
			return;
		} catch (final NumberFormatException nfe) {
			MyDialogPanel.showMessageDialog(null, "Number of iterations wanted must be a positive integer");
		} catch (IncompatibleSimilarityFunctionException e) {
			e.printStackTrace();
		}
	}

	private void updatePermuteMethod() throws TiconeUnloadingException {
		final TiconeCytoscapeClusteringResult utils = GUIUtility.getCurrentlySelectedClusteringResultPanel()
				.getClusteringResult();

		final IShuffle permuteMethod = (IShuffle) this.permuteComboBox.getSelectedItem();
		utils.setIPermutate(permuteMethod);
	}

	public JPanel getMainPanel() {
		return this.mainPanel;
	}

	private void mergePatternsAction() throws TiconeUnloadingException, InterruptedException {
		final TiconeClusteringResultPanel resultPanel = GUIUtility.getCurrentlySelectedClusteringResultPanel();
		final IClusters patternsToMerge = resultPanel.getSelectedClusters();
		if (patternsToMerge.size() <= 1) {
			JOptionPane.showMessageDialog(null, "Select at least 2 clusters to merge.");
			return;
		}
		final MergeSelectedClustersTaskFactory mergePatternsTaskFactory = new MergeSelectedClustersTaskFactory(
				patternsToMerge.stream().mapToInt(c -> c.getClusterNumber()).toArray(), resultPanel);

		final TaskIterator taskIterator = mergePatternsTaskFactory.createTaskIterator();

		final CyNetwork network = GUIUtility.getCurrentlyVisualizedNetwork();

		if (CyNetworkUtil.isNetworkForCurrentlyVisibleClustering(network)) {
			if (CyNetworkUtil.isClusterSimilarityNetwork(network)) {
				taskIterator.append(new ClusterSimilarityNetworkTaskFactory(resultPanel).createTaskIterator());
			} else if (CyNetworkUtil.isObjectSimilarityNetwork(network)) {
				taskIterator.append(new ObjectSimilarityNetworkTaskFactory(resultPanel)
						.setOnlyObjectClusterEdges(!CyNetworkUtil.isTotalObjectSimilarityNetwork(network))
						.createTaskIterator());
			}
		}

		final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
		taskManager.execute(taskIterator);
	}

	private void mergeSimilarClustersAction() throws TiconeUnloadingException, InterruptedException {
		final TiconeClusteringResultPanel resultPanel = GUIUtility.getCurrentlySelectedClusteringResultPanel();

		try {
			Double valueOf = Double.valueOf(
					JOptionPane.showInputDialog(null, "Please specify a similarity threshold as numeric value:"));
			ISimilarityFunction similarityFunction = resultPanel.getResult().getSimilarityFunction();
			ISimilarityValue threshold;
			if (similarityFunction instanceof ISimpleSimilarityFunction)
				threshold = ((ISimpleSimilarityFunction) similarityFunction).value(valueOf, null);
			else
				// TODO
				threshold = new PearsonCorrelationFunction().value(valueOf, null);

			final MergeClustersAboveSimilarityThresholdTaskFactory mergePatternsTaskFactory = new MergeClustersAboveSimilarityThresholdTaskFactory(
					threshold, resultPanel);

			final TaskIterator taskIterator = mergePatternsTaskFactory.createTaskIterator();

			final CyNetwork network = GUIUtility.getCurrentlyVisualizedNetwork();

			if (CyNetworkUtil.isNetworkForCurrentlyVisibleClustering(network)) {
				if (CyNetworkUtil.isClusterSimilarityNetwork(network)) {
					taskIterator.append(new ClusterSimilarityNetworkTaskFactory(resultPanel).createTaskIterator());
				} else if (CyNetworkUtil.isObjectSimilarityNetwork(network)) {
					taskIterator.append(new ObjectSimilarityNetworkTaskFactory(resultPanel)
							.setOnlyObjectClusterEdges(!CyNetworkUtil.isTotalObjectSimilarityNetwork(network))
							.createTaskIterator());
				}
			}

			final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
			taskManager.execute(taskIterator);
		} catch (NumberFormatException | HeadlessException e) {
			return;
		}
	}

	private void addPatternAction()
			throws IncompatiblePrototypeComponentException, MissingPrototypeFactoryException, TiconeUnloadingException {
		final TiconeClusteringResultPanel resultPanel = GUIUtility.getCurrentlySelectedClusteringResultPanel();
		final JFrame newWindow = new JFrame("Add predefined cluster prototype");
		final AddPatternPanel addPatternPanel = new AddPatternPanel(
				resultPanel.getClusteringResult().getNumberOfTimePoints(), newWindow, resultPanel);

		newWindow.add(addPatternPanel.getPanel());
		newWindow.setLocation(200, 200);
		newWindow.pack();
		newWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		newWindow.setVisible(true);
	}

	private void showLeastFittingObjectsAction() throws TiconeUnloadingException {
		final TiconeClusteringResultPanel resultPanel = GUIUtility.getCurrentlySelectedClusteringResultPanel();
		final FilterObjectsDialog dialog = new FilterObjectsDialog(resultPanel);
		dialog.pack();
		dialog.setVisible(true);
	}

	private void showClusterSummaryAction() throws TiconeUnloadingException, InterruptedException {
		final TiconeClusteringResultPanel resultPanel = GUIUtility.getCurrentlySelectedClusteringResultPanel();
		final ClusterSummaryFrame frame = new ClusterSummaryFrame(resultPanel);
		frame.setVisible(true);
		frame.setSize(1270, 600);
	}

	private void showHistoryAction() throws InterruptedException, TiconeUnloadingException {
		final TiconeClusteringResultPanel resultPanel = GUIUtility.getCurrentlySelectedClusteringResultPanel();
		final HistoryFrame window = new HistoryFrame(resultPanel);
		window.add(window.getMainPanel());
//		window.add(new ClusterHistoryGraphPanel(resultPanel.getClusteringResult().getClusterHistory(), resultPanel));
		window.setLocation(200, 200);
		window.pack();
		window.setVisible(true);
		window.setPreferredSize(new Dimension(600, 1000));
		window.setMinimumSize(new Dimension(600, 500));
		window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private void saveClustersAction() throws TiconeUnloadingException {
		final TiconeClusteringResultPanel resultPanel = GUIUtility.getCurrentlySelectedClusteringResultPanel();
		final JFileChooser jFileChooser = new JFileChooser();
		final int choice = jFileChooser.showSaveDialog(null);
		if (choice == JFileChooser.APPROVE_OPTION) {
			try {
				this.saveClustersToFile(resultPanel.getClusteringResult().getClusteringProcess().getLatestClustering(),
						jFileChooser.getSelectedFile().getAbsolutePath());
			} catch (final IOException ioe) {
				ioe.printStackTrace();
			}
		}
		ExportToPDFUtil.createClusterSummaryPanel(resultPanel.getClusteringResult().getFeatureStore(),
				resultPanel.getClusteringResult().getClusteringProcess().getLatestClustering(),
				resultPanel.getClusteringResult());
	}

	private void saveClustersToFile(final IClusterObjectMapping patternObjectMapping, final String clustersFileName)
			throws IOException {
		final List<String> lines = new ArrayList<>();
		for (final ICluster cl : patternObjectMapping.getClusters()) {
			for (final ITimeSeriesObject obj : cl.getObjects()) {
				lines.add(String.format("%s\t%s", cl.getName(), obj.getName()));
			}
		}
		final Path file = Paths.get(clustersFileName);
		Files.write(file, lines, Charset.forName("UTF-8"));
	}

	private void setupActionChangesButtonPanel() {
		this.actionPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.CENTER;

		this.setupApplyActionsButton();
		constraints.gridx = 0;
		constraints.gridy = 0;
		this.actionPanel.add(this.applyActionsButton, constraints);

		this.setupDiscardActionsButton();
		constraints.gridx = 1;
		constraints.gridy = 0;
		this.actionPanel.add(this.discardActionsButton, constraints);

	}

	private void setupApplyActionsButton() {
		this.applyActionsButton = new JButton("Apply changes");
		this.applyActionsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				TiconeClusteringResultPanel resultPanel;
				try {
					resultPanel = GUIUtility.getCurrentlySelectedClusteringResultPanel();
				} catch (final TiconeUnloadingException e2) {
					return;
				}
				try {
					resultPanel.getClusteringProcess().applyActionsBeforeNextIteration();

					GUIUtility.hideActionPanel(resultPanel);
					resultPanel.getClusteringResult().setupPatternStatusMapping();
					GUIUtility.updateGraphPanel(resultPanel);
					// TODO
					// showCardOnPanel("iteration");
				} catch (final ClusterOperationException e1) {
					MyDialogPanel.showMessageDialog(e1.getMessage());
				} catch (InterruptedException | TiconeUnloadingException e1) {
				}
			}
		});
	}

	private void setupDiscardActionsButton() {
		this.discardActionsButton = new JButton("Discard changes");
		this.discardActionsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					final TiconeClusteringResultPanel resultPanel = GUIUtility
							.getCurrentlySelectedClusteringResultPanel();
					resultPanel.getClusteringProcess().resetActionsToApplyBeforeNextIteration();
					GUIUtility.hideActionPanel(resultPanel);
					resultPanel.getClusteringResult().setupPatternStatusMapping();
					GUIUtility.updateGraphPanel(resultPanel);
				} catch (InterruptedException | TiconeUnloadingException e1) {
				}
				// TODO
				// showCardOnPanel("iteration");
			}
		});

	}

	private void setupComputeButton() {
		this.computeButton = new JButton("Compute");
		this.computeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				try {
					final TiconeClusteringResultPanel resultPanel = GUIUtility
							.getCurrentlySelectedClusteringResultPanel();

					final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
					final ClusterObjectsTaskFactory clusterObjectsTaskFactory = new ClusterObjectsTaskFactory(
							resultPanel.getClusteringProcess());
					final TaskIterator taskIterator = clusterObjectsTaskFactory.createTaskIterator();

					final CyNetwork network = GUIUtility.getCurrentlyVisualizedNetwork();

					if (CyNetworkUtil.isNetworkForCurrentlyVisibleClustering(network)) {
						if (CyNetworkUtil.isClusterSimilarityNetwork(network)) {
							taskIterator
									.append(new ClusterSimilarityNetworkTaskFactory(resultPanel).createTaskIterator());
						} else if (CyNetworkUtil.isObjectSimilarityNetwork(network)) {
							taskIterator.append(new ObjectSimilarityNetworkTaskFactory(resultPanel)
									.setOnlyObjectClusterEdges(!CyNetworkUtil.isTotalObjectSimilarityNetwork(network))
									.createTaskIterator());
						}
					}

					taskManager.execute(taskIterator);
				} catch (final TiconeUnloadingException e) {
					return;
				} catch (final Exception e) {
					// Something went wrong while trying to setting up data, and
					// trying to run clustering.
					e.printStackTrace();
				}
			}
		});
	}

	private void setupLuckyButton() {
		this.luckyButton = new JButton("Until Convergence");
		this.luckyButton.setEnabled(false);
		this.luckyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					ClustersAllButtonsPanel.this.luckyAction();
				} catch (final TiconeUnloadingException | InterruptedException e1) {
				}
			}
		});
	}

	private void luckyAction() throws TiconeUnloadingException, InterruptedException {
		final TiconeClusteringResultPanel resultPanel = GUIUtility.getCurrentlySelectedClusteringResultPanel();
		final LuckyTaskFactory luckyTaskFactory = new LuckyTaskFactory(resultPanel);

		final TaskIterator taskIterator = luckyTaskFactory.createTaskIterator();

		final CyNetwork network = GUIUtility.getCurrentlyVisualizedNetwork();

		if (CyNetworkUtil.isNetworkForCurrentlyVisibleClustering(network)) {
			if (CyNetworkUtil.isClusterSimilarityNetwork(network)) {
				taskIterator.append(new ClusterSimilarityNetworkTaskFactory(resultPanel).createTaskIterator());
			} else if (CyNetworkUtil.isObjectSimilarityNetwork(network)) {
				taskIterator.append(new ObjectSimilarityNetworkTaskFactory(resultPanel)
						.setOnlyObjectClusterEdges(!CyNetworkUtil.isTotalObjectSimilarityNetwork(network))
						.createTaskIterator());
			}
		}

		final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
		taskManager.execute(taskIterator);
	}

	public void updateComputeButtonString(final boolean firstIteration) throws TiconeUnloadingException {
		final TiconeClusteringResultPanel resultPanel = GUIUtility.getCurrentlySelectedClusteringResultPanel();
		if (resultPanel == null)
			return;
		if (firstIteration) {
			this.computeButton.setText("Compute");
			this.luckyButton.setEnabled(false);
		} else {
			final int iterationNum = resultPanel.getClusteringResult().getClusteringProcess().getHistory()
					.getIterationNumber() + 1;
			this.computeButton.setText("Next Iteration (" + iterationNum + ")");
			this.luckyButton.setEnabled(true);
		}
	}

	public void updateClusterChartLimitType(final CHART_Y_LIMITS_TYPE limitType) {
		this.ignoreClusterChartComboBoxSelection = true;
		this.clusterChartLimitComboBox.setSelectedItem(limitType);
		this.ignoreClusterChartComboBoxSelection = false;
	}

	public void updateClusterFilter(final IFilter<ICluster> clusterFilter) {
		// simulate click if state changed
		if (clusterFilter != null && clusterFilter.isActive() && !this.filterClusters.isSelected())
			this.filterClusters.doClick();
		if ((clusterFilter == null || !clusterFilter.isActive()) && this.filterClusters.isSelected())
			this.filterClusters.doClick();
		if (clusterFilter != null && clusterFilter.isActive()) {
			this.filterClustersFeature.setSelectedItem(clusterFilter.getFeature());
			this.filterClustersInput.setText(clusterFilter.getFilterInput() + "");
			this.filterClustersOperator.setSelectedItem(clusterFilter.getFilterOperator());
		}
	}

	@Override
	public void clusteringChanged(ClusteringChangeEvent e) {
		// clustering has changed
		if (e.getClustering() instanceof TiconeClusteringResult) {
			try {
				this.updateComputeButtonString(false);
			} catch (final TiconeUnloadingException e1) {
				return;
			}
			// deactivate
			if (this.filterClusters.isSelected())
				this.filterClusters.doClick();
		}
	}

	private boolean hasPermutedFeatureValues(final IPValueCalculationResult result)
			throws IncompatibleFeatureValueProviderException {
		return result != null && result.hasPermutedFeatureValues();
	}

	@Override
	public void pvalueCalculationResultStored(PValueResultStorageEvent e) {
		try {
			if (!(e.getSource() instanceof TiconeClusteringResult))
				return;

			TiconeClusteringResult result = (TiconeClusteringResult) e.getSource();

			if (e.getIteration() < Collections.max(result.getIterations()))
				return;

			boolean enabled = false;
			final IPValueCalculationResult pvalueCalculationResult = e.getPvalueResult();
			enabled = hasPermutedFeatureValues(pvalueCalculationResult);
			clearFeatureValuesButton.setEnabled(enabled);
			visualizeFeatureDistributionButton.setEnabled(enabled);

			if (e.getPredecessor() != null)
				e.getPredecessor().removeChangeListener(this);
			if (pvalueCalculationResult != null)
				pvalueCalculationResult.addChangeListener(this);
		} catch (IncompatibleFeatureValueProviderException e1) {
		}
	}

	@Override
	public void tabSelectionChanged(TiconeTabSelectionChangedEvent event) {
		final TiconeClusteringResultPanel previouslySelectedPanel = (TiconeClusteringResultPanel) event
				.getPreviouslySelectedPanel();
		final TiconeClusteringResultPanel currentlySelectedPanel = (TiconeClusteringResultPanel) event
				.getCurrentlySelectedPanel();
		boolean enabled = false;
		try {
			enabled = currentlySelectedPanel != null
					&& hasPermutedFeatureValues(currentlySelectedPanel.getResult().getPvalueCalculationResult());
		} catch (IncompatibleFeatureValueProviderException e) {
			e.printStackTrace();
		}
		clearFeatureValuesButton.setEnabled(enabled);
		visualizeFeatureDistributionButton.setEnabled(enabled);

		if (previouslySelectedPanel != null) {
			previouslySelectedPanel.getResult().removeClusteringIterationAddedListener(this);
			previouslySelectedPanel.getResult().removeClusteringIterationDeletionListener(this);
			previouslySelectedPanel.getResult().removePValueResultStorageListener(this);
			final IPValueCalculationResult pvalueCalculationResult = previouslySelectedPanel.getResult()
					.getPvalueCalculationResult();
			if (pvalueCalculationResult != null)
				pvalueCalculationResult.removeChangeListener(this);
		}
		if (currentlySelectedPanel != null) {
			currentlySelectedPanel.getResult().addClusteringIterationAddedListener(this);
			currentlySelectedPanel.getResult().addClusteringIterationDeletionListener(this);
			currentlySelectedPanel.getResult().addPValueResultStorageListener(this);

			final IPValueCalculationResult pvalueCalculationResult = currentlySelectedPanel.getResult()
					.getPvalueCalculationResult();
			if (pvalueCalculationResult != null)
				pvalueCalculationResult.addChangeListener(this);
		}
	}

	@Override
	public void clusteringIterationDeleted(ClusteringIterationDeletedEvent e) {
		final int deletedIteration = e.getIteration();
		final ITiconeClusteringResult clusteringResult = e.getClustering();

		boolean enabled = false;

		if (deletedIteration > 1) {
			final IPValueCalculationResult lastPValueResult = clusteringResult
					.getPvalueCalculationResult(deletedIteration - 1);
			if (lastPValueResult != null) {
				lastPValueResult.addChangeListener(this);

				enabled = lastPValueResult.hasPermutedFeatureValues();
			}
		}

		clearFeatureValuesButton.setEnabled(enabled);
		visualizeFeatureDistributionButton.setEnabled(enabled);

// 		TODO: somehow we need to remove the listener from the previous pvalue result
//		final IPValueCalculationResult pvalueCalculationResult = clusteringResult.getPvalueCalculationResult();
//		boolean enabled = pvalueCalculationResult != null && pvalueCalculationResult.hasPermutedFeatureValues();
//		clearFeatureValuesButton.setEnabled(enabled);
//		visualizeFeatureDistributionButton.setEnabled(enabled);
//
//		if (pvalueCalculationResult != null)
//			pvalueCalculationResult.addChangeListener(this);
	}

	@Override
	public void clusteringIterationAdded(ClusteringIterationAddedEvent e) {
		final int newIteration = e.getIteration();
		final ITiconeClusteringResult clusteringResult = e.getClustering();

		final IPValueCalculationResult pvalueCalculationResult = clusteringResult.getPvalueCalculationResult();
		boolean enabled = pvalueCalculationResult != null && pvalueCalculationResult.hasPermutedFeatureValues();
		clearFeatureValuesButton.setEnabled(enabled);
		visualizeFeatureDistributionButton.setEnabled(enabled);

		if (newIteration > 1) {
			final IPValueCalculationResult lastPValueResult = clusteringResult
					.getPvalueCalculationResult(newIteration - 1);
			if (lastPValueResult != null)
				lastPValueResult.removeChangeListener(this);
		}
		if (pvalueCalculationResult != null)
			pvalueCalculationResult.addChangeListener(this);

	}

	@Override
	public void resultChanged(TiconeResultChangeEvent e) {
		if (e.getSource() instanceof IPValueCalculationResult) {
			IPValueCalculationResult result = (IPValueCalculationResult) e.getSource();
			boolean enabled = false;
			try {
				enabled = hasPermutedFeatureValues(result);
			} catch (IncompatibleFeatureValueProviderException e1) {
			}
			clearFeatureValuesButton.setEnabled(enabled);
			visualizeFeatureDistributionButton.setEnabled(enabled);
		}

	}

	{
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
		$$$setupUI$$$();
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
	 * edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		mainPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(scrollPane,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null,
						0, false));
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(7, 1, new Insets(10, 10, 10, 10), -1, -1));
		scrollPane.setViewportView(panel1);
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1));
		panel1.add(panel2,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		panel2.setBorder(BorderFactory.createTitledBorder("Perform Iterations"));
		final JLabel label1 = new JLabel();
		label1.setText("Perform single iteration:");
		panel2.add(label1,
				new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(207, 16), null, 0, false));
		final JLabel label2 = new JLabel();
		label2.setText("Perform iterations until convergence:");
		panel2.add(label2,
				new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(207, 16), null, 0, false));
		computeButton.setText("Compute");
		panel2.add(computeButton,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		luckyButton.setText("Until Convergence");
		panel2.add(luckyButton,
				new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final Spacer spacer1 = new Spacer();
		panel2.add(spacer1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		final Spacer spacer2 = new Spacer();
		panel2.add(spacer2, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		modifyClusteringPanel.setLayout(new GridLayoutManager(7, 2, new Insets(0, 0, 0, 0), -1, -1));
		panel1.add(modifyClusteringPanel,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JLabel label3 = new JLabel();
		label3.setText("Add a cluster by prototype:");
		modifyClusteringPanel.add(label3,
				new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(211, 16), null, 0, false));
		final JLabel label4 = new JLabel();
		label4.setText("Merge clusters:");
		modifyClusteringPanel.add(label4,
				new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(211, 16), null, 0, false));
		final JLabel label5 = new JLabel();
		label5.setText("Filter objects (e.g. by variance):");
		modifyClusteringPanel.add(label5,
				new GridConstraints(5, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(211, 16), null, 0, false));
		addPredefinedPatternsButton.setText("Add a cluster prototype ...");
		modifyClusteringPanel.add(addPredefinedPatternsButton,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		mergePatternsButton.setText("Merge selected clusters");
		modifyClusteringPanel.add(mergePatternsButton,
				new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		showLestFittingObjectsButton.setText("Filter objects ...");
		modifyClusteringPanel.add(showLestFittingObjectsButton,
				new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final Spacer spacer3 = new Spacer();
		modifyClusteringPanel.add(spacer3, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		final Spacer spacer4 = new Spacer();
		modifyClusteringPanel.add(spacer4, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		final Spacer spacer5 = new Spacer();
		modifyClusteringPanel.add(spacer5, new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		mergeSimilarClustersButton.setText("Merge similar clusters ...");
		modifyClusteringPanel.add(mergeSimilarClustersButton,
				new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		visHistoryExportPanel.setLayout(new GridLayoutManager(8, 2, new Insets(0, 0, 0, 0), -1, -1));
		panel1.add(visHistoryExportPanel,
				new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JLabel label6 = new JLabel();
		label6.setText("Show a history of all performed iterations:");
		visHistoryExportPanel.add(label6,
				new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label7 = new JLabel();
		label7.setText("Export clusters of this iteration to file:");
		visHistoryExportPanel.add(label7,
				new GridConstraints(4, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label8 = new JLabel();
		label8.setText("Show a summary of the clusters:");
		visHistoryExportPanel.add(label8,
				new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		showClusterSummaryButton.setText("Show summary");
		visHistoryExportPanel.add(showClusterSummaryButton,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		showHistoryButton.setText("Show history");
		visHistoryExportPanel.add(showHistoryButton,
				new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		exportClustersButton.setText("Export clusters as TSV");
		visHistoryExportPanel.add(exportClustersButton,
				new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final Spacer spacer6 = new Spacer();
		visHistoryExportPanel.add(spacer6, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		final Spacer spacer7 = new Spacer();
		visHistoryExportPanel.add(spacer7, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		final Spacer spacer8 = new Spacer();
		visHistoryExportPanel.add(spacer8, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		final JLabel label9 = new JLabel();
		label9.setText("Y axis limits of cluster charts:");
		visHistoryExportPanel.add(label9,
				new GridConstraints(6, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final Spacer spacer9 = new Spacer();
		visHistoryExportPanel.add(spacer9, new GridConstraints(7, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		visHistoryExportPanel.add(clusterChartLimitComboBox,
				new GridConstraints(7, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		permutationPanel.setLayout(new GridLayoutManager(12, 2, new Insets(0, 0, 0, 0), -1, -1));
		panel1.add(permutationPanel,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JLabel label10 = new JLabel();
		label10.setText("Permutations:");
		permutationPanel.add(label10,
				new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label11 = new JLabel();
		label11.setText("Combine p-values:");
		label11.setVisible(false);
		permutationPanel.add(label11,
				new GridConstraints(8, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		combinePvaluesComboBox.setVisible(false);
		permutationPanel.add(combinePvaluesComboBox,
				new GridConstraints(9, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		calculatePValuesButton.setText("Calculate P-values");
		permutationPanel.add(calculatePValuesButton,
				new GridConstraints(10, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		permutationPanel.add(permuteComboBox,
				new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		permutationPanel.add(clusterPValueFeatures,
				new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null,
						new Dimension(150, 50), null, 0, false));
		final JLabel label12 = new JLabel();
		label12.setText("Features for cluster fitness:");
		permutationPanel.add(label12,
				new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		permutationPanel.add(clusterPValueConditionalFeatures,
				new GridConstraints(4, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null,
						new Dimension(150, 50), null, 0, false));
		final JLabel label13 = new JLabel();
		label13.setText("Conditional features:");
		permutationPanel.add(label13,
				new GridConstraints(3, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		storeFeatureDistributionsCheckBox = new JCheckBox();
		storeFeatureDistributionsCheckBox.setText("Store feature distributions");
		storeFeatureDistributionsCheckBox.setToolTipText(
				"Whether feature values of random clusters should be stored in memory. This allows to inspect their distributions, but can require a large amount of memory.");
		permutationPanel.add(storeFeatureDistributionsCheckBox,
				new GridConstraints(7, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		numberOfIterationsRefinementStepsTextField = new JTextField();
		numberOfIterationsRefinementStepsTextField.setText("1000");
		permutationPanel.add(numberOfIterationsRefinementStepsTextField,
				new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		clearFeatureValuesButton.setText("Clear");
		permutationPanel.add(clearFeatureValuesButton,
				new GridConstraints(11, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		visualizeFeatureDistributionButton.setText("Visualize");
		permutationPanel.add(visualizeFeatureDistributionButton,
				new GridConstraints(11, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		permutationPanel.add(permutationSeedTextField,
				new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		final JLabel label14 = new JLabel();
		label14.setText("Seed:");
		permutationPanel.add(label14,
				new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		filterClustersPanel.setLayout(new GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1));
		panel1.add(filterClustersPanel,
				new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		filterClusters.setText("Show only clusters fulfilling this condition:");
		filterClustersPanel.add(filterClusters,
				new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		filterApplyBtn.setText("Apply Filter");
		filterClustersPanel.add(filterApplyBtn,
				new GridConstraints(3, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		filterClustersPanel.add(filterClustersFeature,
				new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		filterClustersPanel.add(filterClustersOperator,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(97, 25), null, 0, false));
		filterClustersPanel.add(filterClustersInput,
				new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(150, -1), null, 0, false));
		cleanUpClusteringPanel.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		panel1.add(cleanUpClusteringPanel,
				new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		deleteAllIterationsExceptButton.setText("Delete all iterations except the last");
		cleanUpClusteringPanel.add(deleteAllIterationsExceptButton,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final Spacer spacer10 = new Spacer();
		cleanUpClusteringPanel.add(spacer10, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		final Spacer spacer11 = new Spacer();
		panel1.add(spacer11, new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return mainPanel;
	}

	private void updatePermutationsRandomSeed() {
		this.permutationSeedTextField.setText(Long.toString(new Random(System.currentTimeMillis()).nextLong()));
	}

}
