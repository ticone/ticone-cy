package dk.sdu.imada.ticone.gui;

/**
 * Created by christian on 9/9/15.
 */
public class TiconeConnectivityResultsPanel extends TiconeTabbedResultsPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7516912791858409184L;

	@Override
	public String getTitle() {
		return "Connectivity";
	}
}
