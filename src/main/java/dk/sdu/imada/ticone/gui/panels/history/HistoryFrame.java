package dk.sdu.imada.ticone.gui.panels.history;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.validity.DunnIndex;
import dk.sdu.imada.ticone.feature.ClusteringFeatureNumberClusters;
import dk.sdu.imada.ticone.feature.ClusteringFeatureValidity;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.panels.valuesample.FeatureValueHistoryBoxplotPanel;
import dk.sdu.imada.ticone.gui.panels.valuesample.FeatureValueHistoryScatterChartPanel;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.util.IClusterHistory;
import dk.sdu.imada.ticone.util.ITiconeResultDestroyedListener;
import dk.sdu.imada.ticone.util.ITiconeResultNameChangeListener;
import dk.sdu.imada.ticone.util.TiconeResultDestroyedEvent;
import dk.sdu.imada.ticone.util.TiconeResultNameChangedEvent;

public class HistoryFrame extends JFrame
		implements ITiconeResultDestroyedListener, WindowListener, ITiconeResultNameChangeListener {
	/**
	 *
	 */
	private static final long serialVersionUID = 5611434590155605313L;
	private final TiconeClusteringResultPanel resultPanel;
	private JTabbedPane tabbedPane1;
	private JPanel mainPanel;
	private JPanel clusterHistoryGraphPanel;
	private JPanel statisticsHistoryGraphPanel;

	public HistoryFrame(TiconeClusteringResultPanel resultPanel) {
		super(String.format("History %s", resultPanel.getResult().getName()));
		this.resultPanel = resultPanel;
		this.resultPanel.getResult().addOnDestroyListener(this);
		this.resultPanel.getResult().addNameChangeListener(this);
		this.addWindowListener(this);
		this.$$$setupUI$$$();
	}

	private void createUIComponents() {
		final List<IFeature<? extends Number>> clusteringFeatures = Arrays
				.asList(new IFeature[] { new ClusteringFeatureNumberClusters()
//				, new ClusteringFeatureAverageObjectClusterSimilarity(resultPanel.getResult().getSimilarityFunction())
				});
		final Map<Integer, ClusterObjectMapping> iterationObjects = new HashMap<>();
		final Map<Integer, IFeatureStore> iterationFeatureStores = new HashMap<>();
		IClusterHistory<ClusterObjectMapping> h = this.resultPanel.getResult().getClusterHistory();
		while (h != null) {
			final int iterationNumber = h.getIterationNumber();
			iterationObjects.put(iterationNumber, h.getClusterObjectMapping());
			iterationFeatureStores.put(iterationNumber, resultPanel.getResult().getFeatureStore(iterationNumber));
			h = h.getParent();
		}

		this.statisticsHistoryGraphPanel = new JPanel(new GridLayout(0, 1, 0, 0));
		for (IFeature<? extends Number> f : clusteringFeatures)
			this.statisticsHistoryGraphPanel
					.add(new FeatureValueHistoryScatterChartPanel<>(ObjectType.CLUSTERING, f, resultPanel.getResult()));
//		this.generalHistoryGraphPanel.add(new FeatureValueHistoryBoxplotPanel<>(ICluster.class,
////				new ClusterFeatureAverageSimilarity(resultPanel.getResult().getSimilarityFunction()),
//				new ClusterFeatureMedianSimilarity(resultPanel.getResult().getSimilarityFunction()),
//				iterationFeatureStores).getChartPanel());
//		this.statisticsHistoryGraphPanel.add(new FeatureValueHistoryBoxplotPanel<>(IClusterObjectMapping.class,
//				new ClusteringFeatureValidity(new SilhouetteValue(), resultPanel.getResult().getSimilarityFunction()),
//				this.resultPanel.getResult()));
		this.statisticsHistoryGraphPanel.add(new FeatureValueHistoryBoxplotPanel<>(ObjectType.CLUSTERING,
				new ClusteringFeatureValidity(new DunnIndex(),
						(ISimilarityFunction) resultPanel.getResult().getSimilarityFunction()),
				this.resultPanel.getResult()));
		try {
			this.clusterHistoryGraphPanel = new ClusterHistoryGraphPanel(resultPanel);
		} catch (InterruptedException e) {
		} catch (IncompatibleSimilarityFunctionException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the mainPanel
	 */
	public JPanel getMainPanel() {
		return this.mainPanel;
	}

	@Override
	public void ticoneResultDestroyed(TiconeResultDestroyedEvent event) {
		EventQueue.invokeLater(() -> this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING)));
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		resultPanel.getResult().removeNewFeatureStoreListener((ClusterHistoryGraphPanel) this.clusterHistoryGraphPanel);
		resultPanel.getResult()
				.removeClusteringIterationAddedListener((ClusterHistoryGraphPanel) this.clusterHistoryGraphPanel);
		resultPanel.getResult()
				.removeClusteringIterationDeletionListener((ClusterHistoryGraphPanel) this.clusterHistoryGraphPanel);
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void resultNameChanged(TiconeResultNameChangedEvent event) {
		this.setTitle(String.format("History %s", event.getNewName()));
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
	 * edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		tabbedPane1 = new JTabbedPane();
		mainPanel.add(tabbedPane1,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null,
						new Dimension(200, 200), null, 0, false));
		tabbedPane1.addTab("Clusters", clusterHistoryGraphPanel);
		tabbedPane1.addTab("Statistics", statisticsHistoryGraphPanel);
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return mainPanel;
	}
}
