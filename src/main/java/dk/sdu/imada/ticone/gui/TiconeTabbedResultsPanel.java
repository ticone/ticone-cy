package dk.sdu.imada.ticone.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;

import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;

import dk.sdu.imada.ticone.gui.util.ITiconeTabSelectionChangedListener;
import dk.sdu.imada.ticone.gui.util.TiconeTabSelectionChangedEvent;
import dk.sdu.imada.ticone.util.ITiconeResultDestroyedListener;
import dk.sdu.imada.ticone.util.TiconeResultDestroyedEvent;

public abstract class TiconeTabbedResultsPanel extends HidableTabbedPane
		implements CytoPanelComponent, ITiconeResultDestroyedListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6623863071699002192L;

	private transient Set<ITiconeTabSelectionChangedListener> tabSelectionChangedListener;
	/**
	 * We keep this counter to be able to distinguish whether we removed or added or
	 * selected a tab in ChangeListener.stateChanged().
	 */
	protected int lastTabCount;

	public TiconeTabbedResultsPanel() {
		super();
		this.tabSelectionChangedListener = new HashSet<>();
		this.setPreferredSize(new Dimension(600, 100));
		this.lastTabCount = 0;
	}

	@Override
	protected JTabbedPane createTabbedPane() {
		return new MyTabbedPane();
	}

	public boolean addTabSelectionChangedListener(final ITiconeTabSelectionChangedListener listener) {
		return this.tabSelectionChangedListener.add(listener);
	}

	public boolean removeTabSelectionChangedListener(final ITiconeTabSelectionChangedListener listener) {
		return this.tabSelectionChangedListener.remove(listener);
	}

	protected void fireTabSelectionChanged(final TiconeTabSelectionChangedEvent event) {
		this.tabSelectionChangedListener.forEach(l -> l.tabSelectionChanged(event));
	}

	@Override
	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.EAST;
	}

	@Override
	public Icon getIcon() {
		BufferedImage image;
		try {
			image = ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("ticone_logo_square.png"));
			return new ImageIcon(image);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Component getComponent() {
		return this;
	}

	public int getLastTabCount() {
		return this.lastTabCount;
	}

	@Override
	public TiconeResultPanel<?> getSelectedComponent() {
		return (TiconeResultPanel<?>) super.getSelectedComponent();
	}

	@Override
	public void addTab(String title, Component component) {
		super.addTab(title, component);
		if (component instanceof TiconeResultPanel)
			((TiconeResultPanel) component).getResult().addOnDestroyListener(this);
		if (this.getTabCount() == 1)
			fireTabSelectionChanged(new TiconeTabSelectionChangedEvent(this, null, (TiconeResultPanel<?>) component));
	}

	@Override
	public void removeTabAt(int index) {
		final Component component = this.getComponentAt(index);
		super.removeTabAt(index);
		if (component instanceof TiconeResultPanel)
			((TiconeResultPanel) component).getResult().removeOnDestroyListener(this);
		if (this.getTabCount() == 0)
			fireTabSelectionChanged(new TiconeTabSelectionChangedEvent(this, (TiconeResultPanel<?>) component, null));
	}

	@Override
	public void ticoneResultDestroyed(TiconeResultDestroyedEvent event) {
		for (int tab = this.getTabCount() - 1; tab >= 0; tab--) {
			TiconeResultPanel componentAt = (TiconeResultPanel) this.getComponentAt(tab);
			if (componentAt.getResult().equals(event.getResult()))
				this.removeTabAt(tab);
		}
	}

	class ButtonTabComponent extends HidableTabbedPane.ButtonTabComponent {

		/**
		 * 
		 */
		private static final long serialVersionUID = -313502648884963341L;

		/**
		 * @param isRemoveAll
		 */
		public ButtonTabComponent(boolean isRemoveAll) {
			super(isRemoveAll);
		}

		@Override
		protected TabButton createTabButton() {
			return new TabButton();
		}

		class TabButton extends dk.sdu.imada.ticone.gui.HidableTabbedPane.ButtonTabComponent.TabButton {

			/**
			 * 
			 */
			private static final long serialVersionUID = 5979841022386394144L;

			@Override
			protected void doRemoveTabAt(int i) {
				final Component c = TiconeTabbedResultsPanel.this.getComponentAt(i);
				if (c instanceof TiconeResultPanel)
					((TiconeResultPanel) c).getResult().destroy();
			}
		}
	}

	class MyTabbedPane extends JTabbedPane {

		/**
		 * 
		 */
		private static final long serialVersionUID = 340982685626629615L;

		private TiconeResultPanel<?> lastSelected;

		public MyTabbedPane() {
			super();
		}

		public MyTabbedPane(int tabPlacement, int tabLayoutPolicy) {
			super(tabPlacement, tabLayoutPolicy);
		}

		public MyTabbedPane(int tabPlacement) {
			super(tabPlacement);
		}

		@Override
		protected void fireStateChanged() {
			final TiconeResultPanel<?> selectedComponent = TiconeTabbedResultsPanel.this.getSelectedComponent();
			final TiconeTabSelectionChangedEvent event = new TiconeTabSelectionChangedEvent(
					TiconeTabbedResultsPanel.this, lastSelected, selectedComponent);
			super.fireStateChanged();
			this.lastSelected = selectedComponent;
			fireTabSelectionChanged(event);
		}
	}

	@Override
	protected ButtonTabComponent createTabComponent(boolean isRemoveAll) {
		return new ButtonTabComponent(isRemoveAll);
	}

	private void readObject(ObjectInputStream in) throws ClassNotFoundException, IOException {
		in.defaultReadObject();
	}
}
