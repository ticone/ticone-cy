package dk.sdu.imada.ticone.gui.panels.clusterchart;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jfree.chart.ChartPanel;

import com.itextpdf.text.Font;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeaturesProxy;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;

public class ClusterChartWithTitle implements IObjectWithFeaturesProxy {
	protected ClusterChartContainer chartPanelContainer;

	protected Dimension lastDimensions;
	protected JPanel lastPanel;

	public ClusterChartWithTitle(final ClusterChartContainer chartPanelContainer) {
		super();
		this.chartPanelContainer = chartPanelContainer;
	}

	public JPanel getChartPanel(final int width, final int height)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException {
		final Dimension dims = new Dimension(width, height);
		if (dims.equals(this.lastDimensions))
			return this.lastPanel;

		final JLabel label = new JLabel(this.getCluster().getName());
		final int labelHeight = 15;
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setPreferredSize(new Dimension(width, labelHeight));
		label.setMaximumSize(new Dimension(width, labelHeight));
		label.setOpaque(false);
		label.setFont(label.getFont().deriveFont(Font.BOLD));

		final JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(label, BorderLayout.NORTH);
		if (this.chartPanelContainer != null) {
			final ChartPanel chartPanel = this.chartPanelContainer.getChartPanel(width, height - labelHeight);
			mainPanel.add(chartPanel, BorderLayout.CENTER);
		}
		this.lastPanel = mainPanel;
		this.lastDimensions = dims;
		return mainPanel;
	}

	public ICluster getCluster() {
		return this.chartPanelContainer.getCluster();
	}

	public ClusterChartContainer getChartPanelContainer() {
		return this.chartPanelContainer;
	}

	@Override
	public String toString() {
		return String.format("%s\t%d", this.getCluster().getName(), this.getCluster().getObjects().size());
	}

	@Override
	public IObjectWithFeatures getObjectWithFeatures() {
		return this.chartPanelContainer.getCluster();
	}
}