package dk.sdu.imada.ticone.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cytoscape.application.swing.CySwingApplication;
import org.cytoscape.application.swing.CytoPanel;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.application.swing.events.CytoPanelComponentSelectedEvent;
import org.cytoscape.application.swing.events.CytoPanelComponentSelectedListener;

import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.panels.clustertable.ClustersAllButtonsPanel;
import dk.sdu.imada.ticone.gui.panels.comparison.ClusteringComparisonFormPanel;
import dk.sdu.imada.ticone.gui.panels.comparison.ClusteringComparisonOperationsFormPanel;
import dk.sdu.imada.ticone.gui.panels.connectivity.ClusterConnectivityFormPanel;
import dk.sdu.imada.ticone.gui.panels.kpm.KPMFormPanel;
import dk.sdu.imada.ticone.gui.panels.main.DataFormPanel;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.GUIUtility.TICONE_RESULTS_TAB;
import dk.sdu.imada.ticone.util.ITiconeResultChangeListener;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeResultChangeEvent;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * Created by christian on 9/9/15.
 */
public class TiconePanel extends JPanel implements CytoPanelComponent, ChangeListener, ITiconeResultChangeListener,
		ContainerListener, CytoPanelComponentSelectedListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4401558407138215239L;
	private JTabbedPane jTabbedPane;
	private ClusterConnectivityFormPanel clusterConnectivityFormPanel;
	private ClusteringComparisonFormPanel clusteringComparisonFormPanel;
	private ClusteringComparisonOperationsFormPanel clusteringComparisonOperationsFormsPanel;
	private DataFormPanel dataFormPanel;
	private JScrollPane dataPanelJScrollPane;
	private KPMFormPanel kpmFormPanel;
	private ClustersAllButtonsPanel clustersTabPanel;

	public enum TICONE_PANEL_CLUSTERINGS_TAB_INDEX {
		START, CLUSTERS, NETWORK_ENRICHMENT
	}

	public enum TICONE_PANEL_COMPARISON_TAB_INDEX {
		COMPARISON, OPERATIONS, NETWORK_ENRICHMENT
	}

	public enum TICONE_PANEL_CONNECTIVITY_TAB_INDEX {
		CONNECTIVITY
	}

	public TiconePanel() throws TiconeUnloadingException, InterruptedException {
		super();
		this.setLayout(new BorderLayout());
		this.initComponents();
		GUIUtility.setTiconePanel(this);
	}

	private void initComponents() throws TiconeUnloadingException, InterruptedException {

		// Top panel
		final JPanel topPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.gridx = 0;
		constraints.gridy = 0;
		final JLabel bannerLabel = new JLabel();
		try {
			final BufferedImage image = ImageIO
					.read(this.getClass().getClassLoader().getResourceAsStream("ticone.png"));
			bannerLabel.setIcon(new ImageIcon(image));
			topPanel.add(bannerLabel, constraints);
		} catch (final IOException ioe) {
			// Banner not found
			ioe.printStackTrace();
		}
		constraints.gridx = 0;
		constraints.gridy = 1;
		this.add(topPanel, BorderLayout.NORTH);

		// Center panel
		final JPanel centerPanel = this.setupCenterPanel();
		this.add(centerPanel, BorderLayout.CENTER);
	}

	public DataFormPanel getDataFormPanel() {
		return this.dataFormPanel;
	}

	public ClustersAllButtonsPanel getClustersTabPanel() {
		return this.clustersTabPanel;
	}

	private JPanel setupCenterPanel() throws TiconeUnloadingException, InterruptedException {
		final JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());

		this.jTabbedPane = this.setUpTabbedPane();
		centerPanel.add(this.jTabbedPane);
		// updateGraphTab(false);

		return centerPanel;
	}

	private JTabbedPane setUpTabbedPane() throws TiconeUnloadingException, InterruptedException {

		final JTabbedPane jTabbedPane = new JTabbedPane();
		jTabbedPane.setPreferredSize(new Dimension(950, 700));

		this.dataFormPanel = new DataFormPanel();
		final JPanel dataPanel = this.dataFormPanel.getMainPanel();
		this.dataPanelJScrollPane = new JScrollPane(dataPanel);
		this.dataPanelJScrollPane.getVerticalScrollBar().setUnitIncrement(32);
		jTabbedPane.addTab("New Clustering", this.dataPanelJScrollPane);

		this.clustersTabPanel = new ClustersAllButtonsPanel();
		jTabbedPane.addTab("Cluster Operations", this.clustersTabPanel.getMainPanel());

		this.kpmFormPanel = new KPMFormPanel();
		jTabbedPane.addTab("Network Operations", this.kpmFormPanel.getMainPanel());

		this.clusterConnectivityFormPanel = new ClusterConnectivityFormPanel();

		this.clusteringComparisonFormPanel = new ClusteringComparisonFormPanel();

		this.clusteringComparisonOperationsFormsPanel = new ClusteringComparisonOperationsFormPanel();

		return jTabbedPane;
	}

	public void setGraphTabInFocus() {
		this.jTabbedPane.setSelectedIndex(1);
	}

	public void updateGraphTab(final boolean enable) {
		for (int i = 0; i < this.jTabbedPane.getTabCount(); i++) {
			if (this.jTabbedPane.getTitleAt(i).equals("Cluster Operations")) {
				this.jTabbedPane.setEnabledAt(i, enable);
			} else if (this.jTabbedPane.getTitleAt(i).equals("Network Operations")) {
				this.jTabbedPane.setEnabledAt(i, enable);
			}
			if (!enable && this.jTabbedPane.getTitleAt(i).equals("New Clustering")) {
				this.jTabbedPane.setSelectedIndex(i);
			}
		}
	}

	@Override
	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.WEST;
	}

	@Override
	public Icon getIcon() {
		BufferedImage image;
		try {
			image = ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("ticone_logo_square.png"));
			return new ImageIcon(image);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getTitle() {
		return "TiCoNE";
	}

	@Override
	public Component getComponent() {
		return this;
	}

	public ClusterConnectivityFormPanel getClusterConnectivityFormPanel() {
		return this.clusterConnectivityFormPanel;
	}

	public ClusteringComparisonFormPanel getDifferentialityFormPanel() {
		return this.clusteringComparisonFormPanel;
	}

	public KPMFormPanel getKpmFormPanel() {
		return this.kpmFormPanel;
	}

	@Override
	// we react to a results tab that was selected
	public void resultChanged(TiconeResultChangeEvent changeEvent) {
		try {
			this.updateTiconeTabs();
		} catch (final TiconeUnloadingException e1) {
		}
	}

	protected void updateTiconeTabs() throws TiconeUnloadingException {
		if (GUIUtility.getCurrentlySelectedResultsTab() == null
				|| GUIUtility.getCurrentlySelectedResultsTab().equals(TICONE_RESULTS_TAB.CLUSTERINGS)) {
			final TiconeClusteringResultsPanel resultPanel = GUIUtility.getTiconeClusteringsResultsPanel();
			// check it wasn't a tab add/removal event
			final int count = resultPanel.getTabCount();
			if (count == resultPanel.lastTabCount) {
				if (count > 0) {
					GUIUtility.updateComputeButtonString(false);
					final TiconeClusteringResultPanel resPan = (TiconeClusteringResultPanel) resultPanel
							.getSelectedComponent();
					GUIUtility.updateClusterChartLimitType(resPan.getClusteringResult().getChartLimits());
					GUIUtility.updateClusterFilter(resPan.getClusteringResult().getClusterFilter());
				}
			}
			// tab added or removed;
			resultPanel.lastTabCount = count;

			// if we have a clustering
			if (count > 0) {
				this.clusteringComparisonFormPanel.updateClusteringJList(resultPanel);
				this.clusterConnectivityFormPanel.updateClusteringJList(resultPanel);
				this.jTabbedPane.setEnabledAt(TICONE_PANEL_CLUSTERINGS_TAB_INDEX.CLUSTERS.ordinal(), true);
				this.jTabbedPane.setEnabledAt(TICONE_PANEL_CLUSTERINGS_TAB_INDEX.NETWORK_ENRICHMENT.ordinal(), true);
			}
			// if we have no clustering left, we disable tabs
			else {
				this.jTabbedPane.setEnabledAt(TICONE_PANEL_CLUSTERINGS_TAB_INDEX.CLUSTERS.ordinal(), false);
				this.jTabbedPane.setEnabledAt(TICONE_PANEL_CLUSTERINGS_TAB_INDEX.NETWORK_ENRICHMENT.ordinal(), false);
				this.jTabbedPane.setSelectedIndex(0);
			}
		}
		// comparisons
		else if (GUIUtility.getCurrentlySelectedResultsTab().equals(TICONE_RESULTS_TAB.COMPARISONS)) {
			final TiconeComparisonResultsPanel resultPanel = GUIUtility.getTiconeComparisonResultsPanel();
			// check it wasn't a tab add/removal event
			final int count = resultPanel.getTabCount();

			// if we have a comparison
			if (count > 0) {
				this.jTabbedPane.setEnabledAt(TICONE_PANEL_COMPARISON_TAB_INDEX.OPERATIONS.ordinal(), true);
				this.jTabbedPane.setEnabledAt(TICONE_PANEL_COMPARISON_TAB_INDEX.NETWORK_ENRICHMENT.ordinal(), true);
			}
			// if we have no clustering left, we disable tabs
			else {
				this.jTabbedPane.setEnabledAt(TICONE_PANEL_COMPARISON_TAB_INDEX.OPERATIONS.ordinal(), false);
				this.jTabbedPane.setEnabledAt(TICONE_PANEL_COMPARISON_TAB_INDEX.NETWORK_ENRICHMENT.ordinal(), false);
				this.jTabbedPane.setSelectedIndex(0);
			}
		}
		// connectivity
		else if (GUIUtility.getCurrentlySelectedResultsTab().equals(TICONE_RESULTS_TAB.CONNECTIVITY)) {
		}
	}

	@Override
	// a tab was added
	public void componentAdded(final ContainerEvent e) {

	}

	@Override
	// a tab was removed
	public void componentRemoved(final ContainerEvent e) {
	}

	@Override
	public void handleEvent(final CytoPanelComponentSelectedEvent e) {
		try {
			final CytoPanel cytoPanel = ServiceHelper.getService(CySwingApplication.class)
					.getCytoPanel(CytoPanelName.EAST);
			if (e.getCytoPanel().equals(cytoPanel)) {
				// clusterings
				while (this.jTabbedPane.getTabCount() > 0)
					this.jTabbedPane.removeTabAt(0);
				if (GUIUtility.getCurrentlySelectedResultsTab() == null
						|| GUIUtility.getCurrentlySelectedResultsTab().equals(TICONE_RESULTS_TAB.CLUSTERINGS)) {
					this.jTabbedPane.addTab("New Clustering", this.dataPanelJScrollPane);

					this.jTabbedPane.addTab("Cluster Operations", this.clustersTabPanel.getMainPanel());

					this.jTabbedPane.addTab("Network Operations", this.kpmFormPanel.getMainPanel());

					GUIUtility.setVisualizeButtonEnabled(true);
				}
				// comparisons
				else if (GUIUtility.getCurrentlySelectedResultsTab().equals(TICONE_RESULTS_TAB.COMPARISONS)) {
					this.jTabbedPane.addTab("New Comparison", this.clusteringComparisonFormPanel.getMainPanel());
					this.jTabbedPane.addTab("Comparison Operations",
							this.clusteringComparisonOperationsFormsPanel.getMainPanel());

					this.kpmFormPanel = new KPMFormPanel();
					this.jTabbedPane.addTab("Network Operations", this.kpmFormPanel.getMainPanel());
					GUIUtility.setVisualizeButtonEnabled(true);
				}
				// connectivity
				else if (GUIUtility.getCurrentlySelectedResultsTab().equals(TICONE_RESULTS_TAB.CONNECTIVITY)) {
					this.jTabbedPane.addTab("New Cluster Connectivity",
							this.clusterConnectivityFormPanel.getMainPanel());
					GUIUtility.setVisualizeButtonEnabled(false);
				}
				this.updateTiconeTabs();
			}
		} catch (final TiconeUnloadingException e1) {
		}
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		try {
			this.updateTiconeTabs();
		} catch (final TiconeUnloadingException e1) {
		}

	}
}
