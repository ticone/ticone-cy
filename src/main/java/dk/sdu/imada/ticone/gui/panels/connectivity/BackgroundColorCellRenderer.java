package dk.sdu.imada.ticone.gui.panels.connectivity;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;

import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;

public class BackgroundColorCellRenderer extends DefaultTableCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6480032717539757615L;

	private static final DecimalFormat formatter = new DecimalFormat("#0.000");

	@Override
	public Component getTableCellRendererComponent(final JTable table, Object value, final boolean isSelected,
			final boolean hasFocus, final int row, final int column) {
		if (value instanceof Double) {
			value = formatter.format(value);
		}

		final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		c.setBackground(row % 2 == 0 ? Color.WHITE
				: UIManager.getLookAndFeelDefaults().contains("Table.alternateRowColor")
						? (Color) UIManager.getLookAndFeelDefaults().get("Table.alternateRowColor")
						: new Color(240, 240, 240));
		return c;
	}
}