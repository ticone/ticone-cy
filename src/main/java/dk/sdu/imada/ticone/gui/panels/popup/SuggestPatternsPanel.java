package dk.sdu.imada.ticone.gui.panels.popup;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskManager;

import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.tasks.suggest.SuggestNewPatternsTaskFactory;
import dk.sdu.imada.ticone.util.ServiceHelper;

/**
 * Created by christian on 8/25/15.
 */
public class SuggestPatternsPanel {

	private final IClusterObjectMapping patternObjectMapping;
	private final JTextField percentLeastFittingTextField;
	private final JTextField numberOfPatternsTextField;
	private final JButton suggestNewPatternsButton;
	private final JFrame window;
	private final JPanel panel;
	protected TiconeClusteringResultPanel resultPanel;

	public SuggestPatternsPanel(final JFrame window, final TiconeClusteringResultPanel resultPanel) {
		super();

		this.resultPanel = resultPanel;
		this.patternObjectMapping = resultPanel.getClusteringResult().getClusteringProcess().getLatestClustering();
		this.window = window;

		this.panel = new JPanel(new GridBagLayout());

		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;

		final JLabel leastfittingLabel = new JLabel("Percent least fitting to recluster:");
		constraints.gridx = 0;
		constraints.gridy = 0;
		this.panel.add(leastfittingLabel, constraints);

		this.percentLeastFittingTextField = new JTextField("10");
		this.percentLeastFittingTextField.setPreferredSize(new Dimension(40, 28));
		constraints.gridx = 0;
		constraints.gridy = 1;
		this.panel.add(this.percentLeastFittingTextField, constraints);

		final JLabel numberOfPatternsLabel = new JLabel("Number of clusters wanted");
		constraints.gridx = 0;
		constraints.gridy = 2;
		this.panel.add(numberOfPatternsLabel, constraints);

		this.numberOfPatternsTextField = new JTextField();
		this.numberOfPatternsTextField.setPreferredSize(new Dimension(40, 28));
		constraints.gridx = 0;
		constraints.gridy = 3;
		this.panel.add(this.numberOfPatternsTextField, constraints);

		this.suggestNewPatternsButton = new JButton("Suggest new clusters");
		this.suggestNewPatternsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				SuggestPatternsPanel.this.suggestPatternsAction();
			}
		});
		constraints.gridx = 0;
		constraints.gridy = 4;
		this.panel.add(this.suggestNewPatternsButton, constraints);

	}

	private void suggestPatternsAction() {
		final int percentLeastFitting = Integer.parseInt(this.percentLeastFittingTextField.getText());
		final int numberOfPatterns = Integer.parseInt(this.numberOfPatternsTextField.getText());
		final SuggestNewPatternsTaskFactory suggestNewPatternsTaskFactory = new SuggestNewPatternsTaskFactory(
				numberOfPatterns, percentLeastFitting, this.resultPanel);
		final TaskIterator taskIterator = suggestNewPatternsTaskFactory.createTaskIterator();
		ServiceHelper.getService(TaskManager.class).execute(taskIterator);
		this.window.dispose();
	}

	public JPanel getPanel() {
		return this.panel;
	}
}
