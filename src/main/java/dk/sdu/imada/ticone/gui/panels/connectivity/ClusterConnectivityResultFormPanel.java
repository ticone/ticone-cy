package dk.sdu.imada.ticone.gui.panels.connectivity;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;

import dk.sdu.imada.ticone.clustering.AbstractNamedTiconeResult;
import dk.sdu.imada.ticone.clustering.ClusteringWithIterationLabel;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.connectivity.ConnectivityResult;
import dk.sdu.imada.ticone.connectivity.ConnectivityResultWrapper;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.gui.panels.CollapsiblePanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartWithTitle;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartWithTitleComparator;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.permute.ShuffleParameter;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.util.CyNetworkUtil;
import dk.sdu.imada.ticone.util.StringExt;

/**
 * Created by wiwiec on 5/30/16.
 */
public class ClusterConnectivityResultFormPanel {
	private final TiconeClusteringConnectivityResultPanel resultPanel;
	private JTabbedPane tabbedPane1;
	private JPanel mainPanel, infoPanel, filterPanel;
	private JPanel directedPane, undirectedPane;
	private JTable directedResultTable, undirectedResultTable;
	private final ConnectivityResultWrapper connectivityResultWrapper;
	private JLabel clusteringLabel;
	private JComboBox networkComboBox;
	private JButton createUndirectedConnectivityNetworkButton, createDirectedConnectivityNetworkButton;
	private JCheckBox onlyShowEachClusterOnceCheckBox, hideAutoClusterPairsCheckBox;
	private JLabel creationDateLabel;

	// p-valuse / permutations
	private JLabel numberPermutationsLabel;
	private JTextField numberPermutationsTextField;
	private JPanel permutationParameterPanel;
	private JLabel permutationMethodLabel;

	private List<Pair<ICluster, ICluster>> patternPairList;

	public ClusterConnectivityResultFormPanel(final TiconeClusteringConnectivityResultPanel resultPanel,
			final ConnectivityResultWrapper connectivityResultWrapper) {
		super();
		this.resultPanel = resultPanel;
		this.connectivityResultWrapper = connectivityResultWrapper;
		this.$$$setupUI$$$();
		this.updateTables();
	}

	public ConnectivityResultWrapper getConnectivityResultWrapper() {
		return this.connectivityResultWrapper;
	}

	private void createUIComponents() {
		this.infoPanel = new CollapsiblePanel("Info", false);
		this.filterPanel = new CollapsiblePanel("Filtering", false);
		// p-values / permutations
		this.numberPermutationsLabel = new JLabel("Number of permutations:");
		this.numberPermutationsTextField = new JTextField();
		this.numberPermutationsTextField
				.setText(this.connectivityResultWrapper.getConnectivityPValueResult().getNumberPermutations() + "");

		final GridBagLayout gridBagLayout = new GridBagLayout();
		this.permutationParameterPanel = new JPanel(gridBagLayout);

		final IShuffle shuffle = this.connectivityResultWrapper.getConnectivityPValueResult().getPermutationFunction();

		this.permutationMethodLabel = new JLabel(shuffle.getName());

		final Field[] fieldsWithAnnotation = FieldUtils.getFieldsWithAnnotation(shuffle.getClass(),
				ShuffleParameter.class);
		int i = 0;
		for (final Field f : fieldsWithAnnotation) {
			f.setAccessible(true);
			final ShuffleParameter anno = f.getAnnotation(ShuffleParameter.class);
			final JLabel label = new JLabel(anno.uiDescription());
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = i;
			this.permutationParameterPanel.add(label, c);

			try {
				final JTextField tf = new JTextField(f.get(shuffle).toString());
				c = new GridBagConstraints();
				c.gridx = 1;
				c.gridy = i;
				this.permutationParameterPanel.add(tf, c);
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				e1.printStackTrace();
			}

			i++;
		}

		this.clusteringLabel = new ClusteringWithIterationLabel(
				new AbstractNamedTiconeResult[] { this.connectivityResultWrapper.getClusteringResult() },
				new int[] { this.connectivityResultWrapper.getClusteringIteration() });
		String networkName = "";
		if (this.connectivityResultWrapper.getDirectedConnectivityResult() != null)
			networkName = this.connectivityResultWrapper.getDirectedConnectivityResult().getNetworkName();
		else if (this.connectivityResultWrapper.getUndirectedConnectivityResult() != null)
			networkName = this.connectivityResultWrapper.getUndirectedConnectivityResult().getNetworkName();
		else if (this.connectivityResultWrapper.getConnectivityPValueResult() != null)
			networkName = this.connectivityResultWrapper.getConnectivityPValueResult().getNetworkName();
		this.networkComboBox = new JComboBox<>(new String[] { StringExt.ellipsis(networkName, 42) });
		this.networkComboBox.setEnabled(false);

		this.creationDateLabel = new JLabel(String.format("%s (%s)",
				this.connectivityResultWrapper.getDate() != null
						? new SimpleDateFormat().format(this.connectivityResultWrapper.getDate())
						: "NA",
				this.connectivityResultWrapper.getTiconeVersion() != null
						? this.connectivityResultWrapper.getTiconeVersion()
						: "<= 1.2.74"));
		this.tabbedPane1 = new JTabbedPane();
		this.tabbedPane1.addContainerListener(new ContainerListener() {

			@Override
			public void componentRemoved(final ContainerEvent e) {
			}

			@Override
			public void componentAdded(final ContainerEvent e) {
				if (ClusterConnectivityResultFormPanel.this.tabbedPane1.getTabCount() == 2) {
					ClusterConnectivityResultFormPanel.this.tabbedPane1.setEnabledAt(0,
							ClusterConnectivityResultFormPanel.this.connectivityResultWrapper
									.getDirectedConnectivityResult() != null);
					ClusterConnectivityResultFormPanel.this.tabbedPane1.setEnabledAt(1,
							ClusterConnectivityResultFormPanel.this.connectivityResultWrapper
									.getUndirectedConnectivityResult() != null);

					if (ClusterConnectivityResultFormPanel.this.connectivityResultWrapper
							.getDirectedConnectivityResult() != null)
						ClusterConnectivityResultFormPanel.this.tabbedPane1.setSelectedIndex(0);
					else
						ClusterConnectivityResultFormPanel.this.tabbedPane1.setSelectedIndex(1);
				}
			}
		});
		this.tabbedPane1.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(final ChangeEvent e) {
				ClusterConnectivityResultFormPanel.this.updateTableFilter();
			}
		});

		this.createUndirectedConnectivityNetworkButton = new JButton("Create Connectivity Network");
		this.createUndirectedConnectivityNetworkButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				final TiconeCytoscapeNetwork undirectedConnectivityNetwork = CyNetworkUtil
						.createUndirectedConnectivityNetwork(
								ClusterConnectivityResultFormPanel.this.resultPanel.getResult());

				// add p-values to edge attributes
				if (ClusterConnectivityResultFormPanel.this.resultPanel.getResult()
						.getConnectivityPValueResult() != null) {
					CyNetworkUtil.addPvaluesToUndirectedConnectivityNetwork(
							ClusterConnectivityResultFormPanel.this.resultPanel.getResult(),
							undirectedConnectivityNetwork);
				}
				ClusterConnectivityResultFormPanel.this.resultPanel.connectivityNetworks
						.add(undirectedConnectivityNetwork);
			}
		});
		this.createDirectedConnectivityNetworkButton = new JButton("Create Connectivity Network");
		this.createDirectedConnectivityNetworkButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				final TiconeCytoscapeNetwork directedConnectivityNetwork = CyNetworkUtil
						.createDirectedConnectivityNetwork(
								ClusterConnectivityResultFormPanel.this.resultPanel.getResult());

				// add p-values to edge attributes
				if (ClusterConnectivityResultFormPanel.this.resultPanel.getResult()
						.getConnectivityPValueResult() != null) {
					CyNetworkUtil.addPvaluesToDirectedConnectivityNetwork(
							ClusterConnectivityResultFormPanel.this.resultPanel.getResult(),
							directedConnectivityNetwork);
				}
				ClusterConnectivityResultFormPanel.this.resultPanel.connectivityNetworks
						.add(directedConnectivityNetwork);
			}
		});

		this.hideAutoClusterPairsCheckBox = new JCheckBox("Hide cluster pairs of a cluster with itself");
		this.hideAutoClusterPairsCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				ClusterConnectivityResultFormPanel.this.updateTableFilter();
			}
		});

		this.onlyShowEachClusterOnceCheckBox = new JCheckBox("Only show each cluster pair once");
		this.onlyShowEachClusterOnceCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				ClusterConnectivityResultFormPanel.this.updateTableFilter();
			}
		});

		if (this.connectivityResultWrapper.getDirectedConnectivityResult() != null) {
			if (this.connectivityResultWrapper.getConnectivityPValueResult() != null)
				this.directedResultTable = new ConnectivityTable(
						new String[] { "Cluster", "Cluster", "EE", "E", "OR", "p" });
			else
				this.directedResultTable = new ConnectivityTable(
						new String[] { "Cluster", "Cluster", "EE", "E", "OR" });
		}
		if (this.directedResultTable == null)
			this.directedResultTable = new JTable();
		if (this.connectivityResultWrapper.getUndirectedConnectivityResult() != null) {
			if (this.connectivityResultWrapper.getConnectivityPValueResult() != null)
				this.undirectedResultTable = new ConnectivityTable(
						new String[] { "Cluster", "Cluster", "EE", "E", "OR", "p" });
			else
				this.undirectedResultTable = new ConnectivityTable(
						new String[] { "Cluster", "Cluster", "EE", "E", "OR" });
		}
		if (this.undirectedResultTable == null)
			this.undirectedResultTable = new JTable();
	}

	private void updateTableFilter() {
		// Directed
		if (this.tabbedPane1.getSelectedIndex() == 0) {
			if (this.directedResultTable != null && this.directedResultTable.getRowSorter() != null) {
				final RowFilter<DefaultTableModel, Object> rf = new RowFilter<DefaultTableModel, Object>() {
					@Override
					public boolean include(final Entry<? extends DefaultTableModel, ? extends Object> entry) {
						if (ClusterConnectivityResultFormPanel.this.hideAutoClusterPairsCheckBox.isSelected())
							return ((ClusterChartWithTitle) entry.getValue(0)).getCluster()
									.getClusterNumber() != ((ClusterChartWithTitle) entry.getValue(1)).getCluster()
											.getClusterNumber();
						return true;
					}
				};
				((TableRowSorter<DefaultTableModel>) this.directedResultTable.getRowSorter()).setRowFilter(rf);
			}
		}
		// Undirected
		else if (this.tabbedPane1.getSelectedIndex() == 1) {
			if (this.undirectedResultTable != null && this.undirectedResultTable.getRowSorter() != null) {
				final RowFilter<DefaultTableModel, Object> rf = new RowFilter<DefaultTableModel, Object>() {

					@Override
					public boolean include(final Entry<? extends DefaultTableModel, ? extends Object> entry) {
						boolean result = true;
						if (ClusterConnectivityResultFormPanel.this.hideAutoClusterPairsCheckBox.isSelected())
							result = ((ClusterChartWithTitle) entry.getValue(0)).getCluster()
									.getClusterNumber() != ((ClusterChartWithTitle) entry.getValue(1)).getCluster()
											.getClusterNumber();
						if (ClusterConnectivityResultFormPanel.this.onlyShowEachClusterOnceCheckBox.isSelected())
							result = result && ((ClusterChartWithTitle) entry.getValue(0)).getCluster()
									.getClusterNumber() <= ((ClusterChartWithTitle) entry.getValue(1)).getCluster()
											.getClusterNumber();
						return result;
					}

				};
				((TableRowSorter<DefaultTableModel>) this.undirectedResultTable.getRowSorter()).setRowFilter(rf);
			}
		}
	}

	protected static int countSelectedRows(final JTable resultTable, final int column) {
		int count = 0;
		for (int row = 0; row < resultTable.getRowCount(); row++) {
			final boolean kpm_selected = (boolean) resultTable.getValueAt(row, column);
			if (kpm_selected)
				count++;
		}
		return count;
	}

	public JPanel getMainPanel() {
		return this.mainPanel;
	}

	private void updateTables() {
		if (this.connectivityResultWrapper.getDirectedConnectivityResult() != null && this.directedResultTable != null)
			this.updateDirectedResultTable();
		if (this.connectivityResultWrapper.getUndirectedConnectivityResult() != null
				&& this.undirectedResultTable != null)
			this.updateUndirectedResultTable();
	}

	private void updateDirectedResultTable() {
		((DefaultTableModel) this.directedResultTable.getModel()).setRowCount(0);

		final TableRowSorter<TableModel> rowSorter = new TableRowSorter<TableModel>(
				this.directedResultTable.getModel()) {
			@Override
			public boolean isSortable(final int column) {
				return true;
			}
		};
		rowSorter.setComparator(0, new ClusterChartWithTitleComparator());
		rowSorter.setComparator(1, new ClusterChartWithTitleComparator());

		this.directedResultTable.setRowSorter(rowSorter);

		final DefaultTableModel tableModel = (DefaultTableModel) this.directedResultTable.getModel();

		final ConnectivityResult directedConnectivityResult = this.connectivityResultWrapper
				.getDirectedConnectivityResult();

		final TiconeCytoscapeClusteringResult utils = this.resultPanel.getClusteringResult();

		// TODO: iteration
		final IClusterObjectMapping pom = this.resultPanel.getClusteringResult().getClusteringProcess()
				.getLatestClustering();

		for (final IClusterPair pair : directedConnectivityResult.getEdgeCount().keySet()) {
			final int edgeCount = directedConnectivityResult.getEdgeCount().get(pair);
			final double expected = directedConnectivityResult.getExpectedEdgeCount().get(pair);
			final double enrichment = directedConnectivityResult.getEdgeCountEnrichment().get(pair);

			ClusterChartWithTitle clusterChartContainer1, clusterChartContainer2;
			final ITimeSeriesObjectList objects1 = pair.getFirst().getObjects(),
					objects2 = pair.getSecond().getObjects();
			if (!utils.hasClusterChartContainer(pair.getFirst(), objects1)) {
				try {
					utils.addClusterChartContainer(pair.getFirst(), objects1, new ClusterChartContainer(
							this.resultPanel.getClusteringResult(), pair.getFirst(), objects1));
				} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
					utils.addClusterChartContainer(pair.getFirst(), objects1, null);
				}
			}
			clusterChartContainer1 = new ClusterChartWithTitle(
					utils.getClusterChartContainer(pair.getFirst(), objects1));

			if (!utils.hasClusterChartContainer(pair.getSecond(), objects2)) {
				try {
					utils.addClusterChartContainer(pair.getSecond(), objects2, new ClusterChartContainer(
							this.resultPanel.getClusteringResult(), pair.getSecond(), objects2));
				} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
					utils.addClusterChartContainer(pair.getSecond(), objects2, null);
				}
			}
			clusterChartContainer2 = new ClusterChartWithTitle(
					utils.getClusterChartContainer(pair.getSecond(), objects2));

			if (this.connectivityResultWrapper.getConnectivityPValueResult() != null) {
				tableModel.addRow(new Object[] { clusterChartContainer1, clusterChartContainer2, expected, edgeCount,
						enrichment, this.connectivityResultWrapper.getConnectivityPValueResult()
								.getDirectedEdgeCountPvalues().get(pair) });
			} else {
				tableModel.addRow(new Object[] { clusterChartContainer1, clusterChartContainer2, expected, edgeCount,
						enrichment });
			}
		}
	}

	private void updateUndirectedResultTable() {
		((DefaultTableModel) this.undirectedResultTable.getModel()).setRowCount(0);

		final TableRowSorter<TableModel> rowSorter = new TableRowSorter<TableModel>(
				this.undirectedResultTable.getModel()) {
			@Override
			public boolean isSortable(final int column) {
				return true;
			}
		};
		rowSorter.setComparator(0, new ClusterChartWithTitleComparator());
		rowSorter.setComparator(1, new ClusterChartWithTitleComparator());

		this.undirectedResultTable.setRowSorter(rowSorter);

		final DefaultTableModel tableModel = (DefaultTableModel) this.undirectedResultTable.getModel();

		final ConnectivityResult directedConnectivityResult = this.connectivityResultWrapper
				.getUndirectedConnectivityResult();

		final TiconeCytoscapeClusteringResult utils = this.resultPanel.getClusteringResult();

		final IClusterObjectMapping pom = this.resultPanel.getClusteringResult().getClusteringProcess()
				.getLatestClustering();

		for (final IClusterPair pair : directedConnectivityResult.getEdgeCount().keySet()) {
			final int edgeCount = directedConnectivityResult.getEdgeCount().get(pair);
			final double expected = directedConnectivityResult.getExpectedEdgeCount().get(pair);
			final double enrichment = directedConnectivityResult.getEdgeCountEnrichment().get(pair);

			ClusterChartWithTitle clusterChartContainer1, clusterChartContainer2;
			final ITimeSeriesObjectList objects1 = pair.getFirst().getObjects(),
					objects2 = pair.getSecond().getObjects();
			if (!utils.hasClusterChartContainer(pair.getFirst(), objects1)) {
				try {
					utils.addClusterChartContainer(pair.getFirst(), objects1, new ClusterChartContainer(
							this.resultPanel.getClusteringResult(), pair.getFirst(), objects1));
				} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
					utils.addClusterChartContainer(pair.getFirst(), objects1, null);
				}
			}
			clusterChartContainer1 = new ClusterChartWithTitle(
					utils.getClusterChartContainer(pair.getFirst(), objects1));

			if (!utils.hasClusterChartContainer(pair.getSecond(), objects2)) {
				try {
					utils.addClusterChartContainer(pair.getSecond(), objects2, new ClusterChartContainer(
							this.resultPanel.getClusteringResult(), pair.getSecond(), objects2));
				} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
					utils.addClusterChartContainer(pair.getSecond(), objects2, null);
				}
			}
			clusterChartContainer2 = new ClusterChartWithTitle(
					utils.getClusterChartContainer(pair.getSecond(), objects2));

			if (this.connectivityResultWrapper.getConnectivityPValueResult() != null)
				tableModel.addRow(new Object[] { clusterChartContainer1, clusterChartContainer2, expected, edgeCount,
						enrichment, this.connectivityResultWrapper.getConnectivityPValueResult()
								.getUndirectedEdgeCountPvalues().get(pair)
						// ,connectivityResultWrapper.getEdgeCrossoverConnectivityResult()
						// .getUndirectedEdgeCountLogOddsSmallerPvalues() !=
						// null
						// ?
						// connectivityResultWrapper.getEdgeCrossoverConnectivityResult()
						// .getUndirectedEdgeCountLogOddsSmallerPvalues().get(pair)
						// : "-",
						// connectivityResultWrapper.getEdgeCrossoverConnectivityResult()
						// .getUndirectedEdgeCountLogOddsLargerPvalues() != null
						// ?
						// connectivityResultWrapper.getEdgeCrossoverConnectivityResult()
						// .getUndirectedEdgeCountLogOddsLargerPvalues().get(pair)
						// : "-"
				});
			else
				tableModel.addRow(new Object[] { clusterChartContainer1, clusterChartContainer2, expected, edgeCount,
						enrichment });
		}
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
	 * edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
		tabbedPane1.setEnabled(true);
		mainPanel.add(tabbedPane1,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null,
						new Dimension(200, 200), null, 0, false));
		directedPane = new JPanel();
		directedPane.setLayout(new BorderLayout(0, 0));
		tabbedPane1.addTab("Directed", directedPane);
		final JScrollPane scrollPane1 = new JScrollPane();
		scrollPane1.setEnabled(false);
		directedPane.add(scrollPane1, BorderLayout.CENTER);
		scrollPane1.setViewportView(directedResultTable);
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		directedPane.add(panel1, BorderLayout.SOUTH);
		final Spacer spacer1 = new Spacer();
		panel1.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		panel1.add(createDirectedConnectivityNetworkButton,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		undirectedPane = new JPanel();
		undirectedPane.setLayout(new BorderLayout(0, 0));
		tabbedPane1.addTab("Undirected", undirectedPane);
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		undirectedPane.add(panel2, BorderLayout.NORTH);
		onlyShowEachClusterOnceCheckBox.setSelected(true);
		panel2.add(onlyShowEachClusterOnceCheckBox,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final Spacer spacer2 = new Spacer();
		panel2.add(spacer2, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		final JScrollPane scrollPane2 = new JScrollPane();
		scrollPane2.setEnabled(false);
		undirectedPane.add(scrollPane2, BorderLayout.CENTER);
		scrollPane2.setViewportView(undirectedResultTable);
		final JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		undirectedPane.add(panel3, BorderLayout.SOUTH);
		final Spacer spacer3 = new Spacer();
		panel3.add(spacer3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		panel3.add(createUndirectedConnectivityNetworkButton,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		filterPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(filterPanel,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel4 = new JPanel();
		panel4.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		filterPanel.add(panel4,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		panel4.add(hideAutoClusterPairsCheckBox,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		infoPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(infoPanel,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel5 = new JPanel();
		panel5.setLayout(new GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1));
		infoPanel.add(panel5,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_NORTH, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JLabel label1 = new JLabel();
		label1.setText("Clustering:");
		panel5.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel5.add(clusteringLabel,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label2 = new JLabel();
		label2.setText("Network:");
		panel5.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel5.add(networkComboBox,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label3 = new JLabel();
		label3.setText("Creation Date:");
		panel5.add(label3, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel5.add(creationDateLabel,
				new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JPanel panel6 = new JPanel();
		panel6.setLayout(new GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1));
		panel5.add(panel6,
				new GridConstraints(3, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		numberPermutationsLabel.setText("Number of permutations:");
		panel6.add(numberPermutationsLabel,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		numberPermutationsTextField.setEditable(false);
		panel6.add(numberPermutationsTextField,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		panel6.add(permutationParameterPanel,
				new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JLabel label4 = new JLabel();
		label4.setText("Permutation method:");
		panel6.add(label4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel6.add(permutationMethodLabel,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return mainPanel;
	}
}
