package dk.sdu.imada.ticone.gui.summary;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Function;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;

import com.itextpdf.text.Font;

import dk.sdu.imada.ticone.clustering.ClusterList;
import dk.sdu.imada.ticone.clustering.ClusteringIterationAddedEvent;
import dk.sdu.imada.ticone.clustering.ClusteringIterationDeletedEvent;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringIterationAddedListener;
import dk.sdu.imada.ticone.clustering.IClusteringIterationDeletionListener;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.clustering.filter.IFilter;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureInformationContent;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.ClusterFeaturePrototypeStandardVariance;
import dk.sdu.imada.ticone.feature.FeaturePvalue;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.gui.FeatureValueLabel;
import dk.sdu.imada.ticone.gui.WrapLayout;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer;
import dk.sdu.imada.ticone.gui.util.ClusterComparatorComboBoxItemsChangedEvent;
import dk.sdu.imada.ticone.gui.util.ClusterComparatorComboBoxItemsInitializedEvent;
import dk.sdu.imada.ticone.gui.util.IClusterComparatorComboBoxItemsChangedListener;
import dk.sdu.imada.ticone.gui.util.IClusterComparatorComboBoxItemsInitializedListener;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.statistics.IPvalue;

public class ClusterSummaryPanel extends JPanel
		implements IClusterComparatorComboBoxItemsInitializedListener, IClusterComparatorComboBoxItemsChangedListener,
		ItemListener, IClusteringIterationAddedListener, IClusteringIterationDeletionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2198445741528913870L;
	private IClusterObjectMapping pom;
	private final TiconeCytoscapeClusteringResult utils;
	private Comparator<ICluster> clusterComparator;
	private int clusterChartPanelWidth, clusterChartPanelHeight;
	private IFeatureStore clusterFeatureStore;
	private Set<FeatureValueLabel> featureValueLabels;

	public ClusterSummaryPanel(final IClusterObjectMapping pom, final TiconeCytoscapeClusteringResult utils,
			final IFeatureStore clusterFeatureStore) {
		this(pom, utils, 400, 160, clusterFeatureStore);
	}

	public ClusterSummaryPanel(final IClusterObjectMapping pom, final TiconeCytoscapeClusteringResult utils,
			final int chartWidth, final int chartHeight, final IFeatureStore clusterFeatureStore) {
		super(new WrapLayout());
		this.pom = pom;
		this.utils = utils;
		this.clusterChartPanelWidth = chartWidth;
		this.clusterChartPanelHeight = chartHeight;
		this.clusterFeatureStore = clusterFeatureStore;
		this.featureValueLabels = new HashSet<>();

		this.utils.addClusteringIterationAddedListener(this);
		this.utils.addClusteringIterationDeletionListener(this);
	}

	void onDispose() {
		this.clearFeatureValueLabels();
	}

	public void setClusterComparator(final Comparator<ICluster> clusterComparator) throws InterruptedException {
		this.clusterComparator = clusterComparator;
		this.addClustersToSummaryPanel(this.pom, this.utils);
	}

	public void setClusterChartPanelWidth(final int clusterChartPanelWidth) throws InterruptedException {
		this.clusterChartPanelWidth = clusterChartPanelWidth;
		this.addClustersToSummaryPanel(this.pom, this.utils);
	}

	public Comparator<ICluster> getClusterComparator() {
		return this.clusterComparator;
	}

	public void setClusterChartPanelHeight(final int clusterChartPanelHeight) throws InterruptedException {
		this.clusterChartPanelHeight = clusterChartPanelHeight;
		this.addClustersToSummaryPanel(this.pom, this.utils);
	}

	private void clearFeatureValueLabels() {
		final Iterator<FeatureValueLabel> it = this.featureValueLabels.iterator();
		while (it.hasNext()) {
			final FeatureValueLabel l = it.next();
			if (l.getFeatureStore() != null)
				l.getFeatureStore().removeFeatureValueStoredListener(l);
			it.remove();
		}
	}

	private void addClustersToSummaryPanel(final IClusterObjectMapping pom, final TiconeCytoscapeClusteringResult utils)
			throws InterruptedException {
		this.removeAll();
		this.clearFeatureValueLabels();
		this.setLayout(new WrapLayout());
		this.setBackground(Color.WHITE);
		final IClusterList clusterList = new ClusterList(pom.getClusters());
		// clusterList = filterClusters(clusterList);
		clusterList.sort(this.clusterComparator);
		final IFilter<ICluster> clusterFilter = utils.getClusterFilter();
		for (final ICluster cluster : clusterList) {
			final ITimeSeriesObjectList timeSeriesDataList = cluster.getObjects();
			final JPanel patternPanel = this.setupClusterPanel(cluster, timeSeriesDataList, utils,
					!clusterFilter.check(this.clusterFeatureStore, cluster));
			this.add(patternPanel);
		}
		// this.setSize(this.getSize());
		this.revalidate();
		this.repaint();
	}

	private JPanel setupClusterPanel(final ICluster cluster, final ITimeSeriesObjectList timeSeriesDataList,
			final TiconeCytoscapeClusteringResult utils, final boolean isFiltered) throws InterruptedException {
		final JPanel wrapPanel = new JPanel(new BorderLayout());
		wrapPanel.setOpaque(false);

		final JPanel clusterLabelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
		final JLabel clusterLabel = new JLabel(cluster.getName());
		if (isFiltered) {
			clusterLabel.setBackground(Color.LIGHT_GRAY);
			clusterLabel.setForeground(Color.GRAY);
		}
		clusterLabel.setFont(clusterLabel.getFont().deriveFont(Font.BOLD));
		clusterLabelPanel.add(clusterLabel);
		wrapPanel.add(clusterLabelPanel, BorderLayout.NORTH);

		final JPanel clusterPanel = new JPanel(new WrapLayout(FlowLayout.LEFT, 10, 2));
		if (isFiltered) {
			clusterPanel.setOpaque(true);
			clusterPanel.setBackground(Color.LIGHT_GRAY);
			clusterPanel.setForeground(Color.GRAY);
		} else
			clusterPanel.setOpaque(false);

		try {
			final ClusterChartContainer patternGraphPanelContainer = new ClusterChartContainer(utils, cluster,
					timeSeriesDataList);
			final ChartPanel chartPanel = patternGraphPanelContainer.getLargeChartPanel();
			chartPanel.setPreferredSize(new Dimension(this.clusterChartPanelWidth, this.clusterChartPanelHeight));
			clusterPanel.add(chartPanel);
		} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
		}

		this.addLabelToPanel("|C|:", new ClusterFeatureNumberObjects(), (Integer d) -> d.toString(), cluster,
				clusterPanel);

		try {
			this.addLabelToPanel("Avg. Similarity:", new ClusterFeatureAverageSimilarity(utils.getSimilarityFunction()),
					(ISimilarityValue s) -> {
						try {
							return String.format("%.3G", s.get());
						} catch (SimilarityCalculationException e) {
							return "--";
						}
					}, cluster, clusterPanel);
		} catch (final Exception e1) {
			e1.printStackTrace();
		}
		this.addLabelToPanel("p:", new FeaturePvalue(ObjectType.CLUSTER),
				(IPvalue p) -> String.format("%.3G", p.getDouble()), cluster, clusterPanel);

		IFeature<Double> f = new ClusterFeaturePrototypeStandardVariance();

		// only add information content, if it has been calculated
		if (this.clusterFeatureStore.featureSet().contains(f)) {
			this.addLabelToPanel("Std. Var:", f, (Double d) -> String.format("%.3G", d), cluster, clusterPanel);
		}

		f = new ClusterFeatureInformationContent();

		// only add information content, if it has been calculated
		if (this.clusterFeatureStore.featureSet().contains(f)) {
			this.addLabelToPanel("IC:", f, (Double d) -> String.format("%.3G", d), cluster, clusterPanel);
		}

		wrapPanel.add(clusterPanel, BorderLayout.CENTER);

		wrapPanel.setPreferredSize(new Dimension(this.clusterChartPanelWidth, this.clusterChartPanelHeight + 130));

		return wrapPanel;
	}

	private <V> void addLabelToPanel(final String text, final IFeature<V> feature,
			final Function<V, String> valueToString, final IObjectWithFeatures object, final JPanel panel) {
		final JPanel labelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 0));
		labelPanel.setOpaque(false);
		final JLabel textLabel = new JLabel(text);
		final FeatureValueLabel<V> valueLabel = new FeatureValueLabel<>(clusterFeatureStore, feature, object,
				valueToString);
		this.featureValueLabels.add(valueLabel);

		labelPanel.add(textLabel);
		labelPanel.add(valueLabel);

		panel.add(labelPanel);
	}

	public int getClusterChartPanelHeight() {
		return this.clusterChartPanelHeight;
	}

	public int getClusterChartPanelWidth() {
		return this.clusterChartPanelWidth;
	}

	@Override
	public void comboBoxItemsInitialized(final ClusterComparatorComboBoxItemsInitializedEvent e) {
		try {
			this.update();
			e.getCb().setSelectedItem(this.clusterComparator);
		} catch (InterruptedException e1) {
		}
	}

	@Override
	public void comboBoxItemsChanged(final ClusterComparatorComboBoxItemsChangedEvent e) {
		final Comparator<ICluster> comp = (Comparator<ICluster>) e.getCb().getSelectedItem();
		try {
			this.setClusterComparator(comp);
		} catch (InterruptedException e1) {
		}
	}

	@Override
	public void itemStateChanged(final ItemEvent e) {
		try {
			final Comparator<ICluster> comp = (Comparator<ICluster>) e.getItem();
			this.setClusterComparator(comp);
		} catch (final Exception ex) {
			ex.printStackTrace();
		}
	}

	private void update() throws InterruptedException {
		this.addClustersToSummaryPanel(this.pom, this.utils);
	}

	@Override
	public void clusteringIterationDeleted(ClusteringIterationDeletedEvent event) {
		final int iteration = event.getIteration();
		this.pom = this.utils.getClusteringOfIteration(iteration - 1);
		this.clusterFeatureStore = event.getClustering().getFeatureStore(iteration - 1);

		try {
			update();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void clusteringIterationAdded(ClusteringIterationAddedEvent event) {
		final int iteration = event.getIteration();
		this.pom = this.utils.getClusteringOfIteration(iteration);
		this.clusterFeatureStore = event.getClustering().getFeatureStore(iteration);

		try {
			update();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
