package dk.sdu.imada.ticone.gui.panels.popup;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import dk.sdu.imada.ticone.clustering.ClusterOperationException;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.suggestclusters.IClusterSuggestion;
import dk.sdu.imada.ticone.clustering.suggestclusters.ISuggestNewCluster;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.gui.panels.MyDialogPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * Created by christian on 9/8/15.
 */
public class SuggestPatternsAcceptPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -221966889511926286L;
	private final JFrame frame;
	private final ISuggestNewCluster suggestNewPatternInterface;
	private final IClusterSuggestion clusteringTransition;

	private final Map<ICluster, JCheckBox> checkBoxMap;

	protected TiconeClusteringResultPanel resultPanel;
	private ClusterFeatureAverageSimilarity cfas;

	public SuggestPatternsAcceptPanel(final JFrame frame, final ISuggestNewCluster suggestNewPatternInterface,
			final IClusterSuggestion clusteringTransition, final TiconeClusteringResultPanel resultPanel)
			throws InterruptedException, IncompatibleSimilarityFunctionException {
		super();
		this.resultPanel = resultPanel;
		this.suggestNewPatternInterface = suggestNewPatternInterface;
		this.clusteringTransition = clusteringTransition;
		this.frame = frame;
		this.cfas = new ClusterFeatureAverageSimilarity(resultPanel.getResult().getSimilarityFunction());
		this.setLayout(new GridBagLayout());
		this.checkBoxMap = new HashMap<>();
		this.initComponents();
	}

	private void initComponents() throws InterruptedException {
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		final JPanel newPatternsPanel = this.setupNewPatternsPanel();
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 2;
		this.add(newPatternsPanel, constraints);
		constraints.gridwidth = 1;

		final JButton saveNewPatternsButton = new JButton("Save selected clusters");
		saveNewPatternsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				try {
					SuggestPatternsAcceptPanel.this.savePatternsAction();
				} catch (final ClusterOperationException e) {
					MyDialogPanel.showMessageDialog(e.getMessage());
				} catch (InterruptedException | TiconeUnloadingException e) {
				}
			}
		});
		constraints.gridx = 0;
		constraints.gridy = 1;
		this.add(saveNewPatternsButton, constraints);

		final JButton discardNewPatternsButton = new JButton("Discard all clusters");
		discardNewPatternsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				SuggestPatternsAcceptPanel.this.frame.dispose();
			}
		});
		constraints.gridx = 1;
		constraints.gridy = 1;
		this.add(discardNewPatternsButton, constraints);
	}

	private void savePatternsAction() throws ClusterOperationException, InterruptedException, TiconeUnloadingException {
		final IClusteringProcess timeSeriesClusteringWithOverrepresentedPatterns = this.resultPanel
				.getClusteringResult().getClusteringProcess();
		this.removePatternsNotMarkedToKeep();
		timeSeriesClusteringWithOverrepresentedPatterns.applySuggestedClusters(this.suggestNewPatternInterface,
				this.clusteringTransition);
		this.resultPanel.getClusteringResult().setupPatternStatusMapping();
		GUIUtility.updateGraphPanel(this.resultPanel);
		this.frame.dispose();
	}

	private void removePatternsNotMarkedToKeep() {
		final IClusterObjectMapping patternObjectMapping = this.clusteringTransition.getNewClusterObjectMapping();
		// Marking the patterns which should be kept and which should not.
		for (final ICluster pattern : patternObjectMapping.getClusters()) {
			if (this.checkBoxMap.get(pattern).isSelected()) {
				this.clusteringTransition.keepPattern(pattern, true);
			} else {
				this.clusteringTransition.keepPattern(pattern, false);
			}
		}
	}

	private JPanel setupNewPatternsPanel() throws InterruptedException {
		final JPanel newPatternsPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.insets = new Insets(10, 10, 10, 10);

		final IClusterObjectMapping patternObjectMapping = this.clusteringTransition.getNewClusterObjectMapping();

		for (final ICluster pattern : patternObjectMapping.getClusters()) {
			final ITimeSeriesObjectList timeSeriesDatas = pattern.getObjects();
			JPanel tempPanel;
			try {
				tempPanel = this.setupPatternPanel(pattern, timeSeriesDatas);
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException
					| SimilarityCalculationException | FeatureCalculationException | IncorrectlyInitializedException
					| IncompatibleFeatureAndObjectException e) {
				tempPanel = null;
			}
			newPatternsPanel.add(tempPanel, constraints);
			constraints.gridx = 0;
			constraints.gridy++;
		}
		return newPatternsPanel;
	}

	private JPanel setupPatternPanel(final ICluster pattern, final ITimeSeriesObjectList timeSeriesDatas)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException,
			SimilarityCalculationException, FeatureCalculationException, IncorrectlyInitializedException,
			IncompatibleFeatureAndObjectException {
		final JPanel patternPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;

		final JLabel patternNumberLabel = new JLabel("Cluster ID: " + pattern.getName());
		constraints.gridx = 0;
		constraints.gridy++;
		patternPanel.add(patternNumberLabel, constraints);

		final JLabel numberOfObjectsLabel = new JLabel("Number of objects: " + timeSeriesDatas.size());
		constraints.gridx = 0;
		constraints.gridy++;
		patternPanel.add(numberOfObjectsLabel, constraints);

		// double avgRSS = new
		// BigDecimal(pattern.getResidualSumOfSquares()).setScale(3,
		// BigDecimal.ROUND_HALF_UP)
		// .doubleValue();
		final JLabel avgSimLabel = new JLabel(
				"Average Similarity: " + String.format("%.3G", cfas.calculate(pattern).getValue().get()));
		constraints.gridx = 0;
		constraints.gridy++;
		patternPanel.add(avgSimLabel, constraints);

		final JCheckBox keepPatternCheckBox = new JCheckBox("Keep cluster");
		keepPatternCheckBox.setSelected(true);
		this.checkBoxMap.put(pattern, keepPatternCheckBox);
		constraints.gridx = 0;
		constraints.gridy++;
		patternPanel.add(keepPatternCheckBox, constraints);

		final JPanel tempPanel = this.setupGraphPanel(pattern, timeSeriesDatas);
		constraints.gridx = 0;
		constraints.gridy++;
		patternPanel.add(tempPanel, constraints);

		return patternPanel;
	}

	private JPanel setupGraphPanel(final ICluster pattern, final ITimeSeriesObjectList timeSeriesDatas)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException {
		final JPanel patternPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.WEST;
		constraints.insets = new Insets(10, 10, 10, 10);

		final ClusterChartContainer patternGraphPanelContainer = new ClusterChartContainer(
				this.resultPanel.getClusteringResult(), pattern, timeSeriesDatas);
		constraints.gridx = 0;
		constraints.gridy = 0;
		patternPanel.add(patternGraphPanelContainer.getChartPanel(), constraints);

		return patternPanel;
	}
}
