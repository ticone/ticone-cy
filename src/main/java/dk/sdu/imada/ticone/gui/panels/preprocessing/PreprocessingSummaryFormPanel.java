package dk.sdu.imada.ticone.gui.panels.preprocessing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.text.SimpleDateFormat;
import java.util.Comparator;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusteringChangeEvent;
import dk.sdu.imada.ticone.clustering.IClusteringChangeListener;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.IInitialClusteringProvider;
import dk.sdu.imada.ticone.clustering.InitialClusteringFromTable;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.gui.panels.CollapsiblePanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.io.ClusteringImportFromTableConfig;
import dk.sdu.imada.ticone.io.IImportColumnMapping;
import dk.sdu.imada.ticone.io.ILoadDataMethod;
import dk.sdu.imada.ticone.io.LoadColumnDataMethod;
import dk.sdu.imada.ticone.io.LoadDataFromTable;
import dk.sdu.imada.ticone.preprocessing.IPreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.IPreprocessingSummaryListener;
import dk.sdu.imada.ticone.preprocessing.PreprocessingSummaryEvent;
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeFactoryException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.statistics.IClusterPValueCalculationResult;
import dk.sdu.imada.ticone.statistics.IPValueCalculationResult;

/**
 * @author Christian Wiwie
 * @since May 31, 2016
 */
public class PreprocessingSummaryFormPanel implements IPreprocessingSummaryListener, IClusteringChangeListener {
	/**
	 *
	 */
	private static final long serialVersionUID = 7246256835150507886L;
	protected TiconeClusteringResultPanel resultPanel;
	protected JScrollPane notInNetworkScrollPane, disagreeingSrollPane, lowVarianceScrollPane;
	protected JScrollPane mainPanel;
	protected JLabel dataLoadSourceLabel;
	protected JTable notInNetworkTable, disagreeingTable, lowVarianceTable;
	protected DefaultTableModel notInNetworkTableModel, disagreeingTableModel, lowVarianceTableModel;
	protected JComboBox dataFromComboBox, objectIdColumnComboBox, replicateColumnComboBox, clusteringMethodComboBox,
			removeDisagreeingComboBox, removeLowVarComboBox, networkComboBox, simFunComboBox;
	protected JList timePointsColumnsList;
	protected JTextField discretizePositiveTextField, discretizeNegativeTextField, numberClustersTextField,
			removeDisagreeingTextField, removeLowVarTextField;
	protected JCheckBox removeDisagreeingCheckBox, removeLowVarCheckBox, removeNotInNetCheckBox;

	// initial clustering
	protected JCheckBox useClusteringMethodCheckBox, importClusteringCheckBox;
	protected JLabel numberOfClustersLabel, importTableLabel, objectIdColumnLabel, clusterIdColumnLabel;
	protected JComboBox importClusteringClusterIdColumnComboBox, importClusteringObjectIdColumnComboBox,
			importClusteringTableComboBox;
	protected JLabel creationDateLabel;
	private JPanel infoPanel, dataPanel, preprocessingPanel, discretizationPanel, clusteringMethodPanel,
			initialClusteringPanel, similarityFunctionPanel, columnMappingPanel, notInNetworkPanel;

	// p-value stuff
	private JPanel pvaluePanel;
	private JTextField numberPermutationsFirstIterationTextField;
	private JList clusterPValuesFeatureList;
	private JComboBox combinePvaluesComboBox;
	private JComboBox permutationMethodComboBox;

	public PreprocessingSummaryFormPanel(final TiconeClusteringResultPanel resultPanel) {
		super();
		this.resultPanel = resultPanel;
		this.$$$setupUI$$$();
		this.resultPanel.getClusteringResult().getPreprocessingSummary().addListener(this);
		this.resultPanel.getClusteringResult().addClusteringChangeListener(this);
	}

	private void createUIComponents() {
		this.infoPanel = new CollapsiblePanel("Info", true);
		this.dataPanel = new CollapsiblePanel("Data", true);
		this.preprocessingPanel = new CollapsiblePanel("Preprocessing", false);
		this.discretizationPanel = new CollapsiblePanel("Discretization", false);
		this.clusteringMethodPanel = new CollapsiblePanel("Supplementary Clustering Method", false);
		this.initialClusteringPanel = new CollapsiblePanel("Initial Clustering", false);
		this.similarityFunctionPanel = new CollapsiblePanel("Similarity Function", false);
		this.columnMappingPanel = new CollapsiblePanel("Column Mapping", false);
		this.notInNetworkPanel = new CollapsiblePanel("Removed Objects Not In Network", false);
		this.pvaluePanel = new CollapsiblePanel("Cluster P-values", true);

		this.creationDateLabel = new JLabel(String.format("%s (%s)",
				this.resultPanel.getClusteringResult().getDate() != null
						? new SimpleDateFormat().format(this.resultPanel.getClusteringResult().getDate())
						: "NA",
				this.resultPanel.getClusteringResult().getTiconeVersion() != null
						? this.resultPanel.getClusteringResult().getTiconeVersion()
						: "<= 1.2.74"));

		this.mainPanel = new JScrollPane();
		this.mainPanel.getVerticalScrollBar().setUnitIncrement(32);
		final TiconeClusteringResult utils = this.resultPanel.getClusteringResult();
		final IPreprocessingSummary summary = utils.getPreprocessingSummary();
		final ILoadDataMethod loadDataMethod = utils.getLoadDataMethod();
		this.dataFromComboBox = new JComboBox<>();
		if (loadDataMethod instanceof LoadDataFromTable)
			this.dataFromComboBox.addItem("From Table");
		else
			this.dataFromComboBox.addItem("From File");
		this.dataLoadSourceLabel = new JLabel(utils.getLoadDataMethod().sourceAsString());

		// column mapping
		final IImportColumnMapping columnMapping = ((LoadColumnDataMethod) loadDataMethod).getColumnMapping();
		this.objectIdColumnComboBox = new JComboBox<>(new String[] { columnMapping.getObjectIdColumnName() });
		this.replicateColumnComboBox = new JComboBox<>(new String[] { columnMapping.getReplicateColumnName() });
		this.timePointsColumnsList = new JList<>(columnMapping.getTimePointsColumnNames());

		// clustering
		this.clusteringMethodComboBox = new JComboBox<>(new String[] { utils.getClusteringMethodBuilder().toString() });
		final IClusteringProcess<ClusterObjectMapping, ?> cl = utils.getClusteringProcess();
		this.numberClustersTextField = new JTextField(cl.getNumberOfInitialClusters() + "");

		// preprocessing
		this.removeDisagreeingCheckBox = new JCheckBox("Remove objects with disagreeing replicates");
		this.removeDisagreeingCheckBox.setSelected(summary.isRemoveObjectsLeastConserved());
		this.removeDisagreeingTextField = new JTextField("" + summary.getRemoveObjectsLeastConservedThreshold());
		if (summary.isRemoveObjectsLeastConservedIsPercent())
			this.removeDisagreeingComboBox = new JComboBox<>(new String[] { "Percent" });
		else
			this.removeDisagreeingComboBox = new JComboBox<>(new String[] { "Threshold" });
		this.removeLowVarCheckBox = new JCheckBox("Remove objects with low standard variance");
		this.removeLowVarCheckBox.setSelected(summary.isRemoveObjectsLowVariance());
		this.removeLowVarTextField = new JTextField("" + summary.getRemoveObjectsLowVarianceThreshold());
		if (summary.isRemoveObjectsLowVarianceIsPercent())
			this.removeLowVarComboBox = new JComboBox<>(new String[] { "Percent" });
		else
			this.removeLowVarComboBox = new JComboBox<>(new String[] { "Threshold" });
		this.removeNotInNetCheckBox = new JCheckBox("Remove objects not in network");
		this.removeNotInNetCheckBox.setSelected(summary.isRemoveObjectsNotInNetwork());
		this.networkComboBox = new JComboBox<>(new String[] { summary.getNetworkName() });

		// discretization
		final IPrototypeBuilder prototypeFactory = utils.getPrototypeBuilder();
		try {
			final DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax discretize = (DiscretizePrototypeWithStepsFromMinToZeroAndZeroToMax) PrototypeComponentType.TIME_SERIES
					.getComponentFactory(prototypeFactory).getDiscretizeFunction();
			this.discretizePositiveTextField = new JTextField(discretize.getPositiveSteps() + "");
			this.discretizeNegativeTextField = new JTextField(discretize.getNegativeSteps() + "");
		} catch (IncompatiblePrototypeComponentException | MissingPrototypeFactoryException e) {
		}
		this.simFunComboBox = new JComboBox<>(new String[] { utils.getSimilarityFunction().toString() });

		this.notInNetworkTableModel = new DefaultTableModel();
		this.notInNetworkTableModel.addColumn("Object ID");
		this.disagreeingTableModel = new DefaultTableModel();
		this.disagreeingTableModel.addColumn("Object ID");
		this.lowVarianceTableModel = new DefaultTableModel();
		this.lowVarianceTableModel.addColumn("Object ID");

		this.notInNetworkTable = new JTable(this.notInNetworkTableModel);
		this.disagreeingTable = new JTable(this.disagreeingTableModel);
		this.lowVarianceTable = new JTable(this.lowVarianceTableModel);

		final ITimeSeriesObjectList notInNetwork = new TimeSeriesObjectList(
				this.resultPanel.getClusteringResult().getPreprocessingSummary().getRemovedObjectsNotInNetwork());
		notInNetwork.sort(new Comparator<ITimeSeriesObject>() {
			@Override
			public int compare(final ITimeSeriesObject o1, final ITimeSeriesObject o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		final ITimeSeriesObjectList disagreeing = new TimeSeriesObjectList(
				this.resultPanel.getClusteringResult().getPreprocessingSummary().getRemovedObjectsLeastConserved());
		disagreeing.sort(new Comparator<ITimeSeriesObject>() {
			@Override
			public int compare(final ITimeSeriesObject o1, final ITimeSeriesObject o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		final ITimeSeriesObjectList lowVariance = new TimeSeriesObjectList(
				this.resultPanel.getClusteringResult().getPreprocessingSummary().getRemovedObjectsLowVariance());
		lowVariance.sort(new Comparator<ITimeSeriesObject>() {
			@Override
			public int compare(final ITimeSeriesObject o1, final ITimeSeriesObject o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		for (final ITimeSeriesObject tsd : notInNetwork) {
			this.notInNetworkTableModel.addRow(new String[] { tsd.getName() });
		}

		for (final ITimeSeriesObject tsd : disagreeing) {
			this.disagreeingTableModel.addRow(new String[] { tsd.getName() });
		}

		for (final ITimeSeriesObject tsd : lowVariance) {
			this.lowVarianceTableModel.addRow(new String[] { tsd.getName() });
		}

		this.notInNetworkScrollPane = new JScrollPane(this.notInNetworkTable);
		this.notInNetworkScrollPane.setVisible(this.notInNetworkTableModel.getRowCount() > 0);
		this.disagreeingSrollPane = new JScrollPane(this.disagreeingTable);
		this.disagreeingSrollPane.setVisible(this.disagreeingTableModel.getRowCount() > 0);
		this.lowVarianceScrollPane = new JScrollPane(this.lowVarianceTable);
		this.lowVarianceScrollPane.setVisible(this.lowVarianceTableModel.getRowCount() > 0);

		// initial clustering
		this.numberOfClustersLabel = new JLabel("Number of clusters:");
		this.importTableLabel = new JLabel("Table:");
		this.objectIdColumnLabel = new JLabel("Object Id Column:");
		this.clusterIdColumnLabel = new JLabel("Cluster Id Column:");
		this.useClusteringMethodCheckBox = new JCheckBox("Use Clustering Method");
		this.importClusteringCheckBox = new JCheckBox("Import Clustering From Table");

		IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider = this.resultPanel
				.getClusteringResult().getInitialClusteringProvider();
		final boolean isInitialFromTable = initialClusteringProvider instanceof InitialClusteringFromTable;
		if (isInitialFromTable) {
			final ClusteringImportFromTableConfig clusteringImportFromTableConfig = ((InitialClusteringFromTable) initialClusteringProvider)
					.getConfig();
			this.importClusteringTableComboBox = new JComboBox<>(
					new String[] { clusteringImportFromTableConfig.getTableName() });
			this.importClusteringObjectIdColumnComboBox = new JComboBox<>(
					new String[] { clusteringImportFromTableConfig.getObjectIdColumnName() });
			this.importClusteringClusterIdColumnComboBox = new JComboBox<>(
					new String[] { clusteringImportFromTableConfig.getClusterIdColumnName() });
		} else {
			this.importClusteringTableComboBox = new JComboBox<>(new String[] {});
			this.importClusteringObjectIdColumnComboBox = new JComboBox<>(new String[] {});
			this.importClusteringClusterIdColumnComboBox = new JComboBox<>(new String[] {});
		}

		this.useClusteringMethodCheckBox.setVisible(!isInitialFromTable);
		this.numberOfClustersLabel.setVisible(!isInitialFromTable);
		this.numberClustersTextField.setVisible(!isInitialFromTable);

		this.importClusteringCheckBox.setVisible(isInitialFromTable);
		this.importTableLabel.setVisible(isInitialFromTable);
		this.importClusteringTableComboBox.setVisible(isInitialFromTable);
		this.objectIdColumnLabel.setVisible(isInitialFromTable);
		this.importClusteringObjectIdColumnComboBox.setVisible(isInitialFromTable);
		this.clusterIdColumnLabel.setVisible(isInitialFromTable);
		this.importClusteringClusterIdColumnComboBox.setVisible(isInitialFromTable);

		this.numberPermutationsFirstIterationTextField = new JTextField("--");
		this.clusterPValuesFeatureList = new JList<IFeature<? extends Number>>();
		this.combinePvaluesComboBox = new JComboBox<String>();
		this.permutationMethodComboBox = new JComboBox<String>();

		final IPValueCalculationResult pvalueResult = resultPanel.getResult().getPvalueCalculationResult();
		this.updatePValues(pvalueResult);
	}

	private void updatePValues(final IPValueCalculationResult pvalueResult) {
		if (pvalueResult == null) {
			this.pvaluePanel.setVisible(false);
			this.numberPermutationsFirstIterationTextField.setText("--");
			this.clusterPValuesFeatureList.setListData(new IFeature[0]);
			((DefaultComboBoxModel<String>) this.combinePvaluesComboBox.getModel()).removeAllElements();
			((DefaultComboBoxModel<String>) this.permutationMethodComboBox.getModel()).removeAllElements();
		} else {
			this.pvaluePanel.setVisible(true);
			this.numberPermutationsFirstIterationTextField
					.setText(pvalueResult.getCalculatePValues().getNumberFinishedPermutations() + "");
			this.clusterPValuesFeatureList
					.setListData(pvalueResult.getCalculatePValues().getFeatures().toArray(new IFeature[0]));
			((DefaultComboBoxModel<String>) this.combinePvaluesComboBox.getModel()).removeAllElements();
			((DefaultComboBoxModel<String>) this.combinePvaluesComboBox.getModel())
					.addElement(pvalueResult.getCalculatePValues().getCombinePValues().getName());
			((DefaultComboBoxModel<String>) this.permutationMethodComboBox.getModel()).removeAllElements();
			((DefaultComboBoxModel<String>) this.permutationMethodComboBox.getModel())
					.addElement(pvalueResult.getCalculatePValues().getShuffle().getName());
		}
	}

	public JScrollPane getMainPanel() {
		return this.mainPanel;
	}

	@Override
	public void preprocessingSummaryChanged(final PreprocessingSummaryEvent e) {
		this.updatePValues((IClusterPValueCalculationResult) resultPanel.getResult().getPvalueCalculationResult());
	}

	@Override
	public void clusteringChanged(final ClusteringChangeEvent e) {
		this.updatePValues(this.resultPanel.getClusteringResult().getPvalueCalculationResult());
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
	 * edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new BorderLayout(0, 0));
		mainPanel.setMaximumSize(new Dimension(2147483647, 2147483647));
		panel1.add(mainPanel, BorderLayout.CENTER);
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(11, 1, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.setViewportView(panel2);
		infoPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel2.add(infoPanel,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		infoPanel.add(panel3,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JLabel label1 = new JLabel();
		label1.setText("Creation Date:");
		panel3.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel3.add(creationDateLabel,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		dataPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel2.add(dataPanel,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel4 = new JPanel();
		panel4.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
		dataPanel.add(panel4,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JLabel label2 = new JLabel();
		label2.setText("Source:");
		panel4.add(label2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		dataFromComboBox.setEnabled(false);
		final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
		defaultComboBoxModel1.addElement("From Table");
		defaultComboBoxModel1.addElement("From File");
		dataFromComboBox.setModel(defaultComboBoxModel1);
		panel4.add(dataFromComboBox,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		panel4.add(dataLoadSourceLabel,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(103, 0),
						null, 0, false));
		preprocessingPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel2.add(preprocessingPanel,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel5 = new JPanel();
		panel5.setLayout(new GridLayoutManager(9, 2, new Insets(0, 0, 0, 0), -1, -1));
		preprocessingPanel.add(panel5,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		removeDisagreeingCheckBox.setEnabled(false);
		removeDisagreeingCheckBox.setText("Remove objects with disagreeing replicates");
		panel5.add(removeDisagreeingCheckBox,
				new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		removeLowVarCheckBox.setEnabled(false);
		removeLowVarCheckBox.setText("Remove objects with low standard variance");
		panel5.add(removeLowVarCheckBox,
				new GridConstraints(3, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		removeDisagreeingComboBox.setEnabled(false);
		final DefaultComboBoxModel defaultComboBoxModel2 = new DefaultComboBoxModel();
		defaultComboBoxModel2.addElement("Percent");
		defaultComboBoxModel2.addElement("Threshold");
		removeDisagreeingComboBox.setModel(defaultComboBoxModel2);
		panel5.add(removeDisagreeingComboBox,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		removeDisagreeingTextField.setEnabled(false);
		panel5.add(removeDisagreeingTextField,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(150, -1), null, 0, false));
		removeLowVarComboBox.setEnabled(false);
		final DefaultComboBoxModel defaultComboBoxModel3 = new DefaultComboBoxModel();
		defaultComboBoxModel3.addElement("Percent");
		defaultComboBoxModel3.addElement("Threshold");
		removeLowVarComboBox.setModel(defaultComboBoxModel3);
		panel5.add(removeLowVarComboBox,
				new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		removeLowVarTextField.setEnabled(false);
		panel5.add(removeLowVarTextField,
				new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(150, -1), null, 0, false));
		removeNotInNetCheckBox.setEnabled(false);
		removeNotInNetCheckBox.setText("Remove objects not in network");
		panel5.add(removeNotInNetCheckBox,
				new GridConstraints(6, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		networkComboBox.setEnabled(false);
		panel5.add(networkComboBox,
				new GridConstraints(7, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		panel5.add(disagreeingSrollPane,
				new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null,
						0, false));
		disagreeingSrollPane.setBorder(BorderFactory.createTitledBorder("Removed Objects With Disagreeing Replicates"));
		disagreeingTable.setPreferredScrollableViewportSize(new Dimension(350, 100));
		disagreeingSrollPane.setViewportView(disagreeingTable);
		panel5.add(lowVarianceScrollPane,
				new GridConstraints(5, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null,
						0, false));
		lowVarianceScrollPane.setBorder(BorderFactory.createTitledBorder("Removed Objects With Low Variance"));
		lowVarianceTable.setPreferredScrollableViewportSize(new Dimension(350, 100));
		lowVarianceScrollPane.setViewportView(lowVarianceTable);
		notInNetworkPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel5.add(notInNetworkPanel,
				new GridConstraints(8, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		notInNetworkPanel.add(notInNetworkScrollPane,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null,
						0, false));
		notInNetworkTable.setPreferredScrollableViewportSize(new Dimension(350, 100));
		notInNetworkScrollPane.setViewportView(notInNetworkTable);
		discretizationPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel2.add(discretizationPanel,
				new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel6 = new JPanel();
		panel6.setLayout(new GridLayoutManager(1, 4, new Insets(0, 0, 0, 0), -1, -1));
		discretizationPanel.add(panel6,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JLabel label3 = new JLabel();
		label3.setText("Positive:");
		panel6.add(label3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		discretizePositiveTextField.setEnabled(false);
		panel6.add(discretizePositiveTextField,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(150, -1), null, 0, false));
		final JLabel label4 = new JLabel();
		label4.setText("Negative:");
		panel6.add(label4, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		discretizeNegativeTextField.setEnabled(false);
		panel6.add(discretizeNegativeTextField,
				new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(150, -1), null, 0, false));
		clusteringMethodPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel2.add(clusteringMethodPanel,
				new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel7 = new JPanel();
		panel7.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		clusteringMethodPanel.add(panel7,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		clusteringMethodComboBox.setEnabled(false);
		panel7.add(clusteringMethodComboBox,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label5 = new JLabel();
		label5.setText("Clustering Method:");
		panel7.add(label5, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		initialClusteringPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel2.add(initialClusteringPanel,
				new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel8 = new JPanel();
		panel8.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		initialClusteringPanel.add(panel8,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel9 = new JPanel();
		panel9.setLayout(new GridLayoutManager(7, 2, new Insets(0, 0, 0, 0), -1, -1));
		panel8.add(panel9,
				new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		useClusteringMethodCheckBox.setEnabled(false);
		useClusteringMethodCheckBox.setSelected(true);
		panel9.add(useClusteringMethodCheckBox,
				new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		numberOfClustersLabel.setText("Number of Clusters:");
		panel9.add(numberOfClustersLabel,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		numberClustersTextField.setEnabled(false);
		panel9.add(numberClustersTextField,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(150, -1), null, 0, false));
		importClusteringCheckBox.setEnabled(false);
		importClusteringCheckBox.setSelected(true);
		panel9.add(importClusteringCheckBox,
				new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		importTableLabel.setText("Table:");
		panel9.add(importTableLabel,
				new GridConstraints(3, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		importClusteringTableComboBox.setEnabled(false);
		panel9.add(importClusteringTableComboBox,
				new GridConstraints(4, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		objectIdColumnLabel.setText("Object Id Column:");
		panel9.add(objectIdColumnLabel,
				new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		importClusteringObjectIdColumnComboBox.setEnabled(false);
		panel9.add(importClusteringObjectIdColumnComboBox,
				new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		clusterIdColumnLabel.setText("Cluster Id Column:");
		panel9.add(clusterIdColumnLabel,
				new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		importClusteringClusterIdColumnComboBox.setEnabled(false);
		panel9.add(importClusteringClusterIdColumnComboBox,
				new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		similarityFunctionPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel2.add(similarityFunctionPanel,
				new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel10 = new JPanel();
		panel10.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		similarityFunctionPanel.add(panel10,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		simFunComboBox.setEnabled(false);
		panel10.add(simFunComboBox,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		columnMappingPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel2.add(columnMappingPanel,
				new GridConstraints(7, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel11 = new JPanel();
		panel11.setLayout(new GridLayoutManager(2, 4, new Insets(0, 0, 0, 0), -1, -1));
		columnMappingPanel.add(panel11,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JLabel label6 = new JLabel();
		label6.setText("Object Id:");
		panel11.add(label6, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label7 = new JLabel();
		label7.setText("Time points:");
		panel11.add(label7, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		objectIdColumnComboBox.setEnabled(false);
		panel11.add(objectIdColumnComboBox,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label8 = new JLabel();
		label8.setText("Replicate:");
		panel11.add(label8, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		replicateColumnComboBox.setEnabled(false);
		panel11.add(replicateColumnComboBox,
				new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JScrollPane scrollPane1 = new JScrollPane();
		panel11.add(scrollPane1,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null,
						0, false));
		timePointsColumnsList.setEnabled(false);
		scrollPane1.setViewportView(timePointsColumnsList);
		pvaluePanel.setLayout(new GridLayoutManager(6, 3, new Insets(0, 0, 0, 0), -1, -1));
		pvaluePanel.setVisible(true);
		panel2.add(pvaluePanel,
				new GridConstraints(9, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		numberPermutationsFirstIterationTextField.setEnabled(false);
		pvaluePanel.add(numberPermutationsFirstIterationTextField,
				new GridConstraints(3, 1, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(150, -1), null, 0, false));
		final JLabel label9 = new JLabel();
		label9.setText("Number of Permutations:");
		pvaluePanel.add(label9, new GridConstraints(2, 0, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label10 = new JLabel();
		label10.setText("Combine p-values:");
		pvaluePanel.add(label10, new GridConstraints(4, 0, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		combinePvaluesComboBox.setEnabled(false);
		pvaluePanel.add(combinePvaluesComboBox,
				new GridConstraints(5, 1, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		permutationMethodComboBox.setEnabled(false);
		pvaluePanel.add(permutationMethodComboBox,
				new GridConstraints(1, 1, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label11 = new JLabel();
		label11.setText("Permutation method:");
		pvaluePanel.add(label11, new GridConstraints(0, 0, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final Spacer spacer1 = new Spacer();
		panel2.add(spacer1, new GridConstraints(10, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
	}
}
