package dk.sdu.imada.ticone.gui.panels.connectivity;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.work.TaskManager;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.IShuffleClustering;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringByRandomlyAssigningObjectsToClusters;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringByShufflingClustersDegreePreserving;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.clustering.pair.ShuffleFirstClusteringOfClusteringPair;
import dk.sdu.imada.ticone.connectivity.ClusterConnectivityTaskFactory;
import dk.sdu.imada.ticone.connectivity.TiconeClusterConnectivityTask.ConnectivityType;
import dk.sdu.imada.ticone.data.ShuffleDatasetGlobally;
import dk.sdu.imada.ticone.data.ShuffleDatasetRowwise;
import dk.sdu.imada.ticone.gui.TiconeClusteringResultsPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.util.CyNetworkComboBox;
import dk.sdu.imada.ticone.gui.util.ShuffleComboBox;
import dk.sdu.imada.ticone.gui.util.VisualClusteringComboBox;
import dk.sdu.imada.ticone.network.IShuffleNetwork;
import dk.sdu.imada.ticone.network.ShuffleNetworkWithEdgeCrossover;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.permute.ShuffleParameter;
import dk.sdu.imada.ticone.permute.ShuffleParameterException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.ITiconeResultChangeListener;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeResultChangeEvent;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * Created by wiwiec on 4/16/16.
 */
public class ClusterConnectivityFormPanel implements ChangeListener, ITiconeResultChangeListener {

	private JPanel mainPanel;
	private JComboBox networkCombobox;
	private JTextField numberNetworkPermutationsTextField;
	private JButton inferButton;
	private JCheckBox undirectedAnalysisCheckbox;
	private JCheckBox directedAnalysisCheckbox;
	private JCheckBox permutationAnalysisCheckbox;
	private JPanel permutationTestPanel;
	private JComboBox<TiconeClusteringResultPanel> clusteringComboBox;
	private JCheckBox twoSidedPermutationTestCheckBox;
	// private JTextField factorEdgeSwapsTextField;
	private JComboBox<IShuffle> permuteComboBox;
	private JPanel permutationMethodParameterPanel;
	private final Map<Field, JTextField> permuteParameterTextFields;

	public JPanel getMainPanel() {
		return this.mainPanel;
	}

	public ClusterConnectivityFormPanel() throws TiconeUnloadingException {
		super();
		this.$$$setupUI$$$();
		GUIUtility.getTiconeClusteringsResultsPanel().addChangeListener(this);
		this.permuteParameterTextFields = new HashMap<>();
	}

	public void updateClusteringJList(final TiconeClusteringResultsPanel resultPanel) {
//		Object oldSelected = clusteringComboBox.getSelectedItem();
//
//		clusteringComboBox.removeAllItems();
//		for (int i = 0; i < resultPanel.getTabCount(); i++) {
//			if (resultPanel.getComponentAt(i) instanceof TiconeClusteringResultPanel) {
//				clusteringComboBox.addItem(((TiconeClusteringResultPanel) resultPanel.getComponentAt(i)));
//			}
//		}
//
//		// setting it to no selection first and then applying optional old
//		// selection ensures that we select nothing if the previously selected
//		// clustering is not in there anymore
//		clusteringComboBox.setSelectedIndex(-1);
//		if (oldSelected != null)
//			clusteringComboBox.setSelectedItem(oldSelected);
	}

	private void createUIComponents() {
		this.clusteringComboBox = new VisualClusteringComboBox();

		this.permutationTestPanel = new JPanel();
		this.inferButton = new JButton();
		this.inferButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					ClusterConnectivityFormPanel.this.inferAction();
				} catch (final TiconeUnloadingException | InterruptedException e1) {
				}
			}
		});
		this.networkCombobox = new CyNetworkComboBox();

		final GridBagLayout gridBagLayout = new GridBagLayout();
		this.permutationMethodParameterPanel = new JPanel(gridBagLayout);

		this.permuteComboBox = new ShuffleComboBox(new IShuffle[] {
				// these two don't make sense here, as they do not keep cluster
				// sizes constant, which heavily bias the number of edges
				// between clusters;
				// new ShufflePrototypeOverTime(), new CreateRandomPrototypes(),
				new ShuffleClusteringByRandomlyAssigningObjectsToClusters(),
				new ShuffleClusteringByShufflingClustersDegreePreserving(), new ShuffleDatasetRowwise(),
				new ShuffleDatasetGlobally(), new ShuffleNetworkWithEdgeCrossover(true),
				new ShuffleNetworkWithEdgeCrossover(false) });
		this.permuteComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				final IShuffle shuffle = (IShuffle) ClusterConnectivityFormPanel.this.permuteComboBox.getSelectedItem();
				final Field[] fieldsWithAnnotation = FieldUtils.getFieldsWithAnnotation(shuffle.getClass(),
						ShuffleParameter.class);
				ClusterConnectivityFormPanel.this.permuteParameterTextFields.clear();
				ClusterConnectivityFormPanel.this.permutationMethodParameterPanel.removeAll();
				int i = 0;
				for (final Field f : fieldsWithAnnotation) {
					final ShuffleParameter anno = f.getAnnotation(ShuffleParameter.class);
					final JLabel label = new JLabel(anno.uiDescription());
					GridBagConstraints c = new GridBagConstraints();
					c.gridx = 0;
					c.gridy = i;
					ClusterConnectivityFormPanel.this.permutationMethodParameterPanel.add(label, c);

					final JTextField tf = new JTextField(anno.uiDefaultValue());
					c = new GridBagConstraints();
					c.gridx = 1;
					c.gridy = i;
					ClusterConnectivityFormPanel.this.permutationMethodParameterPanel.add(tf, c);

					ClusterConnectivityFormPanel.this.permuteParameterTextFields.put(f, tf);

					i++;
				}
				ClusterConnectivityFormPanel.this.permutationMethodParameterPanel.invalidate();
				ClusterConnectivityFormPanel.this.permutationTestPanel.revalidate();
				ClusterConnectivityFormPanel.this.permutationTestPanel.repaint();
			}
		});

		this.numberNetworkPermutationsTextField = new JTextField("1000");
		this.permutationAnalysisCheckbox = new JCheckBox("Calculate p-values with permutation test");
		this.permutationAnalysisCheckbox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				ClusterConnectivityFormPanel.this.permutationTestPanel
						.setVisible(ClusterConnectivityFormPanel.this.permutationAnalysisCheckbox.isSelected());
			}
		});
	}

	private void inferAction() throws TiconeUnloadingException, InterruptedException {
		final CyNetwork network = (CyNetwork) this.networkCombobox.getSelectedItem();
		final TiconeClusteringResultPanel clusteringPanel = (TiconeClusteringResultPanel) this.clusteringComboBox
				.getSelectedItem();
		final IClusters selectedClusters = clusteringPanel.getSelectedClusters();

		int numberPermutations;
		try {
			numberPermutations = Integer.valueOf(this.numberNetworkPermutationsTextField.getText());
		} catch (final Exception e) {
			JOptionPane.showMessageDialog(null, "The number of permutations could not be parsed.");
			return;
		}

		// validate permute parameters
		final IShuffle permutationFunction = (IShuffle) this.permuteComboBox.getSelectedItem();
		for (final Field f : this.permuteParameterTextFields.keySet()) {
			final ShuffleParameter anno = f.getAnnotation(ShuffleParameter.class);
			final String stringValue = this.permuteParameterTextFields.get(f).getText();
			Object value;
			// TODO: other types?
			if (f.getType().equals(double.class) || f.getType().equals(Double.class))
				value = Double.valueOf(stringValue);
			else if (f.getType().equals(ISimilarityFunction.class)) {
				// TODO: other similarity functions?!
				if (stringValue.equals("Pearson Correlation"))
					value = new PearsonCorrelationFunction();
				else {
					value = new InverseEuclideanSimilarityFunction();
				}
			} else {
				JOptionPane.showMessageDialog(null, String.format(
						"The value '%s' for shuffle parameter '%s' could not be parsed. Please enter a valid value.",
						stringValue, anno.uiName()));
				return;
			}

			try {
				f.setAccessible(true);
				f.set(permutationFunction, value);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		try {
			permutationFunction.validateParameters();
		} catch (final ShuffleParameterException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			return;
		}

		if (this.permutationAnalysisCheckbox.isSelected() && numberPermutations < 1) {
			JOptionPane.showMessageDialog(null, "The number of permutations has to be at least 1");
			return;
		}

		if (this.permutationAnalysisCheckbox.isSelected()) {
		}

		final Set<ConnectivityType> connectivityTypes = new HashSet<>();
		if (this.directedAnalysisCheckbox.isSelected())
			connectivityTypes.add(ConnectivityType.DIRECTED);
		if (this.undirectedAnalysisCheckbox.isSelected())
			connectivityTypes.add(ConnectivityType.UNDIRECTED);
		if (this.permutationAnalysisCheckbox.isSelected())
			connectivityTypes.add(ConnectivityType.PERMUTATION_TEST);

		if (connectivityTypes.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Please select at least 1 kind of connectivity analysis.");
			return;
		}

		if (selectedClusters.size() < 2) {
			JOptionPane.showMessageDialog(null, "Please select at least 2 clusters to perform connectivity analysis.");

			// focus one the clustering
			final TiconeClusteringResultsPanel ticoneClusteringsResultsPanel = GUIUtility
					.getTiconeClusteringsResultsPanel();
			((JTabbedPane) ticoneClusteringsResultsPanel.getParent())
					.setSelectedComponent(ticoneClusteringsResultsPanel);
			if (selectedClusters.size() < 1) {
				ticoneClusteringsResultsPanel.setSelectedComponent(clusteringPanel);
				clusteringPanel.setGraphTabInFocus();
			}
			return;
		}

		final ClusterObjectMapping pom = clusteringPanel.getClusteringResult().getClusteringProcess()
				.getLatestClustering();

		ClusterConnectivityTaskFactory clusterConnectivityTaskFactory;

		final TiconeClusteringResultPanel clusteringResultPanel = (TiconeClusteringResultPanel) this.clusteringComboBox
				.getSelectedItem();
		final TiconeCytoscapeClusteringResult clusteringResult = clusteringResultPanel.getClusteringResult();

		final TiconeCytoscapeNetwork ticoneNetwork = new TiconeCytoscapeNetwork(network);

		if (permutationFunction instanceof IShuffleNetwork) {
			clusterConnectivityTaskFactory = new ClusterConnectivityTaskFactory(clusteringResultPanel, network, pom,
					selectedClusters, numberPermutations, connectivityTypes,
					this.twoSidedPermutationTestCheckBox.isSelected(), (IShuffleNetwork) permutationFunction);
		} else if (permutationFunction instanceof IShuffleClustering) {
			final IShuffleClustering shuffleClustering = (IShuffleClustering) permutationFunction;

			if (permutationFunction instanceof ShuffleClusteringByShufflingClustersDegreePreserving) {
				((ShuffleClusteringByShufflingClustersDegreePreserving) permutationFunction).setNetwork(ticoneNetwork);
			}

			clusterConnectivityTaskFactory = new ClusterConnectivityTaskFactory(
					(TiconeClusteringResultPanel) this.clusteringComboBox.getSelectedItem(), network, pom,
					selectedClusters, numberPermutations, connectivityTypes,
					this.twoSidedPermutationTestCheckBox.isSelected(),
					new ShuffleFirstClusteringOfClusteringPair(shuffleClustering));
		} else {
//				if (permutationFunction instanceof CreateRandomPrototypes) {
//					CreateRandomPrototypes s1 = (CreateRandomPrototypes) permutationFunction.newInstance();
//
//					s1.setAllObjects(pom.getAllObjects());
//
//					shuffleClustering = new ShuffleClusteringWithRandomPrototypes(getSimilarityFunction(), s1);
//				} else if (permutationFunction instanceof ShufflePrototypeOverTime) {
//					shuffleClustering = new ShuffleClusteringByShufflingPrototypes(getSimilarityFunction(),
//							new ShufflePrototypeOverTime());
//				} else
//					if (permutationFunction instanceof ShuffleDatasetRowwise
//						|| permutationFunction instanceof ShuffleDatasetGlobally) {
//					IShuffleDataset s1 = (IShuffleDataset) permutationFunction.newInstance();
//
//					shuffleClustering = new ShuffleClusteringByShufflingDataset(clusteringResult.getClusteringMethod(),
//							pom.getClusters().size(), getSimilarityFunction(), s1,
//							clusteringResult.getClusterHistory() == null);
//				} else {
			// TODO
			return;
//				}
		}
//		}

		final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
		taskManager.execute(clusterConnectivityTaskFactory.createTaskIterator());
	}

	@Override
	public void stateChanged(final ChangeEvent e) {
		// clustering has changed
		// if (e.getSource() instanceof TiCoNECytoscapeClusteringResult) {
		// this.updateClusterJList((TiCoNECytoscapeClusteringResult)
		// e.getSource());
		// }
		// different clustering selected
	}

	@Override
	public void resultChanged(TiconeResultChangeEvent changeEvent) {
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
	 * edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(5, 1, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(panel1,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel1.add(panel2,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null,
						new Dimension(446, 79), null, 0, false));
		panel2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Clustering"));
		panel2.add(clusteringComboBox,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label1 = new JLabel();
		label1.setText("Choose clustering for which to analyze connectivity:");
		panel2.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayoutManager(6, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel1.add(panel3,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		panel3.setBorder(BorderFactory.createTitledBorder("Type of connectivity analysis"));
		final JPanel panel4 = new JPanel();
		panel4.setLayout(new GridLayoutManager(6, 2, new Insets(0, 0, 0, 0), -1, -1));
		panel3.add(panel4,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_VERTICAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		undirectedAnalysisCheckbox = new JCheckBox();
		undirectedAnalysisCheckbox.setSelected(true);
		undirectedAnalysisCheckbox.setText("Undirected");
		panel4.add(undirectedAnalysisCheckbox,
				new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(343, 24), null, 0, false));
		directedAnalysisCheckbox = new JCheckBox();
		directedAnalysisCheckbox.setText("Directed");
		panel4.add(directedAnalysisCheckbox,
				new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(343, 24), null, 0, false));
		permutationAnalysisCheckbox.setEnabled(false);
		permutationAnalysisCheckbox.setSelected(true);
		permutationAnalysisCheckbox.setText("Calculate p-values with permutation test");
		permutationAnalysisCheckbox.setVisible(true);
		panel4.add(permutationAnalysisCheckbox,
				new GridConstraints(4, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(343, 24), null, 0, false));
		final JLabel label2 = new JLabel();
		label2.setText("Treat edges in network as undirected");
		panel4.add(label2,
				new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(343, 16), null, 0, false));
		final JLabel label3 = new JLabel();
		label3.setText("Treat edges in network as directed");
		panel4.add(label3,
				new GridConstraints(3, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(343, 16), null, 0, false));
		permutationTestPanel.setLayout(new GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1));
		permutationTestPanel.setVisible(true);
		panel4.add(permutationTestPanel,
				new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null,
						new Dimension(343, 28), null, 0, false));
		final JLabel label4 = new JLabel();
		label4.setText("Number of permutations:");
		permutationTestPanel.add(label4,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		numberNetworkPermutationsTextField.setText("1000");
		permutationTestPanel.add(numberNetworkPermutationsTextField,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(100, -1), null, 0, false));
		twoSidedPermutationTestCheckBox = new JCheckBox();
		twoSidedPermutationTestCheckBox.setSelected(true);
		twoSidedPermutationTestCheckBox.setText("Two-sided permutation test");
		permutationTestPanel.add(twoSidedPermutationTestCheckBox,
				new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		permutationTestPanel.add(permuteComboBox,
				new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		permutationTestPanel.add(permutationMethodParameterPanel,
				new GridConstraints(3, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		inferButton.setText("Analyze Cluster Connectivity");
		panel1.add(inferButton,
				new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(446, 32), null, 0, false));
		final Spacer spacer1 = new Spacer();
		panel1.add(spacer1,
				new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1,
						GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(446, 14), null, 0, false));
		final JPanel panel5 = new JPanel();
		panel5.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel1.add(panel5,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null,
						new Dimension(446, 79), null, 0, false));
		panel5.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Network"));
		panel5.add(networkCombobox,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label5 = new JLabel();
		label5.setText("Choose a network:");
		panel5.add(label5, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return mainPanel;
	}
}
