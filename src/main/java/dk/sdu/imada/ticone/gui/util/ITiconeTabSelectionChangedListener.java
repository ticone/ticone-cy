/**
 * 
 */
package dk.sdu.imada.ticone.gui.util;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 1, 2019
 *
 */
public interface ITiconeTabSelectionChangedListener {
	void tabSelectionChanged(TiconeTabSelectionChangedEvent event);
}
