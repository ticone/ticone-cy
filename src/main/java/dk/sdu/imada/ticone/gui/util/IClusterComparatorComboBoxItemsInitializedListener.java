/**
 * 
 */
package dk.sdu.imada.ticone.gui.util;

import java.io.Serializable;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 27, 2017
 *
 */
public interface IClusterComparatorComboBoxItemsInitializedListener extends Serializable {
	void comboBoxItemsInitialized(final ClusterComparatorComboBoxItemsInitializedEvent e);
}
