package dk.sdu.imada.ticone.gui;

import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.util.TiconeTabManager;

public class TiconeClusteringResultsPanel extends TiconeTabbedResultsPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8628031751238172734L;

	/**
	 * 
	 */
	public TiconeClusteringResultsPanel() {
		super();
		this.addTabSelectionChangedListener(TiconeTabManager.getSingleton());
	}

	@Override
	public String getTitle() {
		return "Clusterings";
	}

	@Override
	public TiconeClusteringResultPanel getSelectedComponent() {
		return (TiconeClusteringResultPanel) super.getSelectedComponent();
	}
}
