package dk.sdu.imada.ticone.gui.panels.clusterchart;

import java.util.Comparator;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.compare.ClusterIDComparator;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.util.IStatusMapping;

public class ClusterChartWithTitleComparator implements Comparator<ClusterChartWithTitle> {

	protected ClusterIDComparator idComparator;

	/**
	 * 
	 */
	public ClusterChartWithTitleComparator() {
		this.idComparator = new ClusterIDComparator();
	}

	@Override
	public int compare(final ClusterChartWithTitle o1, final ClusterChartWithTitle o2) {
		if (o1.getCluster().getName().length() != o2.getCluster().getName().length())
			return o1.getCluster().getName().length() - o2.getCluster().getName().length();
		return o1.getCluster().getName().compareTo(o2.getCluster().getName());
	}

	/**
	 * @return the statusMapping
	 */
	public IStatusMapping<? extends IObjectWithFeatures> getStatusMapping() {
		return this.idComparator.getStatusMapping();
	}

	/**
	 * @return the decreasing
	 */
	public boolean isDecreasing() {
		return this.idComparator.isDecreasing();
	}

	/**
	 * @param decreasing the decreasing to set
	 */
	public ClusterChartWithTitleComparator setDecreasing(final boolean decreasing) {
		this.idComparator.setDecreasing(decreasing);
		return this;
	}

	/**
	 * @param reverseComparator the reverseComparator to set
	 */
	public void setReverseComparator(final boolean reverseComparator) {
		this.idComparator.setReverseComparator(reverseComparator);
	}

	/**
	 * @param statusMapping the statusMapping to set
	 */
	public ClusterChartWithTitleComparator setStatusMapping(IStatusMapping<ICluster> statusMapping) {
		this.idComparator.setStatusMapping(statusMapping);
		return this;
	}
}