/**
 * 
 */
package dk.sdu.imada.ticone.gui.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JComboBox;

import dk.sdu.imada.ticone.clustering.ClusteringChangeEvent;
import dk.sdu.imada.ticone.clustering.IClusteringChangeListener;
import dk.sdu.imada.ticone.clustering.IStatusMappingListener;
import dk.sdu.imada.ticone.clustering.StatusMappingEvent;
import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.clustering.compare.AbstractTiconeComparator;
import dk.sdu.imada.ticone.clustering.compare.ClusterCompareException;
import dk.sdu.imada.ticone.clustering.compare.ClusterIDComparator;
import dk.sdu.imada.ticone.clustering.compare.ClusterPrototypeComparator;
import dk.sdu.imada.ticone.clustering.compare.FeatureComparator;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureInformationContent;
import dk.sdu.imada.ticone.feature.ClusterFeatureMedianSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.ClusterFeaturePrototypeStandardVariance;
import dk.sdu.imada.ticone.feature.FeaturePvalue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.feature.store.INewFeatureStoreListener;
import dk.sdu.imada.ticone.feature.store.NewFeatureStoreEvent;
import dk.sdu.imada.ticone.gui.panels.MyDialogPanel;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 27, 2017
 *
 */
public class ClusterComparatorComboBox extends JComboBox<Comparator<? extends IObjectWithFeatures>>
		implements INewFeatureStoreListener, IClusteringChangeListener, IStatusMappingListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -485875850640077545L;

	protected boolean decreasing;

	protected TiconeClusteringResult clustering;

	protected Set<IClusterComparatorComboBoxItemsInitializedListener> itemsInitializedListener;
	protected Set<IClusterComparatorComboBoxItemsChangedListener> itemsChangedListener;

	public ClusterComparatorComboBox(final TiconeClusteringResult clustering) throws InterruptedException {
		super();
		this.clustering = clustering;
		this.itemsInitializedListener = new HashSet<>();
		this.itemsChangedListener = new HashSet<>();

		this.addComparators();

		this.clustering.addClusteringChangeListener(this);
		this.clustering.addNewFeatureStoreListener(this);
		this.clustering.getClusterStatusMapping().addStatusMappingListener(this);
	}

	/**
	 * @param decreasing the decreasing to set
	 */
	public void setDecreasing(final boolean decreasing) {
		if (this.decreasing == decreasing)
			return;

		this.decreasing = decreasing;

		for (int i = 0; i < this.getItemCount(); i++) {
			final Comparator<?> c = this.getItemAt(i);
			if (c instanceof AbstractTiconeComparator)
				((AbstractTiconeComparator) c).setDecreasing(decreasing);
		}
		this.fireClusterComparatorsChanged(new ClusterComparatorComboBoxItemsChangedEvent(this));
	}

	/**
	 * @return the decreasing
	 */
	public boolean isDecreasing() {
		return this.decreasing;
	}

	private void addComparators() throws InterruptedException {
		final int selectedIndex = this.getSelectedIndex();
		this.removeAllItems();
		final IFeatureStore store = this.clustering.getFeatureStore();
		final List<AbstractTiconeComparator> comps = new ArrayList<>();
		comps.add(new ClusterIDComparator());
		try {
			comps.add(new ClusterPrototypeComparator(
					this.clustering.getClusterHistory().getClusterObjectMapping().getClusters().asList(),
					(ISimilarityFunction) this.clustering.getSimilarityFunction()));
		} catch (final ClusterCompareException e) {
			MyDialogPanel.showMessageDialog("Cluster comparator initialization",
					"The Cluster prototype comparator could not be initialized and will not be available. Error: "
							+ e.getMessage());
		}
		comps.add(new FeatureComparator<>(new FeaturePvalue(ObjectType.CLUSTER), store));
		comps.add(new FeatureComparator<>(new ClusterFeatureNumberObjects(), store));
		try {
			comps.add(new FeatureComparator<>(
					new ClusterFeatureAverageSimilarity(this.clustering.getSimilarityFunction()), store));
		} catch (final IncompatibleSimilarityFunctionException e) {
			MyDialogPanel.showMessageDialog("Cluster comparator initialization",
					"The Cluster prototype comparator could not be initialized and will not be available. Error: "
							+ e.getMessage());
		}
		try {
			comps.add(new FeatureComparator<>(
					new ClusterFeatureMedianSimilarity(this.clustering.getSimilarityFunction()), store));
		} catch (final IncompatibleSimilarityFunctionException e) {
			MyDialogPanel.showMessageDialog("Cluster comparator initialization",
					"The Cluster prototype comparator could not be initialized and will not be available. Error: "
							+ e.getMessage());
		}
		comps.add(new FeatureComparator<>(new ClusterFeaturePrototypeStandardVariance(), store));
		comps.add(new FeatureComparator<>(new ClusterFeatureInformationContent(), store));

		for (final AbstractTiconeComparator comp : comps.toArray(new AbstractTiconeComparator[0])) {
			comp.setReverseComparator(true);
			comp.setStatusMapping(this.clustering.getClusterStatusMapping());
			comp.setDecreasing(this.decreasing);
			this.addItem(comp);
		}
		this.setSelectedIndex(selectedIndex);

		this.fireClusterComparatorsInitialized(new ClusterComparatorComboBoxItemsInitializedEvent(this));
	}

	public void addItemsInitializedListener(final IClusterComparatorComboBoxItemsInitializedListener l) {
		this.itemsInitializedListener.add(l);
	}

	public void removeItemsInitializedListener(final IClusterComparatorComboBoxItemsInitializedListener l) {
		this.itemsInitializedListener.remove(l);
	}

	private void fireClusterComparatorsInitialized(final ClusterComparatorComboBoxItemsInitializedEvent e) {
		for (final IClusterComparatorComboBoxItemsInitializedListener l : this.itemsInitializedListener) {
			l.comboBoxItemsInitialized(e);
		}
	}

	public void addItemsChangedListener(final IClusterComparatorComboBoxItemsChangedListener l) {
		this.itemsChangedListener.add(l);
	}

	public void removeItemsChangedListener(final IClusterComparatorComboBoxItemsChangedListener l) {
		this.itemsChangedListener.remove(l);
	}

	private void fireClusterComparatorsChanged(final ClusterComparatorComboBoxItemsChangedEvent e) {
		for (final IClusterComparatorComboBoxItemsChangedListener l : this.itemsChangedListener) {
			l.comboBoxItemsChanged(e);
		}
	}

	@Override
	public void newFeatureStore(final NewFeatureStoreEvent e) {
		try {
			this.addComparators();
		} catch (InterruptedException e1) {
		}
	}

	@Override
	public void clusteringChanged(final ClusteringChangeEvent e) {
		try {
			this.addComparators();
		} catch (InterruptedException e1) {
		}
	}

	@Override
	public void clusterStatusMappingChanged(final StatusMappingEvent e) {
		try {
			this.addComparators();
		} catch (InterruptedException e1) {
		}
	}
}
