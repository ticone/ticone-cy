/**
 * 
 */
package dk.sdu.imada.ticone.gui.util;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.gui.jtable.AbstractTiconeTableCellRenderer;

public class ObjectIDCellRenderer<OBJ extends IObjectWithFeatures> extends AbstractTiconeTableCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4886636103204486234L;

	public ObjectIDCellRenderer() {
		super();
		this.setUI(new VerticalLabelUI());
	}

	@Override
	public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
			final boolean hasFocus, final int row, final int column) {
		final IObjectWithFeatures object = getObjectFromValue(table, row, column, value);
		if (object == null)
			return new JLabel("ERROR");

		final JLabel c = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		c.setBackground(this.backgroundColor);
		c.setText(object.getName());

		this.setColors(table, object, isSelected, hasFocus, row, column, c);

		return c;
	}
}