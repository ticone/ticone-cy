package dk.sdu.imada.ticone.gui.panels.clusterchart;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;

import dk.sdu.imada.ticone.clustering.ClusterList;
import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ClusterSelectionChangeEvent;
import dk.sdu.imada.ticone.clustering.ClusterSet;
import dk.sdu.imada.ticone.clustering.ClusteringChangeEvent;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.clustering.IClusterSelectionListener;
import dk.sdu.imada.ticone.clustering.IClusterSet;
import dk.sdu.imada.ticone.clustering.IClusteringChangeListener;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.IStatusMappingListener;
import dk.sdu.imada.ticone.clustering.StatusMappingEvent;
import dk.sdu.imada.ticone.clustering.compare.AbstractTiconeComparator;
import dk.sdu.imada.ticone.clustering.compare.ClusterCompareException;
import dk.sdu.imada.ticone.clustering.compare.ClusterIDComparator;
import dk.sdu.imada.ticone.clustering.compare.FeatureComparator;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.ClusterFeatureNumberObjects;
import dk.sdu.imada.ticone.feature.FeaturePvalue;
import dk.sdu.imada.ticone.feature.FeaturePvalueValue;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.store.FeatureStoreChangedEvent;
import dk.sdu.imada.ticone.feature.store.FeatureValuesStoredEvent;
import dk.sdu.imada.ticone.feature.store.FeaturesRemovedEvent;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.feature.store.IFeatureStoreChangedListener;
import dk.sdu.imada.ticone.feature.store.IFeatureStoreFeatureRemovedListener;
import dk.sdu.imada.ticone.feature.store.IFeatureValueStoredListener;
import dk.sdu.imada.ticone.feature.store.INewFeatureStoreListener;
import dk.sdu.imada.ticone.feature.store.NewFeatureStoreEvent;
import dk.sdu.imada.ticone.gui.jtable.AbstractTiconeTableCellRenderer;
import dk.sdu.imada.ticone.gui.jtable.MyComponentCellRenderer;
import dk.sdu.imada.ticone.gui.util.ColumnHeaderToolTips;
import dk.sdu.imada.ticone.gui.util.FeatureCellRenderer;
import dk.sdu.imada.ticone.gui.util.ObjectIDCellRenderer;
import dk.sdu.imada.ticone.gui.util.VerticalTableHeaderCellRenderer;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.similarity.ICompositeSimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.ITimeSeriesSimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.statistics.IPValueResultStorageListener;
import dk.sdu.imada.ticone.statistics.PValueResultStorageEvent;
import dk.sdu.imada.ticone.util.ClusterChartWithButtonsComparator;
import dk.sdu.imada.ticone.util.CyNetworkUtil;
import dk.sdu.imada.ticone.util.IClusterStatusMapping;
import dk.sdu.imada.ticone.util.IStatusMapping;
import dk.sdu.imada.ticone.util.ITiconeResultChangeListener;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeResultChangeEvent;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * Created by christian on 02-02-16.
 */
public class ClusterChartTable implements IClusteringChangeListener, IStatusMappingListener,
		IFeatureStoreChangedListener, INewFeatureStoreListener, TableModelListener, IFeatureValueStoredListener,
		IFeatureStoreFeatureRemovedListener, IPValueResultStorageListener, ITiconeResultChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1698895510905202577L;

	private IClusterList patternList;
	private IClusterStatusMapping clusterStatusMapping;
	private JTable graphTable;
	private MyTableModel graphTableModel;
	private List<ClusterChartContainer> patternGraphPanelContainerList;
	private final JPanel mainPanel;
	private final boolean showOptions;
	private final int columns;

	private Map<Integer, ClusterChartWithButtonsPanel> panelMap;

	protected TiconeClusteringResultPanel resultPanel;
	private ObjectIDCellRenderer<ICluster> patternCellRenderer;
	private FeatureCellRenderer<ICluster> clusterNumberObjectsRenderer, clusterSimilarityRenderer,
			clusterPValueRenderer;
	private MyComponentCellRenderer componentCellRenderer;

	public enum CLUSTER_TABLE_COLUMN {
		CLUSTER_ID, NUMBER_OBJECTS, SIMILARITY, PVALUE, CLUSTER_CHART, CLUSTER_SELECT
	}

	private IClusterSet selectedClusters;
	private Set<IClusterSelectionListener> clusterSelectionListener;

	public ClusterChartTable(final JPanel mainPanel, final boolean showOptions,
			final TiconeClusteringResultPanel resultPanel) throws IncompatibleSimilarityFunctionException {
		super();
		this.mainPanel = mainPanel;
		this.showOptions = showOptions;
		this.graphTableModel = new MyTableModel();
		this.graphTable = new MyTable(this.graphTableModel);
		this.patternList = new ClusterList();
		this.resultPanel = resultPanel;
		this.columns = CLUSTER_TABLE_COLUMN.values().length;
		this.clusterStatusMapping = resultPanel.getClusteringResult().getClusterStatusMapping();
		this.setupJTable();

//		resultPanel.getClusteringResult().addClusteringChangeListener(this);
		resultPanel.getClusteringResult().addNewFeatureStoreListener(this);
		resultPanel.getClusteringResult().addPValueResultStorageListener(this);
		resultPanel.getClusteringResult().getFeatureStore().addFeatureStoreChangeListener(this);
		resultPanel.getClusteringResult().getFeatureStore().addFeatureValueStoredListener(this);
		this.selectedClusters = new ClusterSet();
		this.clusterSelectionListener = new HashSet<>();

		this.addClusterSelectionListener(new IClusterSelectionListener() {

			@Override
			public void clusterSelectionChanged(ClusterSelectionChangeEvent event) {
				try {
					final CyNetwork network = ServiceHelper.getService(CyApplicationManager.class).getCurrentNetwork();
					if (network == null)
						return;
					if (!CyNetworkUtil.isNetworkForCurrentlyVisibleClustering(network))
						return;

					if (!CyNetworkUtil.isClusterSimilarityNetwork(network))
						return;

					final Set<Integer> newlySelectedClusters = event.getNewlySelectedClusters().stream()
							.map(c -> c.getClusterNumber()).collect(Collectors.toSet());
					final Set<Integer> newlyUnselectedClusters = event.getNewlyUnselectedClusters().stream()
							.map(c -> c.getClusterNumber()).collect(Collectors.toSet());

					final CyTable defaultNodeTable = network.getDefaultNodeTable();
					for (CyRow row : defaultNodeTable.getAllRows()) {
						int clusterNumber = row.get("cluster_number", Integer.class);
						if (newlySelectedClusters.contains(clusterNumber))
							row.set(CyNetwork.SELECTED, true);
						if (newlyUnselectedClusters.contains(clusterNumber))
							row.set(CyNetwork.SELECTED, false);
					}
				} catch (TiconeUnloadingException e) {
				}

			}

		});
	}

	public boolean addClusterSelectionListener(final IClusterSelectionListener listener) {
		return this.clusterSelectionListener.add(listener);
	}

	public boolean removeClusterSelectionListener(final IClusterSelectionListener listener) {
		return this.clusterSelectionListener.remove(listener);
	}

	private void fireClusterSelectionChanged(final IClusters newlySelectedClusters,
			final IClusters newlyUnselectedClusters) {
		final ClusterSelectionChangeEvent event = new ClusterSelectionChangeEvent(
				resultPanel.getClusteringResult().getClusteringProcess().getLatestClustering(), newlySelectedClusters,
				newlyUnselectedClusters);
		for (IClusterSelectionListener l : this.clusterSelectionListener)
			l.clusterSelectionChanged(event);
	}

	public static int getClusterTableColumnIndex(final CLUSTER_TABLE_COLUMN column) {
		switch (column) {
		case CLUSTER_ID:
			return getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_SELECT) + 1;
		case NUMBER_OBJECTS:
			return getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID) + 1;
		case SIMILARITY:
			return getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.NUMBER_OBJECTS) + 1;
		case PVALUE:
			return getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.SIMILARITY) + 1;
		case CLUSTER_CHART:
			return getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.PVALUE) + 1;
		case CLUSTER_SELECT:
			return 0;
		default:
			return -1;
		}
	}

	public int getClusterTableColumnWidth(final CLUSTER_TABLE_COLUMN column) {
		switch (column) {
		case CLUSTER_ID:
			return 36;
		case NUMBER_OBJECTS:
			return 48;
		case SIMILARITY:
			return 48;
		case PVALUE:
			return 48;
		case CLUSTER_CHART:
			return 250;
		case CLUSTER_SELECT:
			return 36;
		default:
			return -1;
		}
	}

	protected void updateClusterStatusMapping(final IClusterStatusMapping csm) {
		if (this.clusterStatusMapping != null)
			this.clusterStatusMapping.removeStatusMappingListener(this);
		this.clusterStatusMapping = csm;
		this.clusterStatusMapping.addStatusMappingListener(this);

		this.clusterNumberObjectsRenderer.setFeatureStore(csm.getFeatureStore());
		this.clusterSimilarityRenderer.setFeatureStore(csm.getFeatureStore());
		this.clusterPValueRenderer.setFeatureStore(csm.getFeatureStore());
	}

	@Override
	public void tableChanged(TableModelEvent e) {
		if (e.getType() == TableModelEvent.UPDATE
				&& e.getColumn() == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_SELECT)) {
			final IClusters newlySelectedClusters = new ClusterSet(), newlyUnselectedClusters = new ClusterSet();
			for (int i = e.getFirstRow(); i <= e.getLastRow(); i++) {
				final ICluster cluster = (ICluster) graphTableModel.getValueAt(i,
						getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID));
				boolean isSelected = (Boolean) graphTableModel.getValueAt(i, e.getColumn());
				if (isSelected) {
					if (selectedClusters.add(cluster))
						newlySelectedClusters.add(cluster);
				} else {
					if (selectedClusters.remove(cluster))
						newlyUnselectedClusters.add(cluster);
				}
			}
			if (!newlySelectedClusters.isEmpty() || !newlyUnselectedClusters.isEmpty())
				this.fireClusterSelectionChanged(newlySelectedClusters, newlyUnselectedClusters);
		}
	}

	private void addListenerToGraphModel() {
		this.graphTableModel.addTableModelListener(this);

	}

	public void updateGraphTable(final IClusterStatusMapping patternStatusMapping) throws InterruptedException {
		final Runnable r = new Runnable() {
			@Override
			public void run() {
				try {

					List<? extends SortKey> sortKeys = null;
					if (graphTable != null && graphTable.getRowSorter() != null) {
						sortKeys = graphTable.getRowSorter().getSortKeys();
					}

					graphTableModel = new MyTableModel();
					graphTable = new JTable(graphTableModel);
					setupJTable();
					panelMap = new HashMap<>();
					updateClusterStatusMapping(patternStatusMapping);
					patternList = new ClusterList();

					for (final ICluster c : patternStatusMapping.getClustersToShow())
						patternList.add(c);

					ISimilarityFunction similarityFunction = resultPanel.getClusteringResult().getSimilarityFunction();
					if (similarityFunction instanceof ICompositeSimilarityFunction) {
						for (final ISimilarityFunction childSF : ((ICompositeSimilarityFunction) similarityFunction)
								.getSimilarityFunctions()) {
							if (childSF instanceof ITimeSeriesSimilarityFunction
									&& childSF instanceof ISimilarityFunction)
								similarityFunction = (ISimilarityFunction) childSF;
						}
					}

					patternGraphPanelContainerList = new ArrayList<>();

					// Set number of rows to be the number of patterns to show
					final int rows = patternList.size();
					graphTableModel = new MyTableModel(rows, columns);
					graphTable.setModel(graphTableModel);
					addListenerToGraphModel();
					final TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(graphTableModel) {
						@Override
						public boolean isSortable(final int columnIndex) {
							if (columnIndex == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_SELECT)) {
								return false;
							}
							return true;
						}

						@Override
						protected void fireSortOrderChanged() {
							for (int i = 0; i < this.getModel().getColumnCount(); i++) {
								final boolean isDecreasing = this.getSortKeys().get(0).getSortOrder()
										.equals(SortOrder.DESCENDING);
								if (this.getComparator(i) instanceof AbstractTiconeComparator)
									((AbstractTiconeComparator) this.getComparator(i)).setDecreasing(isDecreasing);
							}
							super.fireSortOrderChanged();
						}
					};
					sorter.setComparator(getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID),
							new ClusterIDComparator().setStatusMapping(clusterStatusMapping));
					sorter.setComparator(getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.NUMBER_OBJECTS),
							new FeatureComparator(new ClusterFeatureNumberObjects(),
									resultPanel.getClusteringResult().getFeatureStore())
											.setStatusMapping(clusterStatusMapping));
					sorter.setComparator(getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.SIMILARITY),
							new FeatureComparator(
									new ClusterFeatureAverageSimilarity(
											resultPanel.getClusteringResult().getSimilarityFunction()),
									resultPanel.getClusteringResult().getFeatureStore())
											.setStatusMapping(clusterStatusMapping));
					sorter.setComparator(getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.PVALUE),
							new FeatureComparator(new FeaturePvalue(ObjectType.CLUSTER),
									resultPanel.getClusteringResult().getFeatureStore())
											.setStatusMapping(clusterStatusMapping));
					try {
						sorter.setComparator(getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_CHART),
								new ClusterChartWithButtonsComparator(patternList,
										(ISimilarityFunction) similarityFunction)
												.setStatusMapping(clusterStatusMapping));
					} catch (ClusterCompareException e1) {
						e1.printStackTrace();
					}

					graphTable.setRowSorter(sorter);
					setupTableHeaders();
					graphTable.getTableHeader().addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(final MouseEvent e) {
							super.mouseClicked(e);
							try {
								final int col = ClusterChartTable.this.graphTable.columnAtPoint(e.getPoint());
								if (col == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_SELECT)) {
									// we always deselect all filtered clusters
									for (int row = 0; row < ClusterChartTable.this.graphTable.getRowCount(); row++) {
										final ICluster c = (ICluster) ClusterChartTable.this.graphTable.getValueAt(row,
												getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID));
										if (ClusterChartTable.this.clusterStatusMapping.getFiltered().contains(c))
											ClusterChartTable.this.graphTable.setValueAt(false, row, col);
									}

									// count how many are selected
									final boolean allSelected = allRowsSelected(ClusterChartTable.this.graphTable, col,
											ClusterChartTable.this.clusterStatusMapping);
									// if not all are selected, we select all, otherwise
									// none
									for (int row = 0; row < ClusterChartTable.this.graphTable.getRowCount(); row++) {
										final ICluster c = (ICluster) ClusterChartTable.this.graphTable.getValueAt(row,
												getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID));
										if (!ClusterChartTable.this.clusterStatusMapping.getFiltered().contains(c))
											ClusterChartTable.this.graphTable.setValueAt(!allSelected, row, col);
									}
								}
								ClusterChartTable.this.graphTable.repaint();
							} catch (final Exception ex) {
								ex.printStackTrace();
							}
						}
					});

					graphTableModel.addTableModelListener(new TableModelListener() {

						@Override
						public void tableChanged(TableModelEvent e) {
							if (e.getColumn() == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_SELECT)) {
								graphTable.getColumnModel()
										.getColumn(getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_SELECT))
										.setHeaderValue(getSelectedHeaderValue());
								graphTable.getTableHeader().repaint();
							}
						}
					});

					graphTableModel.setRowCount(0);
					graphTableModel.setRowCount(rows);

					// Add all paterns to the table
					addRowsToTable();

					updatePanel();

					if (sortKeys != null && !sortKeys.isEmpty())
						graphTable.getRowSorter().setSortKeys(sortKeys);
					else {
						// by default we sort by the cluster ID
						// this also ensures, that filtered clusters are always shown at the
						// lower end of the table
						graphTable.getRowSorter().setSortKeys(Arrays.asList(new SortKey(
								getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID), SortOrder.ASCENDING)));
					}

				} catch (InterruptedException e) {
				} catch (IncompatibleSimilarityFunctionException e2) {
				}
			}
		};
		if (EventQueue.isDispatchThread())
			r.run();
		else {
			try {
				EventQueue.invokeAndWait(r);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}

	protected static boolean allRowsSelected(final JTable resultTable, final int column, final IStatusMapping csm) {
		boolean result = true;
		for (int row = 0; row < resultTable.getRowCount(); row++) {
			final ICluster c = (ICluster) resultTable.getValueAt(row,
					getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID));
			if (csm.getFiltered().contains(c))
				continue;
			result &= (boolean) resultTable.getValueAt(row, column);
		}
		return result;
	}

	protected static int numberRowsSelected(final JTable resultTable, final int column, final IStatusMapping csm) {
		int result = 0;
		for (int row = 0; row < resultTable.getRowCount(); row++) {
			final ICluster c = (ICluster) resultTable.getValueAt(row,
					getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID));
			if (csm.getFiltered().contains(c))
				continue;
			Object valueAt = resultTable.getValueAt(row, column);
			if (valueAt == null)
				continue;
			if ((boolean) valueAt)
				result++;
		}
		return result;
	}

	public void updatePanel() throws InterruptedException {
		final Runnable r = new Runnable() {
			@Override
			public void run() {
				mainPanel.removeAll();
				final JScrollPane scrollPane = new JScrollPane(graphTable);
				mainPanel.add(scrollPane);
				mainPanel.repaint();
				mainPanel.revalidate();
			}
		};
		if (EventQueue.isDispatchThread())
			r.run();
		else {
			try {
				EventQueue.invokeAndWait(r);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @return the graphTable
	 */
	public JTable getTable() {
		return this.graphTable;
	}

	private void setupJTable() throws IncompatibleSimilarityFunctionException {
		addListenerToGraphModel();

		this.graphTableModel.setColumnCount(this.columns);

		// Setup cell renderers
		this.setupCellEditorAndRenderer();
		this.setupTableHeaders();

		this.graphTable.getTableHeader().setReorderingAllowed(false);
		this.graphTable.setAutoCreateColumnsFromModel(false);

		this.graphTable.setRowHeight(165);
		this.graphTable.setShowVerticalLines(true);
		this.graphTable.setShowHorizontalLines(true);
		this.graphTable.setFocusable(true);
		this.graphTable.setRowSelectionAllowed(false);
		this.graphTable.setCellSelectionEnabled(true);
		this.graphTable.setAutoCreateRowSorter(true);

		this.graphTable.getActionMap().put("copy", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -1215993205312427877L;

			@Override
			public void actionPerformed(ActionEvent e) {
				int[] selectedRows = graphTable.getSelectedRows();
				int[] selectedColumns = graphTable.getSelectedColumns();

				final IFeatureStore featureStore = resultPanel.getClusteringResult().getFeatureStore();

				final StringBuilder sb = new StringBuilder();

				// header
				for (int c : selectedColumns) {
					TableCellRenderer cellRenderer = graphTable.getCellRenderer(0, c);
					if (cellRenderer instanceof FeatureCellRenderer) {
						final IFeature feature = ((FeatureCellRenderer) cellRenderer).getFeature();
						sb.append(feature.getName());
						sb.append("\t");
					} else if (c == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID)) {
						sb.append("Cluster Number\t");
					}
				}
				sb.append("\n");

				for (int r : selectedRows) {
					for (int c : selectedColumns) {
						TableCellRenderer cellRenderer = graphTable.getCellRenderer(r, c);
						if (cellRenderer instanceof FeatureCellRenderer) {
							final IFeature feature = ((FeatureCellRenderer) cellRenderer).getFeature();
							final IObjectWithFeatures object = (IObjectWithFeatures) graphTable.getValueAt(r, c);
							if (featureStore.hasFeatureFor(feature, object)) {
								final IFeatureValue featureValue = featureStore.getFeatureValue(object, feature);
								sb.append(featureValue);
							}
							sb.append("\t");
						} else if (c == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID)) {
							sb.append(((ICluster) graphTable.getValueAt(r, c)).getClusterNumber());
							sb.append("\t");
						}
					}
					sb.append("\n");
				}

				StringSelection stringSelection = new StringSelection(sb.toString());
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, stringSelection);
			}
		});

//		this.graphTable.getActionMap().setParent(null);
//		this.graphTable.getInputMap().setParent(null);

		// graphTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	}

	private String getSelectedHeaderValue() {
		int nSelected = numberRowsSelected(graphTable, getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_SELECT),
				this.clusterStatusMapping);
		return getSelectedHeaderValue(nSelected);
	}

	private String getSelectedHeaderValue(int nSelected) {
		String selectedHeader;
		if (this.clusterStatusMapping.isFilterActive()) {
			selectedHeader = String.format("<html>Selected<br>(%d / %d)</html>", nSelected,
					this.patternList.size() - this.clusterStatusMapping.getFiltered().size());
		} else {
			selectedHeader = String.format("<html>Selected<br>(%d / %d)</html>", nSelected, this.patternList.size());
		}
		return selectedHeader;
	}

	private void setupTableHeaders() {
		// Setup headers and column width
		final int[] width = new int[] { 36, 36, 48, 48, 48, 250 };

		final String clusterIdHeader, selectedHeader = getSelectedHeaderValue(0);
		if (this.clusterStatusMapping.isFilterActive()) {
			clusterIdHeader = String.format("<html>Cluster ID<br>(%d / %d)</html>",
					this.patternList.size() - this.clusterStatusMapping.getFiltered().size(), this.patternList.size());
		} else {
			clusterIdHeader = String.format("<html>Cluster ID<br>(%d)</html>", this.patternList.size());
		}

		final String[] headers = new String[] {
				selectedHeader, clusterIdHeader, String.format("<html>Objects<br>(%d)</html>", this.resultPanel
						.getClusteringResult().getClusterHistory().getClusterObjectMapping().getAllObjects().size()),
				"Similarity", "p", "Prototype" };

		final TableColumnModel columnModel = this.graphTable.getColumnModel();
		for (int i = 0; i < width.length; i++) {
			this.graphTable.getColumnModel().getColumn(i).setHeaderValue(headers[i]);
			this.graphTable.getColumnModel().getColumn(i).setPreferredWidth(width[i]);
			this.graphTable.getColumnModel().getColumn(i).setMinWidth(width[i]);
			final TableColumn tableColumn = columnModel.getColumn(i);
			if (i == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID)) {
				tableColumn.setCellRenderer(this.patternCellRenderer);
				tableColumn.setCellEditor(this.patternCellRenderer);
			} else if (i == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_CHART)) {
				tableColumn.setCellRenderer(this.componentCellRenderer);
				tableColumn.setCellEditor(this.componentCellRenderer);
			} else if (i == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.NUMBER_OBJECTS)) {
				tableColumn.setCellRenderer(this.clusterNumberObjectsRenderer);
				tableColumn.setCellEditor(this.clusterNumberObjectsRenderer);
			} else if (i == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.SIMILARITY)) {
				tableColumn.setCellRenderer(this.clusterSimilarityRenderer);
				tableColumn.setCellEditor(this.clusterSimilarityRenderer);
			} else if (i == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.PVALUE)) {
				tableColumn.setCellRenderer(this.clusterPValueRenderer);
				tableColumn.setCellEditor(this.clusterPValueRenderer);
			}
		}

		// Set pattern number and #objects columns max width
		this.graphTable.getColumnModel().getColumn(getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID))
				.setMaxWidth(40);
		this.graphTable.getColumnModel().getColumn(getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID))
				.setMaxWidth(40);

		if (this.showOptions) {
			// Set checkboxes max width
			this.graphTable.getColumnModel().getColumn(getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_SELECT))
					.setMaxWidth(30);
		}

		// Set vertical header labels
		final TableCellRenderer headerRenderer = new VerticalTableHeaderCellRenderer();
		final Enumeration<TableColumn> columnEnum = this.graphTable.getColumnModel().getColumns();
		while (columnEnum.hasMoreElements()) {
			columnEnum.nextElement().setHeaderRenderer(headerRenderer);
		}

		// Update the similarity header
		if (this.resultPanel.getClusteringResult().getClusteringProcess() != null) {
			this.updateSimilarityHeader(
					this.resultPanel.getClusteringResult().getClusteringProcess().getAverageObjectClusterSimilarity());
		}

		this.setupTableHeaderToolTips();

	}

	private void updateSimilarityHeader(final ISimilarityValue sim) {
		if (sim != null) {
			final TableColumnModel columnModel = this.graphTable.getColumnModel();
			final TableColumn column = columnModel
					.getColumn(getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.SIMILARITY));
			try {
				final double avgPearson = sim.get();
				if ((Double.isInfinite(avgPearson) || Double.isNaN(avgPearson) || this.patternList.size() <= 0)) {
				} else {
					column.setHeaderValue(
							"<html>Similarity<br>(avg: " + String.format("%.3G", avgPearson) + ")</html>");
					this.graphTable.getTableHeader().repaint();
				}
			} catch (SimilarityCalculationException e) {
				column.setHeaderValue("<html>Similarity<br>(avg: --)</html>");
				this.graphTable.getTableHeader().repaint();
			}
		}
	}

	private void updatePvalueHeader(final FeaturePvalueValue pval) {
		if (pval != null) {
			final TableColumnModel columnModel = this.graphTable.getColumnModel();
			final TableColumn column = columnModel.getColumn(getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.PVALUE));
			final double p = pval.getValue().getDouble();
			if ((Double.isInfinite(p) || Double.isNaN(p) || this.patternList.size() <= 0)) {
			} else {
				column.setHeaderValue("<html>p<br>(" + String.format("%.3G", p) + ")</html>");
				this.graphTable.getTableHeader().repaint();
			}
		}
	}

	private void setupTableHeaderToolTips() {
		final JTableHeader header = this.graphTable.getTableHeader();
		final ColumnHeaderToolTips tips = new ColumnHeaderToolTips();
		String clusterIdTooltip;
		if (this.clusterStatusMapping.isFilterActive())
			clusterIdTooltip = String.format(
					"<html>The cluster IDs.<br>Number of shown clusters after filtering: %d<br>Total number of clusters: %d</html>",
					this.patternList.size() - this.clusterStatusMapping.getFiltered().size(), this.patternList.size());
		else
			clusterIdTooltip = String.format(
					"<html>The IDs of clusters.<br>Number of shown clusters after filtering: No filter active<br>Total number of clusters: %d</html>",
					this.patternList.size());
		final String[] columnToolTips = new String[] {
				"Select a subset of clusters to perform further analysis, such as cluster connectivity or clustering comparison analyses.",
				clusterIdTooltip,
				String.format("<html>Number of objects assigned to cluster<br/>Total number of objects: %d</html>",
						this.resultPanel.getClusteringResult().getClusterHistory().getClusterObjectMapping()
								.getAllObjects().size()), // # Objects
				"The average similarity of objects to their assigned prototype",
				"Probability of observing a cluster at random with the same feature quality or better (e.g., equal or larger average similarity)", // p-value
				"<html>A chart of the cluster with options to:<br>" + "1. Zoom in on the graph.<br>"
						+ "2. Keep the cluster prototype for next iteration. (No refinement will be performed).<br>"
						+ "3. Delete cluster, the objects assigned to the cluster, both or the least fitting objects.<br>"
						+ "4. Split the cluster; with a new clustering, or based on the two least similar objects.</html>"

		};

		for (int c = 0; c < this.graphTable.getColumnCount(); c++) {
			final TableColumn col = this.graphTable.getColumnModel().getColumn(c);
			tips.setToolTip(col, columnToolTips[c]);
		}
		header.addMouseMotionListener(tips);
	}

	private void setupCellEditorAndRenderer() throws IncompatibleSimilarityFunctionException {
		this.patternCellRenderer = new ObjectIDCellRenderer();
		this.componentCellRenderer = new MyComponentCellRenderer();

		this.clusterNumberObjectsRenderer = new FeatureCellRenderer(new ClusterFeatureNumberObjects(),
				this.resultPanel.getClusteringResult().getFeatureStore());
		this.clusterSimilarityRenderer = new FeatureCellRenderer(
				new ClusterFeatureAverageSimilarity(this.resultPanel.getClusteringResult().getSimilarityFunction()),
				this.resultPanel.getClusteringResult().getFeatureStore());
		this.clusterPValueRenderer = new FeatureCellRenderer(new FeaturePvalue(ObjectType.CLUSTER),
				this.resultPanel.getClusteringResult().getFeatureStore());
	}

	private void addRowsToTable() throws InterruptedException {
		for (final AbstractTiconeTableCellRenderer r : new AbstractTiconeTableCellRenderer[] { this.patternCellRenderer,
				this.componentCellRenderer, this.clusterNumberObjectsRenderer, this.clusterSimilarityRenderer,
				this.clusterPValueRenderer }) {
			r.setDeletedObjects(this.clusterStatusMapping.getDeleted());
			r.setNewObjects(this.clusterStatusMapping.getNew());
			r.setFilteredObjects(this.clusterStatusMapping.getFiltered());
		}
		int row = 0;

		for (int i = 0; i < this.patternList.size(); i++) {
			final ICluster pattern = this.patternList.get(i);
			if (this.clusterStatusMapping.getSplitClusters().contains(pattern)) {
				continue;
			}
			this.addRowToTable(pattern, pattern.getObjects(), row);
			row++;
			final IClusters children = this.clusterStatusMapping.getChildren(pattern);
			if (children != null) {
				for (final ICluster child : children) {
					this.addRowToTable(child, child.getObjects(), row);
					row++;
				}
			}
		}
	}

	private void addRowToTable(final ICluster cluster, final ITimeSeriesObjectList patternsDataList, final int row)
			throws InterruptedException {
		boolean deletedCluster = false;
		boolean newCluster = false;
		if (this.clusterStatusMapping.getDeleted().contains(cluster)) {
			deletedCluster = true;
		} else if (this.clusterStatusMapping.getNew().contains(cluster)) {
			newCluster = true;
		}
		this.graphTable.setValueAt(cluster, row, getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID));

		this.graphTable.setValueAt(cluster, row, getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.NUMBER_OBJECTS));
		this.graphTable.setValueAt(cluster, row, getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.PVALUE));
		this.graphTable.setValueAt(cluster, row, getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.SIMILARITY));

		this.graphTable.setValueAt(false, row, getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_SELECT));

		// Setup new PatternGraphPanel
		ClusterChartWithButtonsPanel graphOptionsPanel;
		try {
			graphOptionsPanel = new ClusterChartWithButtonsPanel(cluster, patternsDataList,
					this.showOptions && !newCluster && !deletedCluster, this.resultPanel);
			this.panelMap.put(row, graphOptionsPanel);
			this.patternGraphPanelContainerList.add(graphOptionsPanel.getPatternGraphPanelContainer());
			this.graphTable.setValueAt(graphOptionsPanel, row,
					getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_CHART));
		} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
			this.patternGraphPanelContainerList.add(null);
			// TODO: add placeholder panel?
		}

	}

	/**
	 * 
	 * @return
	 * @deprecated use {@link #getSelectedClusters()} instead.
	 */
	@Deprecated
	public IClusterList getVisualizePatterns() {
		return new ClusterList(this.getSelectedClusters());
	}

	/**
	 * 
	 * @return
	 * @deprecated use {@link #getSelectedClusters()} instead.
	 */
	@Deprecated
	public IClusterList getMergePatterns() {
		return new ClusterList(this.getSelectedClusters());
	}

	public IClusterList getSelectedClusters() {
		return selectedClusters.asList();
	}

	public void setSelectedClusters(final IClusterList selectedClusters) {
		final IClusterSet newlySelected = selectedClusters.asSet();
		newlySelected.removeAll(this.selectedClusters);

		final IClusterSet newlyUnselected = this.selectedClusters.copy();
		newlyUnselected.removeAll(selectedClusters);

		if (newlySelected.isEmpty() && newlyUnselected.isEmpty())
			return;
		Object[] values = new Object[newlySelected.size() + newlyUnselected.size()];
		int[] rows = new int[values.length];
		int column = getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_SELECT);
		int c = 0;
		for (int row = 0; row < this.graphTable.getRowCount(); row++) {
			final ICluster rowCluster = (ICluster) this.graphTable.getValueAt(row,
					getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID));
			if (newlySelected.contains(rowCluster)) {
				values[c] = true;
				rows[c] = row;
				c++;
			} else if (newlyUnselected.contains(rowCluster)) {
				values[c] = false;
				rows[c] = row;
				c++;
			}
		}

		this.graphTableModel.setValuesAt(values, rows, column);
	}

	/**
	 * 
	 * @return
	 * @deprecated use {@link #getSelectedClusters()} instead.
	 */
	@Deprecated
	public IClusters getKPMPatterns() {
		return new ClusterList(this.getSelectedClusters());
	}

	public IClusters getPatternList() {
		return this.patternList;
	}

	@Override
	public void clusteringChanged(final ClusteringChangeEvent e) {
		try {
			updateGraphTable(clusterStatusMapping);
		} catch (InterruptedException e1) {
		}
	}

	@Override
	public void clusterStatusMappingChanged(final StatusMappingEvent e) {
		try {
			updateGraphTable(clusterStatusMapping);
		} catch (InterruptedException e1) {
		}
	}

	@Override
	public void featureStoreChanged(final FeatureStoreChangedEvent e) {
		try {
			updateGraphTable(clusterStatusMapping);
		} catch (InterruptedException e1) {
		}
	}

	@Override
	public void newFeatureStore(final NewFeatureStoreEvent e) {
		if (e.getOldStore() != null) {
			e.getOldStore().removeFeatureStoreChangeListener(this);
			e.getOldStore().removeFeatureValueStoredListener(this);
		}
		e.getNewStore().addFeatureStoreChangeListener(this);
		e.getNewStore().addFeatureValueStoredListener(this);

		this.selectedClusters = new ClusterSet();
	}

	class MyTable extends JTable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4517314061614079025L;

		public MyTable() {
			super();
		}

		public MyTable(int numRows, int numColumns) {
			super(numRows, numColumns);
		}

		public MyTable(Object[][] rowData, Object[] columnNames) {
			super(rowData, columnNames);
		}

		public MyTable(TableModel dm, TableColumnModel cm, ListSelectionModel sm) {
			super(dm, cm, sm);
		}

		public MyTable(TableModel dm, TableColumnModel cm) {
			super(dm, cm);
		}

		public MyTable(TableModel dm) {
			super(dm);
		}

		public MyTable(Vector rowData, Vector columnNames) {
			super(rowData, columnNames);
		}

		@Override
		protected boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed) {
			return super.processKeyBinding(ks, e, condition, pressed);
		}

		@Override
		protected void processKeyEvent(KeyEvent e) {
			super.processKeyEvent(e);
		}
	}

	class MyTableModel extends DefaultTableModel {
		/**
		 * 
		 */
		private static final long serialVersionUID = -6906803337351280282L;

		public MyTableModel() {
			super();
		}

		public MyTableModel(int rowCount, int columnCount) {
			super(rowCount, columnCount);
		}

		public MyTableModel(Object[] columnNames, int rowCount) {
			super(columnNames, rowCount);
		}

		public MyTableModel(Object[][] data, Object[] columnNames) {
			super(data, columnNames);
		}

		public MyTableModel(Vector columnNames, int rowCount) {
			super(columnNames, rowCount);
		}

		public MyTableModel(Vector data, Vector columnNames) {
			super(data, columnNames);
		}

		@Override
		public Class getColumnClass(final int columnIndex) {
			if (columnIndex == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_SELECT)) {
				return Boolean.class;
			} else if (columnIndex == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_ID)) {
				return ICluster.class;
			} else if (columnIndex == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.NUMBER_OBJECTS)) {
				return ICluster.class;
			} else if (columnIndex == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.SIMILARITY)) {
				return ICluster.class;
			} else if (columnIndex == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.PVALUE)) {
				return ICluster.class;
			} else if (columnIndex == getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.CLUSTER_CHART)) {
				return ClusterChartWithButtonsPanel.class;
			}
			return Object.class;
		}

		public void setValuesAt(Object[] values, int[] rows, int column) {
			if (values.length != rows.length)
				throw new IllegalArgumentException("Arguments must have the same length");
			for (int i = 0; i < values.length; i++) {
				Object aValue = values[i];
				int row = rows[i];
				Vector rowVector = (Vector) dataVector.elementAt(row);
				rowVector.setElementAt(aValue, column);
			}
			fireTableChanged(
					new TableModelEvent(graphTableModel, 0, Integer.MAX_VALUE, column, TableModelEvent.UPDATE));
		}
	}

	private void updatePValues() {
		updatePValues(null);
	}

	private void updatePValues(final Collection<? extends IObjectWithFeatures> clusters) {
		final int column = getClusterTableColumnIndex(CLUSTER_TABLE_COLUMN.PVALUE);
		for (int r = 0; r < graphTableModel.getRowCount(); r++) {
			final ICluster cluster = (ICluster) graphTableModel.getValueAt(r, column);

			if (clusters == null || clusters.contains(cluster))
				graphTableModel.fireTableCellUpdated(r, column);
		}
	}

	@Override
	public void featureValuesStored(FeatureValuesStoredEvent e) {
		IFeature<?> f = clusterPValueRenderer.getFeature();
		if (e.getObjectFeaturePairs().containsKey(f)) {
			final Collection<IObjectWithFeatures> clusters = e.getObjectFeaturePairs()
					.get(clusterPValueRenderer.getFeature());
			updatePValues(clusters);
		}

		final ClusterObjectMapping latestClustering = this.resultPanel.getClusteringResult().getClusteringProcess()
				.getLatestClustering();

		try {
			f = new ClusterFeatureAverageSimilarity(this.resultPanel.getClusteringResult().getSimilarityFunction());
			if (e.getObjectFeaturePairs().containsKey(f)
					&& e.getObjectFeaturePairs().get(f).contains(latestClustering)) {
				updateSimilarityHeader(this.resultPanel.getClusteringResult().getClusteringProcess()
						.getAverageObjectClusterSimilarity());
			}
		} catch (IncompatibleSimilarityFunctionException e1) {
			e1.printStackTrace();
		}

		f = new FeaturePvalue(ObjectType.CLUSTERING);
		final IFeatureStore featureStore = this.resultPanel.getClusteringResult().getFeatureStore();
		if (featureStore.hasFeatureFor(f, latestClustering))
			updatePvalueHeader((FeaturePvalueValue) featureStore.getFeatureValue(latestClustering, f));
	}

	@Override
	public void storeFeatureRemoved(FeaturesRemovedEvent e) {
		final Collection<IFeature<?>> removedFeatures = e.getRemovedFeatures();
		if (removedFeatures.contains(clusterPValueRenderer.getFeature()))
			updatePValues();
	}

	@Override
	public void pvalueCalculationResultStored(PValueResultStorageEvent event) {
		if (event.getPredecessor() != null)
			event.getPredecessor().removeChangeListener(this);
		if (event.getPvalueResult() != null)
			event.getPvalueResult().addChangeListener(this);

		updatePValues();
	}

	@Override
	public void resultChanged(TiconeResultChangeEvent changeEvent) {
		updatePValues();
	}
}
