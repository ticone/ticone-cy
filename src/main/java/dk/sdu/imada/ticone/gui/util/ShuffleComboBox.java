package dk.sdu.imada.ticone.gui.util;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;

import dk.sdu.imada.ticone.permute.IShuffle;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 26, 2018
 *
 */
public class ShuffleComboBox extends JComboBox<IShuffle> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5447854046592049243L;

	public ShuffleComboBox() {
		super();
		this.setRenderer(new ShuffleListCellRenderer());
	}

	public ShuffleComboBox(final IShuffle[] items) {
		super(items);
		this.setRenderer(new ShuffleListCellRenderer());
	}

}

class ShuffleListCellRenderer extends DefaultListCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 419716065998393007L;

	@Override
	public Component getListCellRendererComponent(final JList<?> list, Object value, final int index,
			final boolean isSelected, final boolean cellHasFocus) {
		if (value != null) {
			value = ((IShuffle) value).getName();
		} else {
			value = "";
		}
		return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}
}