package dk.sdu.imada.ticone.gui.panels.comparison;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.apache.commons.lang3.tuple.Triple;
import org.jfree.chart.ChartPanel;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import dk.sdu.imada.ticone.clustering.AbstractNamedTiconeResult;
import dk.sdu.imada.ticone.clustering.ClusterList;
import dk.sdu.imada.ticone.clustering.ClusteringWithIterationLabel;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.IStatusMappingListener;
import dk.sdu.imada.ticone.clustering.StatusMappingEvent;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.clustering.compare.AbstractTiconeComparator;
import dk.sdu.imada.ticone.clustering.compare.FeatureComparator;
import dk.sdu.imada.ticone.clustering.filter.FilterEvent;
import dk.sdu.imada.ticone.clustering.filter.IFilter;
import dk.sdu.imada.ticone.clustering.filter.IFilterListener;
import dk.sdu.imada.ticone.clustering.pair.ClusterPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.comparison.CytoscapeClusteringComparisonResult;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.ClusterPairFeatureNumberCommonObjects;
import dk.sdu.imada.ticone.feature.ClusterPairFeaturePercentageCommonObjects;
import dk.sdu.imada.ticone.feature.ClusterPairFeaturePrototypeSimilarity;
import dk.sdu.imada.ticone.feature.ClusterPairFeaturePvalue;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.gui.TiconeClusteringResultsPanel;
import dk.sdu.imada.ticone.gui.TiconeResultPanel;
import dk.sdu.imada.ticone.gui.panels.CollapsiblePanel;
import dk.sdu.imada.ticone.gui.panels.MyDialogPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartTable;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartWithButtonsPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartWithTitle;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartWithTitleCellRenderer;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartWithTitleComparator;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.util.FeatureCellRenderer;
import dk.sdu.imada.ticone.gui.util.VerticalTableHeaderCellRenderer;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.IStatusMapping;
import dk.sdu.imada.ticone.util.ITiconeResultNameChangeListener;
import dk.sdu.imada.ticone.util.TiconeResultNameChangedEvent;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * Created by wiwiec on 5/30/16.
 */
public class ClusteringComparisonResultFormPanel
		implements ITiconeResultNameChangeListener, IFilterListener, IStatusMappingListener {
	private final TiconeResultPanel resultPanel;
	private JPanel mainPanel, infoPanel, filterPanel;
	private JTable resultTable;
	private DefaultTableModel tableModel;
	private final CytoscapeClusteringComparisonResult comparisonResult;
	private JLabel simFunctionLabel, clusteringsLabel;
	private JCheckBox hideClusterPairsCheckBox, onlyCommonObjectsInClusterChartsCheckBox;
	private JLabel creationDateLabel;
	private JScrollPane tableScrollPane;

	private List<IClusterPair> patternPairList;
	protected ClusterChartWithTitleCellRenderer clusterChartWithTitleCellRenderer;
	private FeatureCellRenderer<IClusterPair> featureCellRendererNumberCommonObjects,
			featureCellRendererPercentageCommonObjects, featureCellRendererPrototypeSimilarity,
			featureCellRendererPValue;

	/**
	 * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
	 * edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
		tableScrollPane = new JScrollPane();
		mainPanel.add(tableScrollPane,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null,
						0, false));
		tableScrollPane.setViewportView(resultTable);
		filterPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(filterPanel,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		filterPanel.add(panel1,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		hideClusterPairsCheckBox.setSelected(true);
		hideClusterPairsCheckBox.setText("Hide cluster pairs without any common objects");
		panel1.add(hideClusterPairsCheckBox,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		onlyCommonObjectsInClusterChartsCheckBox.setText("Show only common objects in cluster charts");
		panel1.add(onlyCommonObjectsInClusterChartsCheckBox,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		infoPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(infoPanel,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1));
		infoPanel.add(panel2,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JLabel label1 = new JLabel();
		label1.setText("Similarity Function:");
		panel2.add(label1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel2.add(simFunctionLabel,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label2 = new JLabel();
		label2.setText("Clusterings:");
		panel2.add(label2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel2.add(clusteringsLabel,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label3 = new JLabel();
		label3.setText("Creation Date:");
		panel2.add(label3, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel2.add(creationDateLabel,
				new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return mainPanel;
	}

	public enum COLUMN_INDEX {
		SELECTED, CLUSTER1, CLUSTER2, COMMON_OBJECTS, COMMON_PERCENT_OBJECTS, PROTOTYPE_SIMILARITY, PVALUE
	}

	public ClusteringComparisonResultFormPanel(final TiconeResultPanel resultPanel,
			final CytoscapeClusteringComparisonResult comparisonResult) throws InterruptedException {
		super();
		this.resultPanel = resultPanel;
		this.comparisonResult = comparisonResult;

		this.$$$setupUI$$$();
		this.updateResultTable();

		this.comparisonResult.getClustering1().addNameChangeListener(this);
		this.comparisonResult.getClustering2().addNameChangeListener(this);
		this.comparisonResult.getClusterPairStatusMapping().addStatusMappingListener(this);
	}

	public static int getComparisonTableColumnIndex(final COLUMN_INDEX column) {
		switch (column) {
		case CLUSTER1:
			return getComparisonTableColumnIndex(COLUMN_INDEX.SELECTED) + 1;
		case CLUSTER2:
			return getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER1) + 1;
		case COMMON_OBJECTS:
			return getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER2) + 1;
		case COMMON_PERCENT_OBJECTS:
			return getComparisonTableColumnIndex(COLUMN_INDEX.COMMON_OBJECTS) + 1;
		case PROTOTYPE_SIMILARITY:
			return getComparisonTableColumnIndex(COLUMN_INDEX.COMMON_PERCENT_OBJECTS) + 1;
		case PVALUE:
			return getComparisonTableColumnIndex(COLUMN_INDEX.PROTOTYPE_SIMILARITY) + 1;
		case SELECTED:
			return 0;
		default:
			return -1;
		}
	}

	private void setupTableSorter() {
		final TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tableModel) {
			@Override
			public boolean isSortable(final int columnIndex) {
				if (columnIndex == getComparisonTableColumnIndex(COLUMN_INDEX.SELECTED)) {
					return false;
				}
				return true;
			}

			@Override
			protected void fireSortOrderChanged() {
				for (int i = 0; i < this.getModel().getColumnCount(); i++) {
					final boolean isDecreasing = this.getSortKeys().get(0).getSortOrder().equals(SortOrder.DESCENDING);
					if (this.getComparator(i) instanceof AbstractTiconeComparator)
						((AbstractTiconeComparator) this.getComparator(i)).setDecreasing(isDecreasing);
				}
				super.fireSortOrderChanged();
			}
		};
		sorter.setComparator(getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER1),
				new ClusterChartWithTitleComparator());
		sorter.setComparator(getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER2),
				new ClusterChartWithTitleComparator());
		sorter.setComparator(getComparisonTableColumnIndex(COLUMN_INDEX.COMMON_OBJECTS),
				new FeatureComparator<>(new ClusterPairFeatureNumberCommonObjects(), comparisonResult.getFeatureStore())
						.setStatusMapping(comparisonResult.getClusterPairStatusMapping()));
		sorter.setComparator(getComparisonTableColumnIndex(COLUMN_INDEX.COMMON_PERCENT_OBJECTS),
				new FeatureComparator<>(new ClusterPairFeaturePercentageCommonObjects(),
						comparisonResult.getFeatureStore())
								.setStatusMapping(comparisonResult.getClusterPairStatusMapping()));
		try {
			sorter.setComparator(getComparisonTableColumnIndex(COLUMN_INDEX.PROTOTYPE_SIMILARITY),
					new FeatureComparator<>(
							new ClusterPairFeaturePrototypeSimilarity(comparisonResult.getSimilarityFunction()),
							comparisonResult.getFeatureStore())
									.setStatusMapping(comparisonResult.getClusterPairStatusMapping()));
		} catch (IncompatibleSimilarityFunctionException e) {
			e.printStackTrace();
		}
		sorter.setComparator(getComparisonTableColumnIndex(COLUMN_INDEX.PVALUE),
				new FeatureComparator<>(new ClusterPairFeaturePvalue(), comparisonResult.getFeatureStore())
						.setStatusMapping(comparisonResult.getClusterPairStatusMapping()));

		resultTable.setRowSorter(sorter);
	}

	private void createUIComponents() {
		this.infoPanel = new CollapsiblePanel("Info", false);
		this.filterPanel = new CollapsiblePanel("Filtering", false);

		this.creationDateLabel = new JLabel(String.format("%s (%s)",
				this.comparisonResult.getDate() != null ? new SimpleDateFormat().format(this.comparisonResult.getDate())
						: "NA",
				this.comparisonResult.getTiconeVersion() != null ? this.comparisonResult.getTiconeVersion()
						: "<= 1.2.74"));
		this.hideClusterPairsCheckBox = new JCheckBox("Hide cluster pairs without any common objects");
		this.hideClusterPairsCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				ClusteringComparisonResultFormPanel.this.updateTableFilter();
			}
		});
		this.onlyCommonObjectsInClusterChartsCheckBox = new JCheckBox("Show only common objects in cluster charts");
		this.onlyCommonObjectsInClusterChartsCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					ClusteringComparisonResultFormPanel.this.updateResultTable();
				} catch (InterruptedException e1) {
				}
			}
		});
		this.simFunctionLabel = new JLabel(this.comparisonResult.getSimilarityFunction().toString());
		this.clusteringsLabel = new ClusteringWithIterationLabel(
				new AbstractNamedTiconeResult[] { this.comparisonResult.getClustering1(),
						this.comparisonResult.getClustering2() },
				new int[] { this.comparisonResult.getClustering1iteration(),
						this.comparisonResult.getClustering2iteration() });

		this.tableModel = new DefaultTableModel() {
			/**
			 *
			 */
			private static final long serialVersionUID = -8318670796885852560L;

			@Override
			public boolean isCellEditable(final int row, final int column) {
				return column == getComparisonTableColumnIndex(COLUMN_INDEX.SELECTED);
			}

			@Override
			public Class<?> getColumnClass(final int columnIndex) {
				if (columnIndex == getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER1)
						|| columnIndex == getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER2)) {
					return ClusterChartWithTitle.class;
				} else if (columnIndex == getComparisonTableColumnIndex(COLUMN_INDEX.SELECTED)) {
					return Boolean.class;
				} else if (ClusteringComparisonResultFormPanel.this.tableModel.getRowCount() == 0) {
					return Object.class;
				} else {
					return IClusterPair.class;
				}
			}
		};
		this.tableModel.addColumn("Selected");
		this.tableModel.addColumn(comparisonResult.getClustering1().getName());
		this.tableModel.addColumn(comparisonResult.getClustering2().getName());
		this.tableModel.addColumn("#Objs.");
		this.tableModel.addColumn("%Objs.");
		this.tableModel.addColumn("Sim.");
		this.tableModel.addColumn("p");

		this.resultTable = new JTable(this.tableModel);

		setupTableSorter();

		clusterChartWithTitleCellRenderer = new ClusterChartWithTitleCellRenderer() {

			/**
			 *
			 */
			private static final long serialVersionUID = -2359763056944800828L;

			@Override
			protected IClusterPair getObjectFromValue(JTable table, int row, int column, Object value) {
				final ClusterChartWithTitle chart1 = (ClusterChartWithTitle) table.getValueAt(row,
						getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER1)),
						chart2 = (ClusterChartWithTitle) table.getValueAt(row,
								getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER2));
				return new ClusterPair(chart1.getCluster(), chart2.getCluster());
			}
		};
		comparisonResult.getFilter().addFilterListener(this);

		final TableColumnModel columnModel = this.resultTable.getColumnModel();
		TableColumn column = columnModel.getColumn(getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER1));
		column.setCellRenderer(clusterChartWithTitleCellRenderer);
		column.setCellEditor(clusterChartWithTitleCellRenderer);

		column = columnModel.getColumn(getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER2));
		column.setCellRenderer(clusterChartWithTitleCellRenderer);
		column.setCellEditor(clusterChartWithTitleCellRenderer);

		for (final int i : new int[] { getComparisonTableColumnIndex(COLUMN_INDEX.SELECTED) }) {
			column = columnModel.getColumn(i);
			column.setMaxWidth(this.getClusterTableColumnWidth(COLUMN_INDEX.SELECTED));
		}

		featureCellRendererNumberCommonObjects = new FeatureCellRenderer<IClusterPair>(
				new ClusterPairFeatureNumberCommonObjects(), comparisonResult.getFeatureStore()) {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}
		};
		column = columnModel.getColumn(getComparisonTableColumnIndex(COLUMN_INDEX.COMMON_OBJECTS));
		column.setCellRenderer(featureCellRendererNumberCommonObjects);
		column.setCellEditor(featureCellRendererNumberCommonObjects);

		featureCellRendererPercentageCommonObjects = new FeatureCellRenderer<>(
				new ClusterPairFeaturePercentageCommonObjects(), comparisonResult.getFeatureStore());
		column = columnModel.getColumn(getComparisonTableColumnIndex(COLUMN_INDEX.COMMON_PERCENT_OBJECTS));
		column.setCellRenderer(featureCellRendererPercentageCommonObjects);
		column.setCellEditor(featureCellRendererPercentageCommonObjects);

		featureCellRendererPrototypeSimilarity = new FeatureCellRenderer<>(new ClusterPairFeaturePrototypeSimilarity(),
				comparisonResult.getFeatureStore());
		column = columnModel.getColumn(getComparisonTableColumnIndex(COLUMN_INDEX.PROTOTYPE_SIMILARITY));
		column.setCellRenderer(featureCellRendererPrototypeSimilarity);
		column.setCellEditor(featureCellRendererPrototypeSimilarity);

		featureCellRendererPValue = new FeatureCellRenderer<>(new ClusterPairFeaturePvalue(),
				comparisonResult.getFeatureStore());
		column = columnModel.getColumn(getComparisonTableColumnIndex(COLUMN_INDEX.PVALUE));
		column.setCellRenderer(featureCellRendererPValue);
		column.setCellEditor(featureCellRendererPValue);

		this.resultTable.setAutoCreateRowSorter(true);

		this.resultTable.addMouseListener(new MouseAdapter() {

			private void showChartPopupMenu(MouseEvent e, final int row, final int column) {
				class PopUpDemo extends JPopupMenu {
					JMenuItem anItem;

					public PopUpDemo() {
						anItem = new JMenuItem("Show cluster in clustering");
						anItem.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								TiconeClusteringResultPanel clusteringPanelToFocus = null;
								TiconeClusteringResultsPanel ticoneClusteringsResultsPanel;

								// cluster that was right clicked
								ICluster cluster = null;
								try {
									ticoneClusteringsResultsPanel = GUIUtility.getTiconeClusteringsResultsPanel();
									int tabCount = ticoneClusteringsResultsPanel.getTabCount();
									if (column == COLUMN_INDEX.CLUSTER1.ordinal()) {
										for (int t = 0; t < tabCount; t++) {
											final Component p = ticoneClusteringsResultsPanel.getComponentAt(t);
											if (p instanceof TiconeClusteringResultPanel) {
												TiconeCytoscapeClusteringResult tmp = ((TiconeClusteringResultPanel) p)
														.getResult();
												if (tmp.equals(comparisonResult.getClustering1())) {
													clusteringPanelToFocus = (TiconeClusteringResultPanel) p;
													break;
												}
											}
										}
										cluster = ((ClusterChartWithTitle) resultTable.getValueAt(row, column))
												.getCluster();
									} else if (column == COLUMN_INDEX.CLUSTER2.ordinal()) {
										for (int t = 0; t < tabCount; t++) {
											final Component p = ticoneClusteringsResultsPanel.getComponentAt(t);
											if (p instanceof TiconeClusteringResultPanel) {
												TiconeCytoscapeClusteringResult tmp = ((TiconeClusteringResultPanel) p)
														.getResult();
												if (tmp.equals(comparisonResult.getClustering2())) {
													clusteringPanelToFocus = (TiconeClusteringResultPanel) p;
													break;
												}
											}
										}
										cluster = ((ClusterChartWithTitle) resultTable.getValueAt(row, column))
												.getCluster();
									}

									if (clusteringPanelToFocus == null || cluster == null)
										return;

									((JTabbedPane) ticoneClusteringsResultsPanel.getParent())
											.setSelectedComponent(ticoneClusteringsResultsPanel);
									ticoneClusteringsResultsPanel.setSelectedComponent(clusteringPanelToFocus);
									clusteringPanelToFocus.setGraphTabInFocus();

									final ClusterChartTable clusterChartTable = clusteringPanelToFocus
											.getClustersTabPanel().getClusterChartTablePanel().getClusterChartTable();
									final JTable table = clusterChartTable.getTable();

									// find the row with clicked cluster
									int row = -1;
									for (int r = 0; r < table.getRowCount(); r++) {
										ClusterChartWithButtonsPanel chart = (ClusterChartWithButtonsPanel) table
												.getValueAt(r, ClusterChartTable.getClusterTableColumnIndex(
														ClusterChartTable.CLUSTER_TABLE_COLUMN.CLUSTER_CHART));
										if (cluster.getClusterNumber() == chart.getCluster().getClusterNumber()) {
											row = r;
											break;
										}
									}

									if (row > -1) {
										table.scrollRectToVisible(table.getCellRect(row, column, true));
										table.setRowSelectionInterval(row, row);
									}
								} catch (final TiconeUnloadingException e1) {
									return;
								}
							}
						});
						add(anItem);
					}
				}

				PopUpDemo menu = new PopUpDemo();
				menu.show(e.getComponent(), e.getX(), e.getY());
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				final int row = ClusteringComparisonResultFormPanel.this.resultTable.rowAtPoint(e.getPoint());
				final int col = ClusteringComparisonResultFormPanel.this.resultTable.columnAtPoint(e.getPoint());

				if (col == getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER1)
						|| col == getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER2)) {
					if (e.isPopupTrigger())
						showChartPopupMenu(e, row, col);
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				final int row = ClusteringComparisonResultFormPanel.this.resultTable.rowAtPoint(e.getPoint());
				final int col = ClusteringComparisonResultFormPanel.this.resultTable.columnAtPoint(e.getPoint());

				if (col == getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER1)
						|| col == getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER2)) {
					if (e.isPopupTrigger())
						showChartPopupMenu(e, row, col);
				}
			}

			@Override
			public void mouseClicked(final MouseEvent e) {
				final int row = ClusteringComparisonResultFormPanel.this.resultTable.rowAtPoint(e.getPoint());
				final int col = ClusteringComparisonResultFormPanel.this.resultTable.columnAtPoint(e.getPoint());

				if (col == getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER1)
						|| col == getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER2)) {
					final ClusterChartWithTitle chart = (ClusterChartWithTitle) ClusteringComparisonResultFormPanel.this.resultTable
							.getValueAt(row, col);
					if (e.isPopupTrigger()) {
						showChartPopupMenu(e, row, col);
					} else if (e.getButton() == MouseEvent.BUTTON1) {
						try {
							final ChartPanel largeChartPanel = chart.getChartPanelContainer().getLargeChartPanel();
							final JFrame newWindow = new JFrame(chart.getCluster().getName());
							newWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
							newWindow.add(largeChartPanel);
							newWindow.setSize(750, 400);
							newWindow.setVisible(true);
						} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e1) {
							MyDialogPanel.showMessageDialog(e1.getMessage());
						} catch (InterruptedException e1) {
						}
					}
				}
			}
		});

		this.resultTable.getTableHeader().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				super.mouseClicked(e);
				final int col = ClusteringComparisonResultFormPanel.this.resultTable.columnAtPoint(e.getPoint());
				if (col == getComparisonTableColumnIndex(COLUMN_INDEX.SELECTED)) {
					// we always deselect all filtered clusters
					for (int row = 0; row < ClusteringComparisonResultFormPanel.this.resultTable.getRowCount(); row++) {
						final IClusterPair pair = (IClusterPair) ClusteringComparisonResultFormPanel.this.resultTable
								.getValueAt(row, getComparisonTableColumnIndex(COLUMN_INDEX.COMMON_OBJECTS));
						if (ClusteringComparisonResultFormPanel.this.comparisonResult.getClusterPairStatusMapping()
								.getFiltered().contains(pair))
							ClusteringComparisonResultFormPanel.this.resultTable.setValueAt(false, row, col);
					}

					// count how many are selected
					final boolean allSelected = allRowsSelected(ClusteringComparisonResultFormPanel.this.resultTable,
							col,
							ClusteringComparisonResultFormPanel.this.comparisonResult.getClusterPairStatusMapping());
					// if not all are selected, we select all, otherwise
					// none
					for (int row = 0; row < ClusteringComparisonResultFormPanel.this.resultTable.getRowCount(); row++) {
						final IClusterPair pair = (IClusterPair) ClusteringComparisonResultFormPanel.this.resultTable
								.getValueAt(row, getComparisonTableColumnIndex(COLUMN_INDEX.COMMON_OBJECTS));
						if (!ClusteringComparisonResultFormPanel.this.comparisonResult.getClusterPairStatusMapping()
								.getFiltered().contains(pair))
							ClusteringComparisonResultFormPanel.this.resultTable.setValueAt(!allSelected, row, col);
					}
					ClusteringComparisonResultFormPanel.this.resultTable.repaint();
				}
			}
		});

		// Set vertical header labels
		final TableCellRenderer headerRenderer = new VerticalTableHeaderCellRenderer();
		final Enumeration<TableColumn> columnEnum = this.resultTable.getColumnModel().getColumns();
		while (columnEnum.hasMoreElements()) {
			columnEnum.nextElement().setHeaderRenderer(headerRenderer);
		}
	}

	private void updateTableFilter() {
		final RowFilter<DefaultTableModel, Object> rf = new RowFilter<DefaultTableModel, Object>() {
			@Override
			public boolean include(final Entry<? extends DefaultTableModel, ? extends Object> entry) {
				if (ClusteringComparisonResultFormPanel.this.hideClusterPairsCheckBox.isSelected())
					return comparisonResult.getFeatureStore().getFeatureValue(
							(IClusterPair) entry.getValue(getComparisonTableColumnIndex(COLUMN_INDEX.COMMON_OBJECTS)),
							new ClusterPairFeatureNumberCommonObjects()).getValue() > 0;
				return true;
			}
		};
		((TableRowSorter<DefaultTableModel>) this.resultTable.getRowSorter()).setRowFilter(rf);

	}

	public JPanel getMainPanel() {
		return this.mainPanel;
	}

	private void updateResultTable() throws InterruptedException {
		final Runnable r = new Runnable() {
			@Override
			public void run() {
				try {

					List<? extends SortKey> sortKeys = null;
					if (resultTable != null && resultTable.getRowSorter() != null) {
						sortKeys = resultTable.getRowSorter().getSortKeys();
					}

					((DefaultTableModel) resultTable.getModel()).setRowCount(0);
					resultTable.setRowHeight(90);
					resultTable.setShowVerticalLines(true);
					resultTable.setShowHorizontalLines(true);
					resultTable.setFocusable(false);
					resultTable.setRowSelectionAllowed(true);
					resultTable.setAutoCreateRowSorter(true);
					resultTable.getTableHeader().setReorderingAllowed(false);

					setupTableSorter();

					updateTableFilter();

					final DefaultTableModel tableModel = (DefaultTableModel) resultTable.getModel();

					final Map<IClusterPair, IFeatureValue<Integer>> commonObjects = comparisonResult.getFeatureStore()
							.getFeatureValueMap(new ClusterPairFeatureNumberCommonObjects(), ObjectType.CLUSTER_PAIR);
					patternPairList = new ArrayList<>();
					for (final IClusterPair pair : commonObjects.keySet()) {
						final ICluster cluster1 = pair.getFirst();
						final ICluster cluster2 = pair.getSecond();
						patternPairList.add(pair);
						ClusterChartContainer clusterChartContainer1, clusterChartContainer2;
						final ITimeSeriesObjectList objects1, objects2;
						if (onlyCommonObjectsInClusterChartsCheckBox.isSelected()) {
							final Set<String> commonObjectIds = comparisonResult.calculateCommonObjects(pair);
							objects1 = new TimeSeriesObjectList();
							for (final ITimeSeriesObject object : cluster1.getObjects()) {
								if (commonObjectIds.contains(object.getName()))
									objects1.add(object);
							}
							objects2 = new TimeSeriesObjectList();
							for (final ITimeSeriesObject object : cluster2.getObjects()) {
								if (commonObjectIds.contains(object.getName()))
									objects2.add(object);
							}
						} else {
							objects1 = cluster1.getObjects();
							objects2 = cluster2.getObjects();
						}
						ClusterChartWithTitle chartWithTitle1 = null, chartWithTitle2 = null;
						if (!comparisonResult.getClustering1().hasClusterChartContainer(cluster1, objects1)) {
							ClusterChartContainer clusterChartContainer = null;
							try {
								if (objects1.size() > 0)
									clusterChartContainer = new ClusterChartContainer(comparisonResult.getClustering1(),
											cluster1, objects1);
							} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
							}
							comparisonResult.getClustering1().addClusterChartContainer(cluster1, objects1,
									clusterChartContainer);
						}
						clusterChartContainer1 = comparisonResult.getClustering1().getClusterChartContainer(cluster1,
								objects1);
						if (!comparisonResult.getClustering2().hasClusterChartContainer(cluster2, objects2)) {
							ClusterChartContainer clusterChartContainer = null;
							try {
								if (objects2.size() > 0)
									clusterChartContainer = new ClusterChartContainer(comparisonResult.getClustering2(),
											cluster2, objects2);
							} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
							}
							comparisonResult.getClustering2().addClusterChartContainer(cluster2, objects2,
									clusterChartContainer);
						}
						clusterChartContainer2 = comparisonResult.getClustering2().getClusterChartContainer(cluster2,
								objects2);

						chartWithTitle1 = new ClusterChartWithTitle(clusterChartContainer1);
						chartWithTitle2 = new ClusterChartWithTitle(clusterChartContainer2);

						tableModel.addRow(
								new Object[] { false, chartWithTitle1, chartWithTitle2, pair, pair, pair, pair });

					}

					updatePanel();

					if (sortKeys != null && !sortKeys.isEmpty())
						resultTable.getRowSorter().setSortKeys(sortKeys);
					else {
						// by default we sort by the cluster ID
						// this also ensures, that filtered clusters are always shown at the
						// lower end of the table
						resultTable.getRowSorter().setSortKeys(Arrays.asList(new SortKey(
								getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER1), SortOrder.ASCENDING)));
					}
				} catch (InterruptedException e) {
				}
			}
		};
		if (EventQueue.isDispatchThread())
			r.run();
		else {
			try {
				EventQueue.invokeAndWait(r);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}

	public Map<IClusters, Map<String, Map<String, Object>>> getSelectedKpmObjects() {
		final Map<IClusters, Map<String, Map<String, Object>>> selectedObjects = new HashMap<>();

		for (int i = 0; i < this.resultTable.getRowCount(); i++) {
			if ((boolean) this.resultTable.getValueAt(i, getComparisonTableColumnIndex(COLUMN_INDEX.SELECTED))) {
				final IClusterPair patternPair = new ClusterPair(
						((ClusterChartWithTitle) this.resultTable.getValueAt(i,
								getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER1))).getCluster(),
						((ClusterChartWithTitle) this.resultTable.getValueAt(i,
								getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER2))).getCluster());

				final Set<String> common = this.comparisonResult.calculateCommonObjects(patternPair);
				final Map<String, Map<String, Object>> commonIds = new HashMap<>();
				for (final String tsd : common) {
					final Map<String, Object> nodeAttributes = new HashMap<>();
					nodeAttributes.put("cluster 1", patternPair.getFirst().getName());
					nodeAttributes.put("cluster 2", patternPair.getSecond().getName());
					commonIds.put(tsd, nodeAttributes);
				}

				final IClusters clusterList = new ClusterList(patternPair.getFirst(), patternPair.getSecond());

				if (!selectedObjects.containsKey(clusterList))
					selectedObjects.put(clusterList, new HashMap<String, Map<String, Object>>());
				selectedObjects.get(clusterList).putAll(commonIds);
			}
		}

		return selectedObjects;
	}

	public List<Triple<ICluster, ICluster, Set<String>>> getSelectedClusterPairsToVisualizeOnNetwork() {
		final List<Triple<ICluster, ICluster, Set<String>>> clusterPairs = new ArrayList<>();

		for (int row = 0; row < this.resultTable.getRowCount(); row++) {
			if ((boolean) this.resultTable.getValueAt(row, getComparisonTableColumnIndex(COLUMN_INDEX.SELECTED))) {
				final ICluster cluster1 = ((ClusterChartWithTitle) this.resultTable.getValueAt(row,
						getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER1))).getCluster();
				final ICluster cluster2 = ((ClusterChartWithTitle) this.resultTable.getValueAt(row,
						getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER2))).getCluster();
				final Set<String> objects = this.comparisonResult
						.calculateCommonObjects(new ClusterPair(cluster1, cluster2));
				final Set<String> objectIds = new HashSet<>();
				for (final String obj : objects) {
					objectIds.add(obj);
				}
				clusterPairs.add(Triple.of(cluster1, cluster2, objectIds));
			}
		}

		return clusterPairs;
	}

	public int getClusterTableColumnWidth(final COLUMN_INDEX column) {
		switch (column) {
		case SELECTED:
			return 20;
		case CLUSTER1:
			return 160;
		case CLUSTER2:
			return 160;
		case COMMON_OBJECTS:
			return 48;
		case COMMON_PERCENT_OBJECTS:
			return 48;
		case PROTOTYPE_SIMILARITY:
			return 48;
		case PVALUE:
			return 48;
		default:
			return -1;
		}
	}

	@Override
	public void resultNameChanged(final TiconeResultNameChangedEvent event) {
		if (comparisonResult.getClustering1().equals(event.getSource())) {
			final TableColumnModel columnModel = this.resultTable.getColumnModel();
			final TableColumn column = columnModel.getColumn(getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER1));
			column.setHeaderValue(event.getNewName());
		} else if (comparisonResult.getClustering2().equals(event.getSource())) {
			final TableColumnModel columnModel = this.resultTable.getColumnModel();
			final TableColumn column = columnModel.getColumn(getComparisonTableColumnIndex(COLUMN_INDEX.CLUSTER2));
			column.setHeaderValue(event.getNewName());
		}
	}

	@Override
	public void filterChanged(FilterEvent e) {
		final IFilter<IClusterPair> filter = (IFilter<IClusterPair>) e.getFilter();
		final Collection<IClusterPair> filteredPairs = new HashSet<>();
		final IFeatureStore featureStore = comparisonResult.getFeatureStore();
		for (IClusterPair cp : featureStore.keySet(ObjectType.CLUSTER_PAIR)) {
			if (!(filter.check(featureStore, cp)))
				filteredPairs.add(cp);
		}
		clusterChartWithTitleCellRenderer.setFilteredObjects(filteredPairs);
		featureCellRendererNumberCommonObjects.setFilteredObjects(filteredPairs);
		featureCellRendererPercentageCommonObjects.setFilteredObjects(filteredPairs);
		featureCellRendererPrototypeSimilarity.setFilteredObjects(filteredPairs);
		featureCellRendererPValue.setFilteredObjects(filteredPairs);
		resultTable.repaint();
	}

	@Override
	public void clusterStatusMappingChanged(final StatusMappingEvent e) {
		try {
			updateResultTable();
			updatePanel();
		} catch (InterruptedException e1) {
		}
	}

	public void updatePanel() throws InterruptedException {
		final Runnable r = new Runnable() {
			@Override
			public void run() {
				tableScrollPane.setViewportView(resultTable);
				tableScrollPane.repaint();
				tableScrollPane.revalidate();
			}
		};
		if (EventQueue.isDispatchThread())
			r.run();
		else {
			try {
				EventQueue.invokeAndWait(r);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}

	protected static boolean allRowsSelected(final JTable resultTable, final int column, final IStatusMapping csm) {
		boolean result = true;
		for (int row = 0; row < resultTable.getRowCount(); row++) {
			final IClusterPair pair = (IClusterPair) resultTable.getValueAt(row,
					getComparisonTableColumnIndex(COLUMN_INDEX.COMMON_OBJECTS));
			if (csm.getFiltered().contains(pair))
				continue;
			result &= (boolean) resultTable.getValueAt(row, column);
		}
		return result;
	}

	protected static int numberRowsSelected(final JTable resultTable, final int column, final IStatusMapping csm) {
		int result = 0;
		for (int row = 0; row < resultTable.getRowCount(); row++) {
			final IClusterPair pair = (IClusterPair) resultTable.getValueAt(row,
					getComparisonTableColumnIndex(COLUMN_INDEX.COMMON_OBJECTS));
			if (csm.getFiltered().contains(pair))
				continue;
			Object valueAt = resultTable.getValueAt(row, column);
			if (valueAt == null)
				continue;
			if ((boolean) valueAt)
				result++;
		}
		return result;
	}

}