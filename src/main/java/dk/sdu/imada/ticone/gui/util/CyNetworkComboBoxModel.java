/**
 * 
 */
package dk.sdu.imada.ticone.gui.util;

import java.util.Iterator;
import java.util.Set;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.model.events.NetworkAboutToBeDestroyedEvent;
import org.cytoscape.model.events.NetworkAboutToBeDestroyedListener;
import org.cytoscape.model.events.NetworkAddedEvent;
import org.cytoscape.model.events.NetworkAddedListener;

import dk.sdu.imada.ticone.util.ServiceHelper;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 22, 2018
 *
 */
public class CyNetworkComboBoxModel extends SortedComboBoxModel<CyNetwork>
		implements NetworkAddedListener, NetworkAboutToBeDestroyedListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3280583463166723564L;

	public CyNetworkComboBoxModel() {
		super(new CyNetworkComparator());
		ServiceHelper.registerService(NetworkAddedListener.class, this);
		ServiceHelper.registerService(NetworkAboutToBeDestroyedListener.class, this);

		this.updateNetworks();
	}

	@Override
	public void handleEvent(final NetworkAboutToBeDestroyedEvent e) {
		final CyNetwork network = e.getNetwork();
		this.removeElement(network);
	}

	@Override
	public void handleEvent(final NetworkAddedEvent e) {
		final CyNetwork network = e.getNetwork();
		this.addElement(network);
	}

	private void updateNetworks() {
		final CyNetworkManager networkManager = ServiceHelper.getService(CyNetworkManager.class);
		final Set<CyNetwork> networks = networkManager.getNetworkSet();
		final Iterator<CyNetwork> networkIterator = networks.iterator();
		this.removeAllElements();

		while (networkIterator.hasNext()) {
			final CyNetwork network = networkIterator.next();
			if (network != null && network.toString() != null) {
				this.addElement(network);
			}
		}
	}

}
