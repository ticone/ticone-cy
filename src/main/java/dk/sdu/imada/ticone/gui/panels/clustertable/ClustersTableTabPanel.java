package dk.sdu.imada.ticone.gui.panels.clustertable;

import java.awt.Dimension;
import java.awt.Insets;
import java.util.Comparator;
import java.util.regex.PatternSyntaxException;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.util.ITiconeResultChangeListener;
import dk.sdu.imada.ticone.util.TiconeResultChangeEvent;

/**
 * Created by wiwiec on 5/31/16.
 */
public class ClustersTableTabPanel implements ITiconeResultChangeListener {
	private final TiconeClusteringResultPanel resultPanel;
	private JPanel mainPanel;
	private JTable clustersTable;
	private DefaultTableModel clustersTableModel;
	private JTextField filterTextField;

	public ClustersTableTabPanel(final TiconeClusteringResultPanel resultPanel) {
		super();
		this.resultPanel = resultPanel;
		this.$$$setupUI$$$();
		resultPanel.getClusteringResult().addChangeListener(this);
	}

	private void createUIComponents() {
		this.clustersTableModel = new DefaultTableModel() {

			/**
			 *
			 */
			private static final long serialVersionUID = 5298927736167461735L;

			@Override
			public boolean isCellEditable(final int row, final int column) {
				return false;
			}

			@Override
			public Class<?> getColumnClass(final int columnIndex) {
				if (columnIndex == 0)
					return Integer.class;
				else if (columnIndex == 1)
					return String.class;
				else if (columnIndex == 2)
					return String.class;
				return Object.class;
			}
		};
		this.clustersTableModel.addColumn("Cluster ID");
		this.clustersTableModel.addColumn("Object ID");
		if (this.resultPanel.getClusteringResult().getIdMapMethod().isActive())
			this.clustersTableModel.addColumn("Alternative ID");

		this.clustersTable = new JTable(this.clustersTableModel);
		this.clustersTable.setAutoCreateRowSorter(true);
		this.clustersTable.getTableHeader().setReorderingAllowed(false);
		this.clustersTable.setRowSorter(new TableRowSorter<DefaultTableModel>(this.clustersTableModel) {
			@Override
			public Comparator<?> getComparator(final int column) {
				if (column == 1 || column == 2) {
					return new Comparator<String>() {
						@Override
						public int compare(final String o1, final String o2) {
							if (o1.length() > o2.length()) {
								return 1;
							} else if (o1.length() < o2.length()) {
								return -1;
							}
							return o1.compareTo(o2);
						}
					};
				}
				return super.getComparator(column);
			}
		});

		this.clustersTable.setColumnSelectionAllowed(true);
		this.clustersTable.setRowSelectionAllowed(true);

		this.filterTextField = new JTextField();
		this.filterTextField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(final DocumentEvent e) {
				this.updateFilter();
			}

			@Override
			public void insertUpdate(final DocumentEvent e) {
				this.updateFilter();
			}

			@Override
			public void changedUpdate(final DocumentEvent e) {
				this.updateFilter();
			}

			private void updateFilter() {
				RowFilter<DefaultTableModel, Object> rf = null;
				// If current expression doesn't parse, don't update.
				try {
					// case insensitive search
					rf = RowFilter.regexFilter("(?i)" + ClustersTableTabPanel.this.filterTextField.getText());
				} catch (final PatternSyntaxException ex) {
					return;
				}
				((TableRowSorter<DefaultTableModel>) ClustersTableTabPanel.this.clustersTable.getRowSorter())
						.setRowFilter(rf);

			}
		});

		this.updateTable();
	}

	public JPanel getMainPanel() {
		return this.mainPanel;
	}

	public JTable getClustersTable() {
		return this.clustersTable;
	}

	@Override
	public void resultChanged(TiconeResultChangeEvent e) {
		this.updateTable();
	}

	protected void updateTable() {
		this.clustersTableModel.setRowCount(0);
		final IClusterObjectMapping pom = this.resultPanel.getClusteringResult().getClusteringProcess()
				.getLatestClustering();
		for (final ICluster p : pom.getClusters()) {
			final ITimeSeriesObjectList tsds = p.getObjects();
			for (final ITimeSeriesObject tsd : tsds) {
				if (this.resultPanel.getClusteringResult().getIdMapMethod().isActive())
					this.clustersTableModel
							.addRow(new Object[] { p.getClusterNumber(), tsd.getName(), tsd.getAlternativeName() });
				else
					this.clustersTableModel.addRow(new Object[] { p.getClusterNumber(), tsd.getName() });
			}
		}
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
	 * edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		final JScrollPane scrollPane1 = new JScrollPane();
		mainPanel.add(scrollPane1,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null,
						0, false));
		scrollPane1.setViewportView(clustersTable);
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(panel1,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		panel1.add(filterTextField,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(150, -1), null, 0, false));
		final JLabel label1 = new JLabel();
		label1.setText("Filter table:");
		panel1.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return mainPanel;
	}
}
