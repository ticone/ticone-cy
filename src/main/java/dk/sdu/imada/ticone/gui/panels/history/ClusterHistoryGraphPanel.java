package dk.sdu.imada.ticone.gui.panels.history;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import dk.sdu.imada.ticone.clustering.ClusterOperationException;
import dk.sdu.imada.ticone.clustering.ClusteringIterationAddedEvent;
import dk.sdu.imada.ticone.clustering.ClusteringIterationDeletedEvent;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringIterationAddedListener;
import dk.sdu.imada.ticone.clustering.IClusteringIterationDeletionListener;
import dk.sdu.imada.ticone.feature.store.INewFeatureStoreListener;
import dk.sdu.imada.ticone.feature.store.NewFeatureStoreEvent;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartTable;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.table.TableFactory;
import dk.sdu.imada.ticone.util.ClusterStatusMapping;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.IClusterHistory;
import dk.sdu.imada.ticone.util.IClusterStatusMapping;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * Created by christian on 9/11/15.
 */
public class ClusterHistoryGraphPanel extends JPanel
		implements INewFeatureStoreListener, IClusteringIterationDeletionListener, IClusteringIterationAddedListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3866693366246545594L;

	private JComboBox<IClusterHistory> historyComboBox;
	private JPanel graphPanel;

	private ClusterChartTable graphTable;

	protected TiconeClusteringResultPanel resultPanel;

	public ClusterHistoryGraphPanel(final TiconeClusteringResultPanel resultPanel)
			throws InterruptedException, IncompatibleSimilarityFunctionException {
		super(new BorderLayout());

		this.resultPanel = resultPanel;
		this.resultPanel.getResult().addNewFeatureStoreListener(this);
		this.resultPanel.getResult().addClusteringIterationAddedListener(this);
		this.resultPanel.getResult().addClusteringIterationDeletionListener(this);
		this.initComponents();
	}

	private void initComponents() throws InterruptedException, IncompatibleSimilarityFunctionException {
		final GridBagConstraints constraints = new GridBagConstraints();

		final JPanel topPanel = this.topPanel();
		constraints.gridx = 0;
		constraints.gridy = 0;
		this.add(topPanel, BorderLayout.NORTH);

		this.graphPanel = new JPanel(new BorderLayout());
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.fill = GridBagConstraints.BOTH;
		this.add(this.graphPanel, BorderLayout.CENTER);
		this.graphTable = new ClusterChartTable(this.graphPanel, false, this.resultPanel);
		this.updateGraphPanel(this.resultPanel.getResult().getClusterHistory());
	}

	private JPanel topPanel() {
		final JPanel topPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		this.historyComboBox = new JComboBox<>();
		this.setupHistoryComboBox();
		this.historyComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				try {
					ClusterHistoryGraphPanel.this.comboBoxAction();
				} catch (final InterruptedException e) {
				}
			}
		});
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.gridx = 0;
		constraints.gridy = 0;
		topPanel.add(this.historyComboBox, constraints);

		final JButton applyHistoryButton = new JButton("Reset to this iteration");
		applyHistoryButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				try {
					ClusterHistoryGraphPanel.this.applyHistoryAction();
				} catch (final ClusterOperationException e) {
					e.printStackTrace();
				} catch (InterruptedException | TiconeUnloadingException e) {
				}
			}
		});
		constraints.gridx = 1;
		constraints.gridy = 0;
		topPanel.add(applyHistoryButton, constraints);
		constraints.anchor = GridBagConstraints.NORTHWEST;

		return topPanel;
	}

	// TODO: Update pattern status mapping
	private void applyHistoryAction() throws ClusterOperationException, InterruptedException, TiconeUnloadingException {
		final IClusterHistory patternHistory = (IClusterHistory) this.historyComboBox.getSelectedItem();
		this.resultPanel.getClusteringResult().getClusteringProcess()
				.resetToIteration(patternHistory.getIterationNumber());
		this.resultPanel.getClusteringResult().setupPatternStatusMapping();
		GUIUtility.updateGraphPanel(this.resultPanel);
		try {
			TableFactory.setupClusterTables(this.resultPanel.getClusteringResult());
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	private void comboBoxAction() throws InterruptedException {
		final IClusterHistory history = (IClusterHistory) this.historyComboBox.getSelectedItem();
		if (history != null) {
			this.updateGraphPanel(history);
		}
	}

	private void updateGraphPanel(final IClusterHistory history) throws InterruptedException {
		final IClusterObjectMapping patternObjectMapping = history.getClusterObjectMapping();
		final IClusterStatusMapping patternStatusMapping = new ClusterStatusMapping(
				this.resultPanel.getClusteringResult().getFeatureStore(history.getIterationNumber()));
		for (final ICluster pattern : patternObjectMapping.getClusters()) {
			patternStatusMapping.addCluster(pattern, pattern.getObjects(), false, false, false);
		}
		this.graphTable.updateGraphTable(patternStatusMapping);
	}

	public void setupHistoryComboBox() {
		this.historyComboBox.removeAllItems();
		IClusterHistory patternHistory = this.resultPanel.getClusteringResult().getClusterHistory();
		while (patternHistory != null) {
			this.historyComboBox.addItem(patternHistory);
			patternHistory = patternHistory.getParent();
		}
	}

	@Override
	public void newFeatureStore(NewFeatureStoreEvent e) {
		try {
			this.setupHistoryComboBox();
			this.updateGraphPanel(this.resultPanel.getClusteringResult().getClusterHistory());
		} catch (InterruptedException e1) {
		}
	}

	@Override
	public void clusteringIterationAdded(ClusteringIterationAddedEvent event) {
		this.historyComboBox.insertItemAt(this.resultPanel.getClusteringResult().getClusterHistory(), 0);
		this.historyComboBox.setSelectedIndex(0);
	}

	@Override
	public void clusteringIterationDeleted(ClusteringIterationDeletedEvent event) {
		for (int i = 0; i < this.historyComboBox.getItemCount(); i++)
			if (((IClusterHistory) this.historyComboBox.getItemAt(i)).getIterationNumber() == event.getIteration()) {
				this.historyComboBox.removeItemAt(i);
				break;
			}
	}
}
