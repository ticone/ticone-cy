package dk.sdu.imada.ticone.gui.util;

import java.awt.Component;
import java.util.Comparator;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkFactory;
import org.cytoscape.model.SavePolicy;

import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.StringExt;

public class CyNetworkComboBox extends ResizableComboBox<CyNetwork> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8067918599249725435L;

	public CyNetworkComboBox() {
		this(new CyNetworkComboBoxModel());
	}

	public CyNetworkComboBox(final CyNetworkComboBoxModel model) {
		super();
		this.setModel(model);

		this.setRenderer(new DefaultListCellRenderer() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -5786187170982601861L;

			@Override
			public Component getListCellRendererComponent(final JList<?> list, Object value, final int index,
					final boolean isSelected, final boolean cellHasFocus) {
				final CyNetwork network = (CyNetwork) value;
				if (network == null)
					value = "";
				else
					value = StringExt.ellipsis(network.getRow(network).get(CyNetwork.NAME, String.class), 42);
				return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			}
		});
	}

	@Override
	protected CyNetwork getInitialPrototypeDisplayValue() {
		final CyNetwork net = ServiceHelper.getService(CyNetworkFactory.class)
				.createNetworkWithPrivateTables(SavePolicy.DO_NOT_SAVE);
		net.getRow(net).set(CyNetwork.NAME, StringExt.repeat("a", this.textWidth));
		return net;
	}

	@Override
	protected ResizableListCellRenderer<CyNetwork> getResizableListCellRendererForTextWidth(final int textWidth) {
		return new CyNetworkListCellRenderer(textWidth);
	}
}

class CyNetworkComparator implements Comparator<CyNetwork> {
	@Override
	public int compare(final CyNetwork o1, final CyNetwork o2) {
		return o1.getRow(o1).get(CyNetwork.NAME, String.class)
				.compareTo(o2.getRow(o2).get(CyNetwork.NAME, String.class));
	};
}

class CyNetworkListCellRenderer extends ResizableListCellRenderer<CyNetwork> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6793893740342693682L;

	public CyNetworkListCellRenderer(final int textWidth) {
		super(textWidth);
	}

	@Override
	protected String getStringForObject(final Object value) {
		final CyNetwork net = (CyNetwork) value;
		return net.getRow(net).get(CyNetwork.NAME, String.class);
	}
}