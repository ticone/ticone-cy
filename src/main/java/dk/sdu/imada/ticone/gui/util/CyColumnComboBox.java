package dk.sdu.imada.ticone.gui.util;

import javax.swing.JComboBox;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyTable;

public class CyColumnComboBox extends JComboBox<CyColumn> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5577678689761386332L;

	protected CyTable table;;

	public void setTable(final CyTable table) {
		this.table = table;
		this.updateItems();
	}

	private void updateItems() {
		this.removeAllItems();
		if (this.table != null) {
			for (final CyColumn col : this.table.getColumns()) {
				this.addItem(col);
			}
		}
		this.setSelectedIndex(-1);
	}

	public CyTable getTable() {
		return this.table;
	}
}
