package dk.sdu.imada.ticone.gui.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Created by christian on 9/9/15.
 */
public class OptionPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2428059480026108411L;

	public OptionPanel(final JLabel optionLabel, final JTextField optionTextField) {
		this.setLayout(new GridBagLayout());

		final GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		this.add(optionLabel, gridBagConstraints);
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		this.add(optionTextField, gridBagConstraints);
	}
}
