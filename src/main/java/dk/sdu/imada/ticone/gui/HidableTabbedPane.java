package dk.sdu.imada.ticone.gui;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicButtonUI;

import dk.sdu.imada.ticone.util.GUIUtility;

public class HidableTabbedPane extends JPanel implements ChangeListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8733401631561697548L;
	protected JTabbedPane tabbedPane;
	protected OneComponentCache oneComponentCache;
	protected Set<ChangeListener> changeListeners;
	protected int lastSelectedTabbedPaneIndex;

	public HidableTabbedPane() {
		super(new BorderLayout());
		this.changeListeners = new HashSet<>();
	}

	protected void initTabbedPane() {
		this.clearTabbedPane();
		this.tabbedPane = createTabbedPane();
		this.lastSelectedTabbedPaneIndex = this.tabbedPane.getSelectedIndex();
		this.tabbedPane.addChangeListener(this);
	}

	/**
	 * @return
	 */
	protected JTabbedPane createTabbedPane() {
		return new JTabbedPane();
	}

	protected void clearTabbedPane() {
		if (this.tabbedPane != null) {
			this.tabbedPane.removeChangeListener(this);
			this.tabbedPane = null;
		}
	}

	public void addTab(final String title, final Component component) {
		try {
			final Runnable runnable = new Runnable() {
				@Override
				public void run() {
					if (HidableTabbedPane.this.getTabCount() == 0) {
						HidableTabbedPane.this.oneComponentCache = new OneComponentCache(component, title);
						HidableTabbedPane.this.add(component, BorderLayout.CENTER);
					} else {
						if (HidableTabbedPane.this.hasOneTab()) {
							HidableTabbedPane.this.removeAll();
							final OneComponentCache oneComp = HidableTabbedPane.this.oneComponentCache;
							HidableTabbedPane.this.clearOneComponent();
							HidableTabbedPane.this.initTabbedPane();
							HidableTabbedPane.this.doAddTab(oneComp.title, oneComp.comp);
							HidableTabbedPane.this.add(HidableTabbedPane.this.tabbedPane, BorderLayout.CENTER);
						}
						HidableTabbedPane.this.doAddTab(title, component);
					}

					if (HidableTabbedPane.this.getParent() instanceof JTabbedPane) {
						final JTabbedPane p = (JTabbedPane) HidableTabbedPane.this.getParent();
						p.setTabComponentAt(p.indexOfComponent(HidableTabbedPane.this), createTabComponent(true));
					}

					HidableTabbedPane.this.fireChanged(new ChangeEvent(HidableTabbedPane.this));
				}
			};
			if (SwingUtilities.isEventDispatchThread())
				runnable.run();
			else
				SwingUtilities.invokeAndWait(runnable);
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected void doAddTab(final String title, final Component component) {
		this.tabbedPane.addTab(title, component);
		this.tabbedPane.setTabComponentAt(this.tabbedPane.getTabCount() - 1, createTabComponent(false));
	}

	protected void clearOneComponent() {
		this.oneComponentCache = null;
	}

	protected boolean hasMoreThanOneTab() {
		return this.tabbedPane != null;
	}

	protected boolean hasOneTab() {
		return this.oneComponentCache != null;
	}

	public void removeTabAt(final int index) {
		final Runnable runnable = new Runnable() {
			@Override
			public void run() {
				JPanel resPanel;
				if (HidableTabbedPane.this.hasMoreThanOneTab()) {
					resPanel = (JPanel) HidableTabbedPane.this.tabbedPane.getComponentAt(index);
					HidableTabbedPane.this.tabbedPane.removeTabAt(index);

					// we have 1 tab left in the tabbed pane
					// remove the single tab from the tabbed pane and add it to the
					// panel directly
					if (HidableTabbedPane.this.tabbedPane.getTabCount() == 1) {
						HidableTabbedPane.this.oneComponentCache = new OneComponentCache(
								HidableTabbedPane.this.tabbedPane.getComponentAt(0),
								HidableTabbedPane.this.tabbedPane.getTitleAt(0));
						HidableTabbedPane.this.clearTabbedPane();
						HidableTabbedPane.this.removeAll();
						HidableTabbedPane.this.add(HidableTabbedPane.this.oneComponentCache.comp, BorderLayout.CENTER);
					} else {
						HidableTabbedPane.this.clearOneComponent();
					}
				} else if (HidableTabbedPane.this.hasOneTab()) {
					resPanel = (JPanel) HidableTabbedPane.this.oneComponentCache.comp;
					HidableTabbedPane.this.remove(resPanel);
					HidableTabbedPane.this.clearOneComponent();

					if (HidableTabbedPane.this.getParent() instanceof JTabbedPane) {
						final JTabbedPane p = (JTabbedPane) HidableTabbedPane.this.getParent();
						p.setTabComponentAt(p.indexOfComponent(HidableTabbedPane.this), null);
					}
				} else
					// there are no tabs
					return;

				if (resPanel instanceof TiconeResultPanel) {
					((TiconeResultPanel) resPanel).onRemove();
					GUIUtility.removeTiconeResultsPanel((TiconeResultPanel) resPanel);
				}
				HidableTabbedPane.this.fireChanged(new ChangeEvent(HidableTabbedPane.this));
			}

		};
		if (SwingUtilities.isEventDispatchThread())
			runnable.run();
		else {
			try {
				SwingUtilities.invokeAndWait(runnable);
			} catch (InvocationTargetException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public int getTabCount() {
		return this.hasMoreThanOneTab() ? this.tabbedPane.getTabCount() : this.hasOneTab() ? 1 : 0;
	}

	public Component getSelectedComponent() {
		return this.hasMoreThanOneTab() ? this.tabbedPane.getSelectedComponent()
				: this.hasOneTab() ? this.oneComponentCache.comp : null;
	}

	public int getSelectedIndex() {
		return this.hasMoreThanOneTab() ? this.tabbedPane.getSelectedIndex() : this.hasOneTab() ? 0 : -1;
	}

	public void setSelectedIndex(final int index) {
		final Runnable runnable = new Runnable() {
			@Override
			public void run() {
				if (HidableTabbedPane.this.hasOneTab())
					return;
				else if (HidableTabbedPane.this.hasMoreThanOneTab())
					HidableTabbedPane.this.tabbedPane.setSelectedIndex(index);
			}
		};

		if (SwingUtilities.isEventDispatchThread())
			runnable.run();
		else {
			try {
				SwingUtilities.invokeAndWait(runnable);
			} catch (InvocationTargetException | InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public void setSelectedComponent(final Component c) {
		final Runnable r = new Runnable() {
			@Override
			public void run() {
				if (HidableTabbedPane.this.hasMoreThanOneTab())
					HidableTabbedPane.this.tabbedPane.setSelectedComponent(c);
			}
		};

		try {
			if (SwingUtilities.isEventDispatchThread())
				r.run();
			else
				SwingUtilities.invokeAndWait(r);
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	public Component getComponentAt(final int index) {
		return this.hasMoreThanOneTab() ? this.tabbedPane.getComponentAt(index)
				: this.hasOneTab() && index == 0 ? this.oneComponentCache.comp : null;
	}

	public void addChangeListener(final ChangeListener l) {
		this.changeListeners.add(l);
	}

	public void removeChangeListener(final ChangeListener l) {
		this.changeListeners.remove(l);
	}

	public void fireChanged(final ChangeEvent e) {
		for (final ChangeListener l : this.changeListeners)
			l.stateChanged(e);
	}

	public JTabbedPane getTabbedPane() {
		return this.tabbedPane;
	}

	@Override
	public void stateChanged(final ChangeEvent e) {
		if (!(e.getSource() == this.tabbedPane))
			return;

		if (this.lastSelectedTabbedPaneIndex != this.tabbedPane.getSelectedIndex()) {
			this.lastSelectedTabbedPaneIndex = this.tabbedPane.getSelectedIndex();
			this.fireChanged(new ChangeEvent(this));
		}
	}

	/**
	 * Component to be used as tabComponent; Contains a JLabel to show the text and
	 * a JButton to close the tab it belongs to
	 */
	class ButtonTabComponent extends JPanel {
		/**
		 * 
		 */
		private static final long serialVersionUID = -7545236940275179630L;
		private boolean isRemoveAll;

		public ButtonTabComponent(final boolean isRemoveAll) {
			// unset default FlowLayout' gaps
			super(new FlowLayout(FlowLayout.LEFT, 0, 0));
			this.isRemoveAll = isRemoveAll;
			this.setOpaque(false);

			// make JLabel read titles from JTabbedPane
			final JLabel label = new JLabel() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 4129245578766656837L;

				@Override
				public String getText() {
					if (!isRemoveAll) {
						final int i = getTabbedPane().indexOfTabComponent(ButtonTabComponent.this);
						if (i != -1) {
							return getTabbedPane().getTitleAt(i);
						}
					} else if (HidableTabbedPane.this.getParent() instanceof JTabbedPane) {
						final JTabbedPane p = (JTabbedPane) HidableTabbedPane.this.getParent();
						final int i = p.indexOfTabComponent(ButtonTabComponent.this);
						if (i != -1) {
							return p.getTitleAt(i);
						}
					}
					return null;
				}
			};

			if (!isRemoveAll) {
				getTabbedPane().getAccessibleContext().addPropertyChangeListener(new PropertyChangeListener() {
					@Override
					public void propertyChange(final PropertyChangeEvent evt) {
						final String property = evt.getPropertyName();
						if (property.equals("AccessibleVisibleData")) {
							// a tab title was changed
							if (evt.getOldValue() instanceof String && evt.getNewValue() instanceof String) {
								tabbedPane.revalidate();
								tabbedPane.repaint();
								tabbedPane.updateUI();
							}
						}
					}
				});

				if (getParent() instanceof JTabbedPane) {
					final JTabbedPane p = (JTabbedPane) getParent();
					final int i = p.indexOfTabComponent(ButtonTabComponent.this);
					if (i != -1) {
						final Icon icon = p.getIconAt(i);
						if (icon != null)
							label.setIcon(icon);
					}
				}
			}

			this.add(label);
			// add more space between the label and the button
			label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));

			// tab button
			final JButton button = createTabButton();
			this.add(button);
			// add more space to the top of the component
			this.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));

			if (!isRemoveAll) {
				final JPopupMenu jPopupMenu = new JPopupMenu();
				final JMenuItem jMenuItem = new JMenuItem("Rename");
				jMenuItem.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(final ActionEvent e) {
						if (!hasMoreThanOneTab())
							return;

						final int i = getTabbedPane().indexOfTabComponent(ButtonTabComponent.this);
						if (i != -1) {
							final Component c = getTabbedPane().getComponentAt(i);
							if (c instanceof TiconeResultPanel) {
								final TiconeResultPanel p = (TiconeResultPanel) c;
								final String userInput = (String) JOptionPane.showInputDialog(ButtonTabComponent.this,
										"The new name:", "Rename '" + p.toString() + "'",
										JOptionPane.INFORMATION_MESSAGE, null, null, p.toString());
								if (userInput != null) {
									p.setResultName(userInput);
								}
							}
						}
					}
				});
				jPopupMenu.add(jMenuItem);
				this.setComponentPopupMenu(jPopupMenu);
				this.addMouseMotionListener(new MouseMotionListener() {

					@Override
					public void mouseMoved(final MouseEvent e) {
						this.redispatchToParent(e);
					}

					@Override
					public void mouseDragged(final MouseEvent e) {
						this.redispatchToParent(e);
					}

					private void redispatchToParent(final MouseEvent e) {
						final Component source = (Component) e.getSource();
						final Component p = ButtonTabComponent.this.getParent().getParent();
						final MouseEvent parentEvent = SwingUtilities.convertMouseEvent(source, e, p);
						p.dispatchEvent(parentEvent);
					}
				});
				this.addMouseListener(new MouseListener() {

					@Override
					public void mouseReleased(final MouseEvent e) {
						this.redispatchToParent(e);
					}

					@Override
					public void mousePressed(final MouseEvent e) {
						this.redispatchToParent(e);
					}

					@Override
					public void mouseExited(final MouseEvent e) {
						this.redispatchToParent(e);
					}

					@Override
					public void mouseEntered(final MouseEvent e) {
						this.redispatchToParent(e);
					}

					@Override
					public void mouseClicked(final MouseEvent e) {
						this.redispatchToParent(e);
					}

					private void redispatchToParent(final MouseEvent e) {
						final Component source = (Component) e.getSource();
						final Component p = ButtonTabComponent.this.getParent().getParent();
						final MouseEvent parentEvent = SwingUtilities.convertMouseEvent(source, e, p);
						p.dispatchEvent(parentEvent);
					}
				});
			}
		}

		class TabButton extends JButton implements ActionListener {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1005750744608340522L;

			public TabButton() {
				final int size = 17;
				this.setPreferredSize(new Dimension(size, size));
				if (ButtonTabComponent.this.isRemoveAll)
					this.setToolTipText("Close all sub-tabs");
				else
					this.setToolTipText("Close this tab");
				// Make the button looks the same for all Laf's
				this.setUI(new BasicButtonUI());
				// Make it transparent
				this.setContentAreaFilled(false);
				// No need to be focusable
				this.setFocusable(false);
				this.setBorder(BorderFactory.createEtchedBorder());
				this.setBorderPainted(false);
				// Making nice rollover effect
				// we use the same listener for all buttons
				this.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseEntered(final MouseEvent e) {
						final Component component = e.getComponent();
						if (component instanceof AbstractButton) {
							final AbstractButton button = (AbstractButton) component;
							button.setBorderPainted(true);
						}
					}

					@Override
					public void mouseExited(final MouseEvent e) {
						final Component component = e.getComponent();
						if (component instanceof AbstractButton) {
							final AbstractButton button = (AbstractButton) component;
							button.setBorderPainted(false);
						}
					}
				});
				this.setRolloverEnabled(true);
				// Close the proper tab by clicking the button
				this.addActionListener(this);
			}

			@Override
			public void actionPerformed(final ActionEvent e) {
				if (!ButtonTabComponent.this.isRemoveAll) {
					if (hasMoreThanOneTab()) {
						final int i = getTabbedPane().indexOfTabComponent(ButtonTabComponent.this);
						if (i != -1) {
							final int result = JOptionPane.showConfirmDialog(this,
									"<html>You are about to close this tab.<br>All its information will be lost. This includes all associated tables, networks and network views.<br><br>Do you want to proceed?</html>",
									"Close Tab", JOptionPane.YES_NO_OPTION);
							if (result == JOptionPane.YES_OPTION) {
								doRemoveTabAt(i);
							}
						}
					}
				} else {
					if (getTabCount() > 0) {
						final int result = JOptionPane.showConfirmDialog(this,
								"<html>You are about to close all tabs.<br>All their information will be lost. This includes all associated tables, networks and network views.<br><br>Do you want to proceed?</html>",
								"Close All Tabs", JOptionPane.YES_NO_OPTION);
						if (result == JOptionPane.YES_OPTION) {
							while (getTabCount() > 0)
								doRemoveTabAt(0);
						}
					}
				}
			}

			protected void doRemoveTabAt(int i) {
				removeTabAt(i);
			}

			// we don't want to update UI for this button
			@Override
			public void updateUI() {
			}

			// paint the cross
			@Override
			protected void paintComponent(final Graphics g) {
				super.paintComponent(g);
				final Graphics2D g2 = (Graphics2D) g.create();
				// shift the image for pressed buttons
				if (this.getModel().isPressed()) {
					g2.translate(1, 1);
				}
				g2.setStroke(new BasicStroke(2));
				g2.setColor(Color.BLACK);
				if (this.getModel().isRollover()) {
					g2.setColor(Color.MAGENTA);
				}
				final int delta = 6;
				g2.drawLine(delta, delta, this.getWidth() - delta - 1, this.getHeight() - delta - 1);
				g2.drawLine(this.getWidth() - delta - 1, delta, delta, this.getHeight() - delta - 1);
				g2.dispose();
			}
		}

		protected TabButton createTabButton() {
			return new TabButton();
		}
	}

	protected ButtonTabComponent createTabComponent(boolean isRemoveAll) {
		return new ButtonTabComponent(isRemoveAll);
	}

	private void onAddSecondComponent() throws InterruptedException {
		try {
			final Runnable runnable = new Runnable() {
				@Override
				public void run() {
					HidableTabbedPane.this.removeAll();
					final OneComponentCache oneComp = HidableTabbedPane.this.oneComponentCache;
					HidableTabbedPane.this.clearOneComponent();
					HidableTabbedPane.this.initTabbedPane();
					HidableTabbedPane.this.doAddTab(oneComp.title, oneComp.comp);
					HidableTabbedPane.this.add(HidableTabbedPane.this.tabbedPane, BorderLayout.CENTER);
				}
			};
			if (SwingUtilities.isEventDispatchThread())
				SwingUtilities.invokeAndWait(runnable);
			else
				runnable.run();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

	}
}

class OneComponentCache {

	protected Component comp;
	// protected int tabCount;
	// keep info of tab if there is only 1
	protected String title;

	/**
	 * 
	 */
	public OneComponentCache(final Component comp, final String title) {
		this.comp = comp;
		this.title = title;
	}
}
