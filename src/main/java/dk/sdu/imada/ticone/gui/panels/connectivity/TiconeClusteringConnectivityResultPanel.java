package dk.sdu.imada.ticone.gui.panels.connectivity;

import java.awt.BorderLayout;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.JTabbedPane;

import org.apache.commons.lang3.tuple.Triple;
import org.cytoscape.application.swing.CySwingApplication;
import org.cytoscape.application.swing.CytoPanel;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.application.swing.CytoPanelState;
import org.cytoscape.model.CyNetwork;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.connectivity.ConnectivityResultWrapper;
import dk.sdu.imada.ticone.gui.TiconeConnectivityResultsPanel;
import dk.sdu.imada.ticone.gui.TiconeResultPanel;
import dk.sdu.imada.ticone.gui.panels.kpm.KPMResultFormPanel;
import dk.sdu.imada.ticone.gui.panels.kpm.KPMResultsTabPanel;
import dk.sdu.imada.ticone.network.TiconeCytoscapeNetwork;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.KPMResultWrapper;
import dk.sdu.imada.ticone.util.ProgramState;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeResultDestroyedEvent;
import dk.sdu.imada.ticone.util.TiconeResultNameChangedEvent;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * Created by christian on 9/9/15.
 */
public class TiconeClusteringConnectivityResultPanel extends TiconeResultPanel<ConnectivityResultWrapper> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8404086408073089941L;

	protected ClusterConnectivityResultFormPanel clusterConnectivityResultFormPanel;
	protected KPMResultsTabPanel kpmResultsTabPanel;
	protected TiconeCytoscapeClusteringResult util;
	protected List<TiconeCytoscapeNetwork> connectivityNetworks;

	public TiconeClusteringConnectivityResultPanel(final TiconeCytoscapeClusteringResult utils,
			final ConnectivityResultWrapper connectivityResult) throws TiconeUnloadingException {
		super(connectivityResult);
		this.util = utils;
		this.connectivityNetworks = new ArrayList<>();
		this.register();
	}

	public void register() throws TiconeUnloadingException {
		this.setLayout(new BorderLayout());
		this.clusterConnectivityResultFormPanel = new ClusterConnectivityResultFormPanel(this, result);
		this.initComponents();
		GUIUtility.addTiconeResultsPanel(this);

		final TiconeConnectivityResultsPanel tabbedPane = GUIUtility.getTiconeConnectivityResultsPanel();
		tabbedPane.addTab(this.toString(), this);
		final int numberTabs = tabbedPane.getTabCount();
		tabbedPane.setSelectedIndex(numberTabs - 1);

		// show the results panel
		final CytoPanel cytoPanel = ServiceHelper.getService(CySwingApplication.class).getCytoPanel(CytoPanelName.EAST);
		cytoPanel.setState(CytoPanelState.DOCK);

		// focus the connectivity tab
		// first focus nothing in order to trigger the stateChanged event of the
		// tabbed pane (necessary for updating available tabs on the control
		// panel)
		((JTabbedPane) tabbedPane.getParent()).setSelectedIndex(-1);
		((JTabbedPane) tabbedPane.getParent()).setSelectedComponent(tabbedPane);

		this.getClusteringResult().addOnDestroyListener(this);

	}

	private void initComponents() {
		this.add(this.clusterConnectivityResultFormPanel.getMainPanel(), BorderLayout.CENTER);
	}

	@Override
	public void saveState(final int tabIndex, final ProgramState state) {
		state.addConnectivityResult(tabIndex, this.result);
	}

	@Override
	public Serializable asSerializable() {
		final ArrayList<Serializable> result = new ArrayList<>();

		result.add(this.result);
		result.add(this.util);
		result.add((ArrayList<TiconeCytoscapeNetwork>) this.connectivityNetworks);

		final ArrayList<KPMResultWrapper> kpmResults = new ArrayList<>();
		if (this.getKpmResultsTabPanel() != null) {
			final List<KPMResultFormPanel> kpmResultPanels = this.getKpmResultsTabPanel().getKpmResultPanels();
			for (final KPMResultFormPanel kpmResultPanel : kpmResultPanels) {
				kpmResults.add(new KPMResultWrapper(
						kpmResultPanel.getNetwork().getRow(kpmResultPanel.getNetwork()).get(CyNetwork.NAME,
								String.class),
						kpmResultPanel.getResultList(), kpmResultPanel.getSelectedPatternForThisResult(),
						kpmResultPanel.getKpmModel()));
			}
		}
		result.add(kpmResults);

		return result;
	}

	@Override
	public TiconeCytoscapeClusteringResult getClusteringResult() {
		return this.util;
	}

	public KPMResultsTabPanel getKpmResultsTabPanel() {
		return this.kpmResultsTabPanel;
	}

	public ConnectivityResultWrapper getClusteringComparisonResult() {
		return this.result;
	}

	public List<Triple<ICluster, ICluster, Set<String>>> getSelectedClusterPairsToVisualizeOnNetwork() {
		// we don't select anything
		return new ArrayList<>();
	}

	@Override
	public void resultNameChanged(final TiconeResultNameChangedEvent event) {
		final String newTitle = event.getNewName();
		JTabbedPane resultsPanel;
		try {
			resultsPanel = GUIUtility.getTiconeConnectivityResultsPanel().getTabbedPane();
		} catch (final TiconeUnloadingException e) {
			return;
		}
		resultsPanel.setTitleAt(resultsPanel.indexOfComponent(this), newTitle);
		resultsPanel.revalidate();
		resultsPanel.repaint();
	}

	@Override
	public void onRemove() {
		super.onRemove();
		for (final TiconeCytoscapeNetwork n : this.connectivityNetworks)
			n.destroy();
	}

	/**
	 * @param connectivityNetworks the connectivityNetworks to set
	 */
	public void addConnectivityNetwork(final TiconeCytoscapeNetwork connectivityNetwork) {
		this.connectivityNetworks.add(connectivityNetwork);
	}

	public static TiconeClusteringConnectivityResultPanel fromSerializable(final Serializable ser)
			throws TiconeUnloadingException, InterruptedException {
		final ArrayList<Serializable> list = (ArrayList<Serializable>) ser;

		final TiconeClusteringConnectivityResultPanel result = new TiconeClusteringConnectivityResultPanel(
				(TiconeCytoscapeClusteringResult) list.get(1), (ConnectivityResultWrapper) list.get(0));

		result.connectivityNetworks = (List<TiconeCytoscapeNetwork>) list.get(2);

		return result;
	}

	@Override
	public void ticoneResultDestroyed(TiconeResultDestroyedEvent event) {
		if (event.getResult().equals(this.util))
			onRemove();
	}
}
