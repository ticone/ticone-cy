/**
 * 
 */
package dk.sdu.imada.ticone.gui.util;

import java.util.HashSet;
import java.util.Set;

import dk.sdu.imada.ticone.gui.TiconeClusteringResultsPanel;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 1, 2019
 *
 */
public class TiconeTabManager implements ITiconeTabSelectionChangedListener {

	private static final TiconeTabManager singleton = new TiconeTabManager();

	public static TiconeTabManager getSingleton() {
		return singleton;
	}

	protected Set<ITiconeTabSelectionChangedListener> clusteringTabSelectionListener;

	/**
	 * only for singleton usage
	 */
	private TiconeTabManager() {
		this.clusteringTabSelectionListener = new HashSet<>();
	}

	protected void forwardClusteringTabChangedEvent(TiconeTabSelectionChangedEvent event) {
		clusteringTabSelectionListener.forEach(l -> l.tabSelectionChanged(event));
	}

	public boolean addClusteringTabSelectionListener(final ITiconeTabSelectionChangedListener listener) {
		return this.clusteringTabSelectionListener.add(listener);
	}

	public boolean removeClusteringTabSelectionListener(final ITiconeTabSelectionChangedListener listener) {
		return this.clusteringTabSelectionListener.remove(listener);
	}

	@Override
	public void tabSelectionChanged(TiconeTabSelectionChangedEvent event) {
		if (event.getSource() instanceof TiconeClusteringResultsPanel) {
			forwardClusteringTabChangedEvent(event);
		}

	}
}
