package dk.sdu.imada.ticone.gui.util;

import java.util.Comparator;

import javax.swing.DefaultComboBoxModel;

/*
 *  Custom model to make sure the items are stored in a sorted order.
 *  The default is to sort in the natural order of the item, but a
 *  Comparator can be used to customize the sort order.
 */
public class SortedComboBoxModel<E> extends DefaultComboBoxModel<E> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9311964619471950L;
	private final Comparator<E> comparator;

	public SortedComboBoxModel(final Comparator<E> comparator) {
		super();
		this.comparator = comparator;
	}

	@Override
	public void addElement(final E element) {
		this.insertElementAt(element, 0);
	}

	@Override
	public void insertElementAt(final E element, int index) {
		final int size = this.getSize();

		// Determine where to insert element to keep model in sorted order
		for (index = 0; index < size; index++) {
			final E o = this.getElementAt(index);

			if (this.comparator.compare(o, element) > 0)
				break;
		}

		super.insertElementAt(element, index);

		// Select an element when it is added to the beginning of the model
		if (index == 0 && element != null) {
			this.setSelectedItem(element);
		}
	}
}