package dk.sdu.imada.ticone.gui.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;

import org.cytoscape.model.CyTable;
import org.cytoscape.model.CyTableFactory;
import org.cytoscape.model.CyTableManager;
import org.cytoscape.model.SavePolicy;
import org.cytoscape.model.events.TableAboutToBeDeletedEvent;
import org.cytoscape.model.events.TableAboutToBeDeletedListener;
import org.cytoscape.model.events.TableAddedEvent;
import org.cytoscape.model.events.TableAddedListener;

import dk.sdu.imada.ticone.network.NetworkUtil;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.StringExt;

public class CyTableComboBox extends ResizableComboBox<CyTable>
		implements TableAddedListener, TableAboutToBeDeletedListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8067918599249725435L;

	public CyTableComboBox() {
		super();
		ServiceHelper.registerService(TableAboutToBeDeletedListener.class, this);
		ServiceHelper.registerService(TableAddedListener.class, this);

		this.setModel(new SortedComboBoxModel<>(new CyTableComparator()));
		// we set the prototype to have a name of length 34, determining the
		// width of the combobox
		this.addPopupMenuListener(new BoundsPopupMenuListener(true, false));

		this.updateTables();
	}

	private void updateTables() {
		final CyTableManager tableManager = ServiceHelper.getService(CyTableManager.class);
		final Set<CyTable> tables = tableManager.getAllTables(false);
		final Iterator<CyTable> tableIterator = tables.iterator();
		this.removeAllItems();

		while (tableIterator.hasNext()) {
			final CyTable table = tableIterator.next();
			if (table != null) {
				this.addItem(table);
			}
		}
		this.setSelectedIndex(-1);
	}

	@Override
	public void handleEvent(final TableAboutToBeDeletedEvent e) {
		final CyTable table = e.getTable();
		this.removeItem(table);
	}

	@Override
	public void handleEvent(final TableAddedEvent e) {
		final CyTable table = e.getTable();
		if (table.isPublic()) {
			this.addItem(table);
		}
	}

	@Override
	protected CyTable getInitialPrototypeDisplayValue() {
		final CyTable table = ServiceHelper.getService(CyTableFactory.class).createTable(
				StringExt.repeat("a", this.textWidth), NetworkUtil.NODE_ATTRIBUTE_OBJECT_NAME, String.class, false,
				false);
		table.setSavePolicy(SavePolicy.DO_NOT_SAVE);
		return table;
	}

	@Override
	protected ResizableListCellRenderer<CyTable> getResizableListCellRendererForTextWidth(final int textWidth) {
		return new CyTableListCellRenderer(textWidth);
	}

}

class CyTableComparator implements Comparator<CyTable> {
	@Override
	public int compare(final CyTable o1, final CyTable o2) {
		return o1.getTitle().compareTo(o2.getTitle());
	};
}

class CyTableListCellRenderer extends ResizableListCellRenderer<CyTable> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2790322874330406520L;

	public CyTableListCellRenderer(final int textWidth) {
		super(textWidth);
	}

	@Override
	protected String getStringForObject(final Object value) {
		return ((CyTable) value).getTitle();
	}
}