package dk.sdu.imada.ticone.gui.panels.popup;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.splitpattern.ISplitCluster;
import dk.sdu.imada.ticone.clustering.splitpattern.ISplitClusterContainer;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.ClusterFeatureAverageSimilarity;
import dk.sdu.imada.ticone.feature.FeatureCalculationException;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartWithButtonsPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.table.TableFactory;
import dk.sdu.imada.ticone.util.ActionContainer;
import dk.sdu.imada.ticone.util.ClusterStatusMapping;
import dk.sdu.imada.ticone.util.ColorUtility;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * Created by christian on 9/4/15.
 */
public class SplitPatternAcceptPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3216691123200907614L;

	ISplitClusterContainer splitPatternContainer;
	ISplitCluster splitPattern;
	JFrame frame;

	protected TiconeClusteringResultPanel resultPanel;
	private ClusterFeatureAverageSimilarity cfas;

	public SplitPatternAcceptPanel(final ISplitCluster splitPatternInterface,
			final ISplitClusterContainer splitPatternContainer, final JFrame frame,
			final TiconeClusteringResultPanel resultPanel)
			throws InterruptedException, IncompatibleSimilarityFunctionException {
		super();
		this.resultPanel = resultPanel;
		this.splitPattern = splitPatternInterface;
		this.splitPatternContainer = splitPatternContainer;
		this.frame = frame;
		this.cfas = new ClusterFeatureAverageSimilarity(resultPanel.getResult().getSimilarityFunction());
		this.setLayout(new BorderLayout());
		this.initComponents();
	}

	private void initComponents() throws InterruptedException {
		final JPanel tempPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(10, 10, 10, 10);
		constraints.anchor = GridBagConstraints.NORTHWEST;
		final JPanel oldPatternPanel = this.setupOldPatternPanel(this.splitPatternContainer.getOldPattern());
		constraints.gridx = 0;
		constraints.gridy = 0;
		tempPanel.add(oldPatternPanel, constraints);

		final JPanel newPatternsPanel = this.setupNewPatternsPanel(this.splitPatternContainer.getNewClusters());
		constraints.gridx = 0;
		constraints.gridy = 1;
		tempPanel.add(newPatternsPanel, constraints);

		final JPanel buttonPanel = this.setupButtonPanel();
		constraints.gridx = 0;
		constraints.gridy = 2;
		tempPanel.add(buttonPanel, constraints);
		final JScrollPane scrollPane = new JScrollPane(tempPanel);
		this.add(scrollPane);
	}

	private JPanel setupNewPatternsPanel(final IClusterObjectMapping patternObjectMapping) throws InterruptedException {
		final JPanel newPatternsPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.insets = new Insets(10, 10, 10, 10);

		final JLabel newPatternLabel = new JLabel("New clusters:");
		constraints.gridx = 0;
		constraints.gridy = 0;
		newPatternsPanel.add(newPatternLabel, constraints);

		constraints.gridx = 0;
		constraints.gridy = 1;
		for (final ICluster pattern : patternObjectMapping.getClusters()) {
			JPanel tempPanel;
			try {
				final ITimeSeriesObjectList timeSeriesDatas = pattern.getObjects();
				tempPanel = this.setupPatternPanel(pattern, timeSeriesDatas, true);
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException
					| SimilarityCalculationException | FeatureCalculationException | IncorrectlyInitializedException
					| IncompatibleFeatureAndObjectException e) {
				tempPanel = null;
			}
			newPatternsPanel.add(tempPanel, constraints);
			constraints.gridx++;
		}

		return newPatternsPanel;
	}

	private JPanel setupPatternPanel(final ICluster pattern, final ITimeSeriesObjectList timeSeriesDatas,
			final boolean newPattern) throws IncompatiblePrototypeComponentException, MissingPrototypeException,
			InterruptedException, SimilarityCalculationException, FeatureCalculationException,
			IncorrectlyInitializedException, IncompatibleFeatureAndObjectException {
		final JPanel patternPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;

		final JLabel patternNumberLabel = new JLabel("Cluster ID: " + pattern.getName());
		constraints.gridx = 0;
		constraints.gridy++;
		patternPanel.add(patternNumberLabel, constraints);

		final int numberOfSets = timeSeriesDatas.size();
		final int objectPerSet = timeSeriesDatas.get(0).getPreprocessedTimeSeriesList().length;

		String numberOfSetsString = "Number of objects: " + timeSeriesDatas.size();
		if (objectPerSet > 1) {
			numberOfSetsString = "Number of object (sets): " + numberOfSets + "  (" + (objectPerSet * numberOfSets)
					+ ")";
		}

		final JLabel numberOfObjectsLabel = new JLabel(numberOfSetsString);
		constraints.gridx = 0;
		constraints.gridy++;
		patternPanel.add(numberOfObjectsLabel, constraints);

		final JLabel avgSimLabel = new JLabel(
				"Average Similarity: " + String.format("%.3G", cfas.calculate(pattern).getValue().get()));
		constraints.gridx = 0;
		constraints.gridy++;
		patternPanel.add(avgSimLabel, constraints);

		final JPanel tempPanel = this.setupGraphPanel(pattern, timeSeriesDatas, newPattern);
		constraints.gridx = 0;
		constraints.gridy++;
		patternPanel.add(tempPanel, constraints);

		return patternPanel;
	}

	private JPanel setupOldPatternPanel(final ICluster pattern) throws InterruptedException {
		final ITimeSeriesObjectList timeSeriesDatas = pattern.getObjects();

		final JPanel oldPatternPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;

		final JLabel oldPatternLabel = new JLabel("Old cluster");
		constraints.gridx = 0;
		constraints.gridy = 0;
		oldPatternPanel.add(oldPatternLabel, constraints);

		constraints.gridx = 0;
		constraints.gridy = 1;
		try {
			oldPatternPanel.add(this.setupPatternPanel(pattern, timeSeriesDatas, false), constraints);
		} catch (IncompatiblePrototypeComponentException | MissingPrototypeException | SimilarityCalculationException
				| FeatureCalculationException | IncorrectlyInitializedException
				| IncompatibleFeatureAndObjectException e) {
		}
		return oldPatternPanel;
	}

	private JPanel setupGraphPanel(final ICluster pattern, final ITimeSeriesObjectList timeSeriesDatas,
			final boolean newPatterns)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException {
		final JPanel patternPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.WEST;
		constraints.insets = new Insets(10, 10, 10, 10);

		final ClusterChartWithButtonsPanel graphOptionsPanel = new ClusterChartWithButtonsPanel(pattern,
				timeSeriesDatas, false, this.resultPanel);
		final ClusterChartContainer patternGraphPanelContainer = graphOptionsPanel.getPatternGraphPanelContainer();
		// PatternGraphPanelContainer patternGraphPanelContainer = new
		// PatternGraphPanelContainer();
		// patternGraphPanelContainer.setupData(pattern, timeSeriesDatas);
		constraints.gridx = 0;
		constraints.gridy = 0;
		// patternPanel.add(patternGraphPanelContainer.createChartPanel(),
		// constraints);
		patternPanel.add(graphOptionsPanel, constraints);

		// Color the candidate objects another color (complement color)
		if (newPatterns && this.splitPatternContainer.getCandidates() != null) {
			final Color graphColor = patternGraphPanelContainer.getGraphColor();

			final Color complementColor = ColorUtility.calculateComplementColor(graphColor);
			int candidateIndex = -1;
			final ITimeSeriesObjectList candidates = this.splitPatternContainer.getCandidates().asList();
			int candidateNumber = -1;
			for (int i = 0; i < candidates.size(); i++) {
				final int index = patternGraphPanelContainer.getObjectDatasetIndex(candidates.get(i));
				if (index > 0) {
					candidateIndex = index;
					candidateNumber = i;
					break;
				}
			}
			for (int i = 0; i < candidates.get(candidateNumber).getPreprocessedTimeSeriesList().length; i++) {
				// TODO
				// patternGraphPanelContainer.getjFreeChart().getXYPlot().getRenderer().setSeriesPaint(candidateIndex
				// + i,
				// complementColor);
			}
		}

		return patternPanel;
	}

	private JPanel setupButtonPanel() {
		final JPanel buttonPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets = new Insets(10, 10, 10, 10);

		final JButton saveButton = new JButton("Save new clusters");
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				try {
					SplitPatternAcceptPanel.this.saveNewPatternsAction();
				} catch (InterruptedException | TiconeUnloadingException e) {
				}
			}
		});
		constraints.gridx = 0;
		constraints.gridy = 0;
		buttonPanel.add(saveButton, constraints);

		final JButton discardButton = new JButton("Discard changes");
		discardButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				SplitPatternAcceptPanel.this.discardPatternsAction();
			}
		});
		constraints.gridx++;
		constraints.gridy = 0;
		buttonPanel.add(discardButton, constraints);

		return buttonPanel;
	}

	private void saveNewPatternsAction() throws InterruptedException, TiconeUnloadingException {
		/*
		 * ITimeSeriesClusteringWithOverrepresentedPatterns
		 * timeSeriesClusteringWithOverrepresentedPatterns =
		 * OverrepresentedPatternUtil.getUtilsForIndex(GUIUtility.
		 * getCurrentlySelectedResultIndex()).
		 * getTimeSeriesClusteringWithOverrepresentedPatterns();
		 * timeSeriesClusteringWithOverrepresentedPatterns.
		 * applySplittedPatterns(splitPattern, splitPatternContainer);
		 */

		final ActionContainer actionContainer = new ActionContainer(this.splitPattern, this.splitPatternContainer);
		this.resultPanel.getClusteringProcess().addNewActionsToApplyBeforeNextIteration(actionContainer);
		GUIUtility.showActionPanel(this.resultPanel);

		this.addNewPatternsToStatusMapping();

		this.setDeleteStatusToOldPattern();

		GUIUtility.updateGraphPanel(this.resultPanel);

		final IClusterObjectMapping pom = this.resultPanel.getClusteringResult().getClusteringProcess()
				.getLatestClustering();

		try {
			TableFactory.setupClusterTables(this.resultPanel.getClusteringResult());
		} catch (final Exception e) {
			e.printStackTrace();
		}

		this.frame.dispose();
	}

	private void addNewPatternsToStatusMapping() {
		final ClusterStatusMapping patternStatusMapping = this.resultPanel.getClusteringResult()
				.getClusterStatusMapping();
		final IClusterObjectMapping patternObjectMapping = this.splitPatternContainer.getNewClusters();
		for (final ICluster pattern : patternObjectMapping.getClusters()) {
			patternStatusMapping.addCluster(pattern, pattern.getObjects(), true, true, false);
		}
	}

	private void setDeleteStatusToOldPattern() {
		final ClusterStatusMapping patternStatusMapping = this.resultPanel.getClusteringResult()
				.getClusterStatusMapping();
		patternStatusMapping.setDeleted(this.splitPatternContainer.getOldPattern());
	}

	private void discardPatternsAction() {
		this.frame.dispose();
	}
}
