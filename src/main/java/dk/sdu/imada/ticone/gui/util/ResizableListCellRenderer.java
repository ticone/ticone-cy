package dk.sdu.imada.ticone.gui.util;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import dk.sdu.imada.ticone.util.StringExt;

public abstract class ResizableListCellRenderer<E> extends DefaultListCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2053861386192753048L;
	protected int textWidth;

	public ResizableListCellRenderer(final int textWidth) {
		super();
		this.textWidth = textWidth;
	}

	@Override
	public Component getListCellRendererComponent(final JList<?> list, Object value, final int index,
			final boolean isSelected, final boolean cellHasFocus) {
		if (value != null) {
			value = this.getStringForObject(value);
			// showing the currently selected item
			if (index == -1) {
				value = StringExt.ellipsis((String) value, this.textWidth);
			}
		} else {
			value = "";
		}
		return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}

	protected abstract String getStringForObject(final Object value);
}