package dk.sdu.imada.ticone.gui.util;

import java.awt.Graphics;

import javax.swing.JComboBox;

public abstract class ResizableComboBox<E> extends JComboBox<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7195583058551004617L;
	protected int textWidth;

	public ResizableComboBox() {
		this(35);
	}

	public ResizableComboBox(final int initialTextWidth) {
		super();

		this.textWidth = initialTextWidth;
		this.setPrototypeDisplayValue(this.getInitialPrototypeDisplayValue());
		this.addPopupMenuListener(new BoundsPopupMenuListener(true, false));
		this.setRenderer(this.getResizableListCellRendererForTextWidth(this.textWidth));
	}

	protected abstract E getInitialPrototypeDisplayValue();

	@Override
	protected void paintComponent(final Graphics g) {
		final int width = this.getWidth();
		int textWidth = 0;
		String text = "";
		while (textWidth < width) {
			text += "a";
			textWidth = g.getFontMetrics().stringWidth(text);
		}
		this.setTextWidth(text.length());
		super.paintComponent(g);
	}

	/**
	 * Set the number of chars to be displayed in the combobox.
	 * 
	 * @param textWidth
	 */
	protected void setTextWidth(final int textWidth) {
		if (this.textWidth != textWidth) {
			this.textWidth = textWidth;
			this.setRenderer(this.getResizableListCellRendererForTextWidth(textWidth));
		}
	}

	protected abstract ResizableListCellRenderer<E> getResizableListCellRendererForTextWidth(final int textWidth);
}