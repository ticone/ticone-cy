package dk.sdu.imada.ticone.gui.panels.popup;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.cytoscape.work.TaskManager;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusteringMethod;
import dk.sdu.imada.ticone.clustering.IClusteringMethodBuilder;
import dk.sdu.imada.ticone.clustering.splitpattern.ISplitCluster;
import dk.sdu.imada.ticone.clustering.splitpattern.SplitClusterBasedOnTwoMostDissimilarObjects;
import dk.sdu.imada.ticone.clustering.splitpattern.SplitClusterWithClustering;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.tasks.split.SplitPatternTaskFactory;
import dk.sdu.imada.ticone.util.ServiceHelper;

/**
 * Created by christian on 8/27/15.
 */
public class SplitPatternPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4424774964678371286L;

	private final JFrame window;
	private final int clusterNumber;

	private JRadioButton pearsonSimilarityRadioButton;
	private JRadioButton euclideanSimilarityRadioButton;

	private JRadioButton leastSimilarObjectsRadioButton;
	private JRadioButton clusteringRadioButton;

	private JTextField numberOfNewPatternsTextField;
	private JLabel numberOfNewPatternsWantedLabel;

	private JLabel clusterFunctionLabel;

	protected TiconeClusteringResultPanel resultPanel;

	public SplitPatternPanel(final JFrame window, final int clusterNumber,
			final TiconeClusteringResultPanel resultPanel) {
		super();
		this.resultPanel = resultPanel;
		this.clusterNumber = clusterNumber;
		this.window = window;
		this.setLayout(new GridBagLayout());
		this.initComponents();
	}

	private void initComponents() {
		final JPanel mainPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;

		final JLabel infoLabel = new JLabel("How would you like to split the cluster?");
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 2;
		mainPanel.add(infoLabel, constraints);
		constraints.gridwidth = 1;

		final ButtonGroup splitMethodButtonGroup = new ButtonGroup();

		this.leastSimilarObjectsRadioButton = new JRadioButton("Based on least similar objects");
		splitMethodButtonGroup.add(this.leastSimilarObjectsRadioButton);
		this.leastSimilarObjectsRadioButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				SplitPatternPanel.this.showNumberOfPatternsWanted(false);
			}
		});
		this.leastSimilarObjectsRadioButton.setSelected(true);
		constraints.gridx = 0;
		constraints.gridy = 1;
		mainPanel.add(this.leastSimilarObjectsRadioButton, constraints);

		this.clusteringRadioButton = new JRadioButton("With a new clustering");
		splitMethodButtonGroup.add(this.clusteringRadioButton);
		this.clusteringRadioButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				SplitPatternPanel.this.showNumberOfPatternsWanted(true);
			}
		});
		constraints.gridx = 1;
		constraints.gridy = 1;
		mainPanel.add(this.clusteringRadioButton, constraints);

		this.numberOfNewPatternsWantedLabel = new JLabel("Number of new clusters: ");
		constraints.gridx = 1;
		constraints.gridy = 2;
		mainPanel.add(this.numberOfNewPatternsWantedLabel, constraints);

		this.numberOfNewPatternsTextField = new JTextField("2");
		this.numberOfNewPatternsTextField.setPreferredSize(new Dimension(50, 24));
		constraints.gridx = 1;
		constraints.gridy = 3;
		mainPanel.add(this.numberOfNewPatternsTextField, constraints);

		this.clusterFunctionLabel = new JLabel("Clustering method:");
		constraints.gridx = 0;
		constraints.gridy = 2;
		mainPanel.add(this.clusterFunctionLabel, constraints);

		this.showNumberOfPatternsWanted(false);

		final JLabel similarityLabel = new JLabel("Choose similarity method");
		constraints.gridx = 0;
		constraints.gridy = 3;
		mainPanel.add(similarityLabel, constraints);

		final ButtonGroup similarityButtonGroup = new ButtonGroup();
		this.pearsonSimilarityRadioButton = new JRadioButton("Shape (Pearson)");
		similarityButtonGroup.add(this.pearsonSimilarityRadioButton);
		this.pearsonSimilarityRadioButton.setSelected(true);
		constraints.gridx = 0;
		constraints.gridy = 4;
		mainPanel.add(this.pearsonSimilarityRadioButton, constraints);

		this.euclideanSimilarityRadioButton = new JRadioButton("Magnitude (Euclidean)");
		similarityButtonGroup.add(this.euclideanSimilarityRadioButton);
		constraints.gridx = 1;
		constraints.gridy = 4;
		mainPanel.add(this.euclideanSimilarityRadioButton, constraints);

		final JButton splitPatternButton = new JButton("Split cluster");
		splitPatternButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				SplitPatternPanel.this.splitPatternAction();
			}
		});
		constraints.gridx = 0;
		constraints.gridy = 5;
		mainPanel.add(splitPatternButton, constraints);

		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.insets = new Insets(10, 10, 10, 10);
		this.add(mainPanel, constraints);
	}

	private void showNumberOfPatternsWanted(final boolean flag) {
		this.numberOfNewPatternsTextField.setVisible(flag);
		this.numberOfNewPatternsWantedLabel.setVisible(flag);
		this.clusterFunctionLabel.setVisible(flag);
		this.window.pack();
	}

	private void splitPatternAction() {
		if (this.leastSimilarObjectsRadioButton.isSelected()) {
			this.splitPatternBasedOnLeastSimilarObjects();
		}
		if (this.clusteringRadioButton.isSelected()) {
			this.splitPatternWithClustering();
		}

	}

	private void splitPatternBasedOnLeastSimilarObjects() {
		this.window.dispose();
		final ISplitCluster splitPattern = new SplitClusterBasedOnTwoMostDissimilarObjects(this.getSimilarity(),
				this.resultPanel.getClusteringResult().getPrototypeBuilder());

		final SplitPatternTaskFactory splitPatternTaskFactory = new SplitPatternTaskFactory(splitPattern,
				this.clusterNumber, this.resultPanel);
		ServiceHelper.getService(TaskManager.class).execute(splitPatternTaskFactory.createTaskIterator());

	}

	private void splitPatternWithClustering() {
		this.window.dispose();
		IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clustering = this.resultPanel
				.getResult().getClusteringMethodBuilder();

		final int numberOfNewPatterns = Integer.parseInt(this.numberOfNewPatternsTextField.getText());
		final ISplitCluster splitPattern = new SplitClusterWithClustering(numberOfNewPatterns, clustering, 42);

		final SplitPatternTaskFactory splitPatternTaskFactory = new SplitPatternTaskFactory(splitPattern,
				this.clusterNumber, this.resultPanel);
		ServiceHelper.getService(TaskManager.class).execute(splitPatternTaskFactory.createTaskIterator());
	}

	private ISimilarityFunction getSimilarity() {
		if (this.pearsonSimilarityRadioButton.isSelected()) {
			return new PearsonCorrelationFunction(this.resultPanel.getClusteringResult().getTimePointWeighting());
		} else if (this.euclideanSimilarityRadioButton.isSelected()) {
			return new InverseEuclideanSimilarityFunction(
					this.resultPanel.getClusteringResult().getTimePointWeighting());
		}
		return new PearsonCorrelationFunction(this.resultPanel.getClusteringResult().getTimePointWeighting());
	}
//
//	private ITimeSeriesObjectList getPatternsData() {
//		return this.clusterNumber.getObjects();
//	}
}
