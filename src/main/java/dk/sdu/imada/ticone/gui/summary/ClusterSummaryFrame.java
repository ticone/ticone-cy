/**
 * 
 */
package dk.sdu.imada.ticone.gui.summary;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.clustering.compare.ClusterIDComparator;
import dk.sdu.imada.ticone.gui.WrapLayout;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.util.ClusterComparatorComboBox;
import dk.sdu.imada.ticone.gui.util.ScreenImage;
import dk.sdu.imada.ticone.util.ExportToPDFUtil;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.IClusterHistory;
import dk.sdu.imada.ticone.util.ITiconeResultDestroyedListener;
import dk.sdu.imada.ticone.util.TiconeResultDestroyedEvent;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 20, 2019
 *
 */
public class ClusterSummaryFrame extends JFrame implements WindowListener, ITiconeResultDestroyedListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4406767250413148271L;

	private final TiconeClusteringResultPanel resultPanel;
	private final ClusterSummaryPanel summaryPanel;

	public ClusterSummaryFrame(final TiconeClusteringResultPanel resultPanel) throws InterruptedException {
		super(String.format("Cluster Summary: %s Iteration %d", resultPanel.toString(),
				resultPanel.getResult().getClusterHistory().getIterationNumber()));
		this.resultPanel = resultPanel;
		this.resultPanel.getResult().addOnDestroyListener(this);

		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		final TiconeCytoscapeClusteringResult clusteringResult = resultPanel.getClusteringResult();
		final IClusterObjectMapping pom = clusteringResult.getClusteringProcess().getLatestClustering();

		summaryPanel = new ClusterSummaryPanel(pom, resultPanel.getClusteringResult(),
				clusteringResult.getFeatureStore());
		final JScrollPane scrollPane1 = new JScrollPane(summaryPanel);
		final JPanel windowPanel = new JPanel(new BorderLayout());
		windowPanel.add(scrollPane1, BorderLayout.CENTER);

		final JPanel buttonPanel = new JPanel(new WrapLayout(FlowLayout.CENTER, 5, 5));

		// cluster sorting
		buttonPanel.add(new JLabel("Sort clusters by: "));
		final ClusterComparatorComboBox chooseClusterSort = new ClusterComparatorComboBox(resultPanel.getResult());
		chooseClusterSort.addItemsInitializedListener(summaryPanel);
		chooseClusterSort.addItemsChangedListener(summaryPanel);
		chooseClusterSort.addItemListener(summaryPanel);
		chooseClusterSort.setSelectedItem(
				new ClusterIDComparator().setStatusMapping(clusteringResult.getClusterStatusMapping()));

		buttonPanel.add(chooseClusterSort);
		final JCheckBox sortDecreasing = new JCheckBox("decreasing");
		sortDecreasing.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(final ChangeEvent e) {
				chooseClusterSort.setDecreasing(sortDecreasing.isSelected());
			}
		});
		buttonPanel.add(sortDecreasing);

		// cluster chart dimensions
		buttonPanel.add(new JLabel("Cluster Chart Width:"));
		final JSpinner chooseClusterChartWidth = new JSpinner(new SpinnerNumberModel(400, 50, 800, 1));
		chooseClusterChartWidth.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(final ChangeEvent e) {
				try {
					summaryPanel.setClusterChartPanelWidth((int) chooseClusterChartWidth.getValue());
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		});
		buttonPanel.add(chooseClusterChartWidth);

		buttonPanel.add(new JLabel("Cluster Chart Height:"));
		final JSpinner chooseClusterChartHeight = new JSpinner(new SpinnerNumberModel(160, 30, 400, 1));
		chooseClusterChartHeight.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(final ChangeEvent e) {
				try {
					summaryPanel.setClusterChartPanelHeight((int) chooseClusterChartHeight.getValue());
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		});
		buttonPanel.add(chooseClusterChartHeight);

		// export to file
		buttonPanel.add(new JLabel("Export cluster summary to file:"));

		final JCheckBox saveAllIterations = new JCheckBox("Include all iterations");
		buttonPanel.add(saveAllIterations);

		final JButton saveResultsButton = new JButton("Export");
		saveResultsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				final JFileChooser jFileChooser = new JFileChooser();
				if (saveAllIterations.isSelected()) {
					final FileNameExtensionFilter fnef = new FileNameExtensionFilter("PDF files (pdf)", "pdf");
					jFileChooser.setFileFilter(fnef);
					final int choice = jFileChooser.showSaveDialog(ClusterSummaryFrame.this);
					if (choice == JFileChooser.APPROVE_OPTION) {
						try {
							exportAllIterationsClusterSummaryToPDF(jFileChooser.getSelectedFile().getAbsolutePath(),
									summaryPanel.getClusterChartPanelWidth(), summaryPanel.getClusterChartPanelHeight(),
									summaryPanel.getClusterComparator());
						} catch (final TiconeUnloadingException | InterruptedException e1) {
						}
					}
				} else {
					final FileNameExtensionFilter fnef = new FileNameExtensionFilter("Image files (png, jpg)", "png",
							"jpg");
					jFileChooser.setFileFilter(fnef);
					final int choice = jFileChooser.showSaveDialog(ClusterSummaryFrame.this);
					if (choice == JFileChooser.APPROVE_OPTION) {
						final String filename = jFileChooser.getSelectedFile().getAbsolutePath();
						ExportToPDFUtil.printPanelToPNG(summaryPanel, filename);
					}
				}
			}
		});
		buttonPanel.add(saveResultsButton);

		windowPanel.add(buttonPanel, BorderLayout.SOUTH);

		scrollPane1.getVerticalScrollBar().setUnitIncrement(24);
		this.add(windowPanel);
	}

	protected void exportAllIterationsClusterSummaryToPDF(final String path, final int clusterChartWidth,
			final int clusterChartHeight, final Comparator<ICluster> comp)
			throws TiconeUnloadingException, InterruptedException {
		final List<BufferedImage> images = new ArrayList<>();
		final TiconeCytoscapeClusteringResult utils = GUIUtility.getCurrentlySelectedClusteringResultPanel()
				.getClusteringResult();
		IClusterHistory hist = utils.getClusterHistory();

		final int clusterChartxMargin = 5, clusterChartyMargin = 5;

		do {
			final IClusterObjectMapping pom = hist.getClusterObjectMapping();

			final int[] xy = this.calculateNumberRowsColumns(clusterChartWidth, clusterChartHeight,
					pom.getClusters().size());

			final int w = clusterChartWidth;
			final int h = clusterChartHeight + 90;

			final int summaryPanelWidth = ((xy[0] * w + (xy[0] + 1) * clusterChartxMargin));
			final int summaryPanelHeight = ((xy[1] * h + (xy[1] + 1) * clusterChartyMargin));

			final ClusterSummaryPanel clusterSummaryPanel = new ClusterSummaryPanel(pom, utils, clusterChartWidth,
					clusterChartHeight, utils.getFeatureStore());
			clusterSummaryPanel.setClusterComparator(comp);

			// dimensions for A4 landscape
			clusterSummaryPanel.setSize(new Dimension(summaryPanelWidth, summaryPanelHeight));
			clusterSummaryPanel.setPreferredSize(new Dimension(summaryPanelWidth, summaryPanelHeight));
			clusterSummaryPanel.doLayout();

			final JPanel mainPanel = new JPanel(new BorderLayout());
			mainPanel.add(clusterSummaryPanel, BorderLayout.CENTER);

			final JPanel iterationTitlePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			iterationTitlePanel.setBackground(Color.WHITE);
			final JLabel iterationTitleLabel = new JLabel(hist.getNote());
			iterationTitleLabel.setBackground(Color.WHITE);
			iterationTitleLabel.setFont(iterationTitleLabel.getFont().deriveFont(Font.BOLD, 30));
			iterationTitlePanel.add(iterationTitleLabel);

			mainPanel.add(iterationTitlePanel, BorderLayout.NORTH);

			final JScrollPane scrollPane1 = new JScrollPane(mainPanel);

			images.add(0, ScreenImage.createImage(scrollPane1));

			hist = hist.getParent();
		} while (hist != null);
		ExportToPDFUtil.printToPDF(images, path);

	}

	protected int[] calculateNumberRowsColumns(final int w, final int h, final int k) {
		final double a4ratioLandscape = 1024.0 / 724;

		final int x = (int) Math.round(Math.sqrt(a4ratioLandscape * k * (h + 90) / w));
		final int y = (int) Math.round(Math.ceil((double) k / x));
		return new int[] { x, y };
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
		this.summaryPanel.onDispose();
		this.resultPanel.getResult().removeOnDestroyListener(this);
		this.resultPanel.getResult().removeClusteringIterationAddedListener(this.summaryPanel);
		this.resultPanel.getResult().removeClusteringIterationDeletionListener(this.summaryPanel);
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void ticoneResultDestroyed(TiconeResultDestroyedEvent event) {
		EventQueue.invokeLater(() -> this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING)));
		this.resultPanel.getResult().removeOnDestroyListener(this);
	}
}
