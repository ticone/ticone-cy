package dk.sdu.imada.ticone.gui.panels.connectivity;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.jfree.chart.ChartPanel;

import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartWithTitle;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartWithTitleCellRenderer;
import dk.sdu.imada.ticone.gui.panels.comparison.ClusteringComparisonResultFormPanel.COLUMN_INDEX;
import dk.sdu.imada.ticone.gui.util.VerticalTableHeaderCellRenderer;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;

public class ConnectivityTable extends JTable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6581332384803196272L;

	public ConnectivityTable(final String[] columnNames) {
		super();

		this.setRowHeight(90);
		this.setShowVerticalLines(true);
		this.setShowHorizontalLines(true);
//		this.setFocusable(false);
		this.setColumnSelectionAllowed(true);
		this.setRowSelectionAllowed(true);

		this.setModel(new ConnectivityTableModel(columnNames));

		final TableColumnModel columnModel = this.getColumnModel();
		TableColumn column;
		for (int i = 0; i < 2; i++) {
			column = columnModel.getColumn(i);
			column.setPreferredWidth(100);
			column.setMinWidth(100);
		}
		for (int i = 2; i < columnModel.getColumnCount(); i++) {
			column = columnModel.getColumn(i);
			column.setMaxWidth(100);
		}

		this.setAutoCreateRowSorter(true);

		// Set vertical header labels
		final TableCellRenderer headerRenderer = new VerticalTableHeaderCellRenderer();
		final Enumeration<TableColumn> columnEnum = this.getColumnModel().getColumns();
		while (columnEnum.hasMoreElements()) {
			columnEnum.nextElement().setHeaderRenderer(headerRenderer);
		}

		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				final int row = ConnectivityTable.this.rowAtPoint(e.getPoint());
				final int col = ConnectivityTable.this.columnAtPoint(e.getPoint());

				if (col == COLUMN_INDEX.CLUSTER1.ordinal() || col == COLUMN_INDEX.CLUSTER2.ordinal()) {
					final ClusterChartWithTitle chart = (ClusterChartWithTitle) ConnectivityTable.this.getValueAt(row,
							col);
					try {
						final ChartPanel largeChartPanel = chart.getChartPanelContainer().getLargeChartPanel();
						final JFrame newWindow = new JFrame(chart.getCluster().getName());
						newWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						newWindow.add(largeChartPanel);
						newWindow.setSize(750, 400);
						newWindow.setVisible(true);
					} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e1) {
						e1.printStackTrace();
					} catch (InterruptedException e1) {
					}
				}
			}
		});
	}

	@Override
	public TableCellRenderer getCellRenderer(final int row, final int column) {
		if (column <= 1)
			return new ClusterChartWithTitleCellRenderer();
		return new BackgroundColorCellRenderer();
	}

	@Override
	public TableCellEditor getCellEditor(final int row, final int column) {
		if (column <= 1)
			return new ClusterChartWithTitleCellRenderer();
		return super.getCellEditor(row, column);
	}
}