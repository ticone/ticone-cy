package dk.sdu.imada.ticone.gui.util;

import java.util.Comparator;
import java.util.Objects;

import dk.sdu.imada.ticone.clustering.IVisualClusteringAddedListener;
import dk.sdu.imada.ticone.clustering.IVisualClusteringRemovedListener;
import dk.sdu.imada.ticone.clustering.VisualClusteringAddedEvent;
import dk.sdu.imada.ticone.clustering.VisualClusteringRemovedEvent;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.util.GUIUtility;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 26, 2018
 *
 */
public class VisualClusteringComboBox extends ResizableComboBox<TiconeClusteringResultPanel>
		implements IVisualClusteringAddedListener, IVisualClusteringRemovedListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4860045506854941592L;

	public VisualClusteringComboBox() {
		super();
		GUIUtility.getVisualClusteringManager().addClusteringRemovedListener(this);
		GUIUtility.getVisualClusteringManager().addClusteringAddedListener(this);

		this.setModel(new SortedComboBoxModel<>(new TiconeCytoscapeClusteringResultComparator()));
		// we set the prototype to have a name of length 34, determining the
		// width of the combobox
		this.addPopupMenuListener(new BoundsPopupMenuListener(true, false));

		this.updateClusterings();
	}

	private void updateClusterings() {
		for (final TiconeClusteringResultPanel result : GUIUtility.getVisualClusteringManager()
				.getClusteringResults()) {
			this.addItem(result);
		}
		this.setSelectedIndex(-1);
	}

	@Override
	public void clusteringAdded(final VisualClusteringAddedEvent event) {
		this.addItem(event.getClusteringResult());
	}

	@Override
	public void clusteringRemoved(final VisualClusteringRemovedEvent e) {
		this.removeItem(e.getClusteringResult());
	}

	@Override
	protected TiconeClusteringResultPanel getInitialPrototypeDisplayValue() {
		// TODO
		return null;
	}

	@Override
	protected ResizableListCellRenderer<TiconeClusteringResultPanel> getResizableListCellRendererForTextWidth(
			final int textWidth) {
		return new TiconeClusteringResultPanelListCellRenderer(textWidth);
	}

}

class TiconeCytoscapeClusteringResultComparator implements Comparator<TiconeClusteringResultPanel> {
	@Override
	public int compare(final TiconeClusteringResultPanel o1, final TiconeClusteringResultPanel o2) {
		return Objects.compare(o1.toString(), o2.toString(), String.CASE_INSENSITIVE_ORDER);
	};
}

class TiconeClusteringResultPanelListCellRenderer extends ResizableListCellRenderer<TiconeClusteringResultPanel> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2790322874330406520L;

	public TiconeClusteringResultPanelListCellRenderer(final int textWidth) {
		super(textWidth);
	}

	@Override
	protected String getStringForObject(final Object value) {
		return ((TiconeClusteringResultPanel) value).toString();
	}
}