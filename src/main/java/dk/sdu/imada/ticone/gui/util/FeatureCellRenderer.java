/**
 * 
 */
package dk.sdu.imada.ticone.gui.util;

import java.awt.Component;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTable;

import dk.sdu.imada.ticone.feature.FeaturePvalueValue;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.fitness.IFitnessValue;
import dk.sdu.imada.ticone.gui.jtable.AbstractTiconeTableCellRenderer;
import dk.sdu.imada.ticone.similarity.ICompositeSimilarityValue;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.statistics.EmpiricalPvalue;
import dk.sdu.imada.ticone.statistics.IPvalue;

public class FeatureCellRenderer<OBJ extends IObjectWithFeatures> extends AbstractTiconeTableCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6210332392533581343L;

	private final IFeature<? extends Comparable<?>> feature;
	private IFeatureStore store;

	public FeatureCellRenderer(final IFeature<? extends Comparable<?>> feature, final IFeatureStore store) {
		super();
		this.feature = feature;
		this.store = store;
	}

	/**
	 * @return the feature
	 */
	public IFeature<? extends Comparable<?>> getFeature() {
		return this.feature;
	}

	public void setFeatureStore(final IFeatureStore store) {
		this.store = store;
	}

	@Override
	public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
			final boolean hasFocus, final int row, final int column) {

		final OBJ object = getObjectFromValue(table, row, column, value);
		if (object == null)
			return new JLabel("ERROR");

		final JLabel c = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		c.setBackground(this.backgroundColor);
		if (!this.store.getFeaturesFor(object).contains(this.feature))
			c.setText("--");
		else {
			final IFeatureValue<? extends Comparable<?>> featureValue = this.store.getFeatureValue(object,
					this.feature);
			final Comparable<?> rawValue = featureValue.getValue();

			if (rawValue instanceof ISimilarityValue) {
				try {
					c.setText(String.format("%.3G", ((ISimilarityValue) rawValue).get()));

					final ISimilarityValue similarityValue = (ISimilarityValue) rawValue;
					if (similarityValue instanceof ICompositeSimilarityValue) {
						try {
							c.setToolTipText(((ICompositeSimilarityValue) similarityValue).toHtmlString());
						} catch (Exception e) {
						}
					}
				} catch (SimilarityCalculationException e) {
					c.setText("--");
				}
			} else if (rawValue instanceof IPvalue) {
				FeaturePvalueValue cast = (FeaturePvalueValue) featureValue;

				double p = ((IPvalue) rawValue).getDouble();

				if (rawValue instanceof EmpiricalPvalue && p == 0.0) {
					c.setText(String.format("< 1/%d", cast.getTotalObservations()));
				} else if (Double.isNaN(p)) {
					c.setText("???");
				} else
					c.setText(String.format("%.3G", p));

				final StringBuilder sb = new StringBuilder("<html>");
				sb.append(String.format("p=%.3f=%d/%d", p, cast.getBetterObservations(), cast.getTotalObservations()));
				sb.append("<br>");
				sb.append("<br>");
				sb.append("Fitness value(s):");
				sb.append("<br>");
				sb.append("<ul>");
				final Map<IFitnessScore, IFitnessValue> fitnessValues = cast.getFitnessValues();
				for (IFitnessScore fs : fitnessValues.keySet()) {
					sb.append("<li>");
					sb.append(fs.getName());
					sb.append(":");
					sb.append(fitnessValues.get(fs));
					sb.append("</li>");
				}
				sb.append("</ul>");
				sb.append("</html>");
				c.setToolTipText(sb.toString());

			} else if (rawValue instanceof Double) {
				c.setText(String.format("%.3G", rawValue));
			} else if (rawValue instanceof Integer)
				c.setText(String.format("%d", rawValue));
			else
				c.setText(featureValue.getValue() + "");
		}
		this.setColors(table, object, isSelected, hasFocus, row, column, c);

		return c;
	}

	@Override
	protected OBJ getObjectFromValue(final JTable table, final int row, final int column, final Object value) {
		return (OBJ) super.getObjectFromValue(table, row, column, value);
	}
}