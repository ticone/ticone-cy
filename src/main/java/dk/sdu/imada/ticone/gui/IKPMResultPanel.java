package dk.sdu.imada.ticone.gui;

import java.util.List;
import java.util.Map;

import javax.swing.JTabbedPane;

import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.gui.panels.kpm.KPMResultsTabPanel;

public interface IKPMResultPanel {
	public KPMResultsTabPanel getKpmResultsTabPanel();

	public Map<IClusters, Map<String, Map<String, Object>>> getSelectedKpmObjects();

	public TiconeCytoscapeClusteringResult getClusteringResult();

	public List<TiconeCytoscapeClusteringResult> getClusteringResults();

	public JTabbedPane getjTabbedPane();
}
