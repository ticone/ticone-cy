package dk.sdu.imada.ticone.gui.util;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 * Created by christian on 14-01-16.
 *
 *
 * Source:
 * http://www.java2s.com/Code/Java/Swing-JFC/SettingColumnHeaderToolTipsinaJTableComponents.htm
 */
public class ColumnHeaderToolTips extends MouseMotionAdapter {

	private TableColumn curCol;
	private final Map tips = new HashMap();

	public void setToolTip(final TableColumn col, final String tooltip) {
		if (tooltip == null) {
			this.tips.remove(col);
		} else {
			this.tips.put(col, tooltip);
		}
	}

	@Override
	public void mouseMoved(final MouseEvent evt) {
		final JTableHeader header = (JTableHeader) evt.getSource();
		final JTable table = header.getTable();
		final TableColumnModel colModel = table.getColumnModel();
		final int vColIndex = colModel.getColumnIndexAtX(evt.getX());
		TableColumn col = null;
		if (vColIndex >= 0) {
			col = colModel.getColumn(vColIndex);
		}
		if (col != this.curCol) {
			header.setToolTipText((String) this.tips.get(col));
			this.curCol = col;
		}
	}
}
