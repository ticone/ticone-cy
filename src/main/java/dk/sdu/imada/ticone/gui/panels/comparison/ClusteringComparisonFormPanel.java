package dk.sdu.imada.ticone.gui.panels.comparison;

import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import org.cytoscape.work.TaskManager;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;

import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.IShuffleClustering;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringByShufflingDataset;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringByShufflingPrototypeTimeSeries;
import dk.sdu.imada.ticone.clustering.ShuffleClusteringWithRandomPrototypeTimeSeries;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.comparison.ClusteringComparisonShuffleClusteringPair;
import dk.sdu.imada.ticone.comparison.ComparisonTaskFactory;
import dk.sdu.imada.ticone.data.CreateRandomTimeSeries;
import dk.sdu.imada.ticone.data.ShuffleDatasetGlobally;
import dk.sdu.imada.ticone.data.ShuffleDatasetRowwise;
import dk.sdu.imada.ticone.data.ShuffleTimeSeries;
import dk.sdu.imada.ticone.data.permute.IShuffleDataset;
import dk.sdu.imada.ticone.gui.TiconeClusteringResultsPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.gui.util.ShuffleComboBox;
import dk.sdu.imada.ticone.gui.util.VisualClusteringComboBox;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * Created by wiwiec on 5/29/16.
 */
public class ClusteringComparisonFormPanel {
	private JButton differentialityBtn;
	private JPanel mainPanel;
	private JComboBox<TiconeClusteringResultPanel> clustering1ComboBox, clustering2ComboBox;
	// private JComboBox<PatternHistory> clustering1IterationComboBox,
	// clustering2IterationComboBox;
	private JComboBox<ISimilarityFunction> simFunctionComboBox;
	private DefaultComboBoxModel<ISimilarityFunction> simFunctionComboBoxModel;
	private JTextField permutationsTextField;
	private JComboBox<IShuffle> permutationFunctionComboBox;

	public JPanel getMainPanel() {
		return this.mainPanel;
	}

	public void updateClusteringJList(final TiconeClusteringResultsPanel resultPanel) {
//		Object oldSelected1 = clustering1ComboBox.getSelectedItem();
//		Object oldSelected2 = clustering2ComboBox.getSelectedItem();
//
//		clustering1ComboBox.removeAllItems();
//		clustering2ComboBox.removeAllItems();
//		for (int i = 0; i < resultPanel.getTabCount(); i++) {
//			if (resultPanel.getComponentAt(i) instanceof TiconeClusteringResultPanel) {
//				clustering1ComboBox.addItem(((TiconeClusteringResultPanel) resultPanel.getComponentAt(i)));
//				clustering2ComboBox.addItem(((TiconeClusteringResultPanel) resultPanel.getComponentAt(i)));
//			}
//		}
//
//		// setting it to no selection first and then applying optional old
//		// selection ensures that we select nothing if the previously selected
//		// clustering is not in there anymore
//		clustering1ComboBox.setSelectedIndex(-1);
//		clustering2ComboBox.setSelectedIndex(-1);
//		if (oldSelected1 != null)
//			clustering1ComboBox.setSelectedItem(oldSelected1);
//		if (oldSelected2 != null)
//			clustering2ComboBox.setSelectedItem(oldSelected2);
	}

	private void addSimilarityFunctionToComboBox(final ISimilarityFunction simFunc) {
		this.simFunctionComboBoxModel.addElement(simFunc);
	}

	private ISimilarityFunction getSelectedSimilarityFunction() {
		return (ISimilarityFunction) this.simFunctionComboBoxModel.getSelectedItem();
	}

	private void createUIComponents() {
		this.clustering1ComboBox = new VisualClusteringComboBox();
		this.clustering2ComboBox = new VisualClusteringComboBox();

		this.simFunctionComboBoxModel = new DefaultComboBoxModel<>();
		this.addSimilarityFunctionToComboBox(new PearsonCorrelationFunction());
		this.addSimilarityFunctionToComboBox(new InverseEuclideanSimilarityFunction());
		this.simFunctionComboBox = new JComboBox<>(this.simFunctionComboBoxModel);

		this.permutationFunctionComboBox = new ShuffleComboBox(
				new IShuffle[] { new ShuffleTimeSeries(), new CreateRandomTimeSeries(),
				// this one doesnt make sense here, because it ruins the principle for the
				// p-value that similar clusters are likely to possess
				// more common objects; in short this can be summarized as the "object-laziness
				// principle", i.e. objects are lazy and do not
				// change between experiments
				// new ShuffleClusteringByShufflingClusters(),
				// these two dont make sense here because they completely scramble the object
				// similarities, and thus
				// of course we dont find clusters with large overlap in objects
				// new ShuffleDatasetRowwise(), new ShuffleDatasetGlobally()
				});

		this.differentialityBtn = new JButton("Compare Clusterings");
		this.differentialityBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					final int permutations = Integer
							.parseInt(ClusteringComparisonFormPanel.this.permutationsTextField.getText());

					final TiconeClusteringResultPanel clusteringPanel1 = (TiconeClusteringResultPanel) ClusteringComparisonFormPanel.this.clustering1ComboBox
							.getSelectedItem();
					final TiconeClusteringResultPanel clusteringPanel2 = (TiconeClusteringResultPanel) ClusteringComparisonFormPanel.this.clustering2ComboBox
							.getSelectedItem();

					final TiconeCytoscapeClusteringResult clusteringResult1 = clusteringPanel1.getClusteringResult();
					final TiconeCytoscapeClusteringResult clusteringResult2 = clusteringPanel2.getClusteringResult();

					final IPrototypeBuilder prototypeFactory = clusteringResult1.getPrototypeBuilder();
					final IPrototypeBuilder prototypeFactory2 = clusteringResult2.getPrototypeBuilder();

					// check whether prototypes are compatible
					if (!(prototypeFactory.producesComparablePrototypesWith(prototypeFactory2))) {
						JOptionPane.showMessageDialog(null,
								"The two selected clusterings cannot be compared, as they have incomparable cluster "
										+ "prototypes with different components.");
						return;
					}

					final IClusters selectedClusters = clusteringPanel1.getSelectedClusters();
					final IClusters selectedClusters2 = clusteringPanel2.getSelectedClusters();

					if (selectedClusters.size() < 1 || selectedClusters2.size() < 1) {
						JOptionPane.showMessageDialog(null, "Please select at least 1 cluster for both clusterings.");

						// focus one of the clusterings with too few selected
						// clusters
						TiconeClusteringResultsPanel ticoneClusteringsResultsPanel = GUIUtility
								.getTiconeClusteringsResultsPanel();
						((JTabbedPane) ticoneClusteringsResultsPanel.getParent())
								.setSelectedComponent(ticoneClusteringsResultsPanel);
						if (selectedClusters.size() < 1) {
							ticoneClusteringsResultsPanel.setSelectedComponent(clusteringPanel1);
							clusteringPanel1.setGraphTabInFocus();
						} else if (selectedClusters2.size() < 1) {
							ticoneClusteringsResultsPanel.setSelectedComponent(clusteringPanel2);
							clusteringPanel2.setGraphTabInFocus();
						}
						return;
					}

					final IShuffle permutationFunction = (IShuffle) ClusteringComparisonFormPanel.this.permutationFunctionComboBox
							.getSelectedItem();

					final IShuffleClustering shuffleClustering1, shuffleClustering2;
					if (permutationFunction instanceof IShuffleClustering) {
						shuffleClustering1 = (IShuffleClustering) permutationFunction.newInstance();
						shuffleClustering2 = (IShuffleClustering) permutationFunction.newInstance();
					} else if (permutationFunction instanceof CreateRandomTimeSeries) {
						final CreateRandomTimeSeries s1 = (CreateRandomTimeSeries) permutationFunction.newInstance();
						final CreateRandomTimeSeries s2 = (CreateRandomTimeSeries) permutationFunction.newInstance();

						s1.setAllObjects(
								clusteringResult1.getClusterHistory().getClusterObjectMapping().getAllObjects());
						s2.setAllObjects(
								clusteringResult2.getClusterHistory().getClusterObjectMapping().getAllObjects());

						shuffleClustering1 = new ShuffleClusteringWithRandomPrototypeTimeSeries(
								ClusteringComparisonFormPanel.this.getSelectedSimilarityFunction(), prototypeFactory,
								s1);
						shuffleClustering2 = new ShuffleClusteringWithRandomPrototypeTimeSeries(
								ClusteringComparisonFormPanel.this.getSelectedSimilarityFunction(), prototypeFactory2,
								s2);
					} else if (permutationFunction instanceof ShuffleTimeSeries) {
						shuffleClustering1 = new ShuffleClusteringByShufflingPrototypeTimeSeries(
								ClusteringComparisonFormPanel.this.getSelectedSimilarityFunction(), prototypeFactory,
								new ShuffleTimeSeries());
						shuffleClustering2 = new ShuffleClusteringByShufflingPrototypeTimeSeries(
								ClusteringComparisonFormPanel.this.getSelectedSimilarityFunction(), prototypeFactory2,
								new ShuffleTimeSeries());
					} else if (permutationFunction instanceof ShuffleDatasetRowwise
							|| permutationFunction instanceof ShuffleDatasetGlobally) {
						final IShuffleDataset s1 = (IShuffleDataset) permutationFunction.newInstance();
						final IShuffleDataset s2 = (IShuffleDataset) permutationFunction.newInstance();

						shuffleClustering1 = new ShuffleClusteringByShufflingDataset(
								clusteringResult1.getClusteringMethodBuilder().setSimilarityFunction(
										ClusteringComparisonFormPanel.this.getSelectedSimilarityFunction()),
								clusteringResult1.getClusterHistory().getClusterObjectMapping().getClusters().size(),
								s1);

						shuffleClustering2 = new ShuffleClusteringByShufflingDataset(
								clusteringResult2.getClusteringMethodBuilder().setSimilarityFunction(
										ClusteringComparisonFormPanel.this.getSelectedSimilarityFunction()),
								clusteringResult2.getClusterHistory().getClusterObjectMapping().getClusters().size(),
								s2);
					} else {
						shuffleClustering1 = null;
						shuffleClustering2 = null;
					}

					final IShuffle shuffleClusteringPair = new ClusteringComparisonShuffleClusteringPair(
							shuffleClustering1, shuffleClustering2);

					ComparisonTaskFactory taskFactory;
					taskFactory = new ComparisonTaskFactory(clusteringResult1, clusteringResult2,
							clusteringResult1.getClusterHistory().getIterationNumber(),
							clusteringResult2.getClusterHistory().getIterationNumber(),
							clusteringPanel1.getSelectedClusters(), clusteringPanel2.getSelectedClusters(),
							ClusteringComparisonFormPanel.this.getSelectedSimilarityFunction(), shuffleClusteringPair,
							permutations);
					final TaskManager taskManager = ServiceHelper.getService(TaskManager.class);
					taskManager.execute(taskFactory.createTaskIterator());
				} catch (NumberFormatException | HeadlessException | IncompatibleSimilarityFunctionException
						| TiconeUnloadingException e1) {
					e1.printStackTrace();
				} catch (InterruptedException e2) {

				}
			}
		});
	}

	{
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
		$$$setupUI$$$();
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
	 * edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayoutManager(4, 1, new Insets(0, 0, 0, 0), -1, -1));
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(panel1,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		panel1.setBorder(BorderFactory.createTitledBorder("Choose two clusterings"));
		panel1.add(clustering1ComboBox,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label1 = new JLabel();
		label1.setText("Clustering 1:");
		panel1.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label2 = new JLabel();
		label2.setText("Clustering 2:");
		panel1.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel1.add(clustering2ComboBox,
				new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		mainPanel.add(differentialityBtn,
				new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(6, 1, new Insets(0, 0, 0, 0), -1, -1));
		mainPanel.add(panel2,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		panel2.setBorder(BorderFactory.createTitledBorder("Settings"));
		final JLabel label3 = new JLabel();
		label3.setText("Similarity Function:");
		panel2.add(label3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label4 = new JLabel();
		label4.setText("Permutations for P-Value Calculation:");
		panel2.add(label4, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel2.add(simFunctionComboBox,
				new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		permutationsTextField = new JTextField();
		permutationsTextField.setText("1000");
		panel2.add(permutationsTextField,
				new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(150, -1), null, 0, false));
		panel2.add(permutationFunctionComboBox,
				new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JLabel label5 = new JLabel();
		label5.setText("Permutation Function:");
		panel2.add(label5, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final Spacer spacer1 = new Spacer();
		mainPanel.add(spacer1, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER,
				GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return mainPanel;
	}
}