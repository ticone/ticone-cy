package dk.sdu.imada.ticone.gui.panels.leastfitting;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskManager;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.data.TimeSeriesObjectList;
import dk.sdu.imada.ticone.gui.panels.MyDialogPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueStorageException;
import dk.sdu.imada.ticone.similarity.InverseEuclideanSimilarityFunction;
import dk.sdu.imada.ticone.similarity.PearsonCorrelationFunction;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.tasks.delete.DeleteObjectsFromClusteringTaskFactory;
import dk.sdu.imada.ticone.tasks.suggest.SuggestNewPatternsTaskFactory;
import dk.sdu.imada.ticone.util.ClusteringActionsUtility;
import dk.sdu.imada.ticone.util.ExtractData;
import dk.sdu.imada.ticone.util.ServiceHelper;

public class LeastFittingObjectsDialog extends JDialog {
	/**
	 *
	 */
	private static final long serialVersionUID = -9181862228803353595L;

	private JPanel contentPane;
	private JLabel numberOfObjectsLabel;
	private JRadioButton shapePearsonRadioButton;
	private JRadioButton magnitudeEuclideanRadioButton;
	private JTextField percentTextField;
	private JButton updateButton;
	private JTextField patternsWantedTextField;
	private JButton suggestPatternsButton;
	private JButton deleteObjectsButton;
	private JButton cancelButton;
	private JPanel graphPanel;

	private int percent;
	private ITimeSeriesObjects timeSeriesDataList;
	private final IClusterObjectMapping patternObjectMapping;

	protected TiconeClusteringResultPanel resultPanel;

	public LeastFittingObjectsDialog(final TiconeClusteringResultPanel resultPanel)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			IncompatibleObjectTypeException, IncompatibleSimilarityValueException,
			IncompatibleSimilarityValueStorageException {
		super();

		this.resultPanel = resultPanel;
		this.$$$setupUI$$$();
		this.patternObjectMapping = resultPanel.getClusteringResult().getClusteringProcess().getLatestClustering();
		// timeSeriesDataList =
		// ExtractData.getLeastFittingObjectsFromWholeDataset(patternObjectMapping,
		// percent,
		// new
		// PearsonCorrelation(resultPanel.getClusteringResult().getTimePointWeighting()));
		this.setContentPane(this.contentPane);
		this.setModal(true);
		this.getRootPane().setDefaultButton(this.deleteObjectsButton);

		this.deleteObjectsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				LeastFittingObjectsDialog.this.onOK();
			}
		});

		this.cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				LeastFittingObjectsDialog.this.onCancel();
			}
		});

		// call onCancel() when cross is clicked
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				LeastFittingObjectsDialog.this.onCancel();
			}
		});

		// call onCancel() on ESCAPE
		this.contentPane.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				LeastFittingObjectsDialog.this.onCancel();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		this.updateAction();
	}

	private void onOK() {
		final DeleteObjectsFromClusteringTaskFactory taskFactory = new DeleteObjectsFromClusteringTaskFactory(
				this.timeSeriesDataList, this.resultPanel);
		ClusteringActionsUtility.startTask(taskFactory, null);
		this.onCancel();
	}

	private void onCancel() {
		contentPane.unregisterKeyboardAction(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
		this.dispose();
	}

	private void createUIComponents() {
		// TODO: place custom component creation code here
		this.numberOfObjectsLabel = new JLabel();
		this.graphPanel = new JPanel();
		this.shapePearsonRadioButton = new JRadioButton();
		this.magnitudeEuclideanRadioButton = new JRadioButton();
		final ButtonGroup similarityButtonGroup = new ButtonGroup();
		similarityButtonGroup.add(this.shapePearsonRadioButton);
		similarityButtonGroup.add(this.magnitudeEuclideanRadioButton);
		this.updateButton = new JButton();
		this.updateButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					LeastFittingObjectsDialog.this.updateAction();
				} catch (SimilarityCalculationException | SimilarityValuesException | IncompatibleObjectTypeException
						| IncompatibleSimilarityValueException | IncompatibleSimilarityValueStorageException e1) {
					MyDialogPanel.showMessageDialog(e1.getMessage());
				} catch (final InterruptedException e1) {
				}
			}
		});
		this.patternsWantedTextField = new JTextField();
		this.suggestPatternsButton = new JButton();
		this.suggestPatternsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				LeastFittingObjectsDialog.this.suggestPatternsAction();
			}
		});
	}

	private void updateAction() throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			IncompatibleObjectTypeException, IncompatibleSimilarityValueException,
			IncompatibleSimilarityValueStorageException {
		try {
			if (this.percentTextField.getText().length() <= 0) {
				throw new NumberFormatException();
			}
			this.percent = Integer.parseInt(this.percentTextField.getText());
			if (this.percent < 1 || this.percent > 100) {
				throw new NumberFormatException();
			}
		} catch (final Exception e) {
			JOptionPane.showMessageDialog(this, "Percent textfield must be a integer between 1 and 100");
		}

		ISimilarityFunction similarityFunction = new PearsonCorrelationFunction(
				this.resultPanel.getClusteringResult().getTimePointWeighting());
		if (this.magnitudeEuclideanRadioButton.isSelected()) {
			final ITimeSeriesObjectList allObjects = new TimeSeriesObjectList();
			for (final ICluster pattern : this.patternObjectMapping.getClusters()) {
				allObjects.addAll(pattern.getObjects());
			}
			similarityFunction = new InverseEuclideanSimilarityFunction(
					this.resultPanel.getClusteringResult().getTimePointWeighting());
		}
		this.timeSeriesDataList = ExtractData.getLeastFittingObjectsFromWholeDataset(this.patternObjectMapping,
				this.percent, similarityFunction);
		this.numberOfObjectsLabel.setText("Number of objects: " + this.timeSeriesDataList.size());
		this.updateGraphPanel();
		this.pack();
	}

	private void suggestPatternsAction() {
		int percentLeastFitting;
		int numberOfPatterns;
		try {
			percentLeastFitting = Integer.parseInt(this.percentTextField.getText());
			if (percentLeastFitting < 1 || percentLeastFitting > 100) {
				throw new NumberFormatException();
			}
		} catch (final Exception e) {
			JOptionPane.showMessageDialog(this, "Percent textfield must be a integer between 1 and 100");
			return;
		}
		try {
			numberOfPatterns = Integer.parseInt(this.patternsWantedTextField.getText());
			if (numberOfPatterns <= 0) {
				throw new NumberFormatException();
			}
		} catch (final Exception e) {
			JOptionPane.showMessageDialog(this, "Patterns wanted textfield must be a integer");
			return;
		}
		final SuggestNewPatternsTaskFactory suggestNewPatternsTaskFactory = new SuggestNewPatternsTaskFactory(
				numberOfPatterns, percentLeastFitting, this.resultPanel);
		final TaskIterator taskIterator = suggestNewPatternsTaskFactory.createTaskIterator();
		ServiceHelper.getService(TaskManager.class).execute(taskIterator);
		this.onCancel();
	}

	private void updateGraphPanel() throws InterruptedException {
		this.graphPanel.removeAll();
		this.graphPanel.updateUI();

		if (this.timeSeriesDataList.size() > 0) {
			try {
				final ClusterChartContainer patternGraphPanelContainer = new ClusterChartContainer(
						this.resultPanel.getClusteringResult(), null, this.timeSeriesDataList);
				this.graphPanel.add(patternGraphPanelContainer.getChartPanel());
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
			}
		}
		this.graphPanel.revalidate();
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT
	 * edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		contentPane = new JPanel();
		contentPane.setLayout(new GridLayoutManager(1, 1, new Insets(10, 10, 10, 10), -1, -1));
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(7, 2, new Insets(0, 0, 0, 0), -1, -1));
		contentPane.add(panel1,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		panel1.add(graphPanel,
				new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		panel1.add(panel2,
				new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		panel2.setBorder(BorderFactory.createTitledBorder("Similarity method"));
		shapePearsonRadioButton.setSelected(true);
		shapePearsonRadioButton.setText("Shape (Pearson)");
		panel2.add(shapePearsonRadioButton,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		magnitudeEuclideanRadioButton.setText("Magnitude (Euclidean)");
		panel2.add(magnitudeEuclideanRadioButton,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		percentTextField = new JTextField();
		percentTextField.setText("10");
		panel1.add(percentTextField,
				new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(150, -1), null, 0, false));
		updateButton.setText("Update");
		panel1.add(updateButton,
				new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label1 = new JLabel();
		label1.setText("Percent least fitting");
		panel1.add(label1, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		numberOfObjectsLabel.setText("Number of objects");
		panel1.add(numberOfObjectsLabel,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
						GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0,
						false));
		final JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayoutManager(1, 3, new Insets(0, 0, 0, 0), -1, -1));
		panel1.add(panel3,
				new GridConstraints(5, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		panel3.setBorder(BorderFactory.createTitledBorder("Suggest new clusters"));
		final JLabel label2 = new JLabel();
		label2.setText("Patterns wanted");
		panel3.add(label2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
				GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		patternsWantedTextField = new JTextField();
		patternsWantedTextField.setText("3");
		panel3.add(patternsWantedTextField,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null,
						new Dimension(150, -1), null, 0, false));
		suggestPatternsButton.setText("Suggest clusters");
		panel3.add(suggestPatternsButton,
				new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel4 = new JPanel();
		panel4.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		panel1.add(panel4,
				new GridConstraints(6, 0, 1, 2, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_VERTICAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null,
						0, false));
		deleteObjectsButton = new JButton();
		deleteObjectsButton.setText("Delete objects");
		panel4.add(deleteObjectsButton,
				new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		cancelButton = new JButton();
		cancelButton.setText("Cancel");
		panel4.add(cancelButton,
				new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL,
						GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
						GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return contentPane;
	}
}
