package dk.sdu.imada.ticone.gui.panels.clusterchart;

import java.awt.BorderLayout;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.cytoscape.application.swing.CySwingApplication;
import org.cytoscape.application.swing.CytoPanel;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.application.swing.CytoPanelState;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkManager;

import dk.sdu.imada.ticone.clustering.ClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterList;
import dk.sdu.imada.ticone.clustering.IClusteringProcess;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.gui.IKPMResultPanel;
import dk.sdu.imada.ticone.gui.TiconeClusteringResultsPanel;
import dk.sdu.imada.ticone.gui.TiconeResultPanel;
import dk.sdu.imada.ticone.gui.panels.clustertable.ClustersTabPanel;
import dk.sdu.imada.ticone.gui.panels.clustertable.ClustersTableTabPanel;
import dk.sdu.imada.ticone.gui.panels.kpm.KPMResultFormPanel;
import dk.sdu.imada.ticone.gui.panels.kpm.KPMResultsTabPanel;
import dk.sdu.imada.ticone.gui.panels.preprocessing.PreprocessingSummaryFormPanel;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.KPMResultWrapper;
import dk.sdu.imada.ticone.util.ProgramState;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeResultDestroyedEvent;
import dk.sdu.imada.ticone.util.TiconeResultNameChangedEvent;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * Created by Christian Wiwie on 9/9/15.
 */
public class TiconeClusteringResultPanel extends TiconeResultPanel<TiconeCytoscapeClusteringResult>
		implements IKPMResultPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2152259027538602847L;
	private JTabbedPane jTabbedPane;

	protected KPMResultsTabPanel kpmResultsTabPanel;

	protected ClustersTabPanel clustersTabPanel;
	protected PreprocessingSummaryFormPanel preprocessingFormPanel;

	protected ClustersTableTabPanel clustersTableTabPanel;

	protected IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> clusteringProcess;

	public enum TAB_INDEX {
		PREPROCESSING, CLUSTERS, CLUSTER_TABLE, NETWORK_ENRICHMENT
	}

	public TiconeClusteringResultPanel(
			final IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> clusteringProcess)
			throws TiconeUnloadingException, InterruptedException, IncompatibleSimilarityFunctionException {
		super(clusteringProcess.getClusteringResult());
		this.clusteringProcess = clusteringProcess;
		this.register();
	}

	public void register()
			throws TiconeUnloadingException, InterruptedException, IncompatibleSimilarityFunctionException {
		// TODO: shouldn't this be done outside of this constructor?
		this.result.addChangeListener(GUIUtility.getTiconePanel().getClusterConnectivityFormPanel());
		this.result.addChangeListener(GUIUtility.getTiconePanel().getKpmFormPanel());
		this.result.addClusteringChangeListener(GUIUtility.getTiconePanel().getClustersTabPanel());
		this.setLayout(new BorderLayout());
		this.initComponents();
		GUIUtility.addTiconeResultsPanel(this);

		final TiconeClusteringResultsPanel tabbedPane = GUIUtility.getTiconeClusteringsResultsPanel();
		TiconeClusteringResultPanel old = (TiconeClusteringResultPanel) tabbedPane.getSelectedComponent();
		tabbedPane.addTab(this.toString(), this);
		final int numberTabs = tabbedPane.getTabCount();
		tabbedPane.setSelectedIndex(numberTabs - 1);
//		TiconeTabManager.getSingleton().forwardClusteringTabChangedEvent(new TiconeTabSelectionChangedEvent(tabbedPane, old, this));

		// show the results panel
		final CytoPanel cytoPanel = ServiceHelper.getService(CySwingApplication.class).getCytoPanel(CytoPanelName.EAST);
		cytoPanel.setState(CytoPanelState.DOCK);

		GUIUtility.getVisualClusteringManager().addClusteringResult(this);
	}

	@Override
	public KPMResultsTabPanel getKpmResultsTabPanel() {
		return this.kpmResultsTabPanel;
	}

	private void initComponents() throws IncompatibleSimilarityFunctionException {
		// Center panel
		final JPanel centerPanel = this.setupCenterPanel();
		this.add(centerPanel, BorderLayout.CENTER);
	}

	private JPanel setupCenterPanel() throws IncompatibleSimilarityFunctionException {
		final JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());

		this.jTabbedPane = this.setUpTabbedPane();
		centerPanel.add(this.jTabbedPane);

		return centerPanel;
	}

	protected JTabbedPane setUpTabbedPane() throws IncompatibleSimilarityFunctionException {
		final JTabbedPane jTabbedPane = new JTabbedPane();

		this.preprocessingFormPanel = new PreprocessingSummaryFormPanel(this);
		jTabbedPane.addTab("Input & Settings", this.preprocessingFormPanel.getMainPanel());

		this.clustersTabPanel = new ClustersTabPanel(this);
		jTabbedPane.addTab("Clusters", this.clustersTabPanel);

		this.clustersTableTabPanel = new ClustersTableTabPanel(this);
		jTabbedPane.addTab("Cluster Table", this.clustersTableTabPanel.getMainPanel());

		this.kpmResultsTabPanel = new KPMResultsTabPanel(this);
		jTabbedPane.addTab("Network Enrichment", this.kpmResultsTabPanel);
		jTabbedPane.setEnabledAt(TAB_INDEX.NETWORK_ENRICHMENT.ordinal(), false);

		jTabbedPane.setSelectedIndex(TAB_INDEX.CLUSTERS.ordinal());

		return jTabbedPane;
	}

	public void setGraphTabInFocus() {
		this.jTabbedPane.setSelectedIndex(1);
	}

	@Override
	public TiconeCytoscapeClusteringResult getClusteringResult() {
		return this.result;
	}

	/**
	 * @return the clusteringProcess
	 */
	public IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> getClusteringProcess() {
		return this.clusteringProcess;
	}

	@Override
	public List<TiconeCytoscapeClusteringResult> getClusteringResults() {
		return Arrays.asList(this.result);
	}

	public ClustersTabPanel getClustersTabPanel() {
		return this.clustersTabPanel;
	}

	@Override
	public JTabbedPane getjTabbedPane() {
		return this.jTabbedPane;
	}

	@Override
	public void saveState(final int tabIndex, final ProgramState state) {
		final IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult> utils = this
				.getClusteringProcess();
		state.addOverrepresentedPatternUtil(tabIndex, utils);
		final KPMResultsTabPanel kpmTabPanel = this.getKpmResultsTabPanel();
		final List<KPMResultFormPanel> kpmResultPanels = kpmTabPanel.getKpmResultPanels();
		final List<KPMResultWrapper> kpmResults = new ArrayList<>();
		for (final KPMResultFormPanel kpmResultPanel : kpmResultPanels) {
			kpmResults
					.add(new KPMResultWrapper(
							kpmResultPanel.getNetwork().getRow(kpmResultPanel.getNetwork()).get(CyNetwork.NAME,
									String.class),
							kpmResultPanel.getResultList(), kpmResultPanel.getSelectedPatternForThisResult(),
							kpmResultPanel.getKpmModel()));
		}
		state.addClusteringKPMResults(tabIndex, kpmResults);
	}

	@Override
	public Serializable asSerializable() {
		final ArrayList<Serializable> result = new ArrayList<>();
		result.add(this.getClusteringProcess());
		final List<KPMResultFormPanel> kpmResultPanels = this.getKpmResultsTabPanel().getKpmResultPanels();
		final ArrayList<KPMResultWrapper> kpmResults = new ArrayList<>();
		for (final KPMResultFormPanel kpmResultPanel : kpmResultPanels) {
			kpmResults
					.add(new KPMResultWrapper(
							kpmResultPanel.getNetwork().getRow(kpmResultPanel.getNetwork()).get(CyNetwork.NAME,
									String.class),
							kpmResultPanel.getResultList(), kpmResultPanel.getSelectedPatternForThisResult(),
							kpmResultPanel.getKpmModel()));
		}
		result.add(kpmResults);
		return result;
	}

	public static TiconeClusteringResultPanel fromSerializable(final Serializable ser)
			throws TiconeUnloadingException, InterruptedException, IncompatibleSimilarityFunctionException {
		final ArrayList<Serializable> list = (ArrayList<Serializable>) ser;

		final TiconeClusteringResultPanel result = new TiconeClusteringResultPanel(
				(IClusteringProcess<ClusterObjectMapping, TiconeCytoscapeClusteringResult>) list.get(0));

		final ArrayList<KPMResultWrapper> kpmResults = (ArrayList<KPMResultWrapper>) list.get(1);

		result.getResult().clearListener();
		GUIUtility.updateGraphPanel(result);

		for (final KPMResultWrapper kpmResult : kpmResults) {
			// take a network with the same name
			final Set<CyNetwork> networks = ServiceHelper.getService(CyNetworkManager.class).getNetworkSet();
			for (final CyNetwork network : networks) {
				final String otherName = network.getRow(network).get(CyNetwork.NAME, String.class);
				if (otherName.equals(kpmResult.getNetworkName())) {
					new KPMResultFormPanel(result, network, kpmResult.getClusters(), kpmResult.getResults(),
							kpmResult.getKpmModel());
					break;
				}
			}
		}
		return result;
	}

	@Override
	public Map<IClusters, Map<String, Map<String, Object>>> getSelectedKpmObjects() {
		final Map<IClusters, Map<String, Map<String, Object>>> result = new HashMap<>();
		final Map<ICluster, Map<String, Map<String, Object>>> tmp = this.clustersTabPanel.getClusterChartTablePanel()
				.getSelectedKpmObjects();
		for (final ICluster cl : tmp.keySet()) {
			result.put(cl.asSingletonList(), tmp.get(cl));
		}
		return result;
	}

	@Override
	public void resultNameChanged(final TiconeResultNameChangedEvent event) {
		final String newTitle = event.getNewName();
		try {
			final JTabbedPane resultPanel = GUIUtility.getTiconeClusteringsResultsPanel().getTabbedPane();
			resultPanel.setTitleAt(resultPanel.indexOfComponent(this), newTitle);
			resultPanel.revalidate();
			resultPanel.repaint();
		} catch (final TiconeUnloadingException e) {
		}
	}

	@Override
	public void onRemove() {
		super.onRemove();
		GUIUtility.getVisualClusteringManager().removeClusteringResult(this);
	}

	public IClusters getSelectedClusters() {
		final ClusterChartTablePanel graphTablePanel = this.getClustersTabPanel().getClusterChartTablePanel();
		return graphTablePanel.getSelectedClusters();
	}

	public void setSelectedClusters(final IClusterList selectedClusters) {
		this.getClustersTabPanel().getClusterChartTablePanel().setSelectedClusters(selectedClusters);
	}

	@Override
	public void ticoneResultDestroyed(TiconeResultDestroyedEvent event) {
	}

}
