package dk.sdu.imada.ticone.gui.panels.leastfitting;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;

/**
 * Created by christian on 20-01-16.
 */
public class LeastFittingObjectsFromOnePatternContainer {

	private ICluster pattern;
	private ITimeSeriesObjects timeSeriesDataToDelete;
	private boolean deleteData = false;

	public ICluster getPattern() {
		return this.pattern;
	}

	public void setPattern(final ICluster pattern) {
		this.pattern = pattern;
	}

	public ITimeSeriesObjects getTimeSeriesDataToDelete() {
		return this.timeSeriesDataToDelete;
	}

	public void setTimeSeriesDataToDelete(final ITimeSeriesObjects timeSeriesDataToDelete) {
		this.timeSeriesDataToDelete = timeSeriesDataToDelete;
	}

	public boolean isDeleteData() {
		return this.deleteData;
	}

	public void setDeleteData(final boolean deleteData) {
		this.deleteData = deleteData;
	}
}
