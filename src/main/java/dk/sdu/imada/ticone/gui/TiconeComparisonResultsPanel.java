package dk.sdu.imada.ticone.gui;

import dk.sdu.imada.ticone.gui.panels.comparison.TiconeClusteringComparisonResultPanel;
import dk.sdu.imada.ticone.util.GUIUtility;

/**
 * Created by christian on 9/9/15.
 */
public class TiconeComparisonResultsPanel extends TiconeTabbedResultsPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4431955642097130930L;

	@Override
	public String getTitle() {
		return "Comparisons";
	}

	@Override
	public void removeTabAt(final int index) {
		final TiconeResultPanel resPanel = (TiconeResultPanel) this.getComponentAt(index);
		if (resPanel instanceof TiconeClusteringComparisonResultPanel) {
			((TiconeClusteringComparisonResultPanel) resPanel).getClusteringComparisonResult()
					.clearComparisonVisualStyle();
			((TiconeClusteringComparisonResultPanel) resPanel).getClusteringComparisonResult().clearComparisonNetwork();
		}
		super.removeTabAt(index);
		GUIUtility.removeTiconeResultsPanel(resPanel);
	}
}
