package dk.sdu.imada.ticone.gui.panels.comparison;

import java.awt.BorderLayout;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JTabbedPane;

import org.apache.commons.lang3.tuple.Triple;
import org.cytoscape.application.swing.CySwingApplication;
import org.cytoscape.application.swing.CytoPanel;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.application.swing.CytoPanelState;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkManager;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.clustering.TiconeCytoscapeClusteringResult;
import dk.sdu.imada.ticone.comparison.CytoscapeClusteringComparisonResult;
import dk.sdu.imada.ticone.gui.IKPMResultPanel;
import dk.sdu.imada.ticone.gui.TiconeComparisonResultsPanel;
import dk.sdu.imada.ticone.gui.TiconeResultPanel;
import dk.sdu.imada.ticone.gui.panels.kpm.KPMResultFormPanel;
import dk.sdu.imada.ticone.gui.panels.kpm.KPMResultsTabPanel;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.KPMResultWrapper;
import dk.sdu.imada.ticone.util.ProgramState;
import dk.sdu.imada.ticone.util.ServiceHelper;
import dk.sdu.imada.ticone.util.TiconeResultDestroyedEvent;
import dk.sdu.imada.ticone.util.TiconeResultNameChangedEvent;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * Created by christian on 9/9/15.
 */
public class TiconeClusteringComparisonResultPanel extends TiconeResultPanel<CytoscapeClusteringComparisonResult>
		implements IKPMResultPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8404086408073089941L;

	protected ClusteringComparisonResultFormPanel clusteringComparisonResultFormPanel;
	protected KPMResultsTabPanel kpmResultsTabPanel;
	private JTabbedPane jTabbedPane;

	public enum TAB_INDEX {
		COMPARISON, NETWORK_ENRICHMENT
	}

	public TiconeClusteringComparisonResultPanel(final CytoscapeClusteringComparisonResult comparisonResult)
			throws TiconeUnloadingException, InterruptedException {
		super(comparisonResult);
		this.register();
	}

	public void register() throws TiconeUnloadingException, InterruptedException {
		this.setLayout(new BorderLayout());
		this.clusteringComparisonResultFormPanel = new ClusteringComparisonResultFormPanel(this, this.result);
		this.initComponents();
		GUIUtility.addTiconeResultsPanel(this);

		final TiconeComparisonResultsPanel tabbedPane = GUIUtility.getTiconeComparisonResultsPanel();
		tabbedPane.addTab(this.toString(), this);
		final int numberTabs = tabbedPane.getTabCount();
		tabbedPane.setSelectedIndex(numberTabs - 1);

		// show the results panel
		final CytoPanel cytoPanel = ServiceHelper.getService(CySwingApplication.class).getCytoPanel(CytoPanelName.EAST);
		cytoPanel.setState(CytoPanelState.DOCK);

		// focus the comparison tab
		// first focus nothing in order to trigger the stateChanged event of the
		// tabbed pane (necessary for updating available tabs on the control
		// panel)
		((JTabbedPane) tabbedPane.getParent()).setSelectedIndex(-1);
		((JTabbedPane) tabbedPane.getParent()).setSelectedComponent(tabbedPane);
	}

	private void initComponents() {
		this.jTabbedPane = this.setUpTabbedPane();
		this.add(this.jTabbedPane, BorderLayout.CENTER);
	}

	@Override
	public void saveState(final int tabIndex, final ProgramState state) {
		state.addClusteringComparisonResult(tabIndex, this.result);
		final KPMResultsTabPanel kpmTabPanel = this.getKpmResultsTabPanel();
		final List<KPMResultFormPanel> kpmResultPanels = kpmTabPanel.getKpmResultPanels();
		final List<KPMResultWrapper> kpmResults = new ArrayList<>();
		for (final KPMResultFormPanel kpmResultPanel : kpmResultPanels) {
			kpmResults
					.add(new KPMResultWrapper(
							kpmResultPanel.getNetwork().getRow(kpmResultPanel.getNetwork()).get(CyNetwork.NAME,
									String.class),
							kpmResultPanel.getResultList(), kpmResultPanel.getSelectedPatternForThisResult(),
							kpmResultPanel.getKpmModel()));
		}
		state.addComparisonKPMResults(tabIndex, kpmResults);
	}

	@Override
	public Serializable asSerializable() {
		final ArrayList<Serializable> result = new ArrayList<>();

		result.add(this.result);
		final List<KPMResultFormPanel> kpmResultPanels = this.getKpmResultsTabPanel().getKpmResultPanels();
		final ArrayList<KPMResultWrapper> kpmResults = new ArrayList<>();
		for (final KPMResultFormPanel kpmResultPanel : kpmResultPanels) {
			kpmResults
					.add(new KPMResultWrapper(
							kpmResultPanel.getNetwork().getRow(kpmResultPanel.getNetwork()).get(CyNetwork.NAME,
									String.class),
							kpmResultPanel.getResultList(), kpmResultPanel.getSelectedPatternForThisResult(),
							kpmResultPanel.getKpmModel()));
		}
		result.add(kpmResults);
		return result;
	}

	protected JTabbedPane setUpTabbedPane() {
		final JTabbedPane jTabbedPane = new JTabbedPane();

		jTabbedPane.addTab("Comparison", this.clusteringComparisonResultFormPanel.getMainPanel());

		this.kpmResultsTabPanel = new KPMResultsTabPanel(this);
		jTabbedPane.addTab("Network Enrichment", this.kpmResultsTabPanel);
		jTabbedPane.setEnabledAt(TAB_INDEX.NETWORK_ENRICHMENT.ordinal(), false);

		return jTabbedPane;
	}

	@Override
	public Map<IClusters, Map<String, Map<String, Object>>> getSelectedKpmObjects() {
		return this.clusteringComparisonResultFormPanel.getSelectedKpmObjects();
	}

	@Override
	public TiconeCytoscapeClusteringResult getClusteringResult() {
		return this.result.getClustering1();
	}

	@Override
	public List<TiconeCytoscapeClusteringResult> getClusteringResults() {
		return Arrays.asList(this.result.getClustering1(), this.result.getClustering2());
	}

	@Override
	public KPMResultsTabPanel getKpmResultsTabPanel() {
		return this.kpmResultsTabPanel;
	}

	@Override
	public JTabbedPane getjTabbedPane() {
		return this.jTabbedPane;
	}

	public CytoscapeClusteringComparisonResult getClusteringComparisonResult() {
		return this.result;
	}

	public List<Triple<ICluster, ICluster, Set<String>>> getSelectedClusterPairsToVisualizeOnNetwork() {
		return this.clusteringComparisonResultFormPanel.getSelectedClusterPairsToVisualizeOnNetwork();
	}

	@Override
	public void resultNameChanged(final TiconeResultNameChangedEvent event) {
		String newTitle = event.getNewName();
		JTabbedPane resultsPanel;
		try {
			resultsPanel = GUIUtility.getTiconeComparisonResultsPanel().getTabbedPane();
		} catch (final TiconeUnloadingException e) {
			return;
		}
		resultsPanel.setTitleAt(resultsPanel.indexOfComponent(this), newTitle);
		resultsPanel.revalidate();
		resultsPanel.repaint();
	}

	public static TiconeClusteringComparisonResultPanel fromSerializable(final Serializable ser)
			throws TiconeUnloadingException, InterruptedException, SimilarityCalculationException {
		final ArrayList<Serializable> list = (ArrayList<Serializable>) ser;

		final TiconeClusteringComparisonResultPanel result = new TiconeClusteringComparisonResultPanel(
				(CytoscapeClusteringComparisonResult) list.get(0));

		final ArrayList<KPMResultWrapper> kpmResults = (ArrayList<KPMResultWrapper>) list.get(1);

		if (kpmResults != null) {
			for (final KPMResultWrapper kpmResult : kpmResults) {
				// take a network with the same name
				final Set<CyNetwork> networks = ServiceHelper.getService(CyNetworkManager.class).getNetworkSet();
				for (final CyNetwork network : networks) {
					final String otherName = network.getRow(network).get(CyNetwork.NAME, String.class);
					if (otherName.equals(kpmResult.getNetworkName())) {
						new KPMResultFormPanel(result, network, kpmResult.getClusters(), kpmResult.getResults(),
								kpmResult.getKpmModel());
						break;
					}
				}
			}
		}

		// init visual style and network
		result.result.initClusteringComparisonVisualStyle();

		return result;
	}

	@Override
	public void ticoneResultDestroyed(TiconeResultDestroyedEvent event) {
	}
}
