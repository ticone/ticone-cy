package dk.sdu.imada.ticone.gui.panels.clustertable;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import dk.sdu.imada.ticone.clustering.ClusterOperationException;
import dk.sdu.imada.ticone.gui.panels.MyDialogPanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartTablePanel;
import dk.sdu.imada.ticone.gui.panels.clusterchart.TiconeClusteringResultPanel;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.util.GUIUtility;
import dk.sdu.imada.ticone.util.TiconeUnloadingException;

/**
 * 
 * @author Christian Wiwie
 * @author Christian Nørskov
 * 
 * @since Oct 16, 2015
 *
 */
public class ClustersTabPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7667395514337361483L;

	protected ClusterChartTablePanel clusterChartTablePanel;
	protected TiconeClusteringResultPanel resultPanel;

	private JPanel actionPanel;
	private JButton applyActionsButton;
	private JButton discardActionsButton;

	public ClustersTabPanel(final TiconeClusteringResultPanel resultPanel)
			throws IncompatibleSimilarityFunctionException {
		super();
		this.resultPanel = resultPanel;
		this.setLayout(new BorderLayout());
		this.initComponents();
	}

	private void initComponents() throws IncompatibleSimilarityFunctionException {
		this.clusterChartTablePanel = new ClusterChartTablePanel(this.resultPanel);
		this.add(this.clusterChartTablePanel, BorderLayout.CENTER);
		this.setupActionChangesButtonPanel();
		this.add(this.actionPanel, BorderLayout.SOUTH);
		this.actionPanel.setVisible(false);
	}

	public ClusterChartTablePanel getClusterChartTablePanel() {
		return this.clusterChartTablePanel;
	}

	private void setupActionChangesButtonPanel() {
		this.actionPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.CENTER;

		this.setupApplyActionsButton();
		constraints.gridx = 0;
		constraints.gridy = 0;
		this.actionPanel.add(this.applyActionsButton, constraints);

		this.setupDiscardActionsButton();
		constraints.gridx = 1;
		constraints.gridy = 0;
		this.actionPanel.add(this.discardActionsButton, constraints);

	}

	private void setupApplyActionsButton() {
		this.applyActionsButton = new JButton("Apply changes");
		this.applyActionsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				TiconeClusteringResultPanel resultPanel;
				try {
					resultPanel = GUIUtility.getCurrentlySelectedClusteringResultPanel();
				} catch (final TiconeUnloadingException e2) {
					return;
				}
				try {
					resultPanel.getClusteringProcess().applyActionsBeforeNextIteration();
					GUIUtility.hideActionPanel(resultPanel);
					resultPanel.getClusteringResult().setupPatternStatusMapping();
					GUIUtility.updateGraphPanel(resultPanel);
					ClustersTabPanel.this.hideActionPanel();
				} catch (final ClusterOperationException e1) {
					MyDialogPanel.showMessageDialog(e1.getMessage());
				} catch (InterruptedException | TiconeUnloadingException e1) {
				}
			}
		});
	}

	private void setupDiscardActionsButton() {
		this.discardActionsButton = new JButton("Discard changes");
		this.discardActionsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					final TiconeClusteringResultPanel resultPanel = GUIUtility
							.getCurrentlySelectedClusteringResultPanel();
					resultPanel.getClusteringProcess().resetActionsToApplyBeforeNextIteration();
					GUIUtility.hideActionPanel(resultPanel);
					resultPanel.getClusteringResult().setupPatternStatusMapping();
					GUIUtility.updateGraphPanel(resultPanel);
					ClustersTabPanel.this.hideActionPanel();
				} catch (InterruptedException | TiconeUnloadingException e1) {
				}
			}
		});
	}

	public void showActionPanel() {
		this.actionPanel.setVisible(true);
	}

	public void hideActionPanel() {
		this.actionPanel.setVisible(false);
	}
}
