package dk.sdu.imada.ticone.constants;

/**
 * Created by christian on 8/7/15.
 */
public class ToolTipText {

	public static final String TOOLTIP_MIN_THRESHOLD_FOR_PAIRWISE_SIMILARITY = "<html>The threshold, for which pairwise similarities are saved.<br>"
			+ "Similarities below this threshold, will be handled as missing values.</html>";
}
